$(document).ready(function () {
    bootstrap();
});

function bootstrap() {

    var protocol = "http://";
    var host = "lmsp1.extorio.com/extorio/apis";
    var session;

    function init() {
        getSession();
    }

    function getSession() {
        var data = {
            username: 'alex',
            password: '101tefl'
        };

        $.ajax({
            url: protocol + host + '/users/login',
            crossDomain: true,
            data: data,
            type: 'POST'
        }).then(function (response) {
            response = JSON.parse(response);
            if (!response.error) {
                session = response.session;
                setSession(response.session);
                var userCourseId = getUserCourseId();
                getUserCourse(userCourseId);
            } else {
                throw response.error;
            }
        });
    }

    function getUserCourseId() {
        return 1;
    }

    function getUserCourse(userCourseId) {
        $.ajax({
            url: protocol + host + '/lms-user-courses/' + userCourseId + '/detail?user_session=' + session,
            crossDomain: true,
            type: 'GET'
        }).then(function (response) {
            response = JSON.parse(response);
            if (!response.error) {
                window.userCourse = response.data;
                window.course = response.course;
                bootstrapAngular();
            } else {
                throw response.errorMessage;
            }
        });
    }

    function setSession(session) {
        window.session = session;
    }

    function bootstrapAngular() {
        var element = $('html');
        angular.bootstrap(element, ['coursePlayerApp']);
    }

    init();

}

