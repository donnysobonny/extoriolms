'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:questionPreviewFileSubmission
 * @description
 * # questionPreviewFileSubmission
 */
angular.module('coursePlayerApp')
    .directive('questionPreviewFileSubmission', function () {
        return {
            template: '<h4>File Submission</h4>' +
            '<hr>' +
            '<strong>Question:</strong>' +
            '<p ng-bind-html="question.questions"></p>' +
            '<hr>' +
            '<strong>Your answer:</strong>' +
            '<p>Not implemented yet</p>',
            restrict: 'E',
            scope: {
                question: '='
            },
            link: function postLink(scope, element, attrs) {
            }
        };
    });
