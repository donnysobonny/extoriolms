'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:questionPreviewMultipleResponse
 * @description
 * # questionPreviewMultipleResponse
 */
angular.module('coursePlayerApp')
    .directive('questionPreviewMultipleResponse', function () {
        return {
            template: '<h4>Multiple Response <small>Select {{question.answers.length}} answers out of {{question.questions.options.length}} options</small></h4>' +
            '<hr>' +
            '<strong>Question:</strong>' +
            '<p ng-bind-html="question.questions.question"></p>' +
            '<hr>' +
            '<strong>Options:</strong>' +
            '<ol>' +
            '<li ng-repeat="option in question.questions.options">{{option}}</li>' +
            '</ol>' +
            '<hr>' +
            '<strong>Your answers:</strong>' +
            '<ul style ="list-style-type: none; padding-left: 20px;">' +
            '<li ng-repeat="answer in answers">{{answer + 1 }}. {{question.questions.options[answer]}}</li>' +
            '</ul>',
            restrict: 'E',
            scope: {
                question: '=',
                answers: '='
            },
            link: function postLink(scope, element, attrs) {
            }
        };
    });
