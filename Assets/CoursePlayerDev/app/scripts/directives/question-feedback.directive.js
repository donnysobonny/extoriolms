'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:quizFeedbackSubmitted
 * @description
 * # quizFeedbackSubmitted
 */
angular.module('coursePlayerApp')
    .directive('questionFeedback', function (shareService) {
        return {
            template: '<div style="margin-bottom: 0" class="panel" ng-class="{\'panel-danger\' : userQuestion.status ===\'failed\', \'panel-default\' : userQuestion.status ===\'started\', \'panel-success\' : userQuestion.status ===\'passed\' }">' +
            '<div class="panel-heading">' +
            '<h3 class="panel-title"><i class="fa" ng-class="{\'fa-times\' : userQuestion.status ===\'failed\', \'fa-clock-o\' : userQuestion.status ===\'started\', \'fa-check\' : userQuestion.status ===\'passed\' }"></i> Question {{questionIndex + 1}}<i ng-click="toggleUserQuestion()" ng-class="{ \'fa-plus-square-o\' : !showUserQuestion, \'fa-minus-square-o\' : showUserQuestion}" class="fa pull-right"></i></h3>' +
            '</div>' +
            '<div class="panel-body" ng-show="showUserQuestion">' +
            '<strong>Feedback</strong>' +
            '<p ng-show="userQuestion.status !== \'started\'" ng-bind-html="userQuestion.response"></p>' +
            '<p ng-show="userQuestion.status === \'started\'">Question is currently awaiting to be marked. Once marked your feedback will appear here.</p>' +
            '<hr>' +
            '<button type="button" class="btn btn-default" ng-click="toggleQuestionPreview()">{{!showQuestion ? \'Show Question\' : \'Hide Question\'}}</button> ' +
            '<hr>' +
            '<div ng-show="showQuestion">' +
            '<div class="well well-lg">' +
            '<question-preview-multiple-choice question="question" answers="answers" ng-show="question.type === \'multiple-choice\'"></question-preview-multiple-choice>' +
            '<question-preview-multiple-response question="question" answers="answers" ng-show="question.type === \'multiple-response\'"></question-preview-multiple-response>' +
            '<question-preview-true-or-false question="question" answers="answers" ng-show="question.type === \'true-or-false\'"></question-preview-true-or-false>' +
            '<question-preview-fill-in-the-blanks question="question" answers="answers" ng-show="question.type === \'fill-in-the-blanks\'"></question-preview-fill-in-the-blanks>' +
            '<question-preview-multiline-text question="question" answers="answers" ng-show="question.type === \'multiline-text\'"></question-preview-multiline-text>' +
            '<question-preview-file-submission question="question" answers="answers" ng-show="question.type === \'file-submission\'"></question-preview-file-submission>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>',
            restrict: 'E',
            scope: {
                userQuestion: '=',
                currentConfig: '=',
                questionIndex: '='
            }
            ,
            link: function postLink(scope, element, attrs) {
                scope.showQuestion = false;
                scope.showUserQuestion = false;

                /**
                 * Toggles the question preview for an item of feedback
                 */
                scope.toggleQuestionPreview = function () {
                    if (!scope.showQuestion) {
                        getQuestion();
                        scope.showQuestion = true;
                    } else {
                        scope.showQuestion = false;
                    }
                };

                /**
                 * Toggles the userQuestion
                 */
                scope.toggleUserQuestion = function () {
                    scope.showUserQuestion = !scope.showUserQuestion;
                };

                /**
                 * gets the data for the question
                 */
                function getQuestion() {
                    var course = shareService.getCourse();
                    scope.question = course.lessons[scope.currentConfig.lessonIndex].modules[scope.currentConfig.moduleIndex].questions[scope.questionIndex];
                    scope.answers = course.lessons[scope.currentConfig.lessonIndex].modules[scope.currentConfig.moduleIndex].questions[scope.questionIndex].answers;
                }

            }
        }
            ;
    });
