'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:fillInTheBlanksDropDown
 * @description
 * # fillInTheBlanksDropDown
 */
angular.module('coursePlayerApp')
    .directive('fillInTheBlanksDropDown', function () {
        return {
            template: '<span uib-dropdown on-toggle="toggled(open)">' +
            '<a href id="simple-dropdown" uib-dropdown-toggle>' +
            ' {{selectedBlank.length ? selectedBlank : \'SELECT BLANK\'}} ' +
            '</a>' +
            '<ul uib-dropdown-menu aria-labelledby="simple-dropdown">' +
            '<li ng-repeat="blank in word track by $index" ng-class="{\'active\' : $index === selectedBlankIndex}">' +
            '<a href ng-click="onBlankSelected($index)">{{blank}}</a>' +
            '</li>' +
            '</ul>' +
            '</span>',
            restrict: 'E',
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
            }
        };
    });

