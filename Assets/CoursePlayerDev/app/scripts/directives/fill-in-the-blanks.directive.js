'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:fillInTheBlanks
 * @description
 * # fillInTheBlanks
 */
angular.module('coursePlayerApp')
 .directive('fillInTheBlanks', function ($compile, $rootScope) {

	/**
	 * Initilise the dropdowns with any previous data
	 */
	function init(scope) {
	 scope.selectedBlankIndex = scope.answers[getInsertIndex(scope.wordIndex, scope.words)];
	 scope.selectedBlank = scope.word[scope.selectedBlankIndex];
	}

	/**
	 * Sets the correct markup for a word or blank
	 * @param scope
	 * @returns {*}
	 */
	function getTemplate(scope) {
	 if (angular.isArray(scope.word)) {
		return "<fill-in-the-blanks-drop-down></fill-in-the-blanks-drop-down>";
	 } else {
		return "<span> {{word}}</span>";
	 }
	}

	/**
	 * Add the selected blank index to the correct position in the answers array
	 * @param answersArray
	 * @param wordIndex
	 * @param selectedBlankIndex
	 * @param wordsArray
	 */
	function setAnswer(answersArray, wordIndex, selectedBlankIndex, wordsArray) {
	 var answerIndex = getInsertIndex(wordIndex, wordsArray);
	 answersArray[answerIndex] = selectedBlankIndex;
	}

	/**
	 * Gets the correct index for the answers.
	 * This really works as count is 1 based and indexes are zero based
	 * Therefore a count will give uses the correct place to insert via index
	 * @param wordIndex
	 * @param wordsArray
	 * @return {number}
	 */
	function getInsertIndex(wordIndex, wordsArray) {
	 var count = 0;
	 for (var i = 0; i < wordIndex; i++) {
		if (angular.isArray(wordsArray[i])) {
		 count++;
		}
	 }
	 return count;
	}

	return {
	 restrict: 'E',
	 scope: {
		word: '=',
		words: '=',
		wordIndex: '@',
		answers: '='
	 },
	 link: function postLink(scope, element, attrs) {

		element.html(getTemplate(scope));
		$compile(element.contents())(scope);

		scope.onBlankSelected = function (selectedBlankIndex) {
		 if (angular.isDefined(attrs.preview)) return;
		 scope.selectedBlank = scope.word[selectedBlankIndex];
		 scope.selectedBlankIndex = selectedBlankIndex;
			if (!$rootScope.isPreviewMode) setAnswer(scope.answers, scope.wordIndex, selectedBlankIndex, scope.words);
		};

		if(scope.answers && angular.isArray(scope.word))init(scope);
	 }
	};
 });
