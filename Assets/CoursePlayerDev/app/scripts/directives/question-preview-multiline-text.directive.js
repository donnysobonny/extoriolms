'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:questionPreviewMultilineText
 * @description
 * # questionPreviewMultilineText
 */
angular.module('coursePlayerApp')
    .directive('questionPreviewMultilineText', function () {
        return {
            template: '<h4>Multi Line Text</h4>' +
            '<hr>' +
            '<strong>Question:</strong>' +
            '<p ng-bind-html="question.questions"></p>' +
            '<hr>' +
            '<strong>Your answer:</strong>' +
            '<p ng-bind-html="answers"></p>',
            restrict: 'E',
            scope: {
                question: '=',
                answers: '='
            },
            link: function postLink(scope, element, attrs) {
            }
        };
    });
