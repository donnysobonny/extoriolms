'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:questionPreviewFillInTheBlanks
 * @description
 * # questionPreviewFillInTheBlanks
 */
angular.module('coursePlayerApp')
    .directive('questionPreviewFillInTheBlanks', function () {
        return {
            template: '<h4>Fill In The Blanks</h4>' +
            '<hr>' +
            '<strong>Question:</strong>' +
            '<br>' +
            '<fill-in-the-blanks ng-repeat="word in question.questions.words track by $index" word="word" words="question.questions.words" word-index="{{$index}}" answers="answers" preview></fill-in-the-blanks>',
            restrict: 'E',
            scope: {
                question: '=',
                answers: '='
            },
            link: function postLink(scope, element, attrs) {
            }
        };
    });
