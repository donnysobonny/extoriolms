'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:questionPreviewMultipleChoice
 * @description
 * # questionPreviewMultipleChoice
 */
angular.module('coursePlayerApp')
    .directive('questionPreviewMultipleChoice', function () {
        return {
            template: '<h4>Multiple Choice <small>Select 1 answer out of {{question.questions.options.length}} options</small></h4>' +
            '<hr>' +
            '<strong>Question:</strong>' +
            '<p ng-bind-html="question.questions.question"></p>' +
            '<hr>' +
            '<strong>Options:</strong>' +
            '<ol>' +
            '<li ng-repeat="option in question.questions.options">{{option}}</li>' +
            '</ol>' +
            '<hr>' +
            '<strong>Your answer:</strong>' +
            '<p>{{answers[0] + 1}}. {{ question.questions.options[answers[0]] }}</p>',
            restrict: 'E',
            scope: {
                question: '=',
                answers: '='
            },
            link: function postLink(scope, element, attrs) {
            }
        };
    });
