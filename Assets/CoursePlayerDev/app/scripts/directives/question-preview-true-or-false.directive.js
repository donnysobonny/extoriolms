'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:questionPreviewTrueOrFalse
 * @description
 * # questionPreviewTrueOrFalse
 */
angular.module('coursePlayerApp')
    .directive('questionPreviewTrueOrFalse', function () {
        return {
            template: '<h4>True or False</h4>' +
            '<hr>' +
            '<strong>Question:</strong>' +
            '<p ng-bind-html="question.questions"></p>' +
            '<hr>' +
            '<strong>Your answer:</strong>' +
            '<p>{{answers[0]}}</p>',
            restrict: 'E',
            scope: {
                question: '=',
                answers: '='
            },
            link: function postLink(scope, element, attrs) {
            }
        };
    });
