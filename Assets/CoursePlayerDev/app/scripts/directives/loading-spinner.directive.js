'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:loadingSpinner
 * @description
 * # loadingSpinner
 */
angular.module('coursePlayerApp')
    .directive('loadingSpinner', function () {
        return {
            scope: {
                message: '@'
            },
            template: '<div class="sk-folding-cube">' +
            '<div class="sk-cube1 sk-cube"></div>' +
            '<div class="sk-cube2 sk-cube"></div>' +
            '<div class="sk-cube4 sk-cube"></div>' +
            '<div class="sk-cube3 sk-cube"></div></div>' +
            '<div id="sk-message">{{message ? message : defaultMessage}}</div>',
            restrict: 'E',
            link: function postLink(scope, element, attrs) {
                scope.defaultMessage = 'Loading';
            }
        };
    });
