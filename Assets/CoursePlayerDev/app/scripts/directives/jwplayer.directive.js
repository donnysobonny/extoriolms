'use strict';

/**
 * @ngdoc directive
 * @name coursePlayerApp.directive:jwplayer
 * @description
 * # jwplayer
 */
angular.module('coursePlayerApp')
    .directive('jwPlayer', function (jwPlayer,
                                     $compile,
                                     $log,
                                     $rootScope) {

        var player;

        /**
         * Set any needed jw player event
         * @param player
         */
        function setPlayerEvents(player) {
            player.on('ready', function () {
                $rootScope.$broadcast('lms-jwplayer-ready');
            });

            player.on('error', function () {
                console.log('Error' + player);
                $rootScope.$broadcast('lms-jwplayer-error');
            });
        }

        /**
         * Creates a new jwplayer element
         * @param scope
         * @param element
         */
        function renderNewJWPlayerElement(scope, element, playerOptions) {
            var playerId = scope.playerId || 'lms-media-player';
            var getTemplate = function (playerId) {
                return '<div id="' + playerId + '"></div>';
            };

            element.html(getTemplate(playerId));
            $compile(element.contents())(scope);
            player = jwPlayer.initJWPlayer(playerId);
            player.setup(playerOptions);
            setPlayerEvents(player);
        }

        /**
         * Directive linker function
         * @param scope
         * @param element
         * @param attrs
         */
        var linker = function postLink(scope, element, attrs) {
            var playerId = scope.playerId || 'lms-media-player';

            scope.$on('$destroy', function () {
                jwPlayer.cleanUpJWPlayer(playerId);
            });

            scope.$on('lms-render-new-jwplayer', function (event, playerOptions) {
                if (!playerOptions) {
                    $log.error("You must provide the JW Player options as parameter in $broadcast");
                } else if (!playerOptions.file.length) {
                    $log.error("Cannot initialize JW Player as no file url provided in the options");
                } else {
                    renderNewJWPlayerElement(scope, element, playerOptions);
                }
            });

        };

        return {
            restrict: 'E',
            scope: {
                playerId: '@'
            },
            link: linker
        };
    });
