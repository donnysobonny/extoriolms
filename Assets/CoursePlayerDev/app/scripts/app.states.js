'use strict';

angular.module('coursePlayerApp')
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

        //$urlRouterProvider.otherwise(function ($location) {

        //return $location.path() + '/overview';
        //});
        $urlRouterProvider.otherwise('/overview');

        $stateProvider
            .state('coursePlayer', {
                abstract: true,
                url: '',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/core/course-player.state.html'
            })

            .state('coursePlayer.root', {
                views: {
                    'navigation': {
                        templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/core/partials/navigation.state.html',
                        controller: 'NavigationCtrl',
                        controllerAs: 'vm'
                    },
                    '': {
                        template: '<div ng-class="$root.animationType" id="lms-animate" ui-view></div>'
                    },
                    'subnav': {
                        templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/core/partials/sub-nav.state.html',
                        controller: 'SubNavigationCtrl',
                        controllerAs: 'vm'
                    },
                    'header': {
                        templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/core/partials/header.state.html',
                        controller: 'HeaderCtrl',
                        controllerAs: 'vm'
                    }
                }
            })

            //Item states
            .state('coursePlayer.root.overview', {
                url: '/overview',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/item/overview.state.html',
                controller: 'OverviewCtrl',
                controllerAs: 'vm',
                params: {
                    module: null,
                    currentConfig: {},
                    userModule: {}
                }
            })

            .state('coursePlayer.root.video', {
                url: '/video',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/item/video.state.html',
                controller: 'VideoCtrl',
                controllerAs: 'vm',
                params: {
                    module: null,
                    currentConfig: {},
                    userModule: {}
                }
            })

            .state('coursePlayer.root.text', {
                url: '/text',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/item/text.state.html',
                controller: 'TextCtrl',
                controllerAs: 'vm',
                params: {
                    module: null,
                    currentConfig: {},
                    userModule: {}
                }
            })

            .state('coursePlayer.root.audio', {
                url: '/audio',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/item/audio.state.html',
                controller: 'AudioCtrl',
                controllerAs: 'vm',
                params: {
                    module: null,
                    currentConfig: {},
                    userModule: {}
                }
            })

            .state('coursePlayer.root.image', {
                url: '/image',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/item/image.state.html',
                controller: 'ImageCtrl',
                controllerAs: 'vm',
                params: {
                    module: {},
                    currentConfig: {},
                    userModule: {}
                }
            })

            .state('coursePlayer.root.document', {
                url: '/document',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/item/document.state.html',
                controller: 'DocumentCtrl',
                controllerAs: 'vm',
                params: {
                    module: null,
                    currentConfig: {},
                    userModule: {}
                }
            })

            //Quiz states

            .state('coursePlayer.root.quiz-landing', {
                url: '/quiz',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/quiz/quiz-landing.state.html',
                controller: 'QuizLandingCtrl',
                controllerAs: 'vm',
                params: {
                    module: null,
                    currentConfig: null,
                    userModule: null
                }
            })

            .state('coursePlayer.root.multiple-choice', {
                url: '/multiple-choice',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/quiz/multiple-choice.state.html',
                controller: 'MultipleChoiceCtrl',
                controllerAs: 'vm',
                params: {
                    question: null,
                    currentConfig: null,
                    userModule: {}
                }
            })

            .state('coursePlayer.root.multiple-response', {
                url: '/multiple-response',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/quiz/multiple-response.state.html',
                controller: 'MultipleResponseCtrl',
                controllerAs: 'vm',
                params: {
                    question: null,
                    currentConfig: null,
                    userModule: {}
                }
            })

            .state('coursePlayer.root.true-or-false', {
                url: '/true-or-false',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/quiz/true-or-false.state.html',
                controller: 'TrueOrFalseCtrl',
                controllerAs: 'vm',
                params: {
                    question: null,
                    currentConfig: null,
                    userModule: {}
                }
            })

            .state('coursePlayer.root.fill-in-the-blanks', {
                url: '/fill-in-the-blanks',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/quiz/fill-in-the-blanks.state.html',
                controller: 'FillInTheBlanksCtrl',
                controllerAs: 'vm',
                params: {
                    question: null,
                    currentConfig: null,
                    userModule: {}
                }
            })

            .state('coursePlayer.root.multiline-text', {
                url: '/multi-line-text',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/quiz/multiline-text.state.html',
                controller: 'MultilineTextCtrl',
                controllerAs: 'vm',
                params: {
                    question: null,
                    currentConfig: null,
                    userModule: {}
                }
            })

            .state('coursePlayer.root.file-submission', {
                url: '/file-submission',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/states/quiz/file-submission.state.html',
                controller: 'FileSubmissionCtrl',
                controllerAs: 'vm',
                params: {
                    question: null,
                    currentConfig: null,
                    userModule: {}
                }
            });

        $locationProvider.html5Mode(true);
    });