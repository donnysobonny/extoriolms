'use strict';

angular
    .module('coursePlayerApp', [
        'ngAnimate',
        'ui.router',
        'ngSanitize',
        'ngResource',
        'ui.bootstrap',
        'pdf',
        'summernote',
        'toaster'
    ]);
