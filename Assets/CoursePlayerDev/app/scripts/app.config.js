angular.module('coursePlayerApp').config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our uk amazon s3 servers
        'https://s3-eu-west-1.amazonaws.com/tefl-lms/**']);
});
