'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.tooltips
 * @description
 * # tooltips
 * Constant in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .constant('TOOLTIPS', {
        restartQuiz: 'You are not able to restart the quiz, Currently you have questions awaiting to be marked.'
    });
