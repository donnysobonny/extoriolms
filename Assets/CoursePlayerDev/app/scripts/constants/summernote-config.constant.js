'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.SUMMERNOTECONFIG
 * @description
 * # SUMMERNOTECONFIG
 * Constant in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .constant('SUMMERNOTE_CONFIG_IMAGE', {
        height: 600,
        focus: true,
        airMode: false,
        toolbar: [
            ['edit', ['undo', 'redo']],
            ['headline', ['style']],
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ['alignment', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['picture', 'hr']],
            ['view', ['fullscreen']]
        ]
    })
    .constant('SUMMERNOTE_CONFIG', {
        height: 600,
        focus: true,
        airMode: false,
        toolbar: [
            ['edit', ['undo', 'redo']],
            ['headline', ['style']],
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ['alignment', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['hr']],
            ['view', ['fullscreen']]
        ]
    });
