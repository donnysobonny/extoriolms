'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:FilesubmissionCtrl
 * @description
 * # FilesubmissionCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('QuizLandingCtrl', function ($rootScope,
                                             $state,
                                             $stateParams,
                                             userCourseService,
                                             stateParamsService,
                                             shareService,
                                             helperService,
                                             TOOLTIPS,
                                             toaster) {
            var vm = this;

            init();

            /**
             * Handles click event to start the quiz when userModules status is pending
             */
            vm.startQuiz = function () {
                startQuiz($stateParams.module, $stateParams.userModule, $stateParams.currentConfig);
            };

            /**
             * Resumes a quiz when when the userModules status is started
             */
            vm.resumeQuiz = function () {
                resumeQuiz($stateParams.module, $stateParams.userModule, $stateParams.currentConfig);
            };

            /**
             * restarts the quiz when the userModules status is pending passed or failed
             */
            vm.restartQuiz = function () {
                startQuiz($stateParams.module, $stateParams.userModule, $stateParams.currentConfig);
            };

            /**
             * Initial function called
             */
            function init() {
                if (!$stateParams.module) {
                    $state.go('coursePlayer.root.overview');
                } else {
                    setVm();
                    if (!$rootScope.isPreviewMode) setAsCurrentUserModule();
                }
            }

            /**
             * Assign any required data to the view model
             */
            function setVm() {
                vm.tooltips = TOOLTIPS;
                vm.module = $stateParams.module;
                vm.currentConfig = $stateParams.currentConfig;
                vm.userModule = $stateParams.userModule;
                if (!$rootScope.isPreviewMode) vm.userQuestion = $stateParams.userModule.userQuestions[$stateParams.currentConfig.questionIndex];
            }

            /**
             * Sets this quiz landing page to the current userModule
             */
            function setAsCurrentUserModule() {
                userCourseService.setCurrentUserModule($stateParams.userModule.id, $stateParams.currentConfig);
            }

            /**
             * Starts the quiz userModule when userModule.status === "pending"
             * @param userModule
             * @param module
             * @param currentConfig
             */
            function startQuiz(module, userModule, currentConfig) {
                $rootScope.isQuiz = true;
                currentConfig.questionIndex = 0;
                shareService.setModule(module);
                shareService.setQuestion(module.questions[0]);
                shareService.setUserModule(userModule);
                if (!$rootScope.isPreviewMode) userCourseService.startUserModule(userModule.id, $stateParams.currentConfig);
                loadQuizState(module, userModule, currentConfig);
                toaster.pop('success', 'Quiz Started');
            }

            /**
             * Resumes a quiz when userModule.status === "started"
             * @param module
             * @param userModule
             * @param currentConfig
             */
            function resumeQuiz(module, userModule, currentConfig) {
                var resumeQuestionIndex = getFirstPendingIndex(userModule.userQuestions);
                $rootScope.isQuiz = true;
                currentConfig.questionIndex = resumeQuestionIndex;
                shareService.setModule(module);
                shareService.setQuestion(module.questions[resumeQuestionIndex]);
                shareService.setUserModule(userModule);
                loadQuizState(module, userModule, currentConfig);
                toaster.pop('success', 'Quiz Resumed');
            }

            /**
             * Load up the first module question with any data it needs being
             * passed in via $stateParams
             * @param module
             * @param userModule
             * @param currentConfig
             */
            function loadQuizState(module, userModule, currentConfig) {
                var stateName = 'coursePlayer.root.' + module.questions[currentConfig.questionIndex].type;
                var stateParams = stateParamsService.NewQuestion(module.questions[currentConfig.questionIndex], currentConfig, userModule);
                $state.go(stateName, stateParams);
            }

            /**
             * Returns the first pending userQuestion index to resume the quiz on or
             * 0 if they are all started
             * @param userQuestions
             */
            function getFirstPendingIndex(userQuestions) {
                var previousIndexes = helperService.getIndexesByPropertyValue('status', 'pending', userQuestions);
                return previousIndexes.length ? previousIndexes[0] : 0;
            }
        }
    )
;
