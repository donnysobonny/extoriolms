'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:FillintheblanksCtrl
 * @description
 * # FillintheblanksCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('FillInTheBlanksCtrl', function ($scope,
                                                 $rootScope,
                                                 $state,
                                                 $stateParams,
                                                 userCourseService) {
        var vm = this;
        init();

        /**
         * Initial function called
         */
        function init() {
            if ($stateParams.question) setVm(); else $state.go('coursePlayer.root.overview');
        }

        /**
         * Sets any required data to the vm
         */
        function setVm() {
            vm.question = $stateParams.question;
            if (!$rootScope.isPreviewMode) vm.userQuestion = $stateParams.userModule.userQuestions[$stateParams.currentConfig.questionIndex];
            if (!$rootScope.isPreviewMode && !vm.userQuestion.userAnswers.length) vm.userQuestion.userAnswers = new Array(countBlanks(vm.question.questions.words));
            $scope.$on('save-user-question', saveUserQuestion);
        }

        /**
         * Counts the number of blanks
         * @param wordsArray
         * @return {number}
         */
        function countBlanks(wordsArray) {
            var blanksCount = 0;
            for (var i = 0; i < wordsArray.length; i++)if (angular.isArray(wordsArray[i])) blanksCount++;
            return blanksCount;
        }

        /**
         * Save the userQuestion
         */
        function saveUserQuestion() {
            if (!$rootScope.isPreviewMode) userCourseService.saveUserQuestion(vm.userQuestion, $stateParams.currentConfig);
        }

    });
