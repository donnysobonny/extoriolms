'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:TrueorfalseCtrl
 * @description
 * # TrueorfalseCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('TrueOrFalseCtrl', function ($scope,
                                             $rootScope,
                                             $state,
                                             $stateParams,
                                             userCourseService) {
        var vm = this;
        init();

        /**
         * Sets the userAnswers array value
         * @param answer
         */
        vm.setAnswer = function (answer) {
            if (!$rootScope.isPreviewMode) vm.userQuestion.userAnswers[0] = answer;
        };

        /**
         * Click event handler for submit userQuestion
         */
        vm.saveUserQuestion = function () {
            if (!$rootScope.isPreviewMode) saveUserQuestion();
        };

        /**
         * initial function called
         * Copy the question so $sce does not mess up properties when setting trusted properties
         */
        function init() {
            if ($stateParams.question) setVm(); else $state.go('coursePlayer.root.overview');
        }

        /**
         * Assign all required data to view model
         */
        function setVm() {
            vm.question = $stateParams.question;
            if (!$rootScope.isPreviewMode) vm.userQuestion = $stateParams.userModule.userQuestions[$stateParams.currentConfig.questionIndex];
            $scope.$on('save-user-question', saveUserQuestion);
        }

        /**
         * Save the userQuestion
         */
        function saveUserQuestion() {
            if (!$rootScope.isPreviewMode) userCourseService.saveUserQuestion(vm.userQuestion, $stateParams.currentConfig);
        }
    });
