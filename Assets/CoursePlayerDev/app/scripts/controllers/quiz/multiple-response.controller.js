'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:MultipleresponseCtrl
 * @description
 * # MultipleresponseCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('MultipleResponseCtrl', function ($scope,
                                                  $rootScope,
                                                  $state,
                                                  $stateParams,
                                                  userCourseService,
                                                  helperService) {
        var vm = this;

        init();

        /**
         * Toggles answers in the answers array
         * @param optionIndex
         */
        vm.toggleMultipleResponseOptions = function (optionIndex) {
            if (!$rootScope.isPreviewMode) helperService.toggleArrayItem(optionIndex, vm.userQuestion.userAnswers);
        };

        /**
         * This initial functional called
         */
        function init() {
            if ($stateParams.question) {
                setVm();
            } else {
                $state.go('coursePlayer.root.overview');
            }
        }

        /**
         * Assign any needed data to the vm
         */
        function setVm() {
            vm.question = $stateParams.question;
            if (!$rootScope.isPreviewMode) vm.userQuestion = $stateParams.userModule.userQuestions[$stateParams.currentConfig.questionIndex];
            $scope.$on('save-user-question', saveUserQuestion);
        }

        /**
         * Save the userQuestion
         */
        function saveUserQuestion() {
            if (!$rootScope.isPreviewMode) userCourseService.saveUserQuestion(vm.userQuestion, $stateParams.currentConfig);
        }

    });
