'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:FilesubmissionCtrl
 * @description
 * # FilesubmissionCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('FileSubmissionCtrl', function ($scope,
                                                $rootScope,
                                                $state,
                                                $stateParams,
                                                userCourseService) {
        var vm = this;

        init();

        /**
         * initial function called
         */
        function init() {
            if ($stateParams.question) setVm(); else $state.go('coursePlayer.root.overview');
        }

        /**
         * Assign all required data to view model
         */
        function setVm() {
            vm.question = $stateParams.question;
            if (!$rootScope.isPreviewMode) vm.userQuestion = $stateParams.userModule.userQuestions[$stateParams.currentConfig.questionIndex];
            $scope.$on('save-user-question', saveUserQuestion);
        }

        /**
         * Save the userQuestion
         */
        function saveUserQuestion() {
            if (!$rootScope.isPreviewMode) userCourseService.saveUserQuestion(vm.userQuestion, $stateParams.currentConfig);
        }

    });
