'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:MultilinetextCtrl
 * @description
 * # MultilinetextCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('MultilineTextCtrl', function ($scope,
                                               $rootScope,
                                               $state,
                                               $stateParams,
                                               SUMMERNOTE_CONFIG,
                                               userCourseService) {
        var vm = this;

        init();

        /**
         * Initial function called
         */
        function init() {
            if ($stateParams.question) setVm(); else $state.go('coursePlayer.root.overview');
        }

        /**
         * Assign data to the view model
         */
        function setVm() {
            vm.question = $stateParams.question;
            if (!$rootScope.isPreviewMode) vm.userQuestion = $stateParams.userModule.userQuestions[$stateParams.currentConfig.questionIndex];
            vm.summernoteConfig = SUMMERNOTE_CONFIG;
            $scope.$on('save-user-question', saveUserQuestion);
        }

        /**
         * Save the userQuestion
         */
        function saveUserQuestion() {
            if (!$rootScope.isPreviewMode) userCourseService.saveUserQuestion(vm.userQuestion, $stateParams.currentConfig);
        }
    });
