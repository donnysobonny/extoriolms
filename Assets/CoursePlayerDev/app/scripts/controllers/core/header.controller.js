'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('HeaderCtrl', function ($rootScope, $window, $timeout) {
        var vm = this;
        setPageOffsetForHeader();

        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                if ($rootScope.translated) translate();
                if ($window.innerWidth <= 768) hideNavigationForMobile();
                setPageOffsetForHeader();
            });

        /**
         * Translate the page to chinese
         */
        vm.translate = function () {
            $rootScope.translated = true;
            translate();
        };

        /**
         * Restore to english
         */
        vm.restore = function () {
            $rootScope.translated = false;
            $window.Microsoft.Translator.FloaterOnClose();
        };

        /**
         * Call the Microsoft Translator and set up handlers
         */
        function translate() {
            $window.Microsoft.Translator.Widget.Translate('en', 'zh-CHS', 5000);
        }

        /**
         * Hides the nav for mobile
         */
        function hideNavigationForMobile() {
            $timeout(function () {
                $rootScope.open = false;
            }, 500);
        }

        /**
         * Sets the margin for the page depending on the size if the header
         */
        function setPageOffsetForHeader() {
            $timeout(function () {
                var headerheight = $('h2.course-page-title').innerHeight();
                $("#lms-player-main").css("margin-top", headerheight+20);
            }, 500);
        }
    });
