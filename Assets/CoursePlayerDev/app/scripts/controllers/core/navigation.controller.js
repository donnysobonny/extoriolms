'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:NavigationCtrl
 * @description
 * # NavigationCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('NavigationCtrl', function ($scope,
                                            $rootScope,
                                            $state,
                                            $log,
                                            helperService,
                                            shareService,
                                            stateParamsService,
                                            userCourseService,
                                            extorioApiService,
                                            toaster,
                                            $timeout) {
        var vm = this;

        init();

        /**
         * Shows the course landing page.
         */
        vm.showCourseOverview = function () {
            vm.currentConfig = null;
            $state.go('coursePlayer.root.overview', {
                module: null,
                currentConfig: 'overview'
            });
        };

        /**
         * Loads up the clicked module
         * @param module
         * @param lessonIndex
         * @param moduleIndex
         */
        vm.loadModule = function (module, lessonIndex, moduleIndex) {
            if (angular.isUndefined(vm.currentConfig)) {
                $rootScope.animationType = 'next';
            }
            else {
                var newConfig = {
                    lessonIndex: lessonIndex,
                    moduleIndex: moduleIndex,
                    isQuiz: false
                };
                setModuleStateAnimationClass(vm.currentConfig, newConfig);
            }

            $rootScope.previousConfig = angular.copy(vm.currentConfig);

            vm.currentConfig = {
                lessonIndex: lessonIndex,
                moduleIndex: moduleIndex,
                isQuiz: helperService.checkForQuiz(module.type)
            };

            if (!$rootScope.isPreviewMode) vm.userModule = userCourseService.getUserModule(vm.currentConfig.lessonIndex, vm.currentConfig.moduleIndex);
            var stateName = 'coursePlayer.root.' + (module.type === 'lecture' ? module.lectureType : 'quiz-landing');
            var stateParams = stateParamsService.NewModule(module, vm.currentConfig, vm.userModule);
            $state.go(stateName, stateParams);
        };

        /**
         * When quiz mode is active this loads a question from the nav
         * @param questionIndex
         * @param question
         */
        vm.loadQuizModuleQuestion = function (question, questionIndex) {
            if (questionIndex === vm.currentConfig.questionIndex) return;
            setQuestionStateAnimationClass(vm.currentConfig.questionIndex, questionIndex);
            $rootScope.previousConfig = angular.copy(vm.currentConfig);
            vm.currentConfig.questionIndex = questionIndex;
            shareService.setQuestion(question);
            if (!$rootScope.isPreviewMode) vm.userModule = userCourseService.getUserModule(vm.currentConfig.lessonIndex, vm.currentConfig.moduleIndex);
            var stateName = vm.quizModule.questions[vm.currentConfig.questionIndex].type;
            var stateParams = stateParamsService.NewQuestion(vm.quizModule.questions[vm.currentConfig.questionIndex], vm.currentConfig, vm.userModule);
            $state.go('coursePlayer.root.' + stateName, stateParams);
        };

        /**
         *Check validates the quiz and submits it for marking
         */
        vm.submitQuizForMarking = function () {
            var pendingUserQuestionsIndexes = helperService.getIndexesByPropertyValue('status', 'pending', vm.userModule.userQuestions);
            if (!pendingUserQuestionsIndexes.length) {
                if (!$rootScope.isPreviewMode) submitQuiz();
            } else {
                toaster.pop('error', 'All Questions must be saved');
                vm.loadQuizModuleQuestion(vm.userModule.userQuestions[pendingUserQuestionsIndexes[0]], pendingUserQuestionsIndexes[0]);
                animateSaveUserQuestionButton();
            }
        };

        /**
         * Cancels out of current quiz with any progress being saved saved
         */
        vm.pauseQuiz = function () {
            pauseQuiz();
        };

        /**
         * Initial function called. Set up the view model
         */
        function init() {
            getCourse();
            getQuizModule();
            if (!$rootScope.isPreviewMode)getUserCourse();
            if (!$rootScope.isPreviewMode)getUserModule();
        }

        /**
         * Gets and sets the course object
         */
        function getCourse() {
            vm.course = shareService.getCourse();
        }

        /**
         * Gets and sets the userCourse
         */
        function getUserCourse() {
            if (!$rootScope.isPreviewMode) vm.userCourse = shareService.getUserCourse();
            $scope.$on('user-course-set', getUserCourse);
        }

        /**
         * Will be typically be called when the nav is flipped into quiz mode
         * Gets and set a module type quiz.
         */
        function getQuizModule() {
            vm.quizModule = shareService.getModule();
            $scope.$on('module-set', getQuizModule);
        }

        function getUserModule() {
            if (!$rootScope.isPreviewMode) vm.userModule = shareService.getUserModule();
            $scope.$on('user-module-set', getUserModule);
        }

        /**
         * Sets the correct animation class for page slides depending if the clicked module if in front or behind.
         * @param currentConfig
         * @param newConfig
         */
        function setModuleStateAnimationClass(currentConfig, newConfig) {

            var next = 'next';
            var previous = 'previous';

            /**
             * Checks the indexes of lessons first to see if new one is in front or behind.
             */
            function checkLessons() {
                if (newConfig.lessonIndex === currentConfig.lessonIndex) {
                    checkModules();
                } else {
                    $rootScope.animationType = newConfig.lessonIndex > currentConfig.lessonIndex ? next : previous;
                }

                /**
                 * If the lesson is the same this will then go and check modules.
                 */
                function checkModules() {
                    if (newConfig.moduleIndex === currentConfig.moduleIndex) return;
                    $rootScope.animationType = newConfig.moduleIndex > currentConfig.moduleIndex ? next : previous;
                }
            }

            if (!currentConfig) $rootScope.animationType = next; else checkLessons();
        }

        /**
         * Sets the correct animation class when clicking questions in the navigation bar
         * @param currentIndex
         * @param newIndex
         */
        function setQuestionStateAnimationClass(currentIndex, newIndex) {
            $rootScope.animationType = newIndex > currentIndex ? 'next' : 'previous';
        }

        /**
         * Submits a quiz for marking
         */
        function submitQuiz() {
            var userModule = userCourseService.getUserModule(vm.currentConfig.lessonIndex, vm.currentConfig.moduleIndex);
            var module = vm.course.lessons[vm.currentConfig.lessonIndex].modules[vm.currentConfig.moduleIndex];
            extorioApiService
                .submitUserModule()
                .save({userModuleId: userModule.id})
                .$promise
                .then(submitQuizSuccess, submitQuizFail);

            /**
             * Handles successful quiz submission
             */
            function submitQuizSuccess(response) {
                toaster.pop('success', 'Quiz submitted');
                angular.merge(userModule, response.data);
                $rootScope.isQuiz = false;
                delete vm.currentConfig.questionIndex;
                var stateParams = stateParamsService.NewModule(module, vm.currentConfig, userModule);
                $state.go('coursePlayer.root.quiz-landing', stateParams);
            }

            /**
             * Handles failed quiz submission
             */
            function submitQuizFail() {
                $log.error("Was not able to submit userModule for marking");
            }
        }

        /**
         * Exit out of quiz mode and returns to the quiz landing page
         */
        function pauseQuiz() {
            toaster.pop('success', 'Progress saved');
            var userModule = userCourseService.getUserModule(vm.currentConfig.lessonIndex, vm.currentConfig.moduleIndex);
            var module = vm.course.lessons[vm.currentConfig.lessonIndex].modules[vm.currentConfig.moduleIndex];
            $rootScope.isQuiz = false;
            delete vm.currentConfig.questionIndex;
            var stateParams = stateParamsService.NewModule(module, vm.currentConfig, userModule);
            $state.go('coursePlayer.root.quiz-landing', stateParams);
        }

        /**
         * Animates the save user quiz button
         */
        function animateSaveUserQuestionButton() {
            $rootScope.showSaveUserQuestionHelp = true;
            $timeout(function () {
                $rootScope.showSaveUserQuestionHelp = false;
            }, 500);
        }

    });
