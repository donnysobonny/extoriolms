'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:SubNavigationCtrl
 * @description
 * # SubNavigationCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('SubNavigationCtrl', function ($state,
                                               subNavigationService,
                                               $scope,
                                               $rootScope,
                                               stateParamsService,
                                               userCourseService,
                                               shareService,
                                               $log) {
            var vm = this;

            init();

            /**
             * Listens in to the $stateParams going from nav controller to lecture/question controllers.
             * Sets currentConfig so next/previous module/question can be retrieved out of scope in the sub-nav.
             * @type {*|(function())}
             */
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                vm.currentConfig = toParams.currentConfig;
                vm.isModuleQuestion = toParams.question != null;
                vm.isModuleTypeQuiz = toParams.module != null && toParams.module.questions.length;
            });

            /**
             * Listens out for global call to load up next item
             */
            $scope.$on('load-next-state', function () {
                vm.next();
            });

            /**
             * Listens out for global call to load up the next item
             */
            $scope.$on('load-previous-state', function () {
                vm.previous();
            });

            /**
             * Click event for the next button
             */
            vm.next = function () {
                $rootScope.animationType = 'next';
                if (vm.isModuleQuestion) getNextQuestion(); else getNextModule()
            };

            /**
             * Click event for the previous button
             */
            vm.previous = function () {
                $rootScope.animationType = 'previous';
                if (vm.isModuleQuestion) getPreviousQuestion(); else getPreviousModule();
            };

            /**
             * Creates a broadcast event so current to submit the current lecture userModule
             */
            vm.submitUserModuleBroadcast = function () {
                $rootScope.$broadcast('submit-lecture-user-module');
            };

            /**
             * Creates a broadcast event to save the current userQuestion
             */
            vm.saveUserQuestionBroadcast = function () {
                $rootScope.$broadcast('save-user-question');
            };

            /**
             * Initial function called after controller has been instantiated
             */
            function init() {
                setCurrentConfigForDefaultState();
            }

            /**
             * When app is loaded up it sets to course overview state
             * This has not modules associated with it so no current obj is needed
             * When this state is activated through the nav config is set to a string through the $stateParams
             * We do the same here to some ui stuff can be hidden
             */
            function setCurrentConfigForDefaultState() {
                if (!vm.currentConfig) vm.currentConfig = 'overview';
            }

            /**
             * Gets the next module from the tracker service
             */
            function getNextModule() {
                var module = subNavigationService.getNextModule(vm.currentConfig);
                if (module) loadModule(module);
            }

            /**
             * Gets the previous module from the tracker service
             */
            function getPreviousModule() {
                var module = subNavigationService.getPreviousModule(vm.currentConfig);
                if (module) loadModule(module);
            }

            /**
             *Loads up the correct state based on its type and sets the $stateParams with the currentConfig details about the module.
             * @param module
             */
            function loadModule(module) {
                if (module) {
                    var stateName = 'coursePlayer.root.' + (module.type === 'lecture' ? module.lectureType : 'quiz-landing');
                    var stateParams = stateParamsService.NewModule(module, vm.currentConfig, getUserModule());
                    $state.go(stateName, stateParams);
                }
            }

            /**
             * Gets the next question when quiz is active
             */
            function getNextQuestion() {
                if (nextQuestionIsDefined(vm.currentConfig.questionIndex)) {
                    var question = subNavigationService.getNextQuestion(vm.currentConfig);
                    loadQuestion(question);
                } else {
                    $log.info("End");
                }
            }

            /**
             * Gets the previous question when quiz is active
             */
            function getPreviousQuestion() {
                if (previousQuestionIsDefined(vm.currentConfig.questionIndex)) {
                    var question = subNavigationService.getPreviousQuestion(vm.currentConfig);
                    loadQuestion(question);
                } else {
                    $log.info("Start");
                }
            }

            /**
             * Loads the passed in question in the current quiz
             * @param question
             */
            function loadQuestion(question) {
                if (question) {
                    var stateName = 'coursePlayer.root.' + question.type;
                    var stateParams = stateParamsService.NewQuestion(question, vm.currentConfig, getUserModule());
                    $state.go(stateName, stateParams);
                }
            }

            /**
             * Gets the user module for the current quiz module
             * Also sets the userModule in the share service to the main navs userModule will be in sync
             * @returns {*}
             */
            function getUserModule() {
                var userModule = userCourseService.getUserModule(vm.currentConfig.lessonIndex, vm.currentConfig.moduleIndex);
                return userModule;
            }

            /**
             * Checks to make sure there is a next question in the course
             */
            function nextQuestionIsDefined(currentQuestionIndex) {
                var quizModule = shareService.getModule();
                var lastQuestionIndex = quizModule.questions.length - 1;
                return currentQuestionIndex !== lastQuestionIndex;
            }

            /**
             * Checks to make sure that it does not try to fetch a question when its on the first one
             */
            function previousQuestionIsDefined() {
                return vm.currentConfig.questionIndex !== 0;
            }
        }
    );
