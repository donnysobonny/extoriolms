'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:TextCtrl
 * @description
 * # TextCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('TextCtrl', function ($scope,
                                      $state,
                                      $stateParams,
                                      $rootScope,
                                      userCourseService) {
        var vm = this;
        init();

        /**
         * Sets the current module of type lecture to passed
         * type lectures are automatically passed
         */
        vm.submitUserModule = function () {
            submitUserModule();
        };


        /**
         * Initial function called
         */
        function init() {
            if (!$stateParams.module) $state.go('coursePlayer.root.overview'); else {
                setVm();
                if (!$rootScope.isPreviewMode) setAsCurrentUserModule();
                if ($stateParams.userModule.status === 'pending' && !$rootScope.isPreviewMode) startUserModule();
                $scope.$on('submit-lecture-user-module', submitUserModule);
            }
        }

        /**
         * Initialise any needed properties on the vm
         */
        function setVm() {
            vm.module = $stateParams.module;
            vm.userModule = $stateParams.userModule;
        }

        /**
         * Sets the current userModule
         */
        function setAsCurrentUserModule() {
            userCourseService.setCurrentUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }

        /**
         * starts the userModule
         */
        function startUserModule() {
            userCourseService.startUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }

        /**
         * Submits the userModule to be automatically passed
         */
        function submitUserModule() {
            if (vm.userModule.status !== 'passed' && !$rootScope.isPreviewMode) userCourseService.submitUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }

    });
