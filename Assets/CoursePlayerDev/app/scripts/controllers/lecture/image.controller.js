'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:ImageCtrl
 * @description
 * # ImageCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('ImageCtrl', function ($scope,
                                       $state,
                                       $stateParams,
                                       userCourseService,
                                       moduleService,
                                       $rootScope,
                                       $log) {
        var vm = this;
        vm.um = $stateParams.userModule;
        init();

        /**
         * Initial functional called
         */
        function init() {
            if (!$stateParams.module) $state.go('coursePlayer.root.overview'); else {
                setVm();
                if (!$rootScope.isPreviewMode) setAsCurrentUserModule();
                if ($stateParams.userModule.status === 'pending' && !$rootScope.isPreviewMode) startUserModule();
                $scope.$on('submit-lecture-user-module', submitUserModule);
            }
        }

        /**
         * Assign data to the view model
         */
        function setVm() {
            vm.module = $stateParams.module;
            vm.userModule = $stateParams.userModule;
            if (!$stateParams.module.lectureFileUrl)getLectureFileUrl(vm.module.id);
        }

        /**
         * Gets the image url
         * @param moduleId
         */
        function getLectureFileUrl(moduleId) {
            $rootScope.$on('lms-jwplayer-error', getLectureFileUrl);
            moduleService.getLectureFileUrl(moduleId).then(function (response) {
                vm.lectureFile = response.data;
            }, function () {
                $log.error('Unable to get image lecture url');
            });
        }

        /**
         * Sets the current userModule
         */
        function setAsCurrentUserModule() {
            userCourseService.setCurrentUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }

        /**
         * starts the userModule
         */
        function startUserModule() {
            userCourseService.startUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }

        /**
         * Submits the userModule to be automatically passed
         */
        function submitUserModule() {
            if (vm.userModule.status !== 'passed' && !$rootScope.isPreviewMode) userCourseService.submitUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }
    });
