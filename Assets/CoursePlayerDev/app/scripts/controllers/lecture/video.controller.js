'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:PlayerCtrl
 * @description
 * # PlayerCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('VideoCtrl', function ($scope,
                                       $state,
                                       $stateParams,
                                       $sce,
                                       $q,
                                       $timeout,
                                       userCourseService,
                                       moduleService,
                                       closedCaptionService,
                                       $log,
                                       $rootScope) {
            var vm = this;

            init();

            /**
             * Initial functional called
             */
            function init() {
                if (!$stateParams.module) $state.go('coursePlayer.root.overview');
                else {
                    setVm();
                    if (!$rootScope.isPreviewMode) setAsCurrentUserModule();
                    if ($stateParams.userModule.status === 'pending' && !$rootScope.isPreviewMode) startUserModule();
                    $scope.$on('submit-lecture-user-module', submitUserModule);
                }
            }

            /**
             * Assign data to the view model
             */
            function setVm() {
                vm.module = $stateParams.module;
                vm.userModule = $stateParams.userModule;
                getFileUrls();
            }

            /**
             * Get any needed file urls
             */
            function getFileUrls() {
                if (vm.module.lectureFileUrl) initJWPlayerOptions(vm.module.lectureFileUrl, vm.module.subtitleFileUrl);
                else if (!vm.module.subtitleFileId) getLectureFileUrl();
                else getLectureFileAndSubtitleUrl();
            }

            /**
             * Gets the video url
             */
            function getLectureFileUrl() {
                $rootScope.$on('lms-jwplayer-error', getLectureFileUrl);
                moduleService.getLectureFileUrl(vm.module.id)
                    .then(function (response) {
                        vm.module.lectureFileUrl = response.data;
                        initJWPlayerOptions(vm.module.lectureFileUrl);
                    }, function () {
                        $log.error('Unable to get video lecture url');
                    });
            }

            function getLectureFileAndSubtitleUrl() {
                $q.all([
                    closedCaptionService.get(vm.module.subtitleFileId),
                    moduleService.getLectureFileUrl(vm.module.id)
                ]).then(function (response) {
                    vm.module.subtitleFileUrl = response[0].data;
                    vm.module.lectureFileUrl = response[1].data;
                    initJWPlayerOptions(vm.module.lectureFileUrl, vm.module.subtitleFileUrl);
                }, function (error) {
                    $log.error('Something in the getLectureAndSubtitleFileUrl() failed :(');
                    $log.error(error);
                });
            }


            /**
             * Jwplayer config
             */
            function initJWPlayerOptions(lectureFileUrl, subtitleFileUrl) {
                var jwPlayerOptions = {
                    file: lectureFileUrl,
                    type: 'mp4',
                    width: '100%',
                    aspectratio: '16:9',
                    primary: 'html5',
                    tracks: [{
                        file: subtitleFileUrl || null,
                        label: "Chinese",
                        kind: "captions",
                        "default": true
                    }],
                    skin: {
                        name: 'vapor',
                        active: '#0DBBA1'
                    }
                };

                initJWPlayer(jwPlayerOptions);
            }

            /**
             * Renders a new JW Player in the directive
             * @param jwPlayerOptions
             */
            function initJWPlayer(jwPlayerOptions) {
                $timeout(function () {
                    $scope.$broadcast('lms-render-new-jwplayer', jwPlayerOptions);
                }, 0);
            }

            /**
             * Sets the current userModule
             */
            function setAsCurrentUserModule() {
                userCourseService.setCurrentUserModule($stateParams.userModule.id, $stateParams.currentConfig);
            }

            /**
             * starts the userModule
             */
            function startUserModule() {
                userCourseService.startUserModule($stateParams.userModule.id, $stateParams.currentConfig);
            }

            /**
             * Submits the userModule to be automatically passed
             */
            function submitUserModule() {
                if (vm.userModule.status !== 'passed' && !$rootScope.isPreviewMode) userCourseService.submitUserModule($stateParams.userModule.id, $stateParams.currentConfig);
            }

        }
    )
;

