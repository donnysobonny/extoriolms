'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:AudioCtrl
 * @description
 * # AudioCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('AudioCtrl', function ($scope,
                                       $state,
                                       $stateParams,
                                       $sce,
                                       $timeout,
                                       $rootScope,
                                       userCourseService,
                                       moduleService,
                                       $log) {
            var vm = this;
            init();

            /**
             * Initial functional called
             */
            function init() {
                if (!$stateParams.module) $state.go('coursePlayer.root.overview'); else {
                    setVm();
                    if (!$rootScope.isPreviewMode) setAsCurrentUserModule();
                    if ($stateParams.userModule.status === 'pending' && !$rootScope.isPreviewMode) startUserModule();
                    $scope.$on('submit-lecture-user-module', submitUserModule);
                }
            }

            /**
             * Assign data to the view model
             */
            function setVm() {
                vm.module = $stateParams.module;
                vm.userModule = $stateParams.userModule;
                if (!vm.module.lectureFileUrl)getLectureFileUrl(); else initJWPlayerOptions(vm.module.lectureFileUrl);
            }

            /**
             * Get the audio file url
             */
            function getLectureFileUrl() {
                moduleService.getLectureFileUrl(vm.module.id).then(function (response) {
                    vm.module.lectureFileUrl = response.data;
                    initJWPlayerOptions(vm.module.lectureFileUrl);
                }, function () {
                    $log.error('Unable to get audio lecture url');
                });
            }

            /**
             * Jwplayer config
             */
            function initJWPlayerOptions(lectureFileUrl) {
                var jwPlayerOptions = {
                    file: lectureFileUrl,
                    type: 'mp3',
                    primary: 'html5',
                    width: 640,
                    height: 30,
                    skin: {
                        name: "vapor",
                        active: '#0DBBA1'
                    }
                };

                initJWPlayer(jwPlayerOptions);
            }

            /**
             * Renders a new JW Player in the directive
             * @param jwPlayerOptions
             */
            function initJWPlayer(jwPlayerOptions) {
                $timeout(function () {
                    $scope.$broadcast('lms-render-new-jwplayer', jwPlayerOptions);
                }, 0);
            }

            /**
             * Sets the current userModule
             */
            function setAsCurrentUserModule() {
                userCourseService.setCurrentUserModule($stateParams.userModule.id, $stateParams.currentConfig);
            }

            /**
             * starts the userModule
             */
            function startUserModule() {
                userCourseService.startUserModule($stateParams.userModule.id, $stateParams.currentConfig);
            }

            /**
             * Submits the userModule to be automatically passed
             */
            function submitUserModule() {
                if (vm.userModule.status !== 'passed' && !$rootScope.isPreviewMode) userCourseService.submitUserModule($stateParams.userModule.id, $stateParams.currentConfig);
            }
        }
    )
;
