'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:OverviewCtrl
 * @description
 * # OverviewCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('OverviewCtrl', function ($sce,
                                          $log,
                                          $rootScope,
                                          shareService,
                                          extorioApiService) {
        var vm = this;

        init();

        /**
         * initial function called
         * Copy the course so $sce does not mess up properties when setting trusted properties
         */
        function init() {
            getCourse();
            if (!$rootScope.isPreviewMode) getProgress();
        }

        /**
         * gets the course. Also listens out for broadcast event when its set and gets it again
         */
        function getCourse() {
            vm.course = shareService.getCourse();
            $rootScope.$on('course-set');
        }

        /**
         * Gets the progress for the userCourse
         */
        function getProgress() {
            var userCourse = shareService.getUserCourse();
            extorioApiService
                .userCourseDetailView()
                .get({userCourseId: userCourse.id})
                .$promise
                .then(function (response) {
                    vm.progress = response.progress;
                }, function (error) {
                    $log.error("Was not able to get progress.");
                });
        }
    });
