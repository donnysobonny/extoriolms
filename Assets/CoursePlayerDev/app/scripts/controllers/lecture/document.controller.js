'use strict';

/**
 * @ngdoc function
 * @name coursePlayerApp.controller:TextCtrl
 * @description
 * # TextCtrl
 * Controller of the coursePlayerApp
 */
angular.module('coursePlayerApp')
    .controller('DocumentCtrl', function ($scope,
                                          $rootScope,
                                          $state,
                                          $stateParams,
                                          userCourseService,
                                          $log,
                                          moduleService) {
        var vm = this;
        init();

        /**
         * Initial functional called
         */
        function init() {
            if (!$stateParams.module) $state.go('coursePlayer.root.overview'); else {
                setVm();
                if (!$rootScope.isPreviewMode) setAsCurrentUserModule();
                if ($stateParams.userModule.status === 'pending' && !$rootScope.isPreviewMode) startUserModule();
                $scope.$on('submit-lecture-user-module', submitUserModule);
            }
        }

        /**
         * Assign data to the view model
         */
        function setVm() {
            vm.module = $stateParams.module;
            vm.userModule = $stateParams.userModule;
            if (!$stateParams.module.lectureFileUrl)getLectureFileUrl();
        }

        /**
         * Get the pdf url from the module
         */
        function getLectureFileUrl() {
            moduleService.getLectureFileUrl(vm.module.id).then(function (response) {
                setPdf(response.data);
            }, function () {
                $log.error('Unable to get document lecture url');
            });
        }

        /**
         * Configure pdf plugin
         * @param url
         */
        function setPdf(url) {
            $scope.httpHeaders = {'Access-Control-Allow-Origin': '*'};
            $scope.pdfUrl = url;
        }

        /**
         * Sets the current userModule
         */
        function setAsCurrentUserModule() {
            userCourseService.setCurrentUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }

        /**
         * starts the userModule
         */
        function startUserModule() {
            userCourseService.startUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }

        /**
         * Submits the userModule to be automatically passed
         */
        function submitUserModule() {
            if (vm.userModule.status !== 'passed' && !$rootScope.isPreviewMode) userCourseService.submitUserModule($stateParams.userModule.id, $stateParams.currentConfig);
        }

    });
