'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.moduleService
 * @description
 * # moduleService
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('moduleService', function (extorioApiService, errorHandlerService, $q) {


        var service = {};

        /**
         * Gets the signed aws url for module media item by module id
         * @param moduleId
         */
        service.getLectureFileUrl = function (moduleId) {
            var deferred = $q.defer();
            extorioApiService
                .moduleUrl()
                .get({moduleId: moduleId})
                .$promise
                .then(function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(error)
                });

            return deferred.promise;
        };
        return {
            getLectureFileUrl: function (moduleId) {
                return service.getLectureFileUrl(moduleId)
            }
        };
    });
