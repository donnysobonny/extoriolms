'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.extorioErrorHandler
 * @description
 * # extorioErrorHandler
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('errorHandlerService', function ($log) {

            var errorHandler = {};
            var errorText = "EXTORIO - ";

            errorHandler.xhr = function (error) {
                if (error.error_message !== "") {
                    $log.error(errorText + error.error_message);
                }
                if (error.warning_message !== "") {
                    $log.warn(errorText + error.warning_message);
                }
                if (error.info_message !== "") {
                    $log.info(errorText + error.info_message);
                }
                if (error.success_message !== "") {
                    $log.log(errorText + error.success_message);
                }
            };

            errorHandler.internal = function (error) {
                $log.error('LMS - ' + error)
            };

            return errorHandler;
        }
    );
