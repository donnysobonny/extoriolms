'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.stateParamsService
 * @description
 * # stateParamsService
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('stateParamsService', function () {
        var service = {};

        /**
         * Creates the $stateParams Obj for UI-Router when loading up a quiz question
         * @param question
         * @param currentConfig
         * @param userModule
         * @returns {{question: *, currentConfig: *, userModule: *}}
         * @constructor
         */
        service.QuestionParams = function (question, currentConfig, userModule) {
            return {
                question: question,
                currentConfig: currentConfig,
                userModule: userModule
            };
        };

        /**
         * Creates the $stateParam obj when loading a module
         * @param module
         * @param currentConfig
         * @param userModule
         * @returns {{module: *, currentConfig: *, userModule: *}}
         * @constructor
         */
        service.ModuleParams = function (module, currentConfig, userModule) {
            return {
                module: module,
                currentConfig: currentConfig,
                userModule: userModule
            };
        };

        return {
            NewQuestion: function (question, currentConfig, userModule) {
                return service.QuestionParams(question, currentConfig, userModule);
            },
            NewModule: function (module, currentConfig, userModule) {
                return service.ModuleParams(module, currentConfig, userModule);
            }
        };
    });
