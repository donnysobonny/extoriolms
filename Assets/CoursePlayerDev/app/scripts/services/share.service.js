'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.shareService
 * @description
 * # shareService
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('shareService', function ($rootScope) {

        var data = {
            course: null,
            userCourse: null,
            module: null,
            userModule: null,
            question: null
        };

        return {
            setCourse: function (course) {
                data.course = course;
                $rootScope.$broadcast('course-set');
            },
            setUserCourse: function (userCourse) {
                data.userCourse = userCourse;
                $rootScope.$broadcast('user-course-set');
            },
            setModule: function (module) {
                data.module = module;
                $rootScope.$broadcast('module-set');
            },
            setUserModule: function (userModule) {
                data.userModule = userModule;
                $rootScope.$broadcast('user-module-set');
            },
            setQuestion: function (question) {
                data.question = question;
                $rootScope.$broadcast('question-set');
            },
            getCourse: function () {
                return data.course;
            },
            getUserCourse: function () {
                return data.userCourse;
            },
            getModule: function () {
                return data.module;
            },
            getUserModule: function () {
                return data.userModule;
            },
            getQuestion: function () {
                return data.question
            }
        };
    });
