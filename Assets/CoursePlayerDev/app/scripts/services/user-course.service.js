'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.userCourseService
 * @description
 * # userCourseService
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('userCourseService', function ($rootScope,
                                            extorioApiService,
                                            shareService,
                                            $log,
                                            toaster) {

        var service = {};

        /**
         * Gets the userCourse
         * @returns {*}
         */
        function getUserCourse() {
            return shareService.getUserCourse();
        }

        /**
         * Gets the current user module form userCourse in the $rootScope
         * @param currentLessonIndex
         * @param currentModuleIndex
         * @returns {*}
         */
        service.getUserModule = function (currentLessonIndex, currentModuleIndex) {
            var userCourse = getUserCourse();

            if (userCourse) {
                var userModule = userCourse.userLessons[currentLessonIndex].userModules[currentModuleIndex];
                if (userModule) return userModule;
                $log.error('No User Module found for current lesson/module index');
            }
        };

        /**
         * Sets the contentStatus to started for userModule
         * @param userModuleId
         * @param currentConfig
         */
        service.startUserModule = function (userModuleId, currentConfig) {
            extorioApiService
                .startUserModule()
                .save({userModuleId: userModuleId})
                .$promise
                .then(function (response) {
                    if (response.data) {
                        angular.merge(service.getUserModule(currentConfig.lessonIndex, currentConfig.moduleIndex), response.data);
                    } else {
                        $log.error("The server returned 200 but the response.data property is undefined. Unable to start this userModule as server has not returned the required data.");
                    }
                }, function () {
                    $log.error("Was not able to start the current user module")
                });
        };

        /**
         * Sets the current user module to true for tracking
         * @param userModuleId
         * @param currentConfig
         */
        service.setCurrentUserModule = function (userModuleId, currentConfig) {
            extorioApiService
                .setCurrentUserModule()
                .save({userModuleId: userModuleId})
                .$promise
                .then(function (response) {
                    if (response.data) {
                        var userCourse = shareService.getUserCourse();
                        angular.merge(userCourse.userLessons[currentConfig.lessonIndex].userModules[currentConfig.moduleIndex], response.data);
                        if ($rootScope.previousConfig) userCourse.userLessons[$rootScope.previousConfig.lessonIndex].userModules[$rootScope.previousConfig.moduleIndex].current = false;
                    } else {
                        $log.error("The server returned 200 but the response.data property is undefined. Unable to set userModule to current as server has not returned the required data.");
                    }
                }, function () {
                    $log.error("Was not able to set user module to current");
                });
        };

        /**
         * Submits a userModule for assessment
         */
        service.submitUserModule = function (userModuleId, currentConfig) {
            extorioApiService
                .submitUserModule()
                .save({userModuleId: userModuleId})
                .$promise
                .then(function (response) {
                    if (response.data) {
                        angular.merge(service.getUserModule(currentConfig.lessonIndex, currentConfig.moduleIndex), response.data);
                        $rootScope.$broadcast('load-next-state');
                    } else {
                        $log.error("The server returned 200 but the response.data property is undefined. Unable to submit userModule as server has not returned the required data.");
                    }
                }, function (error) {
                    $log.error("Was not able to submit the current user module");
                });
        };

        /**
         * Saves the current question.
         * API returns an updated user question
         * @param userQuestion
         * @param currentConfig
         */
        service.saveUserQuestion = function (userQuestion, currentConfig) {
            extorioApiService
                .userQuestion()
                .save({
                    data: userQuestion,
                    userQuestionId: userQuestion.id
                })
                .$promise
                .then(function (response) {
                    if (!response.data) {
                        $log.error("The server returned 200 but the response.data property is undefined. Unable to update userQuestion as server has not returned the required data.");
                    } else {
                        var userCourse = shareService.getUserCourse();
                        angular.merge(userCourse.userLessons[currentConfig.lessonIndex].userModules[currentConfig.moduleIndex].userQuestions[currentConfig.questionIndex], response.data);
                        toaster.pop('success', 'Question Saved');
                        $rootScope.$broadcast('load-next-state');
                    }
                }, function () {
                    $log.error('Was not able to save the current userCourse');
                });
        };

        return {
            getUserModule: function (currentLessonIndex, currentModuleIndex) {
                return service.getUserModule(currentLessonIndex, currentModuleIndex)
            },
            startUserModule: function (userModuleId, currentConfig) {
                return service.startUserModule(userModuleId, currentConfig)
            },
            setCurrentUserModule: function (userModuleId, currentConfig) {
                return service.setCurrentUserModule(userModuleId, currentConfig)
            },
            submitUserModule: function (userModuleId, currentConfig) {
                return service.submitUserModule(userModuleId, currentConfig)
            },
            saveUserQuestion: function (userQuestion, currentConfig) {
                return service.saveUserQuestion(userQuestion, currentConfig);
            }
        };
    });
