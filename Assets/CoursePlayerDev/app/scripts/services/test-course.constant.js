'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.testCourse
 * @description
 * # testCourse
 * Constant in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .constant('TEST_COURSE', {
        "ownerId": 3,
        "dateCreated": "2016-01-05 11:47:15",
        "dateUpdated": "2016-01-05 11:47:15",
        "name": "My New Course",
        "description": "<p>This is the course description.</p>",
        "category": {
            "subCategories": [],
            "name": "Main Category",
            "parentId": 0,
            "icon": "square",
            "id": 1
        },
        "subCategory": {
            "subCategories": [],
            "name": "Sub Category",
            "parentId": 1,
            "icon": "square",
            "id": 2
        },
        "difficulty": "novice",
        "tags": [
            {
                "name": "Tag One",
                "description": "This is tag one",
                "icon": "apple",
                "id": 1
            }
        ],
        "expiryMinutes": 10,
        "certificateTemplate": {
            "ownerId": 4,
            "siteDefault": true,
            "name": "default themplate",
            "description": "this is the default template",
            "body": "Well Done!",
            "id": 4
        },
        "approval": "drafting",
        "cancelledReason": "",
        "selfEnrollable": true,
        "lessons": [
            {
                "id": null,
                "name": "Section One",
                "description": "This is section one description",
                "modules": [
                    {
                        "id": null,
                        "name": "Test video lecture",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda blanditiis consectetur consequuntur error exercitationem fugit laborum minima nulla, pariatur quos ratione veniam voluptate voluptatem. Aliquam amet, architecto aspernatur consequuntur, delectus deserunt dicta dolor, dolorem eligendi eos error excepturi impedit laudantium libero magni molestias necessitatibus neque saepe suscipit veritatis vitae voluptates?",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "video",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/videos/123123123.mp4",
                        "lectureBody": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor numquam quia reiciendis repudiandae. Corporis deleniti, et explicabo laudantium quas unde? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor numquam quia reiciendis repudiandae. Corporis deleniti, et explicabo laudantium quas unde?",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Audio Lecture One",
                        "description": "This is audio lecture one description",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "audio",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/audio/The Entertainer.mp3",
                        "lectureBody": "This is audio lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Image Lecture One",
                        "description": "This is image lecture one",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "image",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/images/123123123123.png",
                        "lectureBody": "This is image lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Text Lecture One",
                        "description": "This is text lecture one description.",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "text",
                        "lectureFile": null,
                        "lectureBody": "This is tex lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Document Lecture One",
                        "description": "This is document lecture one description.",
                        "previewable": false,
                        "type": "lecture",
                        "lectureType": "document",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/documents/Projectproposal2.pdf",
                        "lectureBody": "This is document lecture one body",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Quiz One",
                        "description": "Quiz one description",
                        "previewable": true,
                        "type": "quiz",
                        "lectureType": null,
                        "lectureFile": null,
                        "lectureBody": null,
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": 3,
                        "questions": [
                            {
                                "id": null,
                                "type": "multiple-choice",
                                "questions": {
                                    "question": "Multiple choice question",
                                    "options": [
                                        "MC One",
                                        "MC Two",
                                        "MC Three"
                                    ]
                                },
                                "help": null,
                                "answer": [
                                    0
                                ],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "multiple-response",
                                "questions": {
                                    "question": "Multiple response Question",
                                    "options": [
                                        "Test1",
                                        "Test2"
                                    ]
                                },
                                "help": null,
                                "answer": [
                                    0,
                                    1
                                ],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "true-or-false",
                                "questions": {
                                    "question": "True or false question"
                                },
                                "help": null,
                                "answer": true,
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "fill-in-the-blanks",
                                "questions": {
                                    "words": [
                                        "This",
                                        "is",
                                        "some",
                                        "text",
                                        "for",
                                        "testing",
                                        "the",
                                        "fill",
                                        "in",
                                        "the",
                                        "blankService",
                                        "section."
                                    ],
                                    "blanks": [
                                        {
                                            "wordIndex": 0,
                                            "wordOptions": [
                                                "this1",
                                                "this2",
                                                "this3"
                                            ]
                                        },
                                        {
                                            "wordIndex": 10,
                                            "wordOptions": [
                                                "blanks1",
                                                "blanks2",
                                                "blanks3",
                                                "blanks4"
                                            ]
                                        },
                                        {
                                            "wordIndex": 11,
                                            "wordOptions": [
                                                "section1",
                                                "section2",
                                                "section3",
                                                "section4"
                                            ]
                                        }
                                    ]
                                },
                                "help": null,
                                "answer": [],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "multiline-text",
                                "questions": "This is a multiline text question",
                                "help": null,
                                "answer": [],
                                "maxChars": 3000,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "file-submission",
                                "questions": "File submission question",
                                "help": null,
                                "answer": [],
                                "maxChars": 2000,
                                "maxFileSize": null,
                                "acceptedFileTypes": [
                                    "application/pdf"
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "id": null,
                "name": "Section Two",
                "description": "This is section one description",
                "modules": [
                    {
                        "id": null,
                        "name": "New video lecture",
                        "description": "Video lecture description.",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "video",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/videos/123123123.mp4",
                        "lectureBody": "Video lecture body",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Audio Lecture One",
                        "description": "This is audio lecture one description",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "audio",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/audio/The Entertainer.mp3",
                        "lectureBody": "This is audio lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Image Lecture One",
                        "description": "This is image lecture one",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "image",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/images/123123123123.png",
                        "lectureBody": "This is image lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Text Lecture One",
                        "description": "This is text lecture one description.",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "text",
                        "lectureFile": null,
                        "lectureBody": "This is tex lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Document Lecture One",
                        "description": "This is document lecture one description.",
                        "previewable": false,
                        "type": "lecture",
                        "lectureType": "document",
                        "lectureFile": "https://s3-us-west-2.amazonaws.com/s.cdpn.io/149125/relativity.pdf",
                        "lectureBody": "This is document lecture one body",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Quiz One",
                        "description": "Quiz one description",
                        "previewable": true,
                        "type": "quiz",
                        "lectureType": null,
                        "lectureFile": null,
                        "lectureBody": null,
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": 3,
                        "questions": [
                            {
                                "id": null,
                                "type": "multiple-choice",
                                "questions": {
                                    "question": "Multiple choice question",
                                    "options": [
                                        "MC One",
                                        "MC Two",
                                        "MC Three"
                                    ]
                                },
                                "help": null,
                                "answer": [
                                    0
                                ],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "multiple-response",
                                "questions": {
                                    "question": "Multiple response Question",
                                    "options": [
                                        "Test1",
                                        "Test2"
                                    ]
                                },
                                "help": null,
                                "answer": [
                                    0,
                                    1
                                ],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "true-or-false",
                                "questions": {
                                    "question": "True or false question"
                                },
                                "help": null,
                                "answer": true,
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "fill-in-the-blanks",
                                "questions": {
                                    "words": [
                                        "This",
                                        "is",
                                        "some",
                                        "text",
                                        "for",
                                        "testing",
                                        "the",
                                        "fill",
                                        "in",
                                        "the",
                                        "blankService",
                                        "section."
                                    ],
                                    "blanks": [
                                        {
                                            "wordIndex": 0,
                                            "wordOptions": [
                                                "this1",
                                                "this2",
                                                "this3"
                                            ]
                                        },
                                        {
                                            "wordIndex": 10,
                                            "wordOptions": [
                                                "blanks1",
                                                "blanks2",
                                                "blanks3",
                                                "blanks4"
                                            ]
                                        },
                                        {
                                            "wordIndex": 11,
                                            "wordOptions": [
                                                "section1",
                                                "section2",
                                                "section3",
                                                "section4"
                                            ]
                                        }
                                    ]
                                },
                                "help": null,
                                "answer": [],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "multiline-text",
                                "questions": "This is a multiline text question",
                                "help": null,
                                "answer": [],
                                "maxChars": 3000,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "file-submission",
                                "questions": "File submission question",
                                "help": null,
                                "answer": [],
                                "maxChars": 2000,
                                "maxFileSize": null,
                                "acceptedFileTypes": [
                                    "application/pdf"
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "id": null,
                "name": "Section Three",
                "description": "This is section one description",
                "modules": [
                    {
                        "id": null,
                        "name": "New video lecture",
                        "description": "Video lecture description.",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "video",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/videos/123123123.mp4",
                        "lectureBody": "Video lecture body",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Audio Lecture One",
                        "description": "This is audio lecture one description",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "audio",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/audio/The Entertainer.mp3",
                        "lectureBody": "This is audio lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Image Lecture One",
                        "description": "This is image lecture one",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "image",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/images/123123123123.png",
                        "lectureBody": "This is image lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Text Lecture One",
                        "description": "This is text lecture one description.",
                        "previewable": true,
                        "type": "lecture",
                        "lectureType": "text",
                        "lectureFile": null,
                        "lectureBody": "This is tex lecture one body.",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Document Lecture One",
                        "description": "This is document lecture one description.",
                        "previewable": false,
                        "type": "lecture",
                        "lectureType": "document",
                        "lectureFile": "https://tefl-lms.s3.amazonaws.com/course-sources/0/documents/Projectproposal.pdf",
                        "lectureBody": "This is document lecture one body",
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": null,
                        "questions": {
                            "id": null,
                            "type": "lecture",
                            "questions": {},
                            "help": null,
                            "answer": [],
                            "maxChars": null,
                            "maxFileSize": null,
                            "acceptedFileTypes": []
                        }
                    },
                    {
                        "id": null,
                        "name": "Quiz One",
                        "description": "Quiz one description",
                        "previewable": true,
                        "type": "quiz",
                        "lectureType": null,
                        "lectureFile": null,
                        "lectureBody": null,
                        "maximumPoints": null,
                        "passPoints": null,
                        "maximumAttempts": 3,
                        "questions": [
                            {
                                "id": null,
                                "type": "multiple-choice",
                                "questions": {
                                    "question": "Multiple choice question",
                                    "options": [
                                        "MC One",
                                        "MC Two",
                                        "MC Three"
                                    ]
                                },
                                "help": null,
                                "answer": [
                                    0
                                ],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "multiple-response",
                                "questions": {
                                    "question": "Multiple response Question",
                                    "options": [
                                        "Test1",
                                        "Test2"
                                    ]
                                },
                                "help": null,
                                "answer": [
                                    0,
                                    1
                                ],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "true-or-false",
                                "questions": {
                                    "question": "True or false question"
                                },
                                "help": null,
                                "answer": true,
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "fill-in-the-blanks",
                                "questions": {
                                    "words": [
                                        "This",
                                        "is",
                                        "some",
                                        "text",
                                        "for",
                                        "testing",
                                        "the",
                                        "fill",
                                        "in",
                                        "the",
                                        "blankService",
                                        "section."
                                    ],
                                    "blanks": [
                                        {
                                            "wordIndex": 0,
                                            "wordOptions": [
                                                "this1",
                                                "this2",
                                                "this3"
                                            ]
                                        },
                                        {
                                            "wordIndex": 10,
                                            "wordOptions": [
                                                "blanks1",
                                                "blanks2",
                                                "blanks3",
                                                "blanks4"
                                            ]
                                        },
                                        {
                                            "wordIndex": 11,
                                            "wordOptions": [
                                                "section1",
                                                "section2",
                                                "section3",
                                                "section4"
                                            ]
                                        }
                                    ]
                                },
                                "help": null,
                                "answer": [],
                                "maxChars": null,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "multiline-text",
                                "questions": "This is a multiline text question",
                                "help": null,
                                "answer": [],
                                "maxChars": 3000,
                                "maxFileSize": null,
                                "acceptedFileTypes": []
                            },
                            {
                                "id": null,
                                "type": "file-submission",
                                "questions": "File submission question",
                                "help": null,
                                "answer": [],
                                "maxChars": 2000,
                                "maxFileSize": null,
                                "acceptedFileTypes": [
                                    "application/pdf"
                                ]
                            }
                        ]
                    }
                ]
            }
        ],
        "defaultFileId": 0,
        "defaultThumbnail": "http://placehold.it/350x350",
        "averageRating": 0,
        "totalNumEnrolled": 0,
        "maximumPoints": 0,
        "totalNumLessons": 0,
        "totalNumLectures": 0,
        "totalNumQuizzes": 0,
        "totalNumQuestions": 0,
        "id": 66
    });
