'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.helpers
 * @description
 * # helpers
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('helperService', function () {
        var service = {};


        /**
         * Sort function
         * @param a
         * @param b
         * @returns {number}
         */
        function sortNumber(a, b) {
            return a - b;
        }

        /**
         * Toggles an item in/out of an array
         * @param item
         * @param array
         */
        service.toggleArrayItem = function (item, array) {
            var index = array.indexOf(item);
            if (index === -1) array.push(item); else array.splice(index, 1);
            if (array.length > 1) array.sort(sortNumber);
        };

        /**
         * Used to determine if module is a quiz or lecture.
         * @returns {boolean}
         * @param moduleType
         */
        service.checkForQuiz = function (moduleType) {
            return moduleType === 'quiz';
        };

        /**
         * Gets all index values of item that match the key value provided
         * @param key
         * @param value
         * @param array
         * @returns {*}
         */
        service.getIndexesByPropertyValue = function (key, value, array) {

            /**
             * Reduce the array
             * @param previousValue
             * @param currentValue
             * @param currentIndex
             * @returns {UrlMatcher|*|{dist}|Array.<T>|string}
             */
            function extractIndex(previousValue, currentValue, currentIndex) {
                if (currentValue[key] === value) var nextValue = previousValue.concat(currentIndex); else nextValue = previousValue;
                return nextValue;
            }

            return array.reduce(extractIndex, []);
        };

        return {
            toggleArrayItem: function (item, array) {
                return service.toggleArrayItem(item, array);
            },
            checkForQuiz: function (moduleType) {
                return service.checkForQuiz(moduleType)
            },
            getIndexesByPropertyValue: function (key, value, array) {
                return service.getIndexesByPropertyValue(key, value, array)
            }
        };
    });
