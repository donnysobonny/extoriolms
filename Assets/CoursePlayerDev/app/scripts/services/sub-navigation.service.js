'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.subNavigationService
 * @description
 * # subNavigationService
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('subNavigationService', function ($rootScope,
                                               shareService,
                                               helperService) {

        var course = shareService.getCourse();
        var service = {};

        /**
         * Gets the next module
         * @param currentConfig
         * @returns {*}
         */
        service.getNextModule = function (currentConfig) {
            var module;
            $rootScope.previousConfig = angular.copy(currentConfig);

            /**
             * Returns true if there is a next lesson in the course
             * @param currentLessonIndex
             * @returns {boolean}
             */
            function isNextLessonDefined(currentLessonIndex) {
                var lastLessonIndex = course.lessons.length - 1;
                return currentLessonIndex !== lastLessonIndex;
            }

            if (angular.isDefined(course.lessons[currentConfig.lessonIndex].modules[currentConfig.moduleIndex + 1])) {
                currentConfig.moduleIndex++;
                module = course.lessons[currentConfig.lessonIndex].modules[currentConfig.moduleIndex];
                currentConfig.isQuiz = helperService.checkForQuiz(module.type);
            } else {
                if (isNextLessonDefined(currentConfig.lessonIndex)) {
                    currentConfig.lessonIndex++;
                    currentConfig.moduleIndex = 0;
                    module = course.lessons[currentConfig.lessonIndex].modules[currentConfig.moduleIndex];
                    currentConfig.isQuiz = helperService.checkForQuiz(module.type);

                } else {
                    console.log("end");
                }
            }
            return module;
        };

        /**
         *Gets the previous module
         * @param currentConfig
         * @return {*}
         */
        service.getPreviousModule = function (currentConfig) {
            var module;
            $rootScope.previousConfig = angular.copy(currentConfig);

            /**
             * Checks to see if there is a previous module exists
             * @param currentConfig
             * @returns {boolean}
             */
            function isPreviousModuleDefined(currentConfig) {
                return !(currentConfig.lessonIndex === 0 && currentConfig.moduleIndex === 0);
            }

            if (angular.isDefined(course.lessons[currentConfig.lessonIndex].modules[currentConfig.moduleIndex - 1])) {
                currentConfig.moduleIndex--;
                module = course.lessons[currentConfig.lessonIndex].modules[currentConfig.moduleIndex];
                currentConfig.isQuiz = helperService.checkForQuiz(module);
            } else {
                if (isPreviousModuleDefined(currentConfig)) {
                    currentConfig.lessonIndex--;
                    currentConfig.moduleIndex = course.lessons[currentConfig.lessonIndex].modules.length - 1;
                    module = course.lessons[currentConfig.lessonIndex].modules[currentConfig.moduleIndex];
                    currentConfig.isQuiz = helperService.checkForQuiz(module);
                } else {
                    console.log("start");
                }
            }
            return module;
        };

        /**
         * Gets the next question from the current quiz module
         * @returns {*}
         * @param currentConfig
         */
        service.getNextQuestion = function (currentConfig) {
            var question;
            var module = shareService.getModule();
            currentConfig.questionIndex = ++currentConfig.questionIndex;
            question = module.questions[currentConfig.questionIndex];
            return question;
        };

        /**
         * Get the previous question from the current quiz module
         * @returns {*}
         * @param currentConfig
         */
        service.getPreviousQuestion = function (currentConfig) {
            var question;
            var module = shareService.getModule();
            currentConfig.questionIndex = --currentConfig.questionIndex;
            question = module.questions[currentConfig.questionIndex];
            return question;
        };

        return {
            getNextModule: function (currentConfig) {
                return service.getNextModule(currentConfig);
            },
            getPreviousModule: function (currentConfig) {
                return service.getPreviousModule(currentConfig);
            },
            getNextQuestion: function (currentConfig) {
                return service.getNextQuestion(currentConfig);
            },
            getPreviousQuestion: function (currentConfig) {
                return service.getPreviousQuestion(currentConfig);
            }
        };
    });
