'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.session
 * @description
 * # session
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('sessionService', function () {

        var service = {};
        var _userSession;

        service.setUserSession = function (userSession) {
            _userSession = userSession;
        };

        service.getUserSession = function () {
            return _userSession;
        };

        return {
            setUserSession: function (userSession) {
                return service.setUserSession(userSession);
            },
            getUserSession: function () {
                return service.getUserSession();
            }
        };
    });
