'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.closedCaptionService
 * @description
 * # closedCaptionService
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('closedCaptionService', function ($q,
                                               $log,
                                               extorioApiService) {

        var service = {};

        /**
         * For the Chines subtitles they have the same names as the videos
         * so we just change the extensions
         * @returns {string}
         * @param subtitleFileId
         */
        service.getCaptions = function (subtitleFileId) {
            return $q(function (resolve, reject) {
                extorioApiService
                    .courseUrl()
                    .get({courseFileId: subtitleFileId})
                    .$promise
                    .then(function (response) {
                        resolve(response);
                    }, function (error) {
                        reject(error);
                    });
            });
        };

        return {
            get: function (subtitleFileId) {
                return service.getCaptions(subtitleFileId);
            }
        };
    });
