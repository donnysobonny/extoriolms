'use strict';

/**
 * @ngdoc service
 * @name coursePlayerApp.extorioApi
 * @description
 * # extorioApi
 * Factory in the coursePlayerApp.
 */
angular.module('coursePlayerApp')
    .factory('extorioApiService', function ($resource) {
        var api = {};

        var host = "/extorio/apis/";

        /**
         * Used to interact with userCourse
         * @type {{fullDetails: *}}
         */
        api.userCourse = {
            detail: $resource(host + 'lms-user-courses/:userCourseId/detail', {userCourseId: '@userCourseId'}),
            detailView: $resource(host + 'lms-user-courses/:userCourseId/detail_view', {userCourseId: '@userCourseId'}),
            byId: $resource(host + 'lms-user-courses/:userCourseId', {userCourseId: '@userCourseId'})
        };

        /**
         *Interacts with user modules
         * @type {{userModule: *, start: *, setCurrent: *, submit: *}}
         */
        api.userModules = {
            userModule: $resource(host + 'lms-user-modules/:userModuleId', {userModuleId: '@userModuleId'}),
            start: $resource(host + 'lms-user-modules/:userModuleId/start', {userModuleId: '@userModuleId'}),
            setCurrent: $resource(host + 'lms-user-modules/:userModuleId/current', {userModuleId: '@userModuleId'}),
            submit: $resource(host + 'lms-user-modules/:userModuleId/submit', {userModuleId: '@userModuleId'})
        };

        /**
         * Service for interacting with the userQuestion api
         * @type {{}}
         */
        api.userQuestion = {
            save: $resource(host + 'lms-user-questions/:userQuestionId/save', {userQuestionId: '@userQuestionId'})
        };

        /**
         * Service for interacting with the courses api
         * @type {{url: *}}
         */
        api.course = {
            url: $resource(host + '/lms-course-files/:courseFileId/url', {courseFileUrl: '@courseFileUrl'})
        };

        /**
         * Gets the signed aws url for module resources
         * @type {{url: *}}
         */
        api.modules = {
            url: $resource(host + 'lms-modules/:moduleId/url', {moduleId: '@moduleId'})
        };
        /**
         *Public methods for interacting with the api
         */
        return {
            userCourseDetail: function () {
                return api.userCourse.detail;
            },
            userCourseDetailView: function () {
                return api.userCourse.detailView;
            },
            userCourse: function () {
                return api.userCourse.byId;
            },
            userModule: function () {
                return api.userModules.userModule;
            },
            startUserModule: function () {
                return api.userModules.start;
            },
            setCurrentUserModule: function () {
                return api.userModules.setCurrent;
            },
            submitUserModule: function () {
                return api.userModules.submit;
            },
            userQuestion: function () {
                return api.userQuestion.save;
            },
            courseUrl: function(){
                return api.course.url;
            },
            moduleUrl: function () {
                return api.modules.url;
            }
        }


    });
