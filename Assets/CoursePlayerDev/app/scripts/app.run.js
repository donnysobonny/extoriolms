'use strict';

angular.module('coursePlayerApp')

    .run(function ($rootScope, shareService, sessionService, $window) {
        $rootScope.isPreviewMode = $window.isPreviewMode;
        shareService.setCourse($window.course);
        shareService.setUserCourse($window.userCourse);
        sessionService.setUserSession($window.session);
    });


