'use strict';

/**
 * @ngdoc directive
 * @name courseCreatorApp.directive:fillInTheBlanksBaseText
 * @description
 * # fillInTheBlanksBaseText
 */
angular.module('courseCreatorApp')
    .directive('fillInTheBlanksBaseText', function ($rootScope) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function postLink(scope, element, attrs, ctrl) {

                //View => Model
                ctrl.$parsers.push(function (viewValue) {
                    return viewValue.match(/[\w-']+|[^\w\s]+/g);
                });

                //Model => View
                ctrl.$formatters.push(function (modelValue) {
                    if (!modelValue) return;

                    var viewValueFlatArray = modelValue.map(function (word) {
                        var blanksCount = 0;
                        if (angular.isArray(word)) {
                            return word[$rootScope.answers[blanksCount]];
                            blanksCount++;
                        } else {
                            return word;
                        }
                    });
                    return viewValueFlatArray.join(' ').replace(/\s+([.,!":])/g, '$1');
                });

                // Override the standard $isEmpty because an empty array means the input is empty.
                ctrl.$isEmpty = function (value) {
                    return !value || !value.length;
                };

            }
        };
    });
