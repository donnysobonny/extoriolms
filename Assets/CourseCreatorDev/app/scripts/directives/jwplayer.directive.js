'use strict';

/**
 * @ngdoc directive
 * @name courseCreatorApp.directive:jwPlayer
 * @description
 * # jwPlayer
 */
angular.module('courseCreatorApp')
    .directive('jwPlayer', function (jwPlayerService, $compile, $log, $rootScope) {

        var player;
        var renderNewJWPlayerElement = function (scope, element) {
            var playerId = scope.playerId || 'lms-media-player';
            var getTemplate = function (playerId) {
                return '<div id="' + playerId + '"></div>';
            };

            element.html(getTemplate(playerId));
            $compile(element.contents())(scope);
            player = jwPlayerService.initJWPlayer(playerId);
            player.setup(scope.playerOptions);
            scope.player = player;

            player.on('ready', function () {
                $rootScope.$broadcast('lms-jwplayer-ready', {
                    playerId: playerId,
                    player: player
                });
            });
        };

        return {
            restrict: 'E',
            scope: {
                playerId: '@',
                playerOptions: '=',
                player: '='
            },
            link: function postLink(scope, element, attrs) {

                var playerId = scope.playerId || 'lms-media-player';

                scope.$on('$destroy', function () {
                    $log.debug('jwplayer onDestroy: ' + playerId);
                    jwPlayerService.cleanUpJWPlayer(playerId);
                });

                scope.$watch(function () {
                    return attrs.ngSrc;
                }, function (value) {
                    //$log.debug('ng-src(' + playerId + ') has changed: ' + value);
                    if (angular.isDefined(scope.playerOptions)) {
                        scope.playerOptions.file = value;
                        renderNewJWPlayerElement(scope, element);
                    }
                });

                if (angular.isDefined(attrs.ngSrc) && angular.isDefined(scope.playerOptions)) {
                    scope.playerOptions.file = attrs.ngSrc;
                    renderNewJWPlayerElement(scope, element);
                }
            }
        };
    });
