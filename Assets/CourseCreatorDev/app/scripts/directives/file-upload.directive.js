'use strict';

/**
 * @ngdoc directive
 * @name courseCreatorApp.directive:fileUpload
 * @description
 * # fileUpload
 */
angular.module('courseCreatorApp')
    .directive('fileUpload', function (fileUploadService,
                                       $rootScope,
                                       fileUrlService,
                                       toaster,
                                       $log) {


            //noinspection UnnecessaryLocalVariableJS
        var fileUpload = {
                scope: {
                    fileId: '=',
                    lectureFile: '=',
                    fileType: '@',
                    courseId: '@',
                    uploadType: '@',
                    saveCourse: '&saveCourse',
                    getFileUrl: '&getFileUrl',
                    showExistingSources: '&',
                    showExistingSourcesIsOpen: '='
                },
                restrict: 'E',
                link: function (scope, element, attrs) {

                    var newButton = angular.element(element.find("button")[0]);
                    var file = angular.element(element.find("input")[0]);

                    bindUiComponents(element);

                    /**
                     * Remove the lecture id and delete any
                     */
                    scope.replaceFile = function () {
                        scope.fileId = null;
                    };

                    /**
                     * Close the existing dialog
                     */
                    scope.closeExistingFiles = function () {
                        scope.showExistingSourcesIsOpen = false;
                    };

                    /**
                     * Bind events to the buttons and input
                     */
                    function bindUiComponents() {
                        newButton.bind('click', function (e) {
                            file[0].click();
                        });
                        element.bind('change', function () {
                            scope.$apply(function () {
                                getLmsCourseFileSignature();
                            });
                        });
                    }

                    /**
                     * Get the signature data from extorio
                     */
                    function getLmsCourseFileSignature() {
                        if (!file[0].files[0]) return;

                        var contentTypeMap = {
                            video: 'videos',
                            audio: 'audio',
                            image: 'images',
                            document: 'documents'

                        };

                        var bucket = 'tefl-lms';
                        var selectedFile = file[0].files[0];
                        var uploadType = scope.uploadType;
                        var fileName = selectedFile.name;
                        var contentType = selectedFile.type;
                        var lectureType = scope.fileType;
                        var fileType = contentTypeMap[lectureType] || scope.fileType;
                        var courseId = scope.courseId;
                        fileUploadService.createLmsCourseFileSignature(bucket, uploadType, fileType, fileName, contentType, courseId)
                            .then(function (response) {
                                scope.fileId = response.data.id;
                                upload(selectedFile, response.signature, response.url);
                            }, function (error) {
                                toaster.pop('error', 'Operation Failed', error.statusText);
                                $log.error('Can\'t receive the needed options for S3 upload');
                            });
                    }

                    /**
                     * Upload the selectedFile to amazon s3
                     * @param signatureData
                     * @param url
                     * @param selectedFile
                     */
                    function upload(selectedFile, signatureData, url) {
                        fileUploadService.uploadFile(signatureData, url, selectedFile, scope)
                            .then(function (xhr) {
                                scope.saveCourse({newApproval: null, successCallback: scope.getFileUrl});
                            }, function (error) {
                                $log.error('File was sent to AmazonS3 but returned an error');
                                toaster.pop('error', error);
                            });
                    }

                    /* if (scope.uploadType === 'module-sources' && scope.fileType !== 'subtitles') scope.$on('course-saved', function () {
                     scope.getFileUrl();
                     });*/
                },
                template: '<div>' +
                '<div class="panel panel-default" ng-if="uploading">' +
                '<div class="panel-body">' +
                '<div class="progress">' +
                '<div class="progress">' +
                '<div class="progress-bar" style="width: {{ progress }}%"></div>' +
                '</div>' +
                '</div>' +
                '<strong ng-if="progress">{{ progress }}%</strong>' +
                '</div>' +
                '</div>' +
                '<div class="panel panel-default">' +
                '<div class = "panel-body">' +
                '<div class = "btn-group pull-right">' +
                '<button class="btn btn-primary" type="button">' +
                '<span ng-if="!fileId">Upload file</span><span ng-if="fileId">Replace File</span>' +
                '</button>' +
                '<button ng-if="uploadType === \'module-sources\' && !showExistingSourcesIsOpen && fileType !== \'subtitles\'" class="btn btn-primary" type="button" ng-click="showExistingSources()">' +
                'Choose Existing File' +
                '</button>' +
                '<button ng-if="uploadType === \'module-sources\' && showExistingSourcesIsOpen && fileType !== \'subtitles\'" class="btn btn-primary" type="button" ng-click="closeExistingFiles()">' +
                'Close Existing Files' +
                '</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<input type="file" style="display: none"/>' +
                '</div>'
            };

            return fileUpload;
        }
    )
;
