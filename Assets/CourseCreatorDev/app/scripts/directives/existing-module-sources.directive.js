'use strict';

/**
 * @ngdoc directive
 * @name courseCreatorApp.directive:existingModuleSources
 * @description
 * # existingModuleSources
 */
angular.module('courseCreatorApp')
    .directive('existingModuleSources', function ($timeout, extorioApiService, $rootScope) {
        var directiveDefinitionObject = {
            templateUrl: '/Extensions/ExtorioLMS/Assets/CourseCreatorDev/app/views/existing-module-sources.html',
            restrict: 'E',
            scope: {
                uploadType: '@',
                fileType: '@',
                fileId: '=',
                saveCourse: '&saveCourse',
                getFileUrl: '&getFileUrl'
            },
            link: function (scope, element, attrs) {
                scope.hasLoaded = false;

                scope.updateLectureFile = function (fileId) {
                    scope.fileId = fileId;
                    $timeout(function () {
                        scope.saveCourse({newApproval: null, successCallback: scope.getFileUrl});
                    }, 10);

                };

                scope.$on('lms-jwplayer-ready', function (event, params) {
                    params.player.play();
                });

                scope.pageChanged = function () {
                    getSources(scope)
                };

                scope.search = function () {
                    getSources(scope);
                };

                function getSources(scope) {
                    scope.hasLoaded = false;

                    var data = {
                        upload_type: scope.uploadType,
                        fileType: scope.fileType,
                        skip: (scope.currentPage * scope.maxSize),
                        limit: scope.maxSize
                    };

                    if (scope.searchTerm) {
                        data.fileName = scope.searchTerm;
                    } else {
                        delete data.fileName;
                    }

                    extorioApiService
                        .courseFilesFilter()
                        .get(data)
                        .$promise
                        .then(function (response) {
                            scope.sources = response.data;
                            scope.totalItems = response.countUnlimited;
                            scope.hasLoaded = true;
                        }, function () {
                            $log.error('Was not able to get existing sources');
                        });
                }

                function init() {
                    scope.maxSize = 5;
                    scope.currentPage = 1;
                    getSources(scope);

                }

                init()
            }

        }
        return directiveDefinitionObject;
    });
