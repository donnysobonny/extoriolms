'use strict';

/**
 * @ngdoc directive
 * @name courseCreatorApp.directive:formGroupValidate
 * @description
 * # formGroupValidate
 */
angular.module('courseCreatorApp')
    .directive('formGroupValidate', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            priority: 1,
            link: function postLink(scope, element, attrs, ctrl) {

                var formGroupElement = element.closest('.form-group');
                scope.$on('show-form-errors', function () {
                    ctrl.$touched = true;
                    toggleValidationClass();
                });

                element.on('blur', toggleValidationClass);
                ctrl.$viewChangeListeners.push(function () {
                    toggleValidationClass();
                });

                function toggleValidationClass() {
                    if (ctrl.$invalid && ctrl.$touched) {
                        formGroupElement.addClass('has-error');
                    } else {
                        formGroupElement.removeClass('has-error');
                    }
                }
            }

        };
    });






