/**
 * courseCreatorApp
 */
angular
    .module('courseCreatorApp')
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

        /*  $urlRouterProvider.otherwise("/");*/

        $stateProvider
            .state('course-create', {
                url: '/:courseId',
                templateUrl: '/Extensions/ExtorioLMS/Assets/CourseCreatorDev/app/views/course.state.html',
                controller: 'CourseCtrl',
                controllerAs: 'vm'
            })
            //Hack to get it to work with extorio edit mode.
            //Each index.php has custom base tags
            .state("course-edit", {
                url: "/:courseId/edit/",
                templateUrl: "/Extensions/ExtorioLMS/Assets/CourseCreatorDev/app/views/course.state.html",
                controller: "CourseCtrl",
                controllerAs: "vm"
            });

        $locationProvider.html5Mode(true);
    });
