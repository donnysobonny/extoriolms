'use strict';

/**
 * @ngdoc service
 * @name courseCreatorApp.course
 * @description
 * # course
 * Factory in the courseCreatorV2App.
 * @type {Object|*}
 */
angular.module('courseCreatorApp')
    .factory('courseService', function () {
        var service = {};
        var globalCourse;

        /**
         * Initialises the the answers property for questions
         * @param type
         * @returns {*}
         */
        function setQuestionAnswer(type) {
            switch (type) {
                case 'multiple-choice':
                    return [0];
                    break;
                case "multiple-response":
                    return [0, 1];
                    break;
                case "true-or-false":
                    return [false];
                    break;
                default:
                    return [];
            }
        }

        /**
         * Returns a value for the module.name property.
         * Will use lecture type and default to module type
         * if lecture type is not present.
         * @param moduleType
         * @param lectureType
         * @returns {string}
         */
        function setDefaultModuleName(moduleType, lectureType) {
            return angular.isDefined(lectureType) ? 'New ' + lectureType + ' lecture' : 'New ' + moduleType;
        }

        /**
         * Helper to set the correct quiz question property
         * @returns {{}}
         * @param type
         */
        function setQuizQuestionType(type) {
            var quizQuestion;
            switch (type) {
                case "multiple-choice":
                case "multiple-response":
                    quizQuestion = setBasicQuestion();
                    break;
                case "fill-in-the-blanks":
                    quizQuestion = setFillInTheBlanksQuestion();
                    break;
                case "true-or-false":
                case "multiline-text":
                case "file-submission":
                    quizQuestion = [];
                    break;
            }
            return quizQuestion;
        }

        /**
         * Creates a new basic question object
         * @returns {{question: null, options: string[]}}
         */
        function setBasicQuestion() {
            return {
                question: null,
                options: ['Question1', 'Question2']
            };
        }

        /**
         * Creates an object for fill in the blanks question
         * @returns {{words: null}}
         */
        function setFillInTheBlanksQuestion() {
            return {
                words: null
            };
        }

        /**
         * A new course object constructor function
         * @returns {{id: null, ownerId: null, dateCreated: null, dateUpdated: null, name: string, description: null, category: number, subCategory: number, difficulty: null, approval: string, tags: number[], expiryMinutes: null, certificateTemplate: null, cancelledReason: null, selfEnrollable: null, lessons: Array, defaultFile: null, defaultThumbnail: null, averageRating: null, totalNumEnrolled: null, totalNumLessons: null, totalNumLectures: null, totalNumQuizzes: null, totalNumQuestions: null}}
         * @constructor
         */
        service.NewCourse = function () {
            return {
                id: null,
                ownerId: null,
                dateCreated: null,
                dateUpdated: null,
                name: 'My New Course',
                description: null,
                category: 1,
                subCategory: 2,
                difficulty: null,
                approval: 'drafting',
                tags: [1],
                expiryMinutes: null,
                certificateTemplate: null,
                cancelledReason: null,
                selfEnrollable: null,
                lessons: [],
                defaultFileId: null,
                defaultThumbnail: null,
                averageRating: null,
                totalNumEnrolled: null,
                totalNumLessons: null,
                totalNumLectures: null,
                totalNumQuizzes: null,
                totalNumQuestions: null

            };
        };

        /**
         *A new certificate template constructor
         * @return {{id: null, ownerId: null, siteDefault: boolean, name: string, description: null, body: null}}
         * @constructor
         */
        service.NewCertificateTemplate = function () {
            return {
                id: null,
                ownerId: null,
                siteDefault: true,
                name: 'test',
                description: null,
                body: null
            };
        };

        /**
         * A new lesson object constructor function
         * @returns {{id: null, name: string, modules: Array}}
         * @constructor
         */
        service.NewLesson = function () {
            return {
                id: null,
                name: 'My New Section',
                modules: []
            };
        };

        /**
         * New module constructor function.
         * type quiz can have many questions
         * type lecture can only have 1 question
         * @returns {{id: null, courseId: null, previewable: boolean, name: *, type: *, quizDescription: null, lectureType: (any), lectureFileId: null, lectureBody: null, maximumPoints: null, passPoints: null, maximumAttempts: null, questions: Array}}
         * @constructor
         * @param moduleType
         * @param lectureType
         */
        service.NewModule = function (moduleType, lectureType) {
            return {
                id: null,
                courseId: null,
                previewable: false,
                name: setDefaultModuleName(moduleType, lectureType),
                type: moduleType,
                quizDescription: null,
                lectureType: lectureType || null,
                lectureFileId: null,
                lectureBody: null,
                maximumPoints: null,
                passPoints: null,
                maximumAttempts: null,
                successFeedback: null,
                failFeedback: null,
                questions: [],
                subtitleFileId: null
            };
        };


        /**
         * A new question constructor function
         * @returns {{id: null, type: *, questions: *, help: null, answers: *, maxChars: null, maxFileSize: null, acceptedFileTypes: Array}}
         * @constructor
         * @param type
         */
        service.NewQuestion = function (type) {
            return {
                id: null,
                type: type,
                questions: setQuizQuestionType(type),
                help: null,
                answers: setQuestionAnswer(type),
                maxChars: null,
                maxFileSize: null,
                acceptedFileTypes: []
            };
        };

        /**
         * Createst a new certificate template object
         * @constructor
         */
        service.NewCertificateTemplate = function (ownerId) {
            return {
                id: null,
                ownerId: ownerId,
                siteDefault: false,
                name: null,
                description: null,
                body: null
            }
        };

        /**
         * Stores the course in a service so i can be retrieved from anywhere.
         * @param course
         */
        service.setCourse = function (course) {
            globalCourse = course
        };

        /**
         * Gets the global course object
         * @returns {*}
         */
        service.getCourse = function () {
            return globalCourse;
        };

        return {
            NewCourse: function () {
                return service.NewCourse();
            },
            NewLesson: function () {
                return service.NewLesson();
            },
            NewModule: function (moduleType, lectureType) {
                return service.NewModule(moduleType, lectureType);
            },
            NewQuestion: function (type) {
                return service.NewQuestion(type);
            },
            NewCertificateTemplate: function (ownerId) {
              return service.NewCertificateTemplate(ownerId)
            },
            setCourse: function (course) {
                return service.setCourse(course);
            },
            getCourse: function () {
                return service.getCourse();
            }

        };
    });
