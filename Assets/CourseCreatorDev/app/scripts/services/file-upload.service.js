'use strict';

/**
 * @ngdoc service
 * @name courseCreatorApp.fileUploadService
 * @description
 * # fileUploadService
 * Factory in the courseCreatorApp.
 */
angular.module('courseCreatorApp')
    .factory('fileUploadService', function (extorioApiService,
                                            $q) {

        var service = {};

        /**
         * Creates the lms-course-file and returns the signature
         * @returns {*}
         * @param bucket
         * @param uploadType
         * @param fileType
         * @param fileName
         * @param contentType
         * @param courseId
         */
        service.createLmsCourseFileSignature = function (bucket, uploadType, fileType, fileName, contentType, courseId) {
            var deferred = $q.defer();

            var data = {
                courseId: courseId,
                data: {
                    bucket: bucket,
                    uploadType: uploadType,
                    fileType: fileType,
                    fileName: fileName,
                    contentType: contentType
                }
            };

            extorioApiService
                .courseFileSignature()
                .save(data)
                .$promise
                .then(function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        /**
         * Uploads the file to Amazon S3 server
         * @param signatureData
         * @param url
         * @param file
         * @param uploadStatus
         * @returns {*}
         */
        service.uploadFile = function (signatureData, url, file, uploadStatus) {
            var deferred = $q.defer();
            var data = buildFormData();
            uploadStatus.uploading = true;

            var xhr = new XMLHttpRequest();
            xhr.upload.addEventListener("progress", uploadProgress, false);
            xhr.addEventListener("load", uploadComplete, false);
            xhr.addEventListener("error", uploadFailed, false);
            xhr.addEventListener("abort", uploadCanceled, false);

            /**Upload the file*/
            uploadStatus.uploading = true;
            xhr.open('POST', url, true);
            xhr.send(data);

            /**
             * Add the amazon signature and file to a new data object
             * @returns {*}
             */
            function buildFormData() {
                var data = new FormData();
                for (var key in signatureData) {
                    //noinspection JSUnfilteredForInLoop
                    data.append(key, signatureData[key]);
                }
                data.append('file', file);
                return data;
            }

            /**
             * Upload progress event handler
             * @param event
             */
            function uploadProgress(event) {
                if (uploadStatus.$apply)uploadStatus.$apply(function () {
                    setProgress();
                }); else {
                    setProgress();
                }
                function setProgress() {
                    uploadStatus.progress = Math.round(event.loaded * 100 / event.total);
                }
            }

            /**
             * Upload complete event handler
             * @param event
             */
            function uploadComplete(event) {
                var xhr = event.srcElement || event.target;
                if (uploadStatus.$apply) {
                    uploadStatus.$apply(function () {
                        setUploadComplete();
                    });
                } else {
                    setUploadComplete()
                }
                function setUploadComplete() {
                    uploadStatus.uploading = false;
                    if (xhr.status === 201 || 204) {
                        uploadStatus.success = true;
                        deferred.resolve(xhr);
                    } else {
                        uploadStatus.success = false;
                        deferred.reject(xhr);
                    }
                }
            }

            /**
             * Upload failed event handler
             * @param event
             */
            function uploadFailed(event) {
                var xhr = event.srcElement || event.target;
                if (uploadStatus.$apply) {
                    uploadStatus.$apply(function () {
                        setUploadFailed();
                    });
                } else {
                    setUploadFailed();
                }
                function setUploadFailed() {
                    uploadStatus.uploading = false;
                    uploadStatus.success = false;
                    deferred.reject(xhr);
                }
            }

            /**
             * Upload Canceled event handler
             * @param event
             */
            function uploadCanceled(event) {
                var xhr = event.srcElement || event.target;
                if (uploadStatus.$apply) {
                    uploadStatus.$apply(function () {
                        setUploadCanceled();
                    });
                } else {
                    setUploadCanceled()
                }
                function setUploadCanceled() {
                    uploadStatus.uploading = false;
                    uploadStatus.success = false;
                    deferred.reject(xhr);
                }
            }

            return deferred.promise;
        };

        service.getBucketName = function () {

        };


        return {
            createLmsCourseFileSignature: function (bucket, uploadType, fileType, fileName, contentType, courseId) {
                return service.createLmsCourseFileSignature(bucket, uploadType, fileType, fileName, contentType, courseId);
            },
            uploadFile: function (signatureData, url, file, scope) {
                return service.uploadFile(signatureData, url, file, scope);
            }
        };
    });
