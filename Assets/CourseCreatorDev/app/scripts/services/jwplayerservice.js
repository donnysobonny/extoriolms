'use strict';

/**
 * @ngdoc service
 * @name courseCreatorApp.jwPlayerService
 * @description
 * # jwPlayerService
 * Service in the courseCreatorApp.
 */
angular.module('courseCreatorApp')
    .factory('jwPlayerService', function ($window) {

        var service = {};
        var myPlayer = {};

        service.existJWPlayer = function (id) {
            return (angular.isDefined(myPlayer) && angular.isDefined(myPlayer[id]) && myPlayer[id] !== null);
        };

        service.initJWPlayer = function (id) {
            service.cleanUpJWPlayer(id);
            myPlayer[id] = $window.jwplayer(id);
            return myPlayer[id];
        };

        service.cleanUpJWPlayer = function (id) {
            if (service.existJWPlayer(id)) {
                myPlayer[id].remove();
                myPlayer[id] = null;
            }
        };

        return {
            existJWPlayer: function (id) {
                return service.existJWPlayer(id);
            },
            initJWPlayer: function (id) {
                return service.initJWPlayer(id);
            },
            cleanUpJWPlayer: function (id) {
                return service.cleanUpJWPlayer(id);
            }
        };
    });
