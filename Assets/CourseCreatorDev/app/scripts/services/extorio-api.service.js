'use strict';

/**
 * @ngdoc service
 * @name courseCreatorApp.exorioApiService
 * @description
 * # exorioApi
 * Factory in the courseCreatorApp.
 * @type {Object|*}
 */
angular.module('courseCreatorApp')
    .factory('extorioApiService', function ($resource) {

        var api = {};
        var path = "/extorio/apis";


        /**
         * Enum lookups
         * @type {{categories: *, difficulties: *, acceptedFileTypes: *}|*}
         */
        api.lookups = {
            categoriesFilter: $resource(path + '/lms-course-categories/filter'),
            categoriesTree: $resource(path + '/lms-course-categories/tree'),
            difficulties: $resource(path + '/lms-enums/content-difficulty'),
            acceptedFileTypes: $resource(path + '/lms-config/uploads'),
            icons: $resource(path + '/font-awesome-icons')
        };

        /**
         * Methods for interacting with courses apis
         * @type {{byId: *, save: *}|*}
         */
        api.course = {
            save: $resource(path + '/lms-courses/'),
            byId: $resource(path + '/lms-courses/:courseId', {courseId: '@courseId'})
        };

        /**
         * Methods for interacting with the course files apis
         * @type {{signature: *}}
         */
        api.courseFile = {
            signature: $resource(path + '/lms-course-files/'),
            filter: $resource(path + '/lms-course-files/filter'),
            url: $resource(path + '/lms-course-files/:courseFileId/url', {courseFileId: '@courseFileId'})
        };

        /**
         * Gets the signed module resource url
         * @type {{display: *}}
         */
        api.modules = {
            lectureFileUrl: $resource(path + '/lms-modules/:moduleId/url')
        };

        /**
         * Methods for interacting with the tags api
         * @type {{}}
         */
        api.tags = {
            filter: $resource(path + '/lms-course-tags/search'),
            create: $resource(path + '/lms-course-tags')
        };

        /**
         * Methods for interacting with the certificate template api
         * @type {{create: *}}
         */
        api.certificateTemplate = {
            create: $resource(path + '/lms-certificate-templates'),
            update: $resource(path + '/lms-certificate-templates/:templateId', {templateId: '@templateId'}),
            filter: $resource(path + '/lms-certificate-templates/filter'),
            siteDefault: $resource(path + '/lms-certificate-templates/site_default')
        };

        return {
            courseCategoriesFilter: function () {
                return api.lookups.categoriesFilter;
            },
            courseCategoriesTree: function () {
                return api.lookups.categoriesTree;
            },
            courseDifficulties: function () {
                return api.lookups.difficulties;
            },
            courseAcceptedFileTypes: function () {
                return api.lookups.acceptedFileTypes;
            },
            courseById: function () {
                return api.course.byId;
            },
            courseSave: function () {
                return api.course.save;
            },
            courseFileSignature: function () {
                return api.courseFile.signature;
            },
            courseFilesFilter: function () {
                return api.courseFile.filter;
            },
            courseFileUrl: function () {
                return api.courseFile.url;
            },
            lectureFileUrl: function () {
                return api.modules.lectureFileUrl;
            },
            tagsFilter: function () {
                return api.tags.filter;
            },
            tagsCreate: function () {
                return api.tags.create;
            },
            createCertificateTemplate: function () {
                return api.certificateTemplate.create;
            },
            updateCertificateTemplate: function () {
                return api.certificateTemplate.update;
            },
            filterCertificateTemplate: function () {
                return api.certificateTemplate.filter;
            },
            siteDefaultCertificateTemplate: function () {
                return api.certificateTemplate.siteDefault;
            },
            icons: function () {
                return api.lookups.icons;
            }
        };
    });
