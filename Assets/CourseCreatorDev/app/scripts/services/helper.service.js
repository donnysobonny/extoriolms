'use strict';

/**
 * @ngdoc service
 * @name courseCreatorApp.helpers
 * @description
 * # helpers
 * Factory in the courseCreatorApp.
 */
angular.module('courseCreatorApp')
    .factory('helperService', function () {
        var service = {};

        /**
         * Adds or removes and item from an array
         * @param array
         * @param item
         */
        service.toggleArrayItem = function (array, item) {
            var itemIndex = array.indexOf(item);
            if (itemIndex === -1) array.push(item); else array.splice(itemIndex, 1);
            if (array.length > 1) array.sort(service.sortNumber);
        };

        /**
         * Sort function
         * @param a
         * @param b
         * @returns {number}
         */
        service.sortNumber = function (a, b) {
            return a - b;
        };

        /**
         * Retuns an items index from the given array ot false if
         * @param array
         * @param item
         * @returns {*}
         */
        service.isInArray = function (array, item) {
            var index = array.indexOf(item);
            return index === -1 ? false : index;
        };

        /**
         * Removes an item by index from an array
         * @param array
         * @param itemIndex
         */
        service.removeItemFormArray = function (array, itemIndex) {
            array.splice(itemIndex, 1);
        };

        return {
            toggleArrayItem: function (array, item) {
                return service.toggleArrayItem(array, item)
            },
            sortNumber: function (a, b) {
                return service.sortNumber(a, b)
            },

            isInArray: function (array, item) {
                return service.isInArray(array, item);
            },
            removeItemFormArray: function (array, itemIndex) {
                return service.removeItemFormArray(array, itemIndex)
            }
        };
    });
