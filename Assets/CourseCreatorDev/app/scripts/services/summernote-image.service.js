'use strict';

/**
 * @ngdoc service
 * @name courseCreatorApp.summernoteImageService
 * @description
 * # summernoteImageService
 * Factory in the courseCreatorApp.
 */
angular.module('courseCreatorApp')
    .factory('summernoteImageService', function ($q, fileUploadService) {
        var service = {};

        service.imageUpload = function (file, courseId, uploadStatus) {
            var deferred = $q.defer();

            var bucket = 'tefl-lms';
            var uploadType = 'module-sources';
            var fileType = "images";
            var selectedFile = file[0];
            var fileName = selectedFile.name;
            var contentType = selectedFile.type;


            fileUploadService.createLmsCourseFileSignature(bucket, uploadType, fileType, fileName, contentType, courseId).then(function (response) {
                fileUploadService.uploadFile(response.signature, response.url, selectedFile, uploadStatus).then(function (xhr) {
                    var url = xhr.getResponseHeader('Location');
                    deferred.resolve(url);
                }, function (error) {
                    deferred.reject('Whoops could not upload the file to aws for summernote' + error);
                });
            }, function (error) {
                deferred.reject('Whoops could not get the aws signature options for summernote' + error);
            });
            return deferred.promise;
        };

        return {
            imageUpload: function (file, moduleId, uploadStatus) {
                return service.imageUpload(file, moduleId, uploadStatus);
            }
        };
    });
