'use strict';

/**
 * @ngdoc service
 * @name courseCreatorApp.fileUrlService
 * @description
 * # fileUrlService
 * Factory in the courseCreatorApp.
 */
angular.module('courseCreatorApp')
    .factory('fileUrlService', function (extorioApiService, $q, $log) {


        var service = {};

        /**
         * Gets the display module from the module id
         * @param moduleId
         * @param noCache
         * @return {*}
         */
        service.getLectureFileUrl = function (moduleId, noCache) {

            var data = {moduleId: moduleId};
            if (noCache) data.nocache = noCache;
            var deferred = $q.defer();
            extorioApiService
                .lectureFileUrl()
                .get(data)
                .$promise
                .then(function (response) {
                    deferred.resolve(response);
                }, function (response) {
                    deferred.reject(response);
                    $log.error('Was not able to get the lecture file url by moduleId');
                });
            return deferred.promise;
        };

        service.getCourseFileUrl = function (courseFileId) {
            var data = {courseFileId: courseFileId};
            var deferred = $q.defer();
            extorioApiService
                .courseFileUrl()
                .get(data)
                .$promise
                .then(function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(response);
                    $log.error('Api called to get course file url failed');
                });
            return deferred.promise;
        };

        return {
            getLectureFileUrl: function (moduleId, noCache) {
                return service.getLectureFileUrl(moduleId, noCache)
            },
            getCourseFileUrl: function (courseFileId) {
                return service.getCourseFileUrl(courseFileId)
            }
        };
    });