'use strict';

/**
 * @ngdoc function
 * @name courseCreatorApp.controller:ModalinstacecontrollerCtrl
 * @description
 * # ModalInstanceCtrl
 * Controller of the courseCreatorApp
 */
angular.module('courseCreatorApp')
    .controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, modalData) {

        var vm = this;

        vm.modalData = modalData;

        vm.ok = function () {
            $uibModalInstance.close(vm.modalData.return || vm.modalData);
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    });
