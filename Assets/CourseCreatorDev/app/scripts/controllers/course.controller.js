'use strict';
/**
 * @ngdoc function
 * @name courseCreatorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the courseCreatorApp
 */
angular.module('courseCreatorApp')
    .controller('CourseCtrl', function ($q,
                                        $log,
                                        $http,
                                        $state,
                                        $scope,
                                        $window,
                                        toaster,
                                        $uibModal,
                                        $rootScope,
                                        FORM_CLASS,
                                        $stateParams,
                                        helperService,
                                        courseService,
                                        $templateCache,
                                        fileUrlService,
                                        extorioApiService,
                                        SUMMERNOTE_CONFIG,
                                        summernoteImageService,
                                        SUMMERNOTE_CONFIG_IMAGE) {

            /**
             * Globals
             */
            var vm = this;
            var question;

            init();

            /**
             * Saves the course in drafting mode.
             */
            vm.saveDraft = function () {
                if (vm.course.id) {
                    saveCourseById();
                } else {
                    saveNewCourse();
                }
            };

            /**
             * Save the course in pending mode
             */
            vm.savePending = function () {
                showSavePendingConfirmation();
            };

            /**
             * Add new lesson button click
             */
            vm.addNewLesson = function () {
                vm.course.lessons.push(courseService.NewLesson());
                saveCourseById();
            };

            /**
             *Edit lesson button
             * @param index
             */
            vm.editLesson = function (index) {
                vm.lessonEditingIndex = index;
            };

            /**
             *Stop editing lesson button
             */
            vm.stopEditLesson = function () {
                vm.lessonEditingIndex = null;
            };

            /**
             * Delete the current Lesson
             * @param lessonIndex
             */
            vm.deleteLesson = function (lessonIndex) {
                deleteItem(vm.course.lessons, lessonIndex);
                vm.lessonEditingIndex = null;
            };

            /**
             * Creates a new module with the correct lecture type
             * @param lectureType
             */
            vm.addLecture = function (lectureType) {
                vm.course.lessons[vm.lessonEditingIndex].modules.push(courseService.NewModule('lecture', lectureType));
                saveCourseById();
            };

            /**
             * Adds a new quiz module
             */
            vm.addQuiz = function () {
                vm.course.lessons[vm.lessonEditingIndex].modules.push(courseService.NewModule('quiz'));
                saveCourseById();
            };

            /**
             * Edit module click event
             * @param index
             * @param module
             */
            vm.editModule = function (index, module) {
                vm.moduleEditingIndex = index;
                vm.showExistingModuleSourcesPanel = false;
                getModuleFileUrls(module);
            };

            /**
             *Stop editing module button
             */
            vm.stopEditModule = function () {
                vm.moduleEditingIndex = null;
            };

            /**
             * Delete the current Module (lecture & quiz)
             * @param moduleIndex
             */
            vm.deleteModule = function (moduleIndex) {
                deleteItem(vm.course.lessons[vm.lessonEditingIndex].modules, moduleIndex);
                vm.stopEditModule();
            };

            /**
             * Adds a new question to a quiz
             * @param type
             */
            vm.addQuestion = function (type) {
                vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex].questions.push(courseService.NewQuestion(type));
                saveCourseById();
            };

            /**
             * Used to edit a question
             * @param index
             */
            vm.editQuestion = function (index) {
                vm.questionEditingIndex = index;
                question = vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex].questions[vm.questionEditingIndex];
                $rootScope.answers = question.answers;
                if (question.type === 'fill-in-the-blanks') vm.isAddingBlanks = !!question.questions.words;
            };

            /**
             *Stops editing the current question
             */
            vm.stopEditQuestion = function () {
                vm.questionEditingIndex = null;
                $rootScope.answers = null;
                vm.selectedWordIndex = null;
            };

            /**
             *Deletes a question from quiz modules
             * @param questionIndex
             */
            vm.deleteQuestion = function (questionIndex) {
                deleteItem(vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex].questions, questionIndex);
                vm.stopEditQuestion();
            };

            /**
             * Adds a new answers for both multiple choice and multiple response
             */
            vm.addMultipleChoiceAnswer = function () {
                vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex].questions[vm.questionEditingIndex].questions.options.push(null);
            };

            /**
             * Toggles the check boxes for multiple response answers
             * @param index
             */
            vm.toggleMultipleResponseOptions = function (index) {
                var array = vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex].questions[vm.questionEditingIndex].answers;
                helperService.toggleArrayItem(array, index);
            };

            /**
             *Set
             * @param index
             */
            vm.setMultipleChoiceAnswer = function (index) {
                if (question.answers[0] !== index) {
                    question.answers = [index];
                }
            };

            /**
             *Toggles file extensions which are allowed for the file uploads question
             * @param fileExtension
             * @param acceptedFileTypesArray
             */
            vm.toggleQuestionAcceptedFileTypes = function (fileExtension, acceptedFileTypesArray) {
                helperService.toggleArrayItem(acceptedFileTypesArray, fileExtension);
            };

            /**
             * Handles the click event for the summernote file upload
             * @param file
             */
            vm.summerNoteFileUpload = function (file) {

                vm.uploadStatus = {
                    uploading: false,
                    progress: 0,
                    success: false
                };
                summernoteImageService.imageUpload(file, vm.course.id, vm.uploadStatus).then(function (response) {
                    summernoteInsertImage(response);
                }, function (error) {
                    $log.error(error);
                });
            };

            /**
             * Strip all markup and paste as plain text
             */
            vm.summernoteOnPaste = function (event) {
                var bufferText = ((event.originalEvent || event).clipboardData || window.clipboardData).getData('Text');
                event.preventDefault();
                setTimeout(function () {
                    document.execCommand('insertText', false, bufferText);
                }, 10);
            };

            /**
             *Enables the adding blank mode
             * Invert the current state of the bool
             */
            vm.addBlanks = function () {
                vm.isAddingBlanks = true;
            };

            /**
             *Converts normal word into a blank array
             * @param wordIndex
             * @param wordsArray
             */
            vm.setWordAsActiveBlank = function (wordIndex, wordsArray) {
                vm.selectedWordIndex = wordIndex;
                vm.currentAnswerIndex = getInsertIndex(wordIndex, wordsArray);
            };

            /**
             * Converts word to a word blank
             * @param wordIndex
             * @param wordsArray
             * @param answersArray
             */
            vm.addNewWordBlank = function (wordIndex, wordsArray, answersArray) {
                if (currentWordIsArray(wordsArray[wordIndex])) {
                    newBlankWord(wordsArray[wordIndex]);
                } else {
                    wordsArray[wordIndex] = [wordsArray[wordIndex]];
                    newBlankWord(wordsArray[wordIndex]);
                    addAnswer(answersArray, wordIndex, 0);
                }
                var currentAnswerIndex = getInsertIndex(wordIndex, wordsArray);
                vm.currentAnswerIndex = currentAnswerIndex;
                shuffleBlanks(wordsArray[wordIndex], currentAnswerIndex);
            };

            /**
             * Returns true when word is an array
             * @param word
             * @returns {*|boolean}
             */
            vm.isWordBlank = function (word) {
                return angular.isArray(word);
            };

            /**
             * Restores a word blank group to its original value
             * @param wordIndex
             * @param wordsArray
             */
            vm.removeWordBlank = function (wordIndex, wordsArray) {
                wordsArray[wordIndex] = wordsArray[wordIndex][question.answers[vm.currentAnswerIndex]];
                removeAnswer(wordIndex);
            };

            /**
             *Shows a modal of existing file which the user has uploaded
             */
            vm.showExistingModuleSources = function () {
                var lectureType = vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex].lectureType;
                var contentTypeMap = {video: 'videos', audio: 'audio', image: 'images', document: 'documents'};
                vm.contentType = contentTypeMap[lectureType];
                vm.uploadType = 'module-sources';
                vm.showExistingModuleSourcesPanel = true;
            };

            /**
             *Creates a new tag
             */
            vm.createTag = function (tagName, tagDescription, tagIconIndex) {
                if (!tagName || !tagDescription || tagIconIndex === null) {
                    toaster.pop('error', 'Missing Information', 'Tag needs a name, description and an icon');
                } else {
                    var newTag = {
                        name: tagName,
                        description: tagDescription,
                        icon: vm.icons[tagIconIndex]
                    };

                    extorioApiService
                        .tagsCreate()
                        .save({data: newTag})
                        .$promise
                        .then(function (response) {
                            vm.course.tags.push(response.data);
                            vm.tagName = null;
                            vm.tagDescription = null;
                            vm.tagIconIndex = null;
                            saveCourseById();
                        }, function (error) {
                            $log.error(error);
                            $log.error('Was not able to create a new tag - ' + tagName);
                        });
                }
            };

            /**
             * Searches existing tags
             * @param tagName
             * @return {*}
             */
            vm.searchTag = function (tagName) {
                return extorioApiService
                    .tagsFilter()
                    .get({phrase: tagName, limit: 15})
                    .$promise
                    .then(function (response) {
                        if (response.data.length) return response.data;
                    }, function (error) {
                        $log.error('Somthing went wrong when searching for ' + tagName);
                        $log.error(error);
                    });
            };

            /**
             * Callback after the tag is selected and
             * will add it to the course.tags array
             * @param tag
             */
            vm.selectTag = function (tag) {
                if (containsTag(tag, vm.course.tags)) {
                    toaster.pop('error', 'Tag already added', tag.name);
                } else {
                    vm.course.tags.push(tag);
                    vm.tagName = null;
                    saveCourseById();

                }
            };

            /**
             * Removes a tag from the course;
             * @param index
             */
            vm.removeTag = function (index) {
                vm.course.tags.splice(index, 1);
                saveCourseById();
            };

            /**
             *Creates a new certificate template
             */
            vm.createNewCertificateTemplate = function () {
                if (vm.course.ownerId) vm.course.certificateTemplate = courseService.NewCertificateTemplate(vm.course.ownerId);
            };

            /**
             * Saves a new certificate template
             */
            vm.saveCertificateTemplate = function (certificateTemplate) {
                extorioApiService
                    .createCertificateTemplate()
                    .save({
                        templateId: certificateTemplate.id,
                        data: certificateTemplate
                    })
                    .$promise
                    .then(function (response) {
                        vm.course.certificateTemplate = response.data;
                        saveCourseById();
                        toaster.pop('success', 'Certificate Created', 'Your course certificate has been created');
                        getCertificateTemplates();
                    });
            };

            /**
             * Updates a certificate template
             */
            vm.updateCertificateTemplate = function (certificateTemplate) {
                extorioApiService
                    .updateCertificateTemplate()
                    .save({templateId: certificateTemplate.id, data: certificateTemplate})
                    .$promise
                    .then(function (response) {
                        vm.course.certificateTemplate = response.data;
                        toaster.pop('success', 'Certificate Updated', 'Your course certificate has been updated');
                        getCertificateTemplates();
                        saveCourseById();
                    }, function (error) {
                        toaster.pop('error', 'Operation Failed', error.statusText);
                        $log.error('Updating certificate template failed :(');
                        $log.error(error);
                    });
            };

            /**
             * User the default certificate template
             */
            vm.useDefaultCertificateTemplate = function () {
                extorioApiService
                    .siteDefaultCertificateTemplate()
                    .get()
                    .$promise
                    .then(function (response) {
                        vm.course.certificateTemplate = response.data;
                        saveCourseById();
                    }, function (error) {
                        $log.error(error);
                        $log.error('Was not able to get the site default certificate template');
                    })
            };

            /**
             * Initialise the App
             */
            function init() {
                vm.lookups = {};
                vm.jwPlayer = undefined;
                vm.formClass = FORM_CLASS;
                vm.treeOptions = {accept: dragAndDropAcceptCheck, dropped: setEditingIndexOnDrop};
                vm.loggedInUserId = $rootScope.loggedInUserId;

                var courseId = getCourseIDForEditing();
                if (courseId) getCourseById(courseId); else {
                    createNewCourse();
                    saveNewCourse();
                }
                initJWPlayerOptions();
                initSummernoteOptions();
                assignFunctionsToVm();
            }


            /**
             * Check for editing state
             * @returns {*|int}
             */
            function getCourseIDForEditing() {
                if ($stateParams.courseId.length) return parseInt($stateParams.courseId);
            }

            /**
             * get lists from the api for drop down menus etc..
             */
            function getLookUps() {

                /**
                 * Api call to get course categories
                 */
                extorioApiService
                    .courseCategoriesTree()
                    .get()
                    .$promise
                    .then(function (response) {
                        vm.lookups.categories = response.data;
                        vm.course.category = vm.lookups.categories[0];
                        vm.course.subCategory = vm.lookups.categories[0].subCategories[0];
                    }, function () {
                        $log.error('Not able to get course categories tree');
                    });

                /**
                 * Api call to get course difficulties
                 */
                extorioApiService
                    .courseDifficulties()
                    .get()
                    .$promise
                    .then(function (response) {
                        vm.lookups.difficulties = response.data;
                    }, function () {
                        $log.error('Not able to get course difficulties');
                    });

                /**
                 * Api call to get accepted file types
                 */
                extorioApiService
                    .courseAcceptedFileTypes()
                    .get()
                    .$promise
                    .then(function (response) {
                        vm.lookups.acceptedFileTypes = response.data;
                    }, function () {
                        $log.error('Not able to get accepted file types');
                    });

                /**
                 * Api call to get Extorio font awesome icons
                 */
                extorioApiService
                    .icons()
                    .get()
                    .$promise
                    .then(function (response) {
                        vm.icons = response.data;
                    }, function (error) {
                        $log.error('Was not able to get icons');
                        $log.error(error);
                    });


            }

            /**
             * Gets the users existing certificate templates
             */
            function getCertificateTemplates() {
                extorioApiService
                    .filterCertificateTemplate()
                    .get()
                    .$promise
                    .then(function (response) {
                        vm.lookups.certificateTemplates = response.data;
                        setExistingCertificateTemplatesDropDownModel();
                    }, function (error) {
                        $log.error('Was not able to get existing certificate templates');
                        $log.error(error);
                    });
            }

            /**
             *Makes sure that the select's model matches the course certificate template
             */
            function setExistingCertificateTemplatesDropDownModel() {
                for (var i in vm.lookups.certificateTemplates) {
                    if (vm.lookups.certificateTemplates[i].id === vm.course.certificateTemplate.id) {
                        vm.course.certificateTemplate = vm.lookups.certificateTemplates[i];
                    }
                }
            }

            /**
             * Saves a course by id.
             * Pass in a new approval string to change the course status.
             * @param newApproval
             * @param successCallback
             */
            function saveCourseById(newApproval, successCallback) {
                if (newApproval) {
                    var oldApproval = angular.copy(vm.course.approval);
                    vm.course.approval = newApproval;
                }
                extorioApiService
                    .courseById()
                    .save({
                        courseId: vm.course.id,
                        data: vm.course
                    })
                    .$promise
                    .then(saveCourseByIdSuccess, saveCourseByIdReject);

                /**
                 *Handles a successful course save by id
                 * @param response
                 */
                function saveCourseByIdSuccess(response) {
                    if (newApproval) {
                        toaster.pop('info', 'Course Submitted For Review', 'Please wait now, one of our team will verify your course before it is approved.');
                    } else {
                        //toaster.pop('success', 'Course Saved', 'A draft has been saved of your course.');
                    }
                    vm.course = angular.merge(vm.course, response.data);
                    if (angular.isFunction(successCallback)) successCallback();
                }

                /**
                 * Handles a failed course save
                 * @param error
                 */
                function saveCourseByIdReject(error) {
                    if (newApproval)vm.course.approval = oldApproval;
                    toaster.pop('error', 'Operation Failed', error.statusText);
                }
            }

            /**
             * Create an initial version of the course so it can have an id
             * Reloads the state with this new id so it is in edit mode
             */
            function saveNewCourse() {
                extorioApiService
                    .courseSave()
                    .save({data: vm.course})
                    .$promise
                    .then(function (response) {
                        vm.course = response.data;
                        $state.go($state.current, {courseId: vm.course.id});
                        getLookUps();
                        getCertificateTemplates();
                    }, function (error) {
                        toaster.pop('error', 'Operation Failed', 'Was unable to create a new course as you are not logged in.');
                        $log.error('Was unable to save the initial course to get id.');
                    });
            }

            /**
             *Creates a new course locally
             */
            function createNewCourse() {
                vm.course = courseService.NewCourse();
            }

            /**
             * Invoked when editing a course to get a course by id
             * @param courseId
             */
            function getCourseById(courseId) {
                extorioApiService
                    .courseById()
                    .get({courseId: courseId})
                    .$promise
                    .then(function (response) {
                        vm.course = response.data;
                        getLookUps();
                        getCertificateTemplates();
                    }, function (error) {
                        toaster.pop('error', 'Operation Failed', 'Was not able to load the current course');
                        $log.error(error);
                    });
            }

            /**
             * Deletes the lesson from the lessons array
             * @param index
             * @param array
             */
            function deleteItem(array, index) {
                helperService.removeItemFormArray(array, index);
                saveCourseById();
            }

            /**
             * Check to make sure that drag and drops are within the correct section.
             * @param sourceNodeScope
             * @param destNodesScope
             * @returns {boolean}
             */
            function dragAndDropAcceptCheck(sourceNodeScope, destNodesScope) {
                var srcType = sourceNodeScope.$element.attr('data-type');
                var dstType = destNodesScope.$element.attr('data-type');
                return srcType === dstType;
            }

            /**
             * This will change the editing index when ui-tree elements are dragged and dropped
             * @param event
             */
            function setEditingIndexOnDrop(event) {
                var srcIndex = event.source.index;
                var dstIndex = event.dest.index;
                var type = angular.element(event.elements.dragging[0].firstChild).attr('data-type');
                var editingIndex = type + 'EditingIndex';
                if (vm[editingIndex] || vm[editingIndex] === 0) {
                    if (vm[editingIndex] === srcIndex) {
                        vm[editingIndex] = dstIndex;
                    } else if (!(srcIndex < vm[editingIndex] && dstIndex < vm[editingIndex]) && !(srcIndex > vm[editingIndex] && dstIndex > vm[editingIndex])) {
                        if ((srcIndex < vm[editingIndex]) && (dstIndex >= vm[editingIndex])) {
                            vm[editingIndex]--;
                        } else {
                            vm[editingIndex]++;
                        }
                    }
                }
            }

            /**
             * Get all the required files for the module being loading up
             */
            function getModuleFileUrls(module) {
                if (module.lectureType === 'video' && !module.subtitleFileId) vm.JWPlayerOptionsVideo.tracks[0].file = "";
                if (module.lectureFileId && !module.lectureFileUrl && !module.subtitleFileId) getLectureFileUrl();
                if (module.subtitleFileId && !module.subtitleFileUrl && !module.lectureFileId) getLectureAndSubtitleFileUrl();
                if (module.lectureFileId && !module.lectureFileUrl && module.subtitleFileId && !module.subtitleFileUrl) getLectureAndSubtitleFileUrl();
            }

            /**
             * Gets the course file url using with the defaultFileId
             */
            function getCourseFileUrl() {
                fileUrlService.getCourseFileUrl(vm.course.defaultFileId)
                    .then(function (response) {
                        vm.course.defaultThumbnail = response.data;
                        saveCourseById();
                    }, function (error) {
                        $log.error('Was not able to get the course file url');
                        $log.error(error);
                    });

            }

            /**
             *Get the media resource url for the current module
             * for the preview
             */
            function getLectureFileUrl() {
                var module = vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex];
                fileUrlService.getLectureFileUrl(module.id, true)
                    .then(function (response) {
                        module.lectureFileUrl = response.data;
                    }, function (error) {
                        $log.error(error);
                        $log.error('Was not able to get preview url for the current module' + error);
                    });
            }

            /**
             * Gets the subtitle file url
             */
            function getSubtitleFileUrl() {
                var module = vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex];
                fileUrlService.getCourseFileUrl(module.subtitleFileId)
                    .then(function (response) {
                        module.subtitleFileUrl = response.data;
                        extendJwPlayerOptions(module.subtitleFileUrl);
                        vm.jwPlayer.setup(vm.JWPlayerOptionsVideo);
                    }, function (error) {
                        $log.error('Was not able to get the subtitle file ');
                        $log.error(error);
                    });
            }

            /**
             *Get both file url and subtitle url for a module
             */
            function getLectureAndSubtitleFileUrl() {
                var module = vm.course.lessons[vm.lessonEditingIndex].modules[vm.moduleEditingIndex];
                $q.all([
                    fileUrlService.getLectureFileUrl(module.id, true),
                    fileUrlService.getCourseFileUrl(module.subtitleFileId)
                ]).then(function (response) {
                    module.subtitleFileUrl = response[1].data;
                    extendJwPlayerOptions(module.subtitleFileUrl);
                    module.lectureFileUrl = response[0].data;
                }, function (error) {
                    $log.error('Something in the getLectureAndSubtitleFileUrl() failed :(');
                    $log.error(error);
                });
            }

            /**
             * Add subtitle file to the jwPlayer options
             */
            function extendJwPlayerOptions(subtitleFileUrl) {
                if (subtitleFileUrl) {
                    vm.JWPlayerOptionsVideo.tracks[0].file = subtitleFileUrl;
                }
            }

            /**
             *Insert the image url into the editor
             * @param imageUrl
             */
            function summernoteInsertImage(imageUrl) {
                $rootScope.currentEditor.summernote('editor.insertImage', imageUrl);
            }

            /**
             * Assign the summer note options which are stored in the constant
             */
            function initSummernoteOptions() {
                vm.summernoteConfigImage = SUMMERNOTE_CONFIG_IMAGE;
                vm.summernoteConfig = SUMMERNOTE_CONFIG;
            }

            /**
             * When a word is selected to have blanks added this checks
             * if its already a blank array
             * @param wordsArrayItem
             * @return {*|boolean}
             */
            function currentWordIsArray(wordsArrayItem) {
                return angular.isArray(wordsArrayItem);
            }

            /**
             * Add the answer to the correct position in the array
             * @param answersArray
             * @param wordIndex
             * @param answer
             */
            function addAnswer(answersArray, wordIndex, answer) {
                answersArray.splice(getInsertIndex(wordIndex, question.questions.words), 0, answer);
            }

            /**
             *Removes an answer form the answers array
             * @param wordIndex
             */
            function removeAnswer(wordIndex) {
                question.answers.splice(getInsertIndex(wordIndex, question.questions.words), 1);
            }

            /**
             * Gets the correct index for the current words answer.
             * @param wordIndex
             * @param wordsArray
             * @return {number}
             */
            vm.getInsertIndex = getInsertIndex;
            function getInsertIndex(wordIndex, wordsArray) {
                var count = 0;
                for (var i = 0; i < wordIndex; i++) {
                    if (angular.isArray(wordsArray[i])) {
                        count++;
                    }
                }
                return count;
            }

            /**
             * Adds a new wordBlank to the current word blanks array
             * @param blanksArray
             */
            function newBlankWord(blanksArray) {
                blanksArray.push(null);
            }

            /**
             * Will randomise blanks and update the value stored in the answers
             * @param blanksArray
             * @param answerIndex
             */
            function shuffleBlanks(blanksArray, answerIndex) {
                var currentAnswer = blanksArray[question.answers[answerIndex]];
                shuffle(blanksArray);
                question.answers[answerIndex] = blanksArray.indexOf(currentAnswer);
            }

            /**
             * Shuffles an array
             * http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
             * @param array
             * @returns {*}
             */
            function shuffle(array) {
                var currentIndex = array.length, temporaryValue, randomIndex;
                while (0 !== currentIndex) {
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }
                return array;
            }

            /**
             *Shows a modal notify the user about how setting the course to pending.
             */
            function showSavePendingConfirmation() {
                var modalData;
                var modalInstance;

                modalData = {title: 'Submit ' + vm.course.name + ' for review?'};

                modalInstance = $uibModal.open({
                    templateUrl: '/Extensions/ExtorioLMS/Assets/CourseCreatorDev/app/views/submit-course-for-review.modal.html',
                    controller: 'ModalInstanceCtrl',
                    controllerAs: 'vm',
                    bindToController: true,
                    size: 'md',
                    resolve: {modalData: modalData}
                });

                modalInstance
                    .result
                    .then(modalInstanceSuccess, modalInstanceReject);
                /**
                 *Called when modal instance has been resolved
                 */
                function modalInstanceSuccess() {
                    saveCourseById('pending');
                }

                /**
                 *Called when modalInstance has been canceled
                 */
                function modalInstanceReject() {
                    $log.info('Modal dismissed');
                }
            }


            /**
             * Initialise JWPlayer configs
             */
            function initJWPlayerOptions() {
                vm.JWPlayerOptionsVideo = {
                    type: 'mp4',
                    width: '100%',
                    primary: 'html5',
                    skin: {
                        name: "vapor",
                        active: "#0dbba1"
                    },
                    tracks: [{
                        file: "",
                        label: "Chinese",
                        kind: "captions",
                        "default": true
                    }]
                };
                vm.JWPlayerOptionsAudio = {
                    type: 'mp3',
                    primary: 'html5',
                    width: '640',
                    height: 30,
                    skin: {
                        name: "vapor",
                        active: "#0dbba1"
                    }
                };
            }

            /**
             * Assign any needed functions to the view Model
             */
            function assignFunctionsToVm() {
                vm.saveCourseById = saveCourseById;
                vm.getLectureModuleLectureFileUrl = getLectureFileUrl;
                vm.getCourseFileUrl = getCourseFileUrl;
                vm.getSubtitleUrl = getSubtitleFileUrl;
            }

            /**
             * Checks to determine if tag is in the vm.course.tags array
             * @param tag
             * @param courseTags
             * @returns {boolean}
             */
            function containsTag(tag, courseTags) {
                for (var index = 0; index < courseTags.length; index++) if (courseTags[index].id === tag.id) return true;
            }
        }
    );


