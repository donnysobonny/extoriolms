'use strict';

/**
 * @ngdoc service
 * @name courseCreatorApp.summernoteConfig
 * @description
 * # SUMMERNOTE_CONFIG
 * Constant in the courseCreatorApp.
 */
/**
 * Configure all the summernote stuff in here.
 * We have a separate one for where images are allowed
 */
angular.module('courseCreatorApp')
    .constant('SUMMERNOTE_CONFIG_IMAGE', {
        height: 300,
        focus: true,
        airMode: false,
        toolbar: [
            ['edit', ['undo', 'redo']],
            ['headline', ['style']],
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ['alignment', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['picture', 'hr']],
            ['view', ['codeview']]
        ]
    })
    .constant('SUMMERNOTE_CONFIG', {
        height: 300,
        focus: true,
        airMode: false,
        toolbar: [
            ['edit', ['undo', 'redo']],
            ['headline', ['style']],
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ['alignment', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['hr']],
            ['view', ['codeview']]
        ]
    });
