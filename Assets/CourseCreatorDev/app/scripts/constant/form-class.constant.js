'use strict';

/**
 * @ngdoc service
 * @name courseCreatorV2App.formClasses
 * @description
 * # FORM_CLASS
 * Constant in the courseCreatorV2App.
 */
angular.module('courseCreatorApp')
    .constant('FORM_CLASS', {
        label: 'col-md-2',
        input: 'col-md-10',
        size: 'input-md'
    });
