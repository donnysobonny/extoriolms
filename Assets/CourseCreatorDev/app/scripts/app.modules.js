'use strict';

/**
 * @ngdoc overview
 * @name courseCreatorApp
 * @description
 * # courseCreatorApp
 *
 * Main module of the application.
 */

angular.module('courseCreatorApp', [
    'ngResource',
    'ngSanitize',
    'ui.router',
    'ui.bootstrap',
    'ui.tree',
    'summernote',
    'toaster'
]);




