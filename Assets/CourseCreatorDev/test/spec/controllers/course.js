'use strict';

describe('Controller: CourseCtrl', function () {

    // load the controller's module
    beforeEach(module('courseCreatorApp'));

    var CourseCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        CourseCtrl = $controller('CourseCtrl', {
            $scope: scope,
            $stateParams: {courseId: '1'}
        });
    }));

    it('Controller should be defined', function () {
        expect(CourseCtrl).toBeDefined();
    });

});
