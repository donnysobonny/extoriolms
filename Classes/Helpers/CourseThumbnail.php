<?php
namespace ExtorioLMS\Classes\Helpers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Models\CourseCategory;
use ExtorioLMS\Classes\Models\CourseTag;

class CourseThumbnail {
    public $id;
    public $name;
    public $description;
    /**
     * @var CourseCategory
     */
    public $category;
    /**
     * @var CourseCategory
     */
    public $subCategory;
    public $averageRating;
    public $difficulty;
    public $totalNumEnrolled;
    public $defaultThumbnail;
    /**
     * @var CourseTag[]
     */
    public $tags;

    public static function n() {
        return new CourseThumbnail();
    }

    public static function constructFromArray($array,&$tagCache=array(),&$categoryCache=array()) {
        $ct = CourseThumbnail::n();
        foreach($ct as $k => $v) {
            if(isset($array[$k])) {
                $ct->$k = $array[$k];
            }
        }

        if(isset($categoryCache[$ct->category])) {
            $ct->category = $categoryCache[$ct->category];
        } else {
            $categoryCache[$ct->category] = $ct->category = CourseCategory::findById($ct->category,1);
        }

        if(isset($categoryCache[$ct->subCategory])) {
            $ct->subCategory = $categoryCache[$ct->subCategory];
        } else {
            $categoryCache[$ct->subCategory] = $ct->subCategory = CourseCategory::findById($ct->subCategory,1);
        }

        for($i = 0; $i < count($ct->tags); $i++) {
            if(isset($tagCache[$ct->tags[$i]])) {
                $ct->tags[$i] = $tagCache[$ct->tags[$i]];
            } else {
                $tagCache[$ct->tags[$i]] = $ct->tags[$i] = CourseTag::findById($ct->tags[$i],1);
            }
        }
        return $ct;
    }

    public function __ToThumbnail($displayDescription = false) {
        $config = Extorio::get()->getConfig();
        $html = "";
        $url = "/courses/".$this->id."/";
        $description = "";
        $thumbnail = strlen($this->defaultThumbnail) ? $this->defaultThumbnail : $config["ExtorioLMS"]["courses"]["default_thumbnail_image"];
        if(strlen($this->description) && $displayDescription) {
            $description = Strings::htmlToPlain($this->description);
            $description = Strings::shorten($description,175);
        }
        $tags = "";
        if(count($this->tags)) {
            $tags = '<hr style="margin: 5px 0px;" />Tags:&nbsp;&nbsp;';
            foreach($this->tags as $tag) {
                $tags .= '<a class="label label-primary" href="/courses/tags/'.$tag->id.'/'.urlencode($tag->name).'?_pg=0"><span class="fa fa-'.$tag->icon.'"></span> '.ucwords($tag->name).'</a>&nbsp;&nbsp;';
            }
            $tags .= '<br />';
        }
        $html .= '<div class="card">
        <div class="card-header" style="background-image: url('.$thumbnail.');">
            <div class="card-header-mask">
            </div>
        </div>
        <div class="card-body">
            <div class="card-body-header">
                <h1><a href="'.$url.'">'.$this->name.'</a></h1>
                <p class="card-body-header-sentence">
                </p>
            </div>
            <p class="card-body-description">
                '.$description.'
            </p>
            <div class="card-body-footer">
                <div class="col-sm-6 textaligncenter"><strong>'.$this->totalNumEnrolled.'</strong><br />Enrolled</div>
                <div class="col-sm-6 textaligncenter"><strong>'.round(($this->averageRating/5)*100).'%</strong><br />Recommend</div>
            </div>
            ';
        
        /*
        
        <div class="card-body-footer">
            <hr style="margin: 5px 0px;" /><a class="label label-primary" href="/courses/categories/'.$this->category->id.'/'.urlencode($this->category->name).'?_pg=0"><span class="fa fa-'.$this->category->icon.'"></span> '.ucwords($this->category->name).'</a>
        &nbsp;>&nbsp;&nbsp;<a class="label label-primary" href="/courses/categories/'.$this->subCategory->id.'/'.urlencode($this->subCategory->name).'?_pg=0"><span class="fa fa-'.$this->subCategory->icon.'"></span> '.ucwords($this->subCategory->name).'</a>
            </div>
            <div class="card-body-footer">
                '.$tags.'
            </div>
            
            */
            
            $html .= '
            <div class="card-body-cta">
                <a class="btn btn-success" href="'.$url.'">View Course</a>
            </div>
        </div>
</div>';
        return $html;
    }
}