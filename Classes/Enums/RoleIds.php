<?php
namespace ExtorioLMS\Classes\Enums;
class RoleIds {
    const user_create = 1;
    const user_modify = 2;
    const user_publicdetail = 3;
    const user_internaldetail = 4;
    const user_notes = 5;
    const user_rolegroup = 6;

    const user_enrol_course_all = 7;
    const user_unenrol_course_all = 8;
    const user_enrol_course_own = 9;
    const user_unenrol_course_own = 10;
    const user_expiry_course_all = 11;
    const user_expiry_course_own = 12;

    const user_course_certs_all = 13;
    const user_course_certs_own = 14;

    const user_mark_course_all = 15;
    const user_mark_course_own = 16;

    const user_group_create = 17;
    const user_group_modify_all = 18;
    const user_group_modify_own = 19;
    const user_group_remove_all = 20;
    const user_group_remove_own = 21;
    const user_group_assign_all = 22;
    const user_group_assign_own = 23;
    const user_group_add_user_all = 24;
    const user_group_add_user_own = 25;
    const user_group_remove_user_all = 26;
    const user_group_remove_user_own = 27;

    const user_group_user_modify = 28;
    const user_group_user_publicdetail = 29;
    const user_group_user_internaldetail = 30;
    const user_group_user_notes = 31;
    const user_group_user_rolegroup = 32;

    const user_group_user_enrol_course = 33;
    const user_group_user_unenrol_course = 34;
    const user_group_user_expiry_course = 35;

    const user_group_user_course_certs = 36;

    const user_group_user_mark_course = 37;

    const course_create = 38;
    const course_modify_all = 39;
    const course_modify_own = 40;
    const course_remove_all = 41;
    const course_remove_own = 42;
    const course_approve_all = 43;
    const course_approve_own = 44;
    const course_close_all = 45;
    const course_close_own = 46;
    const course_cancel_all = 47;
    const course_cancel_own = 48;

    const course_posts_create = 49;
    const course_posts_modify_all = 50;
    const course_posts_modify_own = 51;
    const course_posts_approve_all = 52;
    const course_posts_approve_own = 53;
    const course_posts_close_all = 54;
    const course_posts_close_own = 55;
    const course_posts_cancel_all = 56;
    const course_posts_cancel_own = 57;
    const course_posts_sticky_all = 58;
    const course_posts_sticky_own = 59;

    const course_category_create = 80;
    const course_category_modify = 81;
    const course_category_remove = 82;

    const course_tag_create = 60;
    const course_tag_modify = 61;
    const course_tag_remove = 62;

    const feedback_create = 63;
    const feedback_modify_all = 64;
    const feedback_modify_own = 65;
    const feedback_approve = 66;
    const feedback_cancel = 84;

    const cert_templates_create = 67;
    const cert_templates_modify_all = 68;
    const cert_templates_modify_own = 69;
    const cert_templates_remove_all = 70;
    const cert_templates_remove_own = 71;
    const cert_templates_site_default = 72;

    const content_difficulty_add = 75;
    const content_difficulty_remove = 76;

    const action_add = 77;
    const action_modify = 78;
    const action_remove = 79;

    const display_admin_link = 83;
}