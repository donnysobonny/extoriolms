<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * course files file type
 *
 * Class CourseFilesFileType
 */
class CourseFilesFileType extends \Core\Classes\Commons\Enum {

	const _images = 'images';
	const _videos = 'videos';
	const _audio = 'audio';
	const _documents = 'documents';
	const _subtitles = 'subtitles';

    public static function values() {
        return array (
  0 => 'images',
  1 => 'videos',
  2 => 'audio',
  3 => 'documents',
  4 => 'subtitles',
);
    }
}