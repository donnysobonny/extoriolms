<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class CourseCategoryPrivileges
 */
class CourseCategoryPrivileges extends \Core\Classes\Commons\Enum {

	const _course_categories_create = 'course_categories_create';
	const _course_categories_modify = 'course_categories_modify';
	const _course_categories_delete = 'course_categories_delete';

    public static function values() {
        return array (
  0 => 'course_categories_create',
  1 => 'course_categories_modify',
  2 => 'course_categories_delete',
);
    }
}