<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class CoursePostPrivileges
 */
class CoursePostPrivileges extends \Core\Classes\Commons\Enum {

	const _course_posts_modify_all = 'course_posts_modify_all';
	const _course_posts_approve_all = 'course_posts_approve_all';
	const _course_posts_approve_own = 'course_posts_approve_own';
	const _course_posts_close_all = 'course_posts_close_all';
	const _course_posts_close_own = 'course_posts_close_own';
	const _course_posts_cancel_all = 'course_posts_cancel_all';
	const _course_posts_cancel_own = 'course_posts_cancel_own';
	const _course_posts_sticky_all = 'course_posts_sticky_all';
	const _course_posts_sticky_own = 'course_posts_sticky_own';

    public static function values() {
        return array (
  0 => 'course_posts_modify_all',
  1 => 'course_posts_approve_all',
  2 => 'course_posts_approve_own',
  3 => 'course_posts_close_all',
  4 => 'course_posts_close_own',
  5 => 'course_posts_cancel_all',
  6 => 'course_posts_cancel_own',
  7 => 'course_posts_sticky_all',
  8 => 'course_posts_sticky_own',
);
    }
}