<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * content difficulty
 *
 * Class ContentDifficulty
 */
class ContentDifficulty extends \Core\Classes\Commons\Enum {

	const _novice = 'novice';
	const _intermediate = 'intermediate';
	const _advanced = 'advanced';

    public static function values() {
        return array (
  0 => 'novice',
  1 => 'intermediate',
  2 => 'advanced',
);
    }
}