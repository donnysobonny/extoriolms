<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class CoursePrivileges
 */
class CoursePrivileges extends \Core\Classes\Commons\Enum {

	const _courses_create = 'courses_create';
	const _courses_modify_all = 'courses_modify_all';
	const _courses_remove_all = 'courses_remove_all';
	const _courses_remove_own = 'courses_remove_own';
	const _courses_approve_all = 'courses_approve_all';
	const _courses_approve_own = 'courses_approve_own';
	const _courses_close_all = 'courses_close_all';
	const _courses_close_own = 'courses_close_own';
	const _courses_cancel_all = 'courses_cancel_all';
	const _courses_cancel_own = 'courses_cancel_own';

    public static function values() {
        return array (
  0 => 'courses_create',
  1 => 'courses_modify_all',
  2 => 'courses_remove_all',
  3 => 'courses_remove_own',
  4 => 'courses_approve_all',
  5 => 'courses_approve_own',
  6 => 'courses_close_all',
  7 => 'courses_close_own',
  8 => 'courses_cancel_all',
  9 => 'courses_cancel_own',
);
    }
}