<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class CourseTagPrivileges
 */
class CourseTagPrivileges extends \Core\Classes\Commons\Enum {

	const _course_tags_create = 'course_tags_create';
	const _course_tags_modify = 'course_tags_modify';
	const _course_tags_delete = 'course_tags_delete';

    public static function values() {
        return array (
  0 => 'course_tags_create',
  1 => 'course_tags_modify',
  2 => 'course_tags_delete',
);
    }
}