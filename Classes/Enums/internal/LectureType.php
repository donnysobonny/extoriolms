<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * lecture type
 *
 * Class LectureType
 */
class LectureType extends \Core\Classes\Commons\Enum {

	const _video = 'video';
	const _audio = 'audio';
	const _image = 'image';
	const _text = 'text';
	const _document = 'document';

    public static function values() {
        return array (
  0 => 'video',
  1 => 'audio',
  2 => 'image',
  3 => 'text',
  4 => 'document',
);
    }
}