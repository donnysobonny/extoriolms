<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * course files upload type
 *
 * Class CourseFilesUploadType
 */
class CourseFilesUploadType extends \Core\Classes\Commons\Enum {

	const _courseSources = 'course-sources';
	const _moduleSources = 'module-sources';
	const _textSources = 'text-sources';
	const _postAttachments = 'post-attachments';
	const _fileSubmission = 'file-submission';

    public static function values() {
        return array (
  0 => 'course-sources',
  1 => 'module-sources',
  2 => 'text-sources',
  3 => 'post-attachments',
  4 => 'file-submission',
);
    }
}