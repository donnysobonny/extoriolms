<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class FeedbackType
 */
class FeedbackType extends \Core\Classes\Commons\Enum {

	const _course = 'course';
	const _session = 'session';

    public static function values() {
        return array (
  0 => 'course',
  1 => 'session',
);
    }
}