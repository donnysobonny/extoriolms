<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * module type
 *
 * Class ModuleType
 */
class ModuleType extends \Core\Classes\Commons\Enum {

	const _lecture = 'lecture';
	const _quiz = 'quiz';

    public static function values() {
        return array (
  0 => 'lecture',
  1 => 'quiz',
);
    }
}