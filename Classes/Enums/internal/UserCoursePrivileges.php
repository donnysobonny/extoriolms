<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class UserCoursePrivileges
 */
class UserCoursePrivileges extends \Core\Classes\Commons\Enum {

	const _user_courses_read_all = 'user_courses_read_all';
	const _user_courses_read_own = 'user_courses_read_own';
	const _user_courses_create_all = 'user_courses_create_all';
	const _user_courses_create_own = 'user_courses_create_own';
	const _user_courses_modify_all = 'user_courses_modify_all';
	const _user_courses_modify_own = 'user_courses_modify_own';
	const _user_courses_delete_all = 'user_courses_delete_all';
	const _user_courses_delete_own = 'user_courses_delete_own';

    public static function values() {
        return array (
  0 => 'user_courses_read_all',
  1 => 'user_courses_read_own',
  2 => 'user_courses_create_all',
  3 => 'user_courses_create_own',
  4 => 'user_courses_modify_all',
  5 => 'user_courses_modify_own',
  6 => 'user_courses_delete_all',
  7 => 'user_courses_delete_own',
);
    }
}