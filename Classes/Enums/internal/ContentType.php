<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * content types
 *
 * Class ContentType
 */
class ContentType extends \Core\Classes\Commons\Enum {

	const _course = 'course';

    public static function values() {
        return array (
  0 => 'course',
);
    }
}