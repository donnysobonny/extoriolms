<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class ContentCategory
 */
class ContentCategory extends \Core\Classes\Commons\Enum {

	const _uncategorized = 'uncategorized';

    public static function values() {
        return array (
  0 => 'uncategorized',
);
    }
}