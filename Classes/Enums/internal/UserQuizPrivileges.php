<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class UserQuizPrivileges
 */
class UserQuizPrivileges extends \Core\Classes\Commons\Enum {

	const _user_quizzes_read_all = 'user_quizzes_read_all';
	const _user_quizzes_read_own = 'user_quizzes_read_own';
	const _user_quizzes_modify_all = 'user_quizzes_modify_all';
	const _user_quizzes_modify_own = 'user_quizzes_modify_own';

    public static function values() {
        return array (
  0 => 'user_quizzes_read_all',
  1 => 'user_quizzes_read_own',
  2 => 'user_quizzes_modify_all',
  3 => 'user_quizzes_modify_own',
);
    }
}