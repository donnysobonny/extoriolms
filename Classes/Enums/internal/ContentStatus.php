<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * content status
 *
 * Class ContentStatus
 */
class ContentStatus extends \Core\Classes\Commons\Enum {

	const _pending = 'pending';
	const _started = 'started';
	const _submitted = 'submitted';
	const _failed = 'failed';
	const _passed = 'passed';

    public static function values() {
        return array (
  0 => 'pending',
  1 => 'started',
  2 => 'submitted',
  3 => 'failed',
  4 => 'passed',
);
    }
}