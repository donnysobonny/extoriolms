<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * question type
 *
 * Class QuestionType
 */
class QuestionType extends \Core\Classes\Commons\Enum {

	const _multipleChoice = 'multiple-choice';
	const _multipleResponse = 'multiple-response';
	const _trueOrFalse = 'true-or-false';
	const _fillInTheBlanks = 'fill-in-the-blanks';
	const _multilineText = 'multiline-text';
	const _fileSubmission = 'file-submission';

    public static function values() {
        return array (
  0 => 'multiple-choice',
  1 => 'multiple-response',
  2 => 'true-or-false',
  3 => 'fill-in-the-blanks',
  4 => 'multiline-text',
  5 => 'file-submission',
);
    }
}