<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class FeedbackPrivileges
 */
class FeedbackPrivileges extends \Core\Classes\Commons\Enum {

	const _feedback_modify_all = 'feedback_modify_all';
	const _feedback_modify_own = 'feedback_modify_own';
	const _feedback_approve_all = 'feedback_approve_all';
	const _feedback_approve_own = 'feedback_approve_own';
	const _feedback_cancel_all = 'feedback_cancel_all';
	const _feedback_cancel_own = 'feedback_cancel_own';

    public static function values() {
        return array (
  0 => 'feedback_modify_all',
  1 => 'feedback_modify_own',
  2 => 'feedback_approve_all',
  3 => 'feedback_approve_own',
  4 => 'feedback_cancel_all',
  5 => 'feedback_cancel_own',
);
    }
}