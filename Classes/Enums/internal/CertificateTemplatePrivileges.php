<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class CertificateTemplatePrivileges
 */
class CertificateTemplatePrivileges extends \Core\Classes\Commons\Enum {

	const _certificate_templates_create = 'certificate_templates_create';
	const _certificate_templates_modify = 'certificate_templates_modify';
	const _certificate_templates_delete = 'certificate_templates_delete';

    public static function values() {
        return array (
  0 => 'certificate_templates_create',
  1 => 'certificate_templates_modify',
  2 => 'certificate_templates_delete',
);
    }
}