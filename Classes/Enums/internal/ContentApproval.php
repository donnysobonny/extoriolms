<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * content approval
 *
 * Class ContentApproval
 */
class ContentApproval extends \Core\Classes\Commons\Enum {

	const _drafting = 'drafting';
	const _pending = 'pending';
	const _approved = 'approved';
	const _closed = 'closed';
	const _cancelled = 'cancelled';

    public static function values() {
        return array (
  0 => 'drafting',
  1 => 'pending',
  2 => 'approved',
  3 => 'closed',
  4 => 'cancelled',
);
    }
}