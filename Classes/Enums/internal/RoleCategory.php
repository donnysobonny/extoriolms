<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class RoleCategory
 */
class RoleCategory extends \Core\Classes\Commons\Enum {

	const _users = 'users';
	const _userEnrolment = 'user-enrolment';
	const _userCertificates = 'user-certificates';
	const _userMarking = 'user-marking';
	const _userGroups = 'user-groups';
	const _userGroupsUsers = 'user-groups-users';
	const _userGroupsUserEnrolment = 'user-groups-user-enrolment';
	const _userGroupsUserCertificates = 'user-groups-user-certificates';
	const _userGroupsUserMarking = 'user-groups-user-marking';
	const _courses = 'courses';
	const _coursePosts = 'course-posts';
	const _courseTags = 'course-tags';
	const _courseCategories = 'course-categories';
	const _feedback = 'feedback';
	const _certificateTemplates = 'certificate-templates';
	const _contentTypes = 'content-types';
	const _actions = 'actions';
	const _general = 'general';

    public static function values() {
        return array (
  0 => 'users',
  1 => 'user-enrolment',
  2 => 'user-certificates',
  3 => 'user-marking',
  4 => 'user-groups',
  5 => 'user-groups-users',
  6 => 'user-groups-user-enrolment',
  7 => 'user-groups-user-certificates',
  8 => 'user-groups-user-marking',
  9 => 'courses',
  10 => 'course-posts',
  11 => 'course-tags',
  12 => 'course-categories',
  13 => 'feedback',
  14 => 'certificate-templates',
  15 => 'content-types',
  16 => 'actions',
  17 => 'general',
);
    }
}