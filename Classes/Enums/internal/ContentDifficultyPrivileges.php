<?php
namespace ExtorioLMS\Classes\Enums;
/**
 * 
 *
 * Class ContentDifficultyPrivileges
 */
class ContentDifficultyPrivileges extends \Core\Classes\Commons\Enum {

	const _content_difficulty_create = 'content_difficulty_create';
	const _content_difficulty_delete = 'content_difficulty_delete';

    public static function values() {
        return array (
  0 => 'content_difficulty_create',
  1 => 'content_difficulty_delete',
);
    }
}