<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Helpers\Query;

/**
 * 
 *
 * Class Question
 */
class Question extends B_Question {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {

    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {

    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        //delete any user questions
        $this->response;
        $uqs = User_Question::findAll(
            Query::n()
                ->where(array(
                    "questionId" => $this->id
                )),1
        );
        foreach($uqs as $uq) {
            $uq->deleteThis();
        }
    }
}