<?php
namespace ExtorioLMS\Classes\Models;
/**
 * 
 *
 * Class CoursePostSubscriber
 */
class CoursePostSubscriber extends B_CoursePostSubscriber {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {

    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {

    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }
}