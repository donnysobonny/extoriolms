<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\ContentApproval;

/**
 * 
 *
 * Class CoursePost
 */
class CoursePost extends B_CoursePost {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();

        //always start of pending
        $this->approval = ContentApproval::_pending;

        $this->dateCreated = date("Y-m-d H:i:s");
    }

    protected function afterCreate() {
        if($this->approval == ContentApproval::_approved && $this->parentId > 0) {
            $this->updateParent();
        }
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();

        //if name or body changed, set to pending
        if($this->name != $this->_old->name || $this->body != $this->_old->body) {
            $this->approval = ContentApproval::_pending;
        }
    }

    protected function afterUpdate() {
        if($this->approval == ContentApproval::_approved && $this->_old->approval != ContentApproval::_approved) {
            if($this->parentId > 0) {
                //update parent
                $this->updateParent();
                //notify subs of parent post
                Extorio::get()->runTask_Background('\ExtorioLMS\Components\Tasks\LmsTasks',false,"send_reply_course_post_emails",array($this->id));
            } else {
                //notify course subs
                Extorio::get()->runTask_Background('\ExtorioLMS\Components\Tasks\LmsTasks',false,"send_new_course_post_emails",array($this->id));
            }
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        //remove replies
        $ps = CoursePost::findAll(
            Query::n()
                ->where(array(
                    "parentId" => $this->id
                )),1
        );
        foreach($ps as $p) {
            $p->deleteThis();
        }
        //remove post subscribers
        $cpss = CoursePostSubscriber::findAll(
            Query::n()
                ->where(array(
                    "coursePostId" => $this->id
                )),1
        );
        foreach($cpss as $cps) {
            $cps->deleteThis();
        }
    }

    private function updateLocks() {
        $this->courseId = $this->_old->courseId;
        $this->parentId = $this->_old->parentId;
        $this->userId = $this->_old->userId;
    }

    private function generalChecks() {
        if(!strlen($this->name) && $this->parentId == 0) {
            throw new \Exception("A course post must have a name/title");
        }
        if(!strlen($this->courseId)) {
            throw new \Exception("A course post must have a courseId");
        }
        if(!strlen($this->userId)) {
            throw new \Exception("A course post must have a userId");
        }

        $this->dateUpdated = date("Y-m-d H:i:s");

        //if approval is pending and auto_approved is enabeld, approve
        if($this->approval == ContentApproval::_pending) {
            $config = Extorio::get()->getConfig();
            if($config["ExtorioLMS"]["course_posts"]["auto_approve"]) {
                $this->approval = ContentApproval::_approved;
            }
        }

        $this->bodyPlain = Strings::htmlToPlain($this->body);
    }

    private function updateParent() {
        $parent = CoursePost::findById($this->parentId,1);
        if($parent) {
            $parent->dateReplied = date("Y-m-d H:i:s");
            $parent->replierId = $this->userId;
            $parent->generateTotalNumReplies();
        }
    }

    public function approve() {
        $this->approval = ContentApproval::_approved;
        $this->pushThis();
    }

    public function close() {
        $this->approval = ContentApproval::_closed;
        $this->pushThis();
    }

    public function cancel() {
        $this->approval = ContentApproval::_cancelled;
        $this->pushThis();
    }

    public function sticky() {
        if($this->parentId > 0) {
            throw new \Exception("A reply cannot be sticky");
        }
        $this->sticky = true;
        $this->pushThis();
    }

    public function unsticky() {
        $this->sticky = false;
        $this->pushThis();
    }

    public function generateTotalNumReplies() {
        $db = Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT count(id) FROM extoriolms_classes_models_coursepost WHERE "parentId" = $1 AND approval = $2';
        $row = $db->query($sql, array($this->id, ContentApproval::_approved))->fetchRow();
        if($row) {
            $this->totalNumReplies = intval($row[0]);
            $this->pushThis();
        }
    }
}