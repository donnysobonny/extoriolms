<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\Query;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentType;

/**
 * 
 *
 * Class Feedback
 */
class Feedback extends B_Feedback {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();

        //always start as pending
        $this->approval = ContentApproval::_pending;

        $this->autoApprove();
    }

    protected function afterCreate() {
        if($this->approval == ContentApproval::_approved) {
            $course = Course::findById($this->courseId,1);
            if($course) {
                $course->generateAverageRating();
            }
        }
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();

        //if feedback changed from having/not having a comment, or comment has changed, set to pending to re-approve
        if(
            (strlen($this->comment) && !strlen($this->_old->comment)) ||
            (!strlen($this->comment) && strlen($this->_old->comment)) ||
            ($this->comment != $this->_old->comment)
        ) {
            $this->approval = ContentApproval::_pending;
        }

        $this->autoApprove();
    }

    protected function afterUpdate() {
        if(
            ($this->approval != $this->_old->approval) ||
            ($this->rating != $this->_old->rating)
        ) {
            $course = Course::findById($this->courseId,1);
            if($course) {
                $course->generateAverageRating();
            }
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        $course = Course::findById($this->courseId,1);
        if($course) {
            $course->generateAverageRating();
        }
    }

    public function approve() {
        $this->approval = ContentApproval::_approved;
        $this->pushThis();
    }

    public function cancel() {
        $this->approval = ContentApproval::_cancelled;
        $this->pushThis();
    }

    private function updateLocks() {
        $this->ownerId = $this->_old->ownerId;
        $this->courseId = $this->_old->courseId;
    }

    private function generalChecks() {
        $this->dateUpdated = date("Y-m-d H:i:s");

        if($this->rating < 1 || $this->rating > 5) {
            throw new \Exception("The rating must be between 1 and 5");
        }
        if(!strlen($this->ownerId)) {
            throw new \Exception("A feedback must have an owner");
        }
        if(!strlen($this->courseId)) {
            throw new \Exception("A feedback must have a course id");
        }
        if(!in_array($this->approval, array(
            ContentApproval::_pending,
            ContentApproval::_approved,
            ContentApproval::_cancelled
        ))) {
            throw new \Exception("The approval status of feedback can be 'pending', 'approved' or 'cancelled'");
        }
        //can not have two feedbacks for the same content and user
        if(Feedback::findOne(Query::n()
            ->where(array(
                "ownerId" => $this->ownerId,
                "courseId" => $this->courseId,
                "id" => array(
                    Query::_ne => $this->id
                )
            ))
            ,1)) {
            throw new \Exception("The same user cannot submit more than one feedback for a single course");
        }
    }

    private function autoApprove() {
        //if pending, check for auto approving
        if($this->approval == ContentApproval::_pending) {
            $config = Extorio::get()->getConfig();
            //if no comment, and can be auto approved
            if(!strlen($this->comment) && $config["ExtorioLMS"]["feedback"]["auto_approve_uncommented"]) {
                $this->approval = ContentApproval::_approved;
            }
            //if comment, and can be auto approved
            elseif($config["ExtorioLMS"]["feedback"]["auto_approve_commented"]) {
                $this->approval = ContentApproval::_approved;
            }
        }
    }
}