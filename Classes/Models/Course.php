<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\Directory;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Enums\PushMode;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Enums\CourseFilesFileType;
use ExtorioLMS\Classes\Enums\LectureType;
use ExtorioLMS\Classes\Enums\ModuleItemType;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Enums\QuestionType;
use ExtorioLMS\Classes\Enums\QuizItemType;
use ExtorioLMS\Classes\Utilities\Content;
use ExtorioLMS\Classes\Utilities\Emails;
use ExtorioLMS\Classes\Utilities\Misc;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class Course
 */
class Course extends B_Course {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();

        //must start off as either pending or drafing
        if(!in_array($this->approval, array(
            ContentApproval::_drafting,
            ContentApproval::_pending
        ))) {
            $this->approval = ContentApproval::_drafting;
        }

        $this->dateCreated = date("Y-m-d H:i:s");

        $this->aggregate();
    }

    protected function afterCreate() {
        $this->generateThumbnail();
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();

        //if status is not drafting, certain elements of of the course cannot be edited, such as:
        // - the structure of the course
        // - Question.questions and Question.answers
        if($this->approval != ContentApproval::_drafting) {
            $oldIds = array();
            foreach($this->_old->lessons as $lesson) {
                $oldIds[$lesson->id] = array();
                foreach($lesson->modules as $module) {
                    $oldIds[$lesson->id][$module->id] = array();
                    foreach($module->questions as $question) {
                        $oldIds[$lesson->id][$module->id] = $question->id;
                    }
                }
            }
            $newIds = array();
            foreach($this->lessons as $lesson) {
                $newIds[$lesson->id] = array();
                foreach($lesson->modules as $module) {
                    $newIds[$lesson->id][$module->id] = array();
                    foreach($module->questions as $question) {
                        $newIds[$lesson->id][$module->id] = $question->id;
                    }
                }
            }
            if($oldIds != $newIds) {
                Extorio::get()->logCustom("compare.log","old: ".print_r($this->_old->__toStdClass(),true));
                Extorio::get()->logCustom("compare.log","new: ".print_r($this->__toStdClass(),true));

                throw new \Exception("You are not able to modify the structure of a course that is not drafting or pending approval");
            }

            $lessons = $this->lessons;
            foreach($lessons as $lesson) {
                $modules = $lesson->modules;
                foreach($modules as $module) {
                    $module->type = $module->_old->type;
                    $module->lectureType = $module->_old->lectureType;
                    if($module->type == ModuleType::_quiz) {
                        $questions = $module->questions;
                        foreach($questions as $question) {
                            $question->type = $question->_old->type;
                            $question->questions = $question->_old->questions;
                            $question->answers = $question->_old->answers;
                        }
                        $module->questions = $questions;
                    }
                }
                $lesson->modules = $modules;
            }
            $this->lessons = $lessons;
        }

        $this->aggregate();
    }

    protected function afterUpdate() {
        //any lessons that no longer exist must be deleted
        $lids = array();
        foreach($this->lessons as $lesson) {
            $lids[] = $lesson->id;
        }
        foreach($this->_old->lessons as $lesson) {
            if(!in_array($lesson->id, $lids)) {
                $lesson->deleteThis();
            }
        }

        if($this->defaultFileId != $this->_old->defaultFileId) {
            $this->generateThumbnail();
        }

        //if changed to approved
        if($this->approval == ContentApproval::_approved && $this->_old->approval != ContentApproval::_approved) {
            Emails::sendCourseApproved_owner($this);
        }
        //if changed to closed
        if($this->approval == ContentApproval::_closed && $this->_old->approval != ContentApproval::_closed) {
            Emails::sendCourseClosed_owner($this);
        }
        //if changed to cancelled
        if($this->approval == ContentApproval::_cancelled && $this->_old->approval != ContentApproval::_cancelled) {
            //send cancel emails in the background
            Extorio::get()->runTask_Background('\ExtorioLMS\Components\Tasks\LmsTasks', false, "send_course_cancellation_emails", array($this->id));
        }
    }

    protected function beforeDelete() {
        if($this->totalNumEnrolled > 0) {
            throw new \Exception("A course cannot be deletd when it has users enrolled on it.");
        }
    }

    protected function afterDelete() {
        //delete lessons
        foreach($this->lessons as $lesson) {
            $lesson->deleteThis();
        }

        //delete user courses
        $ucs = User_Course::findAll(
            Query::n()
                ->where(array(
                    "courseId" => $this->id
                )),1
        );
        foreach($ucs as $uc) {
            $uc->deleteThis();
        }
        //remove course posts
        $cps = CoursePost::findAll(
            Query::n()
                ->where(array(
                    "courseId" => $this->id
                )),1
        );
        foreach($cps as $cp) {
            $cp->deleteThis();
        }
        //remove subscribers
        $cs = CourseSubscriber::findAll(
            Query::n()
                ->where(array(
                    "courseId" => $this->id
                )),1
        );
        foreach($cs as $c) {
            $c->deleteThis();
        }
    }

    private function updateLocks() {

    }

    /**
     * @throws \Exception
     */
    private function generalChecks() {

        $this->dateUpdated = date("Y-m-d H:i:s");

        //must have a name
        if(!strlen($this->name)) {
            throw new \Exception("A course cannot exist without a name");
        }

        //cannot create/modify tags from here
        $tags = array();
        foreach($this->tags as $tag) {
            if($tag->_pushMode == PushMode::update) {
                $tag = clone($tag->_old);
                $tags[] = $tag;
            }
        }
        $this->tags = $tags;

        //if the course has no certificate template, use the site default
        if(!$this->certificateTemplate) {
            $this->certificateTemplate = CertificateTemplate::findOne(
                Query::n()->where(array(
                    "siteDefault" => true
                )),1
            );
        } else {
            if($this->certificateTemplate->_pushMode == PushMode::update && $this->certificateTemplate->id == $this->certificateTemplate->_old->id) {
                $this->certificateTemplate = clone($this->certificateTemplate->_old);
            }
        }

        foreach($this->lessons as $lesson) {
            foreach($lesson->modules as $module) {
                $module->courseId = $this->id;
                switch($module->type) {
                    case ModuleType::_lecture:
                        $module->maximumPoints = 1;
                        $module->passPoints = 1;
                        $module->maximumAttempts = 0;
                        $module->questions = array();
                        break;
                    case ModuleType::_quiz:
                        $module->maximumPoints = count($module->questions);
                        $module->previewable = false;
                        $module->lectureFileId = 0;
                        $module->lectureBody = "";
                        break;
                }
            }
        }

        if(!$this->category) {
            throw new \Exception("A course must have a category");
        }
        if(!$this->subCategory) {
            throw new \Exception("A course must have a sub category");
        } else {
            //make sure the sub category belongs to the category
            if($this->subCategory->parentId != $this->category->id) {
                throw new \Exception("The sub category must belong to the category");
            }
        }

        //if the approval is not drafting, then we should check for some minimals
        if($this->approval != ContentApproval::_drafting) {
            //if still no certificate template
            if(!$this->certificateTemplate) {
                throw new \Exception("A course must have a certificate template, and no site default template could be found.");
            }

            //course must have at least one lesson
            if(count($this->lessons) <= 0) {
                throw new \Exception("A course must have at least one lesson");
            } else {
                foreach($this->lessons as $lesson) {
                    if(count($lesson->modules) <= 0) {
                        throw new \Exception("A lesson must have at least one module");
                    }
                    foreach($lesson->modules as $module) {
                        if($module->type == ModuleType::_quiz && count($module->questions) <= 0) {
                            throw new \Exception("A quiz module must have at least one question");
                        }
                    }
                }
            }

            $config = Extorio::get()->getConfig();
            //expiry minutes more than the min
            if($this->expiryMinutes < $config["ExtorioLMS"]["courses"]["minimum_expiry_minutes"]) {
                Extorio::get()->messageWarning("The courses expiry minutes cannot be less than ".$config["ExtorioLMS"]["courses"]["minimum_expiry_minutes"].". The expiry minutes have been amended to fix this.");
                $this->expiryMinutes = $config["ExtorioLMS"]["courses"]["minimum_expiry_minutes"];
            }
            //if courses cannot be self enrollable, set it to false
            if(!$config["ExtorioLMS"]["courses"]["allow_self_enrollable"]) {
                $this->selfEnrollable = false;
            }

            //if status is pending, and auto_approve is enabled, auto approve the course
            if($this->approval == ContentApproval::_pending && $config["ExtorioLMS"]["courses"]["auto_approve"]) {
                $this->approval = ContentApproval::_approved;
            }

            $lessons = $this->lessons;
            foreach($lessons as $lesson) {
                //must have a name
                if(!strlen($lesson->name)) {
                    throw new \Exception("All lessons must have a name");
                }
                $modules = $lesson->modules;
                foreach($modules as $module) {
                    if(!strlen($module->name)) {
                        throw new \Exception("All modules must have a name");
                    }
                    //quizzes cannot be previewable
                    if($module->type == ModuleType::_quiz && $module->previewable) {
                        Extorio::get()->messageWarning("Quiz modules cannot be previewable. This has been amended");
                        $module->previewable = false;
                    }
                    //if previewable not allowed
                    if(!$config["ExtorioLMS"]["modules"]["allow_previewable"]) {
                        $module->previewable = false;
                    }
                    switch($module->type) {
                        case ModuleType::_lecture :
                            $module->maximumPoints = $module->passPoints = 1;
                            switch($module->lectureType) {
                                case LectureType::_video :
                                    if(!$config["ExtorioLMS"]["modules"]["lectures"]["allow_video"]) {
                                        throw new \Exception("Video lecture types are currently not allowed");
                                    }
                                    break;
                                case LectureType::_audio :
                                    if(!$config["ExtorioLMS"]["modules"]["lectures"]["allow_audio"]) {
                                        throw new \Exception("Audio lecture types are currently not allowed");
                                    }
                                    break;
                                case LectureType::_image :
                                    if(!$config["ExtorioLMS"]["modules"]["lectures"]["allow_images"]) {
                                        throw new \Exception("Image lecture types are currently not allowed");
                                    }
                                    break;
                                case LectureType::_text :
                                    if(!$config["ExtorioLMS"]["modules"]["lectures"]["allow_text"]) {
                                        throw new \Exception("Text lecture types are currently not allowed");
                                    }
                                    break;
                                case LectureType::_document :
                                    if(!$config["ExtorioLMS"]["modules"]["lectures"]["allow_documents"]) {
                                        throw new \Exception("Document lecture types are currently not allowed");
                                    }
                                    break;
                            }
                            $module->questions = array();
                            break;
                        case ModuleType::_quiz :
                            //check maximum attempts
                            if($module->maximumAttempts < $config["ExtorioLMS"]["modules"]["quizzes"]["minimum_maximum_attempts"]) {
                                Extorio::get()->messageWarning("The minimum maximum attempts of a quiz module is ".$config["ExtorioLMS"]["modules"]["quizzes"]["minimum_maximum_attempts"].". The maximum attempts have been amended to fix this.");
                                $module->maximumAttempts = $config["ExtorioLMS"]["modules"]["quizzes"]["minimum_maximum_attempts"];
                            }
                            //make sure the pass points is of a high enough percentage
                            $module->maximumPoints = count($module->questions);
                            $perc = round($module->maximumPoints * ($config["ExtorioLMS"]["modules"]["quizzes"]["minimum_pass_points_percentage"] / 100));
                            if($module->passPoints < $perc) {
                                Extorio::get()->messageWarning("The pass points of each module must be at least ".$config["ExtorioLMS"]["modules"]["quizzes"]["minimum_pass_points_percentage"]." percent of the maximum points. The pass points have been amended to fix this.");
                                $module->passPoints = $perc;
                            } elseif($module->passPoints > $module->maximumPoints) {
                                $module->passPoints = $module->maximumPoints;
                            }
                            $questions = $module->questions;
                            foreach($questions as $question) {
                                switch($question->type) {
                                    case QuestionType::_multipleChoice :
                                        if(!$config["ExtorioLMS"]["modules"]["quizzes"]["allow_multiple_choice"]) {
                                            throw new \Exception("Multiple choice questions are currently not allowed");
                                        }

                                        //must have 1 answer
                                        if(count($question->answers) != 1) {
                                            throw new \Exception("A multiple choice question must have one answer");
                                        }
                                        break;
                                    case QuestionType::_multipleResponse :
                                        if(!$config["ExtorioLMS"]["modules"]["quizzes"]["allow_multiple_response"]) {
                                            throw new \Exception("Multiple choice questions are currently not allowed");
                                        }

                                        //must have at least 1 answer
                                        if(count($question->answers) < 1) {
                                            throw new \Exception("A multiple response question must have at least one answer");
                                        }
                                        break;
                                    case QuestionType::_trueOrFalse :
                                        if(!$config["ExtorioLMS"]["modules"]["quizzes"]["allow_true_false"]) {
                                            throw new \Exception("Multiple choice questions are currently not allowed");
                                        }

                                        //must have one answer
                                        if(count($question->answers) != 1) {
                                            throw new \Exception("True-or-false questions must have one answer");
                                        }
                                        break;
                                    case QuestionType::_fillInTheBlanks :
                                        if(!$config["ExtorioLMS"]["modules"]["quizzes"]["allow_fill_gaps"]) {
                                            throw new \Exception("Multiple choice questions are currently not allowed");
                                        }

                                        //must have at least one answer
                                        if(count($question->answers) < 1) {
                                            throw new \Exception("Fill-in-the-blanks questions must have at least one answer");
                                        }
                                        break;
                                    case QuestionType::_multilineText :
                                        if(!$config["ExtorioLMS"]["modules"]["quizzes"]["allow_multiline_text"]) {
                                            throw new \Exception("Multiple choice questions are currently not allowed");
                                        }
                                        break;
                                    case QuestionType::_fileSubmission :
                                        if(!$config["ExtorioLMS"]["modules"]["quizzes"]["allow_file_submission"]) {
                                            throw new \Exception("Multiple choice questions are currently not allowed");
                                        }
                                        break;
                                }
                            }
                            $module->questions = $questions;
                            break;
                    }
                }
                $lesson->modules = $modules;
            }
            $this->lessons = $lessons;
        }
    }

    private function aggregate() {
        $this->maximumPoints =
            $this->totalNumLessons =
                $this->totalNumLectures =
                    $this->totalNumQuizzes =
                        $this->totalNumQuestions =
                            0;

        foreach($this->lessons as $lesson) {
            $this->totalNumLessons++;
            foreach($lesson->modules as $module) {
                switch($module->type) {
                    case ModuleType::_lecture :
                        $this->totalNumLectures++;
                        break;
                    case ModuleType::_quiz :
                        $this->totalNumQuizzes++;
                        break;
                }
                $this->maximumPoints += $module->maximumPoints;
                $this->totalNumQuestions += count($module->questions);
            }
        }
    }

    public function approve() {
        $this->approval = ContentApproval::_approved;
        $this->pushThis();
    }

    public function close() {
        $this->approval = ContentApproval::_closed;
        $this->pushThis();
    }

    public function cancel($reason) {
        $this->approval = ContentApproval::_cancelled;
        $this->cancelledReason = $reason;
        $this->pushThis();
    }

    /**
     * Create a clone of this course
     *
     * @return Course
     */
    public function cloneThis($name = null) {

        $clone = clone($this);
        $clone->id = null;
        if(strlen($name)) {
            $clone->name = $name;
        }
        $lessons = $clone->lessons;
        foreach($lessons as $lesson) {
            $lesson->id = null;
            $modules = $lesson->modules;
            foreach($modules as $module) {
                $module->id = null;
                $questions = $module->questions;
                foreach($questions as $question) {
                    $question->id = null;
                }
                $module->questions = $questions;
            }
            $lesson->modules = $modules;
        }

        $loggedInUser = Extorio::get()->getLoggedInUser();
        if($loggedInUser) {
            $clone->ownerId = $loggedInUser->id;
        }

        $clone->lessons = $lessons;
        $clone->approval = ContentApproval::_drafting;
        $clone->averageRating = $clone->totalNumEnrolled = 0;

        $clone->pushThis();

        return $clone;
    }

    public function generateThumbnail() {
        if(strlen($this->defaultFileId) > 0) {
            $courseFile = CourseFile::findById($this->defaultFileId,1);
            if($courseFile) {
                if(!Directory::exsistsFromPath("Extensions/ExtorioLMS/Assets/course-thumbnails")) {
                    Directory::createFromPath("Extensions/ExtorioLMS/Assets/course-thumbnails");
                }
                switch($courseFile->fileType) {
                    case CourseFilesFileType::_images:
                        //create a copy of the file
                        $dummy = File::constructFromPath($courseFile->fileName);
                        $copyName = "Extensions/ExtorioLMS/Assets/course-thumbnails/".$dummy->fileName()."_thumbnail.".$dummy->extension();
                        $file = File::createFromPath($copyName);
                        $file->write(file_get_contents($courseFile->privateUrl()));
                        $image = $file->__toImage();
                        //set the largest side to 448
                        $image->resizeLargestSide(448);
                        //crop to 448 by 252
                        $image->cropAtCenter(448,252);
                        //save
                        $image->save();

                        $this->defaultThumbnail = "/".$copyName;
                        $this->pushThis();
                        break;
                }
            }
        }
    }

    public function generateAverageRating() {
        $db = Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        AVG(rating)

        FROM extoriolms_classes_models_feedback

        WHERE
        "courseId" = $1 AND
        approval = $2
        ';
        $row = $db->query($sql, array($this->id, ContentApproval::_approved))->fetchRow();
        $this->averageRating = floatval($row[0]);
        $this->pushThis();
    }

    public function generateTotalNumEnrolled() {
        $db = Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        count(id)

        FROM extoriolms_classes_models_user_course

        WHERE
        "courseId" = $1
        ';
        $row = $db->query($sql, array($this->id))->fetchRow();
        $this->totalNumEnrolled = intval($row[0]);
        $this->pushThis();
    }
}