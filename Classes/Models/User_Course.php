<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\User;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Utilities\Emails;

/**
 * 
 *
 * Class User_Course
 */
class User_Course extends B_User_Course {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();

        //user can only be enrolled on course onces
        if(User_Course::findOne(
            Query::n()
                ->where(array(
                    "userId" => $this->userId,
                    "courseId" => $this->courseId
                )),1
        )) {
            throw new \Exception("Cannot create duplicate enrolments");
        }

        //always start off pending
        $this->status = ContentStatus::_pending;
    }

    protected function afterCreate() {
        $course = Course::findById($this->courseId,INF);
        if($course) {
            $course->generateTotalNumEnrolled();

            //if expiry is set on_create, set it now
            $config = Extorio::get()->getConfig();
            if($config["ExtorioLMS"]["courses"]["expiry_set_when"] == "on_create") {
                if($course->expiryMinutes > 0) {
                    $this->expiryDate = date("Y-m-d H:i:s", time() + (60*$course->expiryMinutes));
                }
            }
        }
        //send the email
        Emails::sendCourseEnrollment_user($this);
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();

        //if changed to starting
        if($this->status == ContentStatus::_started && $this->_old->status != ContentStatus::_started) {
            $this->dateStarted = date("Y-m-d H:i:s");
        }
        //if changed to passed
        if($this->status == ContentStatus::_passed && $this->_old->status != ContentStatus::_passed) {
            $this->datePassed = date("Y-m-d H:i:s");
        }
    }

    protected function afterUpdate() {
        //if gone from pending to started, generate the user modules and user questions
        if($this->status == ContentStatus::_started && $this->_old->status == ContentStatus::_pending) {
            $course = Course::findById($this->courseId,INF);
            foreach($course->lessons as $lesson) {
                foreach($lesson->modules as $module) {
                    $um = User_Module::n();
                    $um->userId = $this->userId;
                    $um->courseId = $this->courseId;
                    $um->userCourseId = $this->id;
                    $um->moduleId = $module->id;
                    $um->status = ContentStatus::_pending;
                    $um->current = false;
                    $um->currentPoints = $um->currentAttempt = 0;
                    $um->userQuestions = array();
                    if($module->type == ModuleType::_quiz) {
                        foreach($module->questions as $question) {
                            $uq = User_Question::n();
                            $uq->questionId = $question->id;
                            $uq->status = ContentStatus::_pending;
                            $um->userQuestions[] = $uq;
                        }
                    }
                    $um->pushThis();
                }
            }
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        //delete all user modules
        $ums = User_Module::findAll(
            Query::n()
                ->where(array(
                    "userCourseId" => $this->id
                )),1
        );
        foreach($ums as $um) {
            $um->deleteThis();
        }
        $course = Course::findById($this->courseId,1);
        if($course) {
            $course->generateTotalNumEnrolled();
        }
    }

    private function updateLocks() {
        $this->userId = $this->_old->userId;
        $this->courseId = $this->_old->courseId;
    }

    private function generalChecks() {
        if(!strlen($this->userId)) {
            throw new \Exception("A user course must have a userId");
        }
        if(!strlen($this->courseId)) {
            throw new \Exception("A user course must have a course id");
        }

        $this->dateUpdated = date("Y-m-d H:i:s");

        //if the status is started, and the expiry date isn't set
        $config = Extorio::get()->getConfig();
        if($this->status == ContentStatus::_started && $this->_old->status != ContentStatus::_started && !strlen($this->expiryDate) && $config["ExtorioLMS"]["courses"]["expiry_set_when"] == "on_start") {
            $course = Course::findById($this->courseId,1);
            if($course) {
                if($course->expiryMinutes > 0) {
                    $this->expiryDate = date("Y-m-d H:i:s", time() + (60*$course->expiryMinutes));
                }
            }
        }
    }

    /**
     * Start the user course
     *
     * @throws \Exception
     */
    public function start() {
        if($this->status == ContentStatus::_pending && (
                !strlen($this->expiryDate) ||
                strtotime($this->expiryDate) > time()
            )) {
            $this->status = ContentStatus::_started;
            $this->pushThis();
        } else {
            throw new \Exception("Your course enrolment cannot currently be started. Either you have already started this course, or it has expired");
        }
    }

    /**
     * Check whether this user course has passed, generated certificate and send passed email if so
     */
    public function checkForPass() {
        $ums = User_Module::findAll(
            Query::n()
                ->where(array(
                    "userCourseId" => $this->id
                )),1
        );
        $foundNotPassed = false;
        $this->currentPoints = 0;
        foreach($ums as $um) {
            if($um->status != ContentStatus::_passed) {
                $foundNotPassed = true;
            }
            $this->currentPoints += $um->currentPoints;
        }
        if(!$foundNotPassed) {
            $this->status = ContentStatus::_passed;
        }

        if($this->status == ContentStatus::_passed) {
            $this->generateCertificate();
            Emails::sendPassedCourse_user($this);
        } else {
            $this->pushThis();
        }
    }

    /**
     * Generate the course certificate for this user course
     *
     * @throws \HTML2PDF_exception
     */
    public function generateCertificate() {
        $course = Course::findById($this->courseId,2);
        if($course) {
            $dest = "Extensions/ExtorioLMS/Assets/certificates/".uniqid().".pdf";

            $mergeVars = array();
            $body = "";
            $user = User::findById($this->userId,2);
            $mergeVars["SHORTNAME"] = $user->shortname;
            $mergeVars["LONGNAME"] = $user->longname;
            $mergeVars["COURSE_NAME"] = $course->name;
            $mergeVars["COURSE_DESCRIPTION"] = $course->description;
            $body = $course->certificateTemplate->fetchBodyMerged($mergeVars);

            //create the pdf
            $pdf = new \HTML2PDF("P","A4","en");
            $pdf->writeHTML($body);
            $pdf->Output($dest,"F");

            $this->certificate = "/".$dest;
            $this->pushThis();
        }
    }
}