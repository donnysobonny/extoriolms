<?php
namespace ExtorioLMS\Classes\Models;
/**
 * 
 *
 * Class CourseSubscriber
 */
class CourseSubscriber extends B_CourseSubscriber {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {

    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {

    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }
}