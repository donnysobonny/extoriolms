<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Extorio;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Enums\QuestionType;

/**
 *
 *
 * Class User_Module
 */
class User_Module extends B_User_Module {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();

        //always start off pending
        $this->status = ContentStatus::_pending;
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {
        //if changed to passed, have the user course check for pass
        if($this->status == ContentStatus::_passed && $this->_old->status != ContentStatus::_passed) {
            $this->datePassed = date("Y-m-d H:i:s");
            $uq = User_Course::findById($this->userCourseId,1);
            if($uq) {
                $uq->checkForPass();
            }
        }
        //if changed to started, have the user course check for pass
        if($this->status == ContentStatus::_started && $this->_old->status != ContentStatus::_started) {
            $this->datePassed = null;
            $uq = User_Course::findById($this->userCourseId,1);
            if($uq) {
                $uq->checkForPass();
            }
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->userId = $this->_old->userId;
        $this->courseId = $this->_old->courseId;
        $this->userCourseId = $this->_old->userCourseId;
        $this->moduleId = $this->_old->moduleId;
    }

    private function generalChecks() {
        $this->dateUpdated = date("Y-m-d H:i:s");

        if(!strlen($this->userId)) {
            throw new \Exception("A user module must have a user id");
        }
        if(!strlen($this->courseId)) {
            throw new \Exception("A user module must have a course id");
        }
        if(!strlen($this->userCourseId)) {
            throw new \Exception("A user module must have a user course id");
        }
        if(!strlen($this->moduleId)) {
            throw new \Exception("A user module must have a module id");
        }

        foreach($this->userQuestions as $uq) {
            $uq->userId = $this->userId;
        }
    }

    public function start() {
        $module = Module::findById($this->moduleId,1);

        //module must be pending, passed or failed in order to start
        if(in_array($this->status, array(
            ContentStatus::_pending,
            ContentStatus::_passed,
            ContentStatus::_failed
        ))) {
            //make sure we have enough attempts
            $this->currentAttempts++;
            if($module->maximumAttempts == 0 || $this->currentAttempts <= $module->maximumAttempts) {
                $this->status = ContentStatus::_started;
                $this->datePassed = null;
                $this->currentPoints = 0;

                //reset all questions
                foreach($this->userQuestions as $uq) {
                    $uq->status = ContentStatus::_pending;
                    $uq->datePassed = null;
                    $uq->userAnswers = array();
                }

                $this->pushThis();
            } else {
                throw new \Exception("You have used all of your attempts for this quiz module");
            }

        } else {
            throw new \Exception("You cannot start this quiz module. Either you have already started it, or it is waiting to be marked");
        }
    }

    public function current() {
        $ums = User_Module::findAll(
            Query::n()->where(array(
                    "userId" => $this->userId,
                    "courseId" => $this->courseId,
                    "current" => true
                )),1
        );
        foreach($ums as $um) {
            $um->current = false;
            $um->pushThis();
        }
        $this->current = true;
        $this->pushThis();
    }

    public function mark() {
        $this->currentPoints = 0;
        $module = Module::findById($this->moduleId,INF);
        if($module) {
            switch($module->type) {
                case ModuleType::_lecture :

                    break;
                case ModuleType::_quiz :

                    $questions = $module->questions;
                    $userQuestions = $this->userQuestions;

                    $foundManual = false;

                    for($i = 0; $i < count($questions); $i++) {
                        if($userQuestions[$i]->status == ContentStatus::_passed) {
                            $this->currentPoints++;
                        }
                        switch($questions[$i]->type) {
                            case QuestionType::_multilineText :
                                $foundManual = $userQuestions[$i]->status == ContentStatus::_started;
                                break;
                            case QuestionType::_fileSubmission :
                                $foundManual = $userQuestions[$i]->status == ContentStatus::_started;
                                break;
                        }
                    }

                    if($foundManual) {
                        $this->status = ContentStatus::_submitted;
                    } else {
                        if($this->currentPoints >= $module->passPoints) {
                            $this->status = ContentStatus::_passed;
                        } else {
                            $this->status = ContentStatus::_failed;
                        }
                    }
                    $this->pushThis();

                    break;
            }
        }
    }

    public function submit() {
        if($this->status != ContentStatus::_started) {
            throw new \Exception("You must start a module before submitting it");
        }

        $this->currentPoints = 0;
        $module = Module::findById($this->moduleId,INF);
        if($module) {
            switch($module->type) {
                case ModuleType::_lecture :
                    $this->currentPoints = $module->maximumPoints;
                    $this->status = ContentStatus::_passed;
                    $this->pushThis();
                    break;
                case ModuleType::_quiz :
                    if($this->status != ContentStatus::_started) {
                        throw new \Exception("You must start a quiz module before submitting");
                    }

                    $questions = $module->questions;
                    $userQuestions = $this->userQuestions;

                    $foundManual = false;

                    for($i = 0; $i < count($questions); $i++) {
                        if($userQuestions[$i]->status != ContentStatus::_started) {
                            throw new \Exception("You must save all questions before submitting");
                        }

                        switch($questions[$i]->type) {
                            case QuestionType::_multipleChoice :
                                //This might be always failing as $userQuestions->userAnswers is not defined
                                if($questions[$i]->answers[0] == $userQuestions[$i]->userAnswers[0]) {
                                    $userQuestions[$i]->status = ContentStatus::_passed;
                                    $userQuestions[$i]->response = $questions[$i]->successFeedback;
                                    $userQuestions[$i]->responseById = 0;
                                    $userQuestions[$i]->responseDate = null;
                                    $this->currentPoints++;
                                } else {
                                    $userQuestions[$i]->status = ContentStatus::_failed;
                                    $userQuestions[$i]->response = $questions[$i]->failFeedback;
                                    $userQuestions[$i]->responseById = 0;
                                    $userQuestions[$i]->responseDate = null;
                                }
                                break;
                            case QuestionType::_multipleResponse :
                                if(count($questions[$i]->answers) == count($userQuestions[$i]->userAnswers)) {
                                    $userQuestions[$i]->status = ContentStatus::_passed;
                                    foreach($questions[$i]->answers as $answer) {
                                        if(!in_array($answer, $userQuestions[$i]->userAnswers)) {
                                            $userQuestions[$i]->status = ContentStatus::_failed;
                                            break;
                                        }
                                    }
                                    if($userQuestions[$i]->status == ContentStatus::_passed) {
                                        $userQuestions[$i]->response = $questions[$i]->successFeedback;
                                        $userQuestions[$i]->responseById = 0;
                                        $userQuestions[$i]->responseDate = null;

                                        $this->currentPoints++;
                                    }
                                } else {
                                    $userQuestions[$i]->status = ContentStatus::_failed;
                                    $userQuestions[$i]->response = $questions[$i]->failFeedback;
                                    $userQuestions[$i]->responseById = 0;
                                    $userQuestions[$i]->responseDate = null;
                                }
                                break;
                            case QuestionType::_trueOrFalse :
                                if($questions[$i]->answers[0] == $userQuestions[$i]->userAnswers[0]) {
                                    $userQuestions[$i]->status = ContentStatus::_passed;
                                    $userQuestions[$i]->response = $questions[$i]->successFeedback;
                                    $userQuestions[$i]->responseById = 0;
                                    $userQuestions[$i]->responseDate = null;
                                    $this->currentPoints++;
                                } else {
                                    $userQuestions[$i]->status = ContentStatus::_failed;
                                    $userQuestions[$i]->response = $questions[$i]->failFeedback;
                                    $userQuestions[$i]->responseById = 0;
                                    $userQuestions[$i]->responseDate = null;
                                }
                                break;
                            case QuestionType::_fillInTheBlanks :
                                if(count($questions[$i]->answers) == count($userQuestions[$i]->userAnswers)) {
                                    $userQuestions[$i]->status = ContentStatus::_passed;
                                    for($j = 0; $j < count($questions[$i]->answers); $j++) {
                                        if($questions[$i]->answers[$j] != $userQuestions[$i]->userAnswers[$j]) {
                                            $userQuestions[$i]->status = ContentStatus::_failed;
                                            break;
                                        }
                                    }
                                    if($userQuestions[$i]->status == ContentStatus::_passed) {
                                        $userQuestions[$i]->response = $questions[$i]->successFeedback;
                                        $userQuestions[$i]->responseById = 0;
                                        $userQuestions[$i]->responseDate = null;
                                        $this->currentPoints++;
                                    }
                                } else {
                                    $userQuestions[$i]->status = ContentStatus::_failed;
                                    $userQuestions[$i]->response = $questions[$i]->failFeedback;
                                    $userQuestions[$i]->responseById = 0;
                                    $userQuestions[$i]->responseDate = null;
                                }
                                break;
                            case QuestionType::_multilineText :
                                $foundManual = true;
                                break;
                            case QuestionType::_fileSubmission :
                                $foundManual = true;
                                break;
                        }
                    }

                    if($foundManual) {
                        $this->status = ContentStatus::_submitted;
                    } else {
                        if($this->currentPoints >= $module->passPoints) {
                            $this->status = ContentStatus::_passed;
                        } else {
                            $this->status = ContentStatus::_failed;
                        }
                    }
                    $this->pushThis();

                    break;
            }
        }
    }
}