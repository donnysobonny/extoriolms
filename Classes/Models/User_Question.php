<?php
namespace ExtorioLMS\Classes\Models;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentStatus;

/**
 * 
 *
 * Class User_Question
 */
class User_Question extends B_User_Question {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();

        $this->status = ContentStatus::_pending;

        if(strlen($this->response) > 0) {
            $this->responseDate = date("Y-m-d H:i:s");
        }
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();

        if($this->status == ContentStatus::_passed && $this->_old->status != ContentStatus::_passed) {
            $this->datePassed = date("Y-m-d H:i:s");
        }

        if(strlen($this->response) && $this->response != $this->_old->response) {
            $this->responseDate = date("Y-m-d H:i:s");
        }
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    public function save() {
        if(!in_array($this->status, array(
            ContentStatus::_pending,
            ContentStatus::_started
        ))) {
            throw new \Exception("A question must be pending or started in order to be saved");
        }
        $this->status = ContentStatus::_started;
        $this->pushThis();
    }

    private function updateLocks() {
        $this->questionId = $this->_old->questionId;
    }

    private function generalChecks() {
        $this->dateUpdated = date("Y-m-d H:i:s");
    }
}