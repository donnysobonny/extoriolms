<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Helpers\Query;
use ExtorioLMS\Classes\Enums\LectureType;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Utilities\Misc;

/**
 * 
 *
 * Class Module
 */
class Module extends B_Module {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {

    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {

    }

    protected function afterUpdate() {
        //if questions become stranded, remove them
        $qids = array();
        foreach($this->questions as $question) {
            $qids[] = $question->id;
        }
        foreach($this->_old->questions as $question) {
            if(!in_array($question->id, $qids)) {
                $question->deleteThis();
            }
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        //delete questions
        foreach($this->questions as $question) {
            $question->deleteThis();
        }
        //delete any user modules
        $ums = User_Module::findAll(
            Query::n()
                ->where(array(
                    "moduleId" => $this->id
                )),1
        );
        foreach($ums as $um) {
            $um->deleteThis();
        }
    }

    public function fetchUrl($age=1800) {
        if($this->type == ModuleType::_lecture) {
            if($this->lectureFileId > 0) {
                $cf = CourseFile::findById($this->lectureFileId,1);
                if($cf) {
                    return $cf->privateUrl($age);
                }
            }
        }
        return "";
    }

    public function fetchPreview($age=1800) {
        ob_start();
        if($this->type == ModuleType::_lecture) {
            switch($this->lectureType) {
                case LectureType::_image:
                    $cf = CourseFile::findById($this->lectureFileId,1);
                    if($cf) {
                        ?>
                        <img style="width: 100%; display: block;" src="<?=$cf->privateUrl($age)?>" alt="<?=$this->name?>" /><br />
                        <?=$this->lectureBody?>
                        <?php
                    }
                    break;
                case LectureType::_video:
                    $uid = uniqid();
                    $cf = CourseFile::findById($this->lectureFileId,1);
                    if($cf) {
                        ?>
                        <div id="<?=$uid?>">

                        </div><br />
                        <?=$this->lectureBody?>
                        <script>
                            $(function() {
                                var player = jwplayer('<?=$uid?>');
                                player.setup({
                                    file: '<?=$cf->privateUrl($age)?>',
                                    width: "100%",
                                    aspectratio: "16:9"
                                });
                            });
                        </script>
                        <?php
                    }
                    break;
                case LectureType::_audio:
                    $uid = uniqid();
                    $cf = CourseFile::findById($this->lectureFileId,1);
                    if($cf) {
                        ?>
                        <div id="<?=$uid?>">

                        </div><br />
                        <?=$this->lectureBody?>
                        <script>
                            $(function() {
                                var player = jwplayer('<?=$uid?>');
                                player.setup({
                                    file: '<?=$cf->privateUrl($age)?>',
                                    width: "100%",
                                    height: 32
                                });
                            });
                        </script>
                        <?php
                    }
                    break;
                case LectureType::_document:
                    $cf = CourseFile::findById($this->lectureFileId,1);
                    if($cf) {
                        ?>
                        <iframe frameborder="0" scrolling="auto" src="<?=$cf->privateUrl($age)?>" style="width: 100%; height: 500px;" allowfullscreen webkitallowfullscreen></iframe><br />
                        <?=$this->lectureBody?>
                        <?php
                    }
                    break;
                case LectureType::_text:
                    echo $this->lectureBody;
                    break;
            }
        }
        $content = ob_get_clean();
        return $content;
    }
}