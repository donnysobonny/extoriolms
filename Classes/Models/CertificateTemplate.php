<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Helpers\Query;

/**
 * 
 *
 * Class CertificateTemplate
 */
class CertificateTemplate extends B_CertificateTemplate {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {
        if($this->siteDefault) {
            $this->unsetOtherDefaults();
        }
    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {
        if($this->siteDefault && !$this->_old->siteDefault) {
            $this->unsetOtherDefaults();
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {
        $this->ownerId = $this->_old->ownerId;
    }

    private function generalChecks() {
        if(!strlen($this->ownerId)) {
            throw new \Exception("The ownder id must be set");
        }
        if(!strlen($this->name)) {
            throw new \Exception("A certificate template cannot exist without a name");
        }
    }

    private function unsetOtherDefaults() {
        $cts = CertificateTemplate::findAll(
            Query::n()
                ->where(array(
                    "siteDefault" => true,
                    "id" => array(Query::_ne, $this->id)
                )),1
        );
        foreach($cts as $ct) {
            $ct->siteDefault = false;
            $ct->pushThis();
        }
    }

    public function fetchBodyMerged($mergeVars=array()) {
        $body = $this->body;
        foreach($mergeVars as $k => $v) {
            $body = str_replace("**".$k."**",$v,$body);
        }
        return $body;
    }
}