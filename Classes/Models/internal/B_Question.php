<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A question
 *
 * Class B_Question
 */
class B_Question extends \Core\Classes\Commons\Model {

    
    /**
     * The type
     *
     * @var string
     */
    public $type;
    
    /**
     * The question data
     *
     * @var array
     */
    public $questions;
    
    /**
     * 
     *
     * @var string
     */
    public $failFeedback;
    
    /**
     * 
     *
     * @var string
     */
    public $successFeedback;
    
    /**
     * The answers
     *
     * @var array
     */
    public $answers;
    
    /**
     * The max chars
     *
     * @var integer
     */
    public $maxChars;
    
    /**
     * The max file size
     *
     * @var integer
     */
    public $maxFileSize;
    
    /**
     * The accepted file types
     *
     * @var array
     */
    public $acceptedFileTypes;
    

    protected static function internal_basicProperties() {
        return array (
  'failFeedback' => 
  array (
    'basicType' => 'textarea',
  ),
  'successFeedback' => 
  array (
    'basicType' => 'textarea',
  ),
  'maxChars' => 
  array (
    'basicType' => 'number',
  ),
  'maxFileSize' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'type' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\QuestionType',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
  0 => 'questions',
  1 => 'answers',
  2 => 'acceptedFileTypes',
);
    }
}