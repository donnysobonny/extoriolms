<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A module
 *
 * Class B_Module
 */
class B_Module extends \Core\Classes\Commons\Model {

    
    /**
     * The name
     *
     * @var string
     */
    public $name;
    
    /**
     * The course id
     *
     * @var integer
     */
    public $courseId;
    
    /**
     * Whether this module is previewable
     *
     * @var bool
     */
    public $previewable;
    
    /**
     * The type
     *
     * @var string
     */
    public $type;
    
    /**
     * The quiz description
     *
     * @var string
     */
    public $quizDescription;
    
    /**
     * The lecture type
     *
     * @var string
     */
    public $lectureType;
    
    /**
     * The lecture file id
     *
     * @var integer
     */
    public $lectureFileId;
    
    /**
     * The lecture body
     *
     * @var string
     */
    public $lectureBody;
    
    /**
     * The subtitle file id
     *
     * @var integer
     */
    public $subtitleFileId;
    
    /**
     * The maximum points
     *
     * @var integer
     */
    public $maximumPoints;
    
    /**
     * The pass points
     *
     * @var integer
     */
    public $passPoints;
    
    /**
     * The maximum attempts
     *
     * @var integer
     */
    public $maximumAttempts;
    
    /**
     * The questions
     *
     * @var \ExtorioLMS\Classes\Models\Question[]
     */
    public $questions;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'courseId' => 
  array (
    'basicType' => 'number',
  ),
  'previewable' => 
  array (
    'basicType' => 'checkbox',
  ),
  'quizDescription' => 
  array (
    'basicType' => 'textarea',
  ),
  'lectureFileId' => 
  array (
    'basicType' => 'number',
  ),
  'lectureBody' => 
  array (
    'basicType' => 'textarea',
  ),
  'subtitleFileId' => 
  array (
    'basicType' => 'number',
  ),
  'maximumPoints' => 
  array (
    'basicType' => 'number',
  ),
  'passPoints' => 
  array (
    'basicType' => 'number',
  ),
  'maximumAttempts' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'type' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\ModuleType',
  ),
  'lectureType' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\LectureType',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
  'questions' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\ExtorioLMS\\Classes\\Models\\Question',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}