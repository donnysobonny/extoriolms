<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A course post subscriber
 *
 * Class B_CoursePostSubscriber
 */
class B_CoursePostSubscriber extends \Core\Classes\Commons\Model {

    
    /**
     * The id of the course post
     *
     * @var integer
     */
    public $coursePostId;
    
    /**
     * The id of the user
     *
     * @var integer
     */
    public $userId;
    

    protected static function internal_basicProperties() {
        return array (
  'coursePostId' => 
  array (
    'basicType' => 'number',
  ),
  'userId' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}