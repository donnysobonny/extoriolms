<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A course post
 *
 * Class B_CoursePost
 */
class B_CoursePost extends \Core\Classes\Commons\Model {

    
    /**
     * The id of the course
     *
     * @var integer
     */
    public $courseId;
    
    /**
     * The parent post of this post
     *
     * @var integer
     */
    public $parentId;
    
    /**
     * The creation date
     *
     * @var string
     */
    public $dateCreated;
    
    /**
     * The update date
     *
     * @var string
     */
    public $dateUpdated;
    
    /**
     * The reply date
     *
     * @var string
     */
    public $dateReplied;
    
    /**
     * The id of the user
     *
     * @var integer
     */
    public $userId;
    
    /**
     * The id of the replier
     *
     * @var integer
     */
    public $replierId;
    
    /**
     * The name of the post
     *
     * @var string
     */
    public $name;
    
    /**
     * The body of the post
     *
     * @var string
     */
    public $body;
    
    /**
     * The body of the post as plain text
     *
     * @var string
     */
    public $bodyPlain;
    
    /**
     * The id of an attachment course file
     *
     * @var integer
     */
    public $attachmentFileId;
    
    /**
     * The approval status of this post
     *
     * @var string
     */
    public $approval;
    
    /**
     * The number of replies
     *
     * @var integer
     */
    public $totalNumReplies;
    
    /**
     * 
     *
     * @var bool
     */
    public $sticky;
    

    protected static function internal_basicProperties() {
        return array (
  'courseId' => 
  array (
    'basicType' => 'number',
  ),
  'parentId' => 
  array (
    'basicType' => 'number',
  ),
  'dateCreated' => 
  array (
    'basicType' => 'datetime',
  ),
  'dateUpdated' => 
  array (
    'basicType' => 'datetime',
  ),
  'dateReplied' => 
  array (
    'basicType' => 'datetime',
  ),
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'replierId' => 
  array (
    'basicType' => 'number',
  ),
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'body' => 
  array (
    'basicType' => 'textarea',
  ),
  'bodyPlain' => 
  array (
    'basicType' => 'textarea',
  ),
  'attachmentFileId' => 
  array (
    'basicType' => 'number',
  ),
  'totalNumReplies' => 
  array (
    'basicType' => 'number',
  ),
  'sticky' => 
  array (
    'basicType' => 'checkbox',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'approval' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\ContentApproval',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}