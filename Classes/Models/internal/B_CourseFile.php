<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A course file
 *
 * Class B_CourseFile
 */
class B_CourseFile extends \Core\Classes\Commons\Model {

    
    /**
     * The owner of the file
     *
     * @var integer
     */
    public $userId;
    
    /**
     * The type of upload
     *
     * @var string
     */
    public $uploadType;
    
    /**
     * The type of file
     *
     * @var string
     */
    public $fileType;
    
    /**
     * The name of the file
     *
     * @var string
     */
    public $fileName;
    
    /**
     * The name of the bucket
     *
     * @var string
     */
    public $bucket;
    
    /**
     * The key
     *
     * @var string
     */
    public $key;
    
    /**
     * The content type
     *
     * @var string
     */
    public $contentType;
    

    protected static function internal_basicProperties() {
        return array (
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'fileName' => 
  array (
    'basicType' => 'textfield',
  ),
  'bucket' => 
  array (
    'basicType' => 'textfield',
  ),
  'key' => 
  array (
    'basicType' => 'textfield',
  ),
  'contentType' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'uploadType' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\CourseFilesUploadType',
  ),
  'fileType' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\CourseFilesFileType',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}