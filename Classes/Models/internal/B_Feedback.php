<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A feedback
 *
 * Class B_Feedback
 */
class B_Feedback extends \Core\Classes\Commons\Model {

    
    /**
     * The owner of the feedback
     *
     * @var integer
     */
    public $ownerId;
    
    /**
     * The id of the course
     *
     * @var integer
     */
    public $courseId;
    
    /**
     * The update date
     *
     * @var string
     */
    public $dateUpdated;
    
    /**
     * The rating
     *
     * @var integer
     */
    public $rating;
    
    /**
     * The comment
     *
     * @var string
     */
    public $comment;
    
    /**
     * The approval of the feedback
     *
     * @var string
     */
    public $approval;
    

    protected static function internal_basicProperties() {
        return array (
  'ownerId' => 
  array (
    'basicType' => 'number',
  ),
  'courseId' => 
  array (
    'basicType' => 'number',
  ),
  'dateUpdated' => 
  array (
    'basicType' => 'datetime',
  ),
  'rating' => 
  array (
    'basicType' => 'number',
  ),
  'comment' => 
  array (
    'basicType' => 'textarea',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'approval' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\ContentApproval',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}