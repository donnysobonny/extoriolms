<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A lesson
 *
 * Class B_Lesson
 */
class B_Lesson extends \Core\Classes\Commons\Model {

    
    /**
     * The name
     *
     * @var string
     */
    public $name;
    
    /**
     * The modules
     *
     * @var \ExtorioLMS\Classes\Models\Module[]
     */
    public $modules;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
  'modules' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\ExtorioLMS\\Classes\\Models\\Module',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}