<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A course tag
 *
 * Class B_CourseTag
 */
class B_CourseTag extends \Core\Classes\Commons\Model {

    
    /**
     * The name
     *
     * @var string
     */
    public $name;
    
    /**
     * The description
     *
     * @var string
     */
    public $description;
    
    /**
     * The icon
     *
     * @var string
     */
    public $icon;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'icon' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\FontAwesomeIcons',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}