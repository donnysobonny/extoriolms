<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A user course
 *
 * Class B_User_Course
 */
class B_User_Course extends \Core\Classes\Commons\Model {

    
    /**
     * The user id
     *
     * @var integer
     */
    public $userId;
    
    /**
     * The course id
     *
     * @var integer
     */
    public $courseId;
    
    /**
     * The status
     *
     * @var string
     */
    public $status;
    
    /**
     * The started date
     *
     * @var string
     */
    public $dateStarted;
    
    /**
     * The updated date
     *
     * @var string
     */
    public $dateUpdated;
    
    /**
     * The passed date
     *
     * @var string
     */
    public $datePassed;
    
    /**
     * The expiry date
     *
     * @var string
     */
    public $expiryDate;
    
    /**
     * The current points
     *
     * @var integer
     */
    public $currentPoints;
    
    /**
     * The certificate
     *
     * @var string
     */
    public $certificate;
    

    protected static function internal_basicProperties() {
        return array (
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'courseId' => 
  array (
    'basicType' => 'number',
  ),
  'dateStarted' => 
  array (
    'basicType' => 'datetime',
  ),
  'dateUpdated' => 
  array (
    'basicType' => 'datetime',
  ),
  'datePassed' => 
  array (
    'basicType' => 'datetime',
  ),
  'expiryDate' => 
  array (
    'basicType' => 'datetime',
  ),
  'currentPoints' => 
  array (
    'basicType' => 'number',
  ),
  'certificate' => 
  array (
    'basicType' => 'textfield',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'status' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\ContentStatus',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}