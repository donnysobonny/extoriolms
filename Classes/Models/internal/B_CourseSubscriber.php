<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A course subscriber
 *
 * Class B_CourseSubscriber
 */
class B_CourseSubscriber extends \Core\Classes\Commons\Model {

    
    /**
     * The id of the course
     *
     * @var integer
     */
    public $courseId;
    
    /**
     * The id of the user
     *
     * @var integer
     */
    public $userId;
    

    protected static function internal_basicProperties() {
        return array (
  'courseId' => 
  array (
    'basicType' => 'number',
  ),
  'userId' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}