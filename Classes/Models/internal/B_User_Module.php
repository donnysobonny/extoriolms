<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A user module
 *
 * Class B_User_Module
 */
class B_User_Module extends \Core\Classes\Commons\Model {

    
    /**
     * The user id
     *
     * @var integer
     */
    public $userId;
    
    /**
     * The course id
     *
     * @var integer
     */
    public $courseId;
    
    /**
     * The user course id
     *
     * @var integer
     */
    public $userCourseId;
    
    /**
     * The module id
     *
     * @var integer
     */
    public $moduleId;
    
    /**
     * The status
     *
     * @var string
     */
    public $status;
    
    /**
     * The current module
     *
     * @var bool
     */
    public $current;
    
    /**
     * The updated date
     *
     * @var string
     */
    public $dateUpdated;
    
    /**
     * The passed date
     *
     * @var string
     */
    public $datePassed;
    
    /**
     * The current points
     *
     * @var integer
     */
    public $currentPoints;
    
    /**
     * The current points
     *
     * @var integer
     */
    public $currentAttempts;
    
    /**
     * The user questions
     *
     * @var \ExtorioLMS\Classes\Models\User_Question[]
     */
    public $userQuestions;
    

    protected static function internal_basicProperties() {
        return array (
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'courseId' => 
  array (
    'basicType' => 'number',
  ),
  'userCourseId' => 
  array (
    'basicType' => 'number',
  ),
  'moduleId' => 
  array (
    'basicType' => 'number',
  ),
  'current' => 
  array (
    'basicType' => 'checkbox',
  ),
  'dateUpdated' => 
  array (
    'basicType' => 'datetime',
  ),
  'datePassed' => 
  array (
    'basicType' => 'datetime',
  ),
  'currentPoints' => 
  array (
    'basicType' => 'number',
  ),
  'currentAttempts' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'status' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\ContentStatus',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
  'userQuestions' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\ExtorioLMS\\Classes\\Models\\User_Question',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}