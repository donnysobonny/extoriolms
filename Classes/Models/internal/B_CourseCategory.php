<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A course category
 *
 * Class B_CourseCategory
 */
class B_CourseCategory extends \Core\Classes\Commons\Model {

    
    /**
     * The name
     *
     * @var string
     */
    public $name;
    
    /**
     * The parent category
     *
     * @var integer
     */
    public $parentId;
    
    /**
     * The icon
     *
     * @var string
     */
    public $icon;
    

    protected static function internal_basicProperties() {
        return array (
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'parentId' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'icon' => 
  array (
    'enumNamespace' => '\\Core\\Classes\\Enums\\FontAwesomeIcons',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}