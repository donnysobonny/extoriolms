<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A certificate template
 *
 * Class B_CertificateTemplate
 */
class B_CertificateTemplate extends \Core\Classes\Commons\Model {

    
    /**
     * The owner id
     *
     * @var integer
     */
    public $ownerId;
    
    /**
     * Whether this template is the default one
     *
     * @var bool
     */
    public $siteDefault;
    
    /**
     * The name
     *
     * @var string
     */
    public $name;
    
    /**
     * The description
     *
     * @var string
     */
    public $description;
    
    /**
     * The body
     *
     * @var string
     */
    public $body;
    

    protected static function internal_basicProperties() {
        return array (
  'ownerId' => 
  array (
    'basicType' => 'number',
  ),
  'siteDefault' => 
  array (
    'basicType' => 'checkbox',
  ),
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'body' => 
  array (
    'basicType' => 'textarea',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}