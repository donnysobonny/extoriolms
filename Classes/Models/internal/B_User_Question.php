<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A user question
 *
 * Class B_User_Question
 */
class B_User_Question extends \Core\Classes\Commons\Model {

    
    /**
     * The user id
     *
     * @var integer
     */
    public $userId;
    
    /**
     * The question id
     *
     * @var integer
     */
    public $questionId;
    
    /**
     * The status
     *
     * @var string
     */
    public $status;
    
    /**
     * The updated date
     *
     * @var string
     */
    public $dateUpdated;
    
    /**
     * The passed date
     *
     * @var string
     */
    public $datePassed;
    
    /**
     * The response
     *
     * @var string
     */
    public $response;
    
    /**
     * The response date
     *
     * @var string
     */
    public $responseDate;
    
    /**
     * The id of the responder
     *
     * @var integer
     */
    public $responseById;
    
    /**
     * The user answers
     *
     * @var array
     */
    public $userAnswers;
    

    protected static function internal_basicProperties() {
        return array (
  'userId' => 
  array (
    'basicType' => 'number',
  ),
  'questionId' => 
  array (
    'basicType' => 'number',
  ),
  'dateUpdated' => 
  array (
    'basicType' => 'datetime',
  ),
  'datePassed' => 
  array (
    'basicType' => 'datetime',
  ),
  'response' => 
  array (
    'basicType' => 'textarea',
  ),
  'responseDate' => 
  array (
    'basicType' => 'datetime',
  ),
  'responseById' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'status' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\ContentStatus',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
);
    }

    protected static function internal_metaProperties() {
        return array (
  0 => 'userAnswers',
);
    }
}