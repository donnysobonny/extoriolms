<?php
namespace ExtorioLMS\Classes\Models;
/**
 * A course
 *
 * Class B_Course
 */
class B_Course extends \Core\Classes\Commons\Model {

    
    /**
     * The owner id
     *
     * @var integer
     */
    public $ownerId;
    
    /**
     * The creation date
     *
     * @var string
     */
    public $dateCreated;
    
    /**
     * The update date
     *
     * @var string
     */
    public $dateUpdated;
    
    /**
     * The name
     *
     * @var string
     */
    public $name;
    
    /**
     * The description
     *
     * @var string
     */
    public $description;
    
    /**
     * The category
     *
     * @var \ExtorioLMS\Classes\Models\CourseCategory
     */
    public $category;
    
    /**
     * The sub category
     *
     * @var \ExtorioLMS\Classes\Models\CourseCategory
     */
    public $subCategory;
    
    /**
     * The difficulty
     *
     * @var string
     */
    public $difficulty;
    
    /**
     * The tags
     *
     * @var \ExtorioLMS\Classes\Models\CourseTag[]
     */
    public $tags;
    
    /**
     * The expiry minutes
     *
     * @var integer
     */
    public $expiryMinutes;
    
    /**
     * The certificate template
     *
     * @var \ExtorioLMS\Classes\Models\CertificateTemplate
     */
    public $certificateTemplate;
    
    /**
     * The approval
     *
     * @var string
     */
    public $approval;
    
    /**
     * The cancelled reason
     *
     * @var string
     */
    public $cancelledReason;
    
    /**
     * Whether this course is self enrollable
     *
     * @var bool
     */
    public $selfEnrollable;
    
    /**
     * The lessons
     *
     * @var \ExtorioLMS\Classes\Models\Lesson[]
     */
    public $lessons;
    
    /**
     * The default file id
     *
     * @var integer
     */
    public $defaultFileId;
    
    /**
     * The default thumbnail
     *
     * @var string
     */
    public $defaultThumbnail;
    
    /**
     * The average rating
     *
     * @var double
     */
    public $averageRating;
    
    /**
     * The total number enrolled
     *
     * @var integer
     */
    public $totalNumEnrolled;
    
    /**
     * The maximum points
     *
     * @var integer
     */
    public $maximumPoints;
    
    /**
     * The total num lessons
     *
     * @var integer
     */
    public $totalNumLessons;
    
    /**
     * The total num lectures
     *
     * @var integer
     */
    public $totalNumLectures;
    
    /**
     * The total num quizzes
     *
     * @var integer
     */
    public $totalNumQuizzes;
    
    /**
     * The total num questions
     *
     * @var integer
     */
    public $totalNumQuestions;
    

    protected static function internal_basicProperties() {
        return array (
  'ownerId' => 
  array (
    'basicType' => 'number',
  ),
  'dateCreated' => 
  array (
    'basicType' => 'datetime',
  ),
  'dateUpdated' => 
  array (
    'basicType' => 'datetime',
  ),
  'name' => 
  array (
    'basicType' => 'textfield',
  ),
  'description' => 
  array (
    'basicType' => 'textarea',
  ),
  'expiryMinutes' => 
  array (
    'basicType' => 'number',
  ),
  'cancelledReason' => 
  array (
    'basicType' => 'textfield',
  ),
  'selfEnrollable' => 
  array (
    'basicType' => 'checkbox',
  ),
  'defaultFileId' => 
  array (
    'basicType' => 'number',
  ),
  'defaultThumbnail' => 
  array (
    'basicType' => 'textfield',
  ),
  'averageRating' => 
  array (
    'basicType' => 'decimal',
  ),
  'totalNumEnrolled' => 
  array (
    'basicType' => 'number',
  ),
  'maximumPoints' => 
  array (
    'basicType' => 'number',
  ),
  'totalNumLessons' => 
  array (
    'basicType' => 'number',
  ),
  'totalNumLectures' => 
  array (
    'basicType' => 'number',
  ),
  'totalNumQuizzes' => 
  array (
    'basicType' => 'number',
  ),
  'totalNumQuestions' => 
  array (
    'basicType' => 'number',
  ),
);
    }

    protected static function internal_enumProperties() {
        return array (
  'difficulty' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\ContentDifficulty',
  ),
  'approval' => 
  array (
    'enumNamespace' => '\\ExtorioLMS\\Classes\\Enums\\ContentApproval',
  ),
);
    }

    protected static function internal_complexProperties() {
        return array (
  'category' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\ExtorioLMS\\Classes\\Models\\CourseCategory',
  ),
  'subCategory' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\ExtorioLMS\\Classes\\Models\\CourseCategory',
  ),
  'tags' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\ExtorioLMS\\Classes\\Models\\CourseTag',
  ),
  'certificateTemplate' => 
  array (
    'type' => 'object',
    'childModelNamespace' => '\\ExtorioLMS\\Classes\\Models\\CertificateTemplate',
  ),
  'lessons' => 
  array (
    'type' => 'array',
    'childModelNamespace' => '\\ExtorioLMS\\Classes\\Models\\Lesson',
  ),
);
    }

    protected static function internal_metaProperties() {
        return array (
);
    }
}