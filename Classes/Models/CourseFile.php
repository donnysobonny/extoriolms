<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Commons\Extorio;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Exceptions\Extension_Exception;
use ExtorioLMS\Classes\Enums\CourseFilesFileType;
use ExtorioLMS\Classes\Enums\CourseFilesUploadType;
use ExtorioLMS\Classes\Utilities\Misc;

/**
 * 
 *
 * Class CourseFile
 */
class CourseFile extends B_CourseFile {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    public function publicUrl() {
        $s3 = Misc::createS3Client();
        return $s3->getObjectUrl($this->bucket, $this->key);
    }

    public function privateUrl($lifeTimeSeconds = 1800) {
        $done = false;
        $url = "";
        if(isset($_SESSION[$this->id."_course_file_url"])) {
            $time = time();
            $expTime = intval($_SESSION[$this->id."_course_file_expire"]);
            if($time < $expTime) {
                $done = true;
                $url = $_SESSION[$this->id."_course_file_url"];
            }
        }

        if(!$done) {
            $_SESSION[$this->id."_course_file_expire"] = time() + $lifeTimeSeconds;

            $s3 = Misc::createS3Client();
            $cmd = $s3->getCommand("GetObject", array(
                "Bucket" => $this->bucket,
                "Key" => $this->key
            ));
            $req = $s3->createPresignedRequest($cmd, "+ ".($lifeTimeSeconds + 60)." seconds");

            $url = (string)$req->getUri();
            $_SESSION[$this->id."_course_file_url"] = $url;
        }

        return $url;
    }

    private function updateLocks() {
        $this->userId = $this->_old->userId;
    }

    private function generalChecks() {
        if(!strlen($this->userId)) {
            throw new \Exception("A course file must have a user id");
        }
        if(!strlen($this->fileName)) {
            throw new \Exception("A course file must have a file name");
        }
        if(!strlen($this->bucket)) {
            throw new \Exception("A course file must have a bucket");
        }
        if(!strlen($this->contentType)) {
            throw new \Exception("A course file must have a content type");
        }

        if($this->uploadType == CourseFilesUploadType::_textSources) {
            $this->fileType = CourseFilesFileType::_images;
        }

        $config = Extorio::get()->getConfig();
        //make sure content type is accepted
        switch($this->fileType) {
            case CourseFilesFileType::_images :
                if(!in_array($this->contentType, $config["ExtorioLMS"]["uploads"]["images"]["accepted_types"])) {
                    throw new \Exception($this->contentType." is not an accepted type for ".$this->fileType);
                }
                break;
            case CourseFilesFileType::_videos :
                if(!in_array($this->contentType, $config["ExtorioLMS"]["uploads"]["videos"]["accepted_types"])) {
                    throw new \Exception($this->contentType." is not an accepted type for ".$this->fileType);
                }
                break;
            case CourseFilesFileType::_audio :
                if(!in_array($this->contentType, $config["ExtorioLMS"]["uploads"]["audio"]["accepted_types"])) {
                    throw new \Exception($this->contentType." is not an accepted type for ".$this->fileType);
                }
                break;
            case CourseFilesFileType::_documents :
                if(!in_array($this->contentType, $config["ExtorioLMS"]["uploads"]["documents"]["accepted_types"])) {
                    throw new \Exception($this->contentType." is not an accepted type for ".$this->fileType);
                }
                break;
            case CourseFilesFileType::_subtitles :
                if(!in_array($this->contentType, $config["ExtorioLMS"]["uploads"]["subtitles"]["accepted_types"])) {
                    throw new \Exception($this->contentType." is not an accepted type for ".$this->fileType);
                }
                break;
        }
        $this->key = $this->uploadType."/".$this->userId."/".$this->fileType."/".$this->fileName;
    }
}