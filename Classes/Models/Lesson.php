<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Helpers\Query;

/**
 * 
 *
 * Class Lesson
 */
class Lesson extends B_Lesson {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {

    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {

    }

    protected function afterUpdate() {
        //any modules that no longer exist should be deleted
        $mids = array();
        foreach($this->modules as $module) {
            $mids[] = $module->id;
        }
        foreach($this->_old->modules as $module) {
            if(!in_array($module->id, $mids)) {
                $module->deleteThis();
            }
        }
    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {
        //delete any modules
        foreach($this->modules as $module) {
            $module->deleteThis();
        }
    }
}