<?php
namespace ExtorioLMS\Classes\Models;
use Core\Classes\Helpers\Query;

/**
 * 
 *
 * Class CourseCategory
 */
class CourseCategory extends B_CourseCategory {

    /**
     * @var CourseCategory[]
     */
    public $subCategories = array();

    /**
     * Get all categories ordered by name, with sub categories
     *
     * @return CourseCategory[]
     */
    public static function getAllCategories() {
        $categories = CourseCategory::findAll(
            Query::n()
                ->where(array(
                    "parentId" => 0
                ))
                ->order(array(
                    "name" => "asc"
                )),1
        );
        foreach($categories as $category) {
            $category->subCategories = CourseCategory::findAll(
                Query::n()
                    ->where(array(
                        "parentId" => $category->id
                    ))
                    ->order(array(
                        "name" => "asc"
                    )),1
            );
        }
        return $categories;
    }

    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {
        //must not be in use
        if(Course::findOne(
            Query::n()
                ->where(array(
                    Query::_or => array(
                        "category.id" => $this->id,
                        "subCategory.id" => $this->id
                    )
                )),1
        )) {
            throw new \Exception("Cannot delete a course category that is in use");
        }
    }

    protected function afterDelete() {
        $subs = CourseCategory::findAll(
            Query::n()
                ->where(array(
                    "parentId" => $this->id
                )),1
        );
        foreach($subs as $sub) {
            $sub->deleteThis();
        }
    }

    private function updateLocks() {

    }

    private function generalChecks() {
        if(!strlen($this->name)) {
            throw new \Exception("A category cannot exist without a name");
        }
    }
}