<?php
namespace ExtorioLMS\Classes\Models;
/**
 * 
 *
 * Class CourseTag
 */
class CourseTag extends B_CourseTag {
    protected function beforeRetrieve() {

    }

    protected function beforeCreate() {
        $this->generalChecks();
    }

    protected function afterCreate() {

    }

    protected function beforeUpdate() {
        $this->updateLocks();
        $this->generalChecks();
    }

    protected function afterUpdate() {

    }

    protected function beforeDelete() {

    }

    protected function afterDelete() {

    }

    private function updateLocks() {

    }

    private function generalChecks() {
        if(!strlen($this->name)) {
            throw new \Exception("A course tag cannot exist without a name");
        }
    }
}