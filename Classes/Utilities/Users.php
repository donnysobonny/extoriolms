<?php
namespace ExtorioLMS\Classes\Utilities;

/**
 * A utility class for handling lms users
 *
 * @author Donny Sutherland <donny@pixelpug.co.uk>
 *
 * Class Users
 */
class Users {
    /**
     * Get whether a user is enrolled on a course
     *
     * @param $userId
     * @param $courseId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userEnrolledOnCourse($userId,$courseId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT id FROM extoriolms_classes_models_user_course
        WHERE "userId" = $1 AND "courseId" = $2
        LIMIT 1
        ';
        return $db->query($sql,array($userId,$courseId))->numRows() >= 1 ? true : false;
    }

    /**
     * Get whether a user course belongs to a certain user
     *
     * @param $userCourseId
     * @param $userId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userEnrolmentBelongsToUser($userCourseId, $userId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT id FROM extoriolms_classes_models_user_course WHERE "userId" = $1 AND id = $2 LIMIT 1
        ';
        $row = $db->query($sql, array(
            $userId,
            $userCourseId
        ))->fetchRow();
        return $row ? true : false;
    }

    /**
     * Get the enrollment status of a course enrolment. Returns NULL if no enrolment exists
     *
     * @param $userId
     * @param $courseId
     *
     * @return null
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userCourseEnrolmentStatus($userId, $courseId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT status FROM extoriolms_classes_models_user_course

        WHERE
        "userId" = $1 AND
        "courseId" = $2

        LIMIT 1
        ';
        $row = $db->query($sql, array(
            $userId,
            $courseId
        ))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    /**
     * Get the status of a user module. Returns NULL if the module could not be found
     *
     * @param $userId
     * @param $moduleId
     *
     * @return null|string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userModuleStatus($userId, $moduleId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT status FROM extoriolms_classes_models_user_module
        WHERE "userId" = $1 AND "moduleId" = $2
        LIMIT 1';
        $query = $db->query($sql, array($userId,$moduleId));
        if($query->numRows() >= 1) {
            $row = $query->fetchRow();
            return strval($row[0]);
        } else {
            return null;
        }
    }

    /**
     * Get whether a user is subscribed to a course
     *
     * @param $userId
     * @param $courseId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userSubscribedToCourse($userId, $courseId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT id FROM extoriolms_classes_models_coursesubscriber WHERE "courseId" = $1 AND "userId" = $2 LIMIT 1';
        $row = $db->query($sql, array(
            $courseId,
            $userId
        ))->fetchRow();
        return $row ? true : false;
    }

    /**
     * Get whether a user is subscribed to a post
     *
     * @param $userId
     * @param $postId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function userSubscribedToPost($userId, $postId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT id FROM extoriolms_classes_models_coursepostsubscriber WHERE "coursePostId" = $1 AND "userId" = $2 LIMIT 1';
        $row = $db->query($sql, array(
            $postId,
            $userId
        ))->fetchRow();
        return $row ? true : false;
    }
}