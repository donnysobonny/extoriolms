<?php
namespace ExtorioLMS\Classes\Utilities;

class Misc {
    /**
     * Create an s3 instance
     *
     * @return \Aws\S3\S3Client
     */
    public static function createS3Client() {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        return new \Aws\S3\S3Client(array(
            'version' => 'latest',
            'region'  => $config["ExtorioLMS"]["aws"]["s3"]["region"],
            'credentials' => array(
                'key' => $config["ExtorioLMS"]["aws"]["s3"]["key"],
                'secret' => $config["ExtorioLMS"]["aws"]["s3"]["secret"]
            )
        ));
    }

}