<?php
namespace ExtorioLMS\Classes\Utilities;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\EmailTransport;
use Core\Classes\Models\User;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Models\B_CoursePostSubscriber;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\CoursePost;
use ExtorioLMS\Classes\Models\CourseSubscriber;
use ExtorioLMS\Classes\Models\User_Course;
use ExtorioLMS\Classes\Models\User_Module;

class Emails {

    #region Course emails

    /**
     * Send the enrollment email to the newly enrolled user
     *
     * @param User_Course $userCourse
     */
    public static function sendCourseEnrollment_user($userCourse) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["courses"]["course_enrollments"]["enabled"]) {
            return;
        }

        $user = User::findById($userCourse->userId,1);
        $course = Course::findById($userCourse->courseId,1);

        if($user && $course) {
            $mergeVars = array();
            $mergeVars["SHORTNAME"] = $user->shortname;
            $mergeVars["LONGNAME"] = $user->longname;
            $mergeVars["COURSE_NAME"] = $course->name;
            $mergeVars["COURSE_URL"] = 'TBC';
            $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
            $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
            $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

            EmailTransport::qs(
                $config["ExtorioLMS"]["emails"]["courses"]["course_enrollments"]["subject"],
                $config["ExtorioLMS"]["emails"]["courses"]["course_enrollments"]["body"],
                $mergeVars,
                null,
                $user->shortname,
                $user->email
            );
        }
    }

    /**
     * Send the course passed email to an enrolled user
     *
     * @param User_Course $userCourse
     */
    public static function sendPassedCourse_user($userCourse) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["courses"]["passed_course"]["enabled"]) {
            return;
        }

        $user = User::findById($userCourse->userId,1);
        $course = Course::findById($userCourse->courseId,1);

        if($user && $course) {
            $mergeVars = array();
            $mergeVars["SHORTNAME"] = $user->shortname;
            $mergeVars["LONGNAME"] = $user->longname;
            $mergeVars["COURSE_NAME"] = $course->name;
            $mergeVars["COURSE_URL"] = 'TBC';
            $mergeVars["CERTIFICATE_URL"] = $config["application"]["address"].$userCourse->certificate;
            $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
            $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
            $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

            EmailTransport::qs(
                $config["ExtorioLMS"]["emails"]["courses"]["passed_course"]["subject"],
                $config["ExtorioLMS"]["emails"]["courses"]["passed_course"]["body"],
                $mergeVars,
                null,
                $user->shortname,
                $user->email
            );
        }
    }

    /**
     * Send the course cancellation email to all enrolled users
     *
     * @param Course $course
     */
    public static function sendCourseCancelled_user($course,$sleepTime=0) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["courses"]["course_cancelled_enrolled"]["enabled"]) {
            return;
        }

        $ues = User_Course::findAll(
            Query::n()
                ->where(array(
                    "courseId" => $course->id
                )),1
        );

        foreach($ues as $ue) {
            $user = User::findById($ue->userId,1);
            $mergeVars = array();
            $mergeVars["SHORTNAME"] = $user->shortname;
            $mergeVars["LONGNAME"] = $user->longname;
            $mergeVars["COURSE_NAME"] = $course->name;
            $mergeVars["COURSE_URL"] = 'TBC';
            $mergeVars["REASON"] = $course->cancelledReason;
            $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
            $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
            $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

            EmailTransport::qs(
                $config["ExtorioLMS"]["emails"]["courses"]["course_cancelled_enrolled"]["subject"],
                $config["ExtorioLMS"]["emails"]["courses"]["course_cancelled_enrolled"]["body"],
                $mergeVars,
                null,
                $user->shortname,
                $user->email
            );

            if($sleepTime > 0) {
                sleep($sleepTime);
            }
        }
    }

    /**
     * Send an email to let a user know their enrolment is soon due to expire
     *
     * @param User_Course $userCourse
     */
    public static function sendCourseExpiry_user($userCourse) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["courses"]["course_expiry"]["enabled"]) {
            return;
        }
        if(is_null($userCourse->expiryDate)) {
            return;
        }

        $user = User::findById($userCourse->userId,1);
        $course = Course::findById($userCourse->courseId,1);

        if($user && $course) {
            $mergeVars = array();
            $mergeVars["SHORTNAME"] = $user->shortname;
            $mergeVars["LONGNAME"] = $user->longname;
            $mergeVars["COURSE_NAME"] = $course->name;
            $mergeVars["COURSE_URL"] = 'TBC';
            $mergeVars["EXPIRY_DATE"] = date("jS F Y, g:i a", strtotime($userCourse->expiryDate));
            $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
            $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
            $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

            EmailTransport::qs(
                $config["ExtorioLMS"]["emails"]["courses"]["course_expiry"]["subject"],
                $config["ExtorioLMS"]["emails"]["courses"]["course_expiry"]["body"],
                $mergeVars,
                null,
                $user->shortname,
                $user->email
            );
        }
    }

    /**
     * Send the course approved email to the owner
     *
     * @param Course $course
     */
    public static function sendCourseApproved_owner($course) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["courses"]["course_approved"]["enabled"]) {
            return;
        }
        if($course->approval != ContentApproval::_approved) {
            return;
        }

        $owner = User::findById($course->ownerId,1);

        if($owner) {
            $mergeVars = array();
            $mergeVars["SHORTNAME"] = $owner->shortname;
            $mergeVars["LONGNAME"] = $owner->longname;
            $mergeVars["COURSE_NAME"] = $course->name;
            $mergeVars["COURSE_URL"] = 'TBC';
            $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
            $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
            $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

            EmailTransport::qs(
                $config["ExtorioLMS"]["emails"]["courses"]["course_approved"]["subject"],
                $config["ExtorioLMS"]["emails"]["courses"]["course_approved"]["body"],
                $mergeVars,
                null,
                $owner->shortname,
                $owner->email
            );
        }
    }

    /**
     * Send the course approved email to the owner
     *
     * @param Course $course
     */
    public static function sendCourseClosed_owner($course) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["courses"]["course_closed"]["enabled"]) {
            return;
        }
        if($course->approval != ContentApproval::_closed) {
            return;
        }

        $owner = User::findById($course->ownerId,1);

        if($owner) {
            $mergeVars = array();
            $mergeVars["SHORTNAME"] = $owner->shortname;
            $mergeVars["LONGNAME"] = $owner->longname;
            $mergeVars["COURSE_NAME"] = $course->name;
            $mergeVars["COURSE_URL"] = 'TBC';
            $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
            $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
            $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

            EmailTransport::qs(
                $config["ExtorioLMS"]["emails"]["courses"]["course_closed"]["subject"],
                $config["ExtorioLMS"]["emails"]["courses"]["course_closed"]["body"],
                $mergeVars,
                null,
                $owner->shortname,
                $owner->email
            );
        }
    }

    /**
     * Send the course cancellation email to the owner
     *
     * @param Course $course
     */
    public static function sendCourseCancelled_owner($course) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["courses"]["course_cancelled"]["enabled"]) {
            return;
        }
        if($course->approval != ContentApproval::_cancelled) {
            return;
        }

        $owner = User::findById($course->ownerId,1);

        if($owner) {
            $mergeVars = array();
            $mergeVars["SHORTNAME"] = $owner->shortname;
            $mergeVars["LONGNAME"] = $owner->shortname;
            $mergeVars["COURSE_NAME"] = $course->name;
            $mergeVars["COURSE_URL"] = 'TBC';
            $mergeVars["REASON"] = $course->cancelledReason;
            $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
            $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
            $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

            EmailTransport::qs(
                $config["ExtorioLMS"]["emails"]["courses"]["course_cancelled"]["subject"],
                $config["ExtorioLMS"]["emails"]["courses"]["course_cancelled"]["body"],
                $mergeVars,
                null,
                $owner->shortname,
                $owner->email
            );
        }
    }

    #endregion

    #region Course Post emails

    /**
     * Send an email to all subscribed users of a course a notification of a new post
     *
     * @param CoursePost $coursePost
     */
    public static function sendNewPost($coursePost, $sleepTime = 0) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["course_posts"]["new_post"]["enabled"]) {
            return;
        }
        //if the post is a reply
        if($coursePost->parentId > 0) {
            return;
        }

        $course = Course::findById($coursePost->courseId,1);
        $css = CourseSubscriber::findAll(
            Query::n()
                ->where(array(
                    "courseId" => $course->id
                )),1
        );

        if($course && count($css)) {
            foreach($css as $cs) {
                $user = User::findById($cs->userId,1);

                $mergeVars = array();
                $mergeVars["SHORTNAME"] = $user->shortname;
                $mergeVars["LONGNAME"] = $user->longname;
                $mergeVars["COURSE_NAME"] = $course->name;
                $mergeVars["POST_NAME"] = $coursePost->name;
                $mergeVars["POST_URL"] = "TBC";
                $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
                $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
                $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

                EmailTransport::qs(
                    $config["ExtorioLMS"]["emails"]["course_posts"]["new_post"]["subject"],
                    $config["ExtorioLMS"]["emails"]["course_posts"]["new_post"]["body"],
                    $mergeVars,
                    null,
                    $user->shortname,
                    $user->email
                );

                if($sleepTime > 0) {
                    sleep($sleepTime);
                }
            }
        }
    }

    /**
     * Send an email to all subscribers of a course post that a new reply has been made
     *
     * @param CoursePost $coursePost
     */
    public static function sendNewReply($coursePost, $sleepTime=0) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["course_posts"]["new_reply"]["enabled"]) {
            return;
        }
        //if post is not a reply
        if($coursePost->parentId <= 0) {
            return;
        }

        $replier = User::findById($coursePost->userId,1);
        $cpss = B_CoursePostSubscriber::findAll(
            Query::n()
                ->where(array(
                    "coursePostId" => $coursePost->parentId
                ))
        );

        if($replier && count($cpss)) {
            foreach($cpss as $cps) {
                $user = User::findById($cps->userId,1);

                $mergeVars = array();
                $mergeVars["SHORTNAME"] = $user->shortname;
                $mergeVars["LONGNAME"] = $user->longname;
                $mergeVars["REPLY_SHORTNAME"] = $replier->shortname;
                $mergeVars["POST_NAME"] = $coursePost->name;
                $mergeVars["POST_URL"] = "TBC";
                $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
                $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
                $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

                EmailTransport::qs(
                    $config["ExtorioLMS"]["emails"]["course_posts"]["new_reply"]["subject"],
                    $config["ExtorioLMS"]["emails"]["course_posts"]["new_reply"]["body"],
                    $mergeVars,
                    null,
                    $user->shortname,
                    $user->email
                );

                if($sleepTime > 0) {
                    sleep($sleepTime);
                }
            }
        }
    }
    #endregion

    #region General emails

    /**
     * Send an email to the quiz owner notifying them that their quiz has been marked
     *
     * @param User_Module $userModule
     */
    public static function sendUserQuizMarked($userModule) {
        $config = \Core\Classes\Commons\Extorio::get()->getConfig();
        if(!$config["ExtorioLMS"]["emails"]["general"]["user_quiz_marked"]["enabled"]) {
            return;
        }

        $user = User::findById($userModule->userId,1);

        if($user) {
            $mergeVars = array();
            $mergeVars["SHORTNAME"] = $user->shortname;
            $mergeVars["LONGNAME"] = $user->longname;
            $mergeVars["COURSE_NAME"] = Content::getCourseName($userModule->courseId);
            $mergeVars["MODULE_NAME"] = Content::getModuleName($userModule->moduleId);
            $mergeVars["MODULE_URL"] = "TBC";
            $mergeVars["APPLICATION_ADDRESS"] = $config["application"]["address"];
            $mergeVars["APPLICATION_NAME"] = $config["application"]["name"];
            $mergeVars["APPLICATION_COMPANY"] = $config["application"]["company"];

            EmailTransport::qs(
                $config["ExtorioLMS"]["emails"]["general"]["user_quiz_marked"]["subject"],
                $config["ExtorioLMS"]["emails"]["general"]["user_quiz_marked"]["body"],
                $mergeVars,
                null,
                $user->shortname,
                $user->email
            );
        }
    }

    #endregion
}