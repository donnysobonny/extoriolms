<?php
namespace ExtorioLMS\Classes\Utilities;
use Core\Classes\Enums\FontAwesomeIcons;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\LectureType;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Helpers\CourseThumbnail;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\CoursePostSubscriber;
use ExtorioLMS\Classes\Models\CourseSubscriber;

class Content {

    public static function getModuleIcon($moduleType,$lectureType) {
        switch($moduleType) {
            case ModuleType::_quiz :
                return FontAwesomeIcons::_questionCircle;
                break;
            case ModuleType::_lecture:
                switch($lectureType) {
                    case LectureType::_video :
                        return FontAwesomeIcons::_fileVideoO;
                        break;
                    case LectureType::_image :
                        return FontAwesomeIcons::_fileImageO;
                        break;
                    case LectureType::_audio :
                        return FontAwesomeIcons::_fileAudioO;
                        break;
                    case LectureType::_text :
                        return FontAwesomeIcons::_fileTextO;
                        break;
                    case LectureType::_document :
                        return FontAwesomeIcons::_filePdfO;
                        break;
                }
                break;
        }
    }

    public static function getUserModuleStatusHtml($status,$content=null,$placement="right") {
        if(!strlen($content)) {
            $content = Utilities\Strings::titleSafe($status);
        }
        switch($status) {
            case ContentStatus::_pending:
                return '<span class="text-warning extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="The user has not yet started this module.">'.$content.'</span>';
                break;
            case ContentStatus::_started:
                return '<span class="text-info extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="The user has started this module, using an attempt.">'.$content.'</span>';
                break;
            case ContentStatus::_passed:
                return '<span class="text-sucess extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="The user has passed this module.">'.$content.'</span>';
                break;
            case ContentStatus::_failed:
                return '<span class="text-danger extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="The user has failed this module.">'.$content.'</span>';
                break;
            case ContentStatus::_submitted:
                return '<span class="text-info extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="The user has submitted the module and it requires manual review.">'.$content.'</span>';
                break;
        }
    }

    public static function getUserCourseStatusHtml($status,$content=null,$placement="right") {
        if(!strlen($content)) {
            $content = Utilities\Strings::titleSafe($status);
        }
        switch($status) {
            case ContentStatus::_pending:
                return '<span class="text-warning extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="The user has not yet started their course work.">'.$content.'</span>';
                break;
            case ContentStatus::_started:
                return '<span class="text-info extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="The user has started their course work.">'.$content.'</span>';
                break;
            case ContentStatus::_passed:
                return '<span class="text-success extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="The user has passed this course.">'.$content.'</span>';
                break;
        }
    }

    public static function getFeedbackApprovalHtml($approval,$content=null,$placement="right") {
        if(!strlen($content)) {
            $content = Utilities\Strings::titleSafe($approval);
        }
        switch($approval) {
            case ContentApproval::_pending:
                return '<span class="text-info extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This feedback is pending approval.">'.$content.'</span>';
                break;
            case ContentApproval::_approved:
                return '<span class="text-success extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This feedback has been approved.">'.$content.'</span>';
                break;
            case ContentApproval::_cancelled:
                return '<span class="text-danger extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This feedback has been cancelled.">'.$content.'</span>';
                break;
        }
    }

    public static function getPostApprovalHtml($approval,$content=null,$placement="right") {
        if(!strlen($content)) {
            $content = Utilities\Strings::titleSafe($approval);
        }
        switch($approval) {
            case ContentApproval::_pending:
                return '<span class="text-info extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This course is pending approval.">'.$content.'</span>';
                break;
            case ContentApproval::_approved:
                return '<span class="text-success extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This post has been approved.">'.$content.'</span>';
                break;
            case ContentApproval::_closed:
                return '<span class="text-warning extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This post is closed, and can therefore not accept new replies.">'.$content.'</span>';
                break;
            case ContentApproval::_cancelled:
                return '<span class="text-danger extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This post is cancelled and can therefore only be accessed by users with sufficient roles.">'.$content.'</span>';
                break;
        }
    }

    public static function getCourseApprovalHtml($approval,$content=null,$placement="right") {
        if(!strlen($content)) {
            $content = Utilities\Strings::titleSafe($approval);
        }
        switch($approval) {
            case ContentApproval::_drafting:
                return '<span class="text-muted extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This course is currently in drafting mode, which means it is still being edited and has not yet been submitted for approval.">'.$content.'</span>';
                break;
            case ContentApproval::_pending:
                return '<span class="text-info extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This course is currently pending approval. It can only accept minimal modifications.">'.$content.'</span>';
                break;
            case ContentApproval::_approved:
                return '<span class="text-success extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This course has been approved, and can therefore take on enrolments and can only accept minimal modifications.">'.$content.'</span>';
                break;
            case ContentApproval::_closed:
                return '<span class="text-warning extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This course is currently closed and can therefore not accept any new enrolments. It can only accept minimal modifications.">'.$content.'</span>';
                break;
            case ContentApproval::_cancelled:
                return '<span class="text-danger extoriolms_tooltip" style="border-bottom: 1px dashed; cursor: help;" data-toggle="tooltip" data-placement="'.$placement.'" title="This course has been cancelled and can only be accessed by users with sufficient roles.">'.$content.'</span>';
                break;
        }
    }

    /**
     * Get whether a module is previewable or not
     *
     * @param $moduleId
     *
     * @return bool
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function isModulePreviewable($moduleId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT previewable FROM extoriolms_classes_models_module WHERE id = $1 LIMIT 1';
        $row = $db->query($sql, array($moduleId))->fetchRow();
        if($row) {
            return Utilities\PGDB::decodeBool($row[0]);
        } else {
            return false;
        }
    }

    /**
     * Get the owner id of a course
     *
     * @param $courseId
     *
     * @return int|null
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getCourseOwnerId($courseId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT "ownerId" FROM extoriolms_classes_models_course WHERE id = $1 LIMIT 1';
        $row = $db->query($sql,array($courseId))->fetchRow();
        if($row) {
            return intval($row[0]);
        } else {
            return null;
        }
    }

    /**
     * Get the number of feedbacks left for a course
     *
     * @param $courseId
     * @param null $where
     * @param array $params
     *
     * @return int
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getCourseFeedbackCount($courseId, $where = null, $params = array()) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT count(id) FROM extoriolms_classes_models_feedback WHERE
        "courseId" = '.intval($courseId).' AND
        approval = (\'approved\')
        ';
        if(strlen($where)) {
            $sql .= " AND (".$where.") ";
        }
        $row = $db->query($sql,$params)->fetchRow();
        if($row) {
            return intval($row[0]);
        } else {
            return 0;
        }
    }

    /**
     * Subscribe a user to a course
     *
     * @param $userId
     * @param $courseId
     *
     * @throws \Exception
     */
    public static function subscribeToCourse($userId, $courseId) {
        if(CourseSubscriber::findOne(
            Query::n()
                ->where(array(
                    "courseId" => $courseId,
                    "userId" => $userId
                )),1
        )) {
            throw new \Exception("Already subscribed to course");
        }
        $cs = CourseSubscriber::n();
        $cs->courseId = $courseId;
        $cs->userId = $userId;
        $cs->pushThis();
    }

    /**
     * Unsubscribe a user from a course
     *
     * @param $userId
     * @param $courseId
     */
    public static function unsubscribeFromCourse($userId, $courseId) {
        $cs = CourseSubscriber::findOne(
            Query::n()
                ->where(array(
                    "courseId" => $courseId,
                    "userId" => $userId
                )),1
        );
        if($cs) {
            $cs->deleteThis();
        }
    }

    /**
     * Subscribe a user to a course post
     *
     * @param $userId
     * @param $coursePostId
     *
     * @throws \Exception
     */
    public static function subscribeToCoursePost($userId, $coursePostId) {
        if(CoursePostSubscriber::findOne(
            Query::n()
                ->where(array(
                    "userId" => $userId,
                    "coursePostId" => $coursePostId
                )),1
        )) {
            throw new \Exception("Already subscribed to post");
        }
        $cps = CoursePostSubscriber::n();
        $cps->userId = $userId;
        $cps->coursePostId = $coursePostId;
        $cps->pushThis();
    }

    /**
     * Unsubscribe a user from a course post
     *
     * @param $userId
     * @param $coursePostId
     */
    public static function unsubscribeFromCoursePost($userId, $coursePostId) {
        $cps = CoursePostSubscriber::findOne(
            Query::n()
                ->where(array(
                    "userId" => $userId,
                    "coursePostId" => $coursePostId
                )),1
        );
        if($cps) {
            $cps->deleteThis();
        }
    }

    /**
     * Get the name of a post
     *
     * @param $postId
     *
     * @return null
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getPostName($postId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT name FROM extoriolms_classes_models_coursepost WHERE id = $1 LIMIT 1';
        $row = $db->query($sql, array($postId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    /**
     * Get the name of a course
     *
     * @param $courseId
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getCourseName($courseId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT name FROM extoriolms_classes_models_course WHERE id = $1 LIMIT 1';
        $row = $db->query($sql, array($courseId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    /**
     * Get the approval of a course
     *
     * @param $courseId
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getCourseApproval($courseId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT approval FROM extoriolms_classes_models_course WHERE id = $1 LIMIT 1';
        $row = $db->query($sql, array($courseId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    /**
     * Get the name of the course related to an enrolment
     *
     * @param $userCourseId
     *
     * @return string
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getCourseNameByUserCourse($userCourseId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        C.name

        FROM extoriolms_classes_models_user_course UE

        LEFT JOIN extoriolms_classes_models_course C ON
        C.id = UE."courseId"

        WHERE
        UE.id = $1

        LIMIT 1
        ';
        $row = $db->query($sql, array(
            $userCourseId
        ))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    public static function getModuleName($moduleId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        name

        FROM extoriolms_classes_models_module

        WHERE
        id = $1

        LIMIT 1
        ';
        $row = $db->query($sql, array($moduleId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    public static function getModuleNameByUserModule($userModuleId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        M.name

        FROM extoriolms_classes_models_user_module UM

        LEFT JOIN extoriolms_classes_module M ON
        M.id = UM."moduleId"

        WHERE
        UM.id = $1

        LIMIT 1
        ';
        $row = $db->query($sql, array($userModuleId))->fetchRow();
        if($row) {
            return $row[0];
        } else {
            return null;
        }
    }

    /**
     * Get course thumbnails in a bootstrap grid
     *
     * @param CourseThumbnail[] $courseThumbnails
     * @param bool $displayDescriptions
     * @param int|null $columns
     *
     * @return string
     */
    public static function getCourseThumbnailsAsGrid($courseThumbnails,$displayDescriptions=false,$columns=null) {
        $html = "";
        if(is_array($courseThumbnails) && count($courseThumbnails)) {
            if(intval($columns) > 0) {
                $nextRow = 0;
                for($i = 0; $i < count($courseThumbnails); $i++) {
                    if($i == $nextRow) {
                        if($i > 0) {
                            //close the row
                            $html .= "</div>";
                        }
                        $html .= '<div class="row">';
                        $nextRow += $columns;
                    }
                    $html .= '<div class="col-sm-';
                    switch($columns) {
                        case 1 :
                            $html .= 12;
                            break;
                        case 2 :
                            $html .= 6;
                            break;
                        case 3 :
                            $html .= 4;
                            break;
                        case 4 :
                            $html .= 3;
                            break;
                        case 6 :
                            $html .= 2;
                            break;
                        case 12 :
                            $html .= 1;
                            break;
                    }
                    $html .= '">';
                    $html .= $courseThumbnails[$i]->__ToThumbnail($displayDescriptions);
                    $html .= "</div>";
                }
                $html .= "</div>";
            } else {
                $html = "<div class='row'>";
                for($i = 0; $i < count($courseThumbnails); $i++) {
                    $html .= '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">';
                    $html .= $courseThumbnails[$i]->__ToThumbnail($displayDescriptions);
                    $html .= "</div>";
                }
                $html .= "</div>";
            }
        }
        return $html;
    }

    /**
     * Get a single course thumbnail
     *
     * @param $courseId
     *
     * @return CourseThumbnail
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getCourseThumbnail($courseId) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        id,
        name,
        description,
        category,
        "subCategory",
        difficulty,
        "averageRating",
        "totalNumEnrolled",
        "defaultThumbnail",
        tags

        FROM extoriolms_classes_models_course

        WHERE id = $1
        ';
        $row = $db->query($sql, array(intval($courseId)))->fetchAssoc();
        if($row) {
            return Content::getCourseThumbnailFromRow($row);
        } else {
            return null;
        }
    }

    /**
     * Get the count of courses
     *
     * @param string $where
     * @param array $params
     *
     * @return int
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getCourseCount(
        $where,
        $params = array()
    ) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $allWhere = "WHERE approval = ('approved') ";
        if(strlen($where)) {
            $allWhere .= "AND (".$where.")";
        }
        $sql = '
        SELECT count(id)

        FROM extoriolms_classes_models_course

        '.$allWhere;
        $query = $db->query($sql,$params);
        while($row = $query->fetchRow()) {
            return intval($row[0]);
            break;
        }
        return 0;
    }

    /**
     * Get an array of course thumbnails using where
     *
     * @param $where
     * @param array $params
     * @param int $limit
     * @param int $skip
     * @param string $orderBy
     * @param string $orderDirection
     *
     * @return CourseThumbnail[]
     * @throws \Core\Classes\Exceptions\DB_Exception
     */
    public static function getCourseThumbnails(
        $where,
        $params = array(),
        $limit = 50,
        $skip = 0,
        $orderBy = "name",
        $orderDirection = "asc"
    ) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $allWhere = "WHERE approval = ('approved') ";
        if(strlen($where)) {
            $allWhere .= "AND (".$where.")";
        }
        $sql = '
        SELECT

        id,
        name,
        description,
        category,
        "subCategory",
        difficulty,
        "averageRating",
        "totalNumEnrolled",
        "defaultThumbnail",
        tags

        FROM extoriolms_classes_models_course

        '.$allWhere.'

        ORDER BY "'.$orderBy.'" '.$orderDirection.'

        LIMIT '.intval($limit).'
        OFFSET '.intval($skip);
        $query = $db->query($sql,$params);
        $thumbnails = array();
        $tagCache = array();
        while($row = $query->fetchAssoc()) {
            $thumbnails[] = Content::getCourseThumbnailFromRow($row,$tagCache);
        }
        return $thumbnails;
    }

    /**
     * Get an array of course thumbnails by course ids
     *
     * @param $courseIds
     * @param int $limit
     * @param int $skip
     * @param string $orderBy
     * @param string $orderDirection
     *
     * @return CourseThumbnail[]
     */
    public static function getCourseThumbnailsByCourseIds(
        $courseIds,
        $limit = 50,
        $skip = 0,
        $orderBy = "name",
        $orderDirection = "asc"
    ) {
        $where = 'id IN (';
        $in = "";
        foreach($courseIds as $id) {
            $in .= intval($id).",";
        }
        $where .= substr($in, 0, strlen($in)-1).")";
        return Content::getCourseThumbnails($where,array(),$limit,$skip,$orderBy,$orderDirection);
    }

    /**
     * Get an array of course thumbnails by category
     *
     * @param int $categoryId
     * @param int $limit
     * @param int $skip
     * @param string $orderBy
     * @param string $orderDirection
     *
     * @return \ExtorioLMS\Classes\Helpers\CourseThumbnail[]
     */
    public static function getCourseThumbnailByCategory(
        $categoryId,
        $limit = 50,
        $skip = 0,
        $orderBy = "name",
        $orderDirection = "asc"
    ) {
        $where = 'category = $1 OR "subCategory" = $1';
        return Content::getCourseThumbnails($where,array($categoryId),$limit,$skip,$orderBy,$orderDirection);
    }

    /**
     * Get the count of courses by category
     *
     * @param int $categoryId
     *
     * @return int
     */
    public static function getCourseCountByCategory($categoryId) {
        return Content::getCourseCount('category = $1 OR "subCategory" = $1',array($categoryId));
    }

    /**
     * Get an array of course thumbnails by tag id
     *
     * @param $tagId
     * @param int $limit
     * @param int $skip
     * @param string $orderBy
     * @param string $orderDirection
     *
     * @return \ExtorioLMS\Classes\Helpers\CourseThumbnail[]
     */
    public static function getCourseThumbnailsByTagId(
        $tagId,
        $limit = 50,
        $skip = 0,
        $orderBy = "name",
        $orderDirection = "asc"
    ) {
        $where = "$1 = ANY(tags)";
        return Content::getCourseThumbnails($where,array($tagId),$limit,$skip,$orderBy,$orderDirection);
    }

    /**
     * Get the count of courses by tag id
     *
     * @param $tagId
     *
     * @return int
     */
    public static function getCourseCountByTagId($tagId) {
        return Content::getCourseCount("$1 = ANY(tags)",array($tagId));
    }

    /**
     * Get an array of course thumbnails by tag ids
     *
     * @param $tagIds
     * @param int $limit
     * @param int $skip
     * @param string $orderBy
     * @param string $orderDirection
     *
     * @return \ExtorioLMS\Classes\Helpers\CourseThumbnail[]
     */
    public static function getCourseThumbnailsByTagIds(
        $tagIds,
        $limit = 50,
        $skip = 0,
        $orderBy = "name",
        $orderDirection = "asc"
    ) {
        $where = "tags && ARRAY[";
        $in = "";
        foreach($tagIds as $id) {
            $in .= intval($id).",";
        }
        $where .= substr($in, 0, strlen($in)-1)."]";
        return Content::getCourseThumbnails($where,array(),$limit,$skip,$orderBy,$orderDirection);
    }

    /**
     * Get the course count by tag ids
     *
     * @param $tagIds
     *
     * @return int
     */
    public static function getCourseCountByTagIds($tagIds) {
        $where = "tags && ARRAY[";
        $in = "";
        foreach($tagIds as $id) {
            $in .= intval($id).",";
        }
        $where .= substr($in, 0, strlen($in)-1)."]";
        return Content::getCourseCount($where);
    }

    /**
     * Get courses related to a specific course
     *
     * @param $courseId
     * @param int $limit
     * @param int $skip
     * @param string $orderBy
     * @param string $orderDirection
     *
     * @return array|\ExtorioLMS\Classes\Helpers\CourseThumbnail[]
     */
    public static function getCourseThumbnailsByRelatedCourse(
        $courseId,
        $limit = 50,
        $skip = 0,
        $orderBy = "name",
        $orderDirection = "asc"
    ) {
        $course = Course::findById($courseId,1);
        if($course) {
            $where = 'id != $1 AND (category = $2 OR "subCategory" = $3 OR tags && ARRAY[';
            $in = "";
            foreach($course->tags as $tag) {
                $in .= $tag.",";
            }
            $where .= substr($in,0,strlen($in)-1)."])";
            return Content::getCourseThumbnails($where,array($course->id,$course->category,$course->subCategory),$limit,$skip,$orderBy,$orderDirection);
        } else {
            return array();
        }
    }

    /**
     * Get the course count by related course
     *
     * @param $courseId
     *
     * @return int
     */
    public static function getCourseCountByRelatedCourse($courseId) {
        $course = Course::findById($courseId,1);
        if($course) {
            $where = 'id != $1 AND (category = $2 OR "subCategory" = $3 OR tags && ARRAY[';
            $in = "";
            foreach($course->tags as $tag) {
                $in .= $tag.",";
            }
            $where .= substr($in,0,strlen($in)-1)."])";
            return Content::getCourseCount($where,array($course->id,$course->category,$course->subCategory));
        } else {
            return 0;
        }
    }

    public static function getCourseThumbnailsBySearchPhrase(
        $searchPhrase,
        $limit = 50,
        $skip = 0,
        $orderBy = "relevance",
        $orderDirection = "desc"
    ) {
        $db = \Core\Classes\Commons\Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT DISTINCT
        ON (c.id) c.id as id,
        c.name as name,
        c.description as description,
        c.category as category,
        c."subCategory" as "subCategory",
        c.difficulty as difficulty,
        c."averageRating" as "averageRating",
        c."totalNumEnrolled" as "totalNumEnrolled",
        c."defaultThumbnail" as "defaultThumbnail",
        c.tags as tags,
        ts_rank_cd(to_tsvector(c.name), query) + ts_rank_cd(to_tsvector(c.description), query) as "relevance"

        FROM extoriolms_classes_models_course c,
        to_tsquery(\''.$searchPhrase.'\') query

        WHERE
        c.approval = (\'approved\') AND (
            query @@ to_tsvector(c.name) OR
            query @@ to_tsvector(c.description)
        )

        ORDER BY c.id, "'.strval($orderBy).'" '.strval($orderDirection).'

        LIMIT '.intval($limit).'
        OFFSET '.intval($skip).'
        ';
        $query = $db->query($sql);
        $thumbnails = array();
        $tagCache = array();
        $categoryCache = array();
        while($row = $query->fetchAssoc()) {
            $thumbnails[] = Content::getCourseThumbnailFromRow($row,$tagCache,$categoryCache);
        }
        return $thumbnails;
    }

    /**
     * Get a course thumbnail from an associative array
     *
     * @param array $row
     *
     * @return CourseThumbnail
     */
    private static function getCourseThumbnailFromRow($row,&$tagCache=array(),&$categoryCache=array()) {
        return CourseThumbnail::constructFromArray(array(
            "id" => intval($row["id"]),
            "name" => strval($row["name"]),
            "description" => strval($row["description"]),
            "category" => intval($row["category"]),
            "subCategory" => intval($row["subCategory"]),
            "difficulty" => strval($row["difficulty"]),
            "averageRating" => floatval($row["averageRating"]),
            "totalNumEnrolled" => intval($row["totalNumEnrolled"]),
            "defaultThumbnail" => strval($row["defaultThumbnail"]),
            "tags" => Utilities\PGDB::decodeArray($row["tags"])
        ),$tagCache,$categoryCache);
    }

    /**
     * Strip a course based on what the viewing user is able to see
     *
     * @param int $viewingUserId
     * @param Course $course
     *
     * @return \stdClass
     */
    public static function stripCourse($viewingUserId,$course) {
        // if viewing user can modify all courses or
        // if owned by the viewing user,
        // return as is
        if(
            $course->ownerId == $viewingUserId ||
            Utilities\Users::userHasPrivilege($viewingUserId,"courses_modify_all")
        ) {
            return $course->__toStdClass();
        }
        // if enrolled on the course, or the course is self enrollable, can see everything but the certificate template and question answerw
        elseif(
            $course->selfEnrollable ||
            \ExtorioLMS\Classes\Utilities\Users::userEnrolledOnCourse($viewingUserId,$course->id)
        ) {
            if($course->certificateTemplate) {
                $course->certificateTemplate = $course->certificateTemplate->id;
            }
            $lessons = $course->lessons;
            foreach($lessons as $lesson) {
                $modules = $lesson->modules;
                foreach($modules as $module) {
                    $questions = $module->questions;
                    for($i = 0; $i < count($questions); $i++) {
                        $questions[$i]->answers = array();
                    }
                    $module->questions = $questions;
                }
                $lesson->modules = $modules;
            }
            $course->lessons = $lessons;
            return $course->__toStdClass();
        } else {
            //certificate template is never visible
            if($course->certificateTemplate) {
                $course->certificateTemplate = $course->certificateTemplate->id;
            }
            //lectures are visible, lessons are visible, modules are visible, but questions are not
            $lessons = $course->lessons;
            foreach($lessons as $lesson) {
                $modules = $lesson->modules;
                foreach($modules as $module) {
                    $questions = $module->questions;
                    $ids = array();
                    foreach($questions as $question) {
                        $ids[] = $question->id;
                    }
                    $module->questions = $ids;
                }
                $lesson->modules = $modules;
            }
            $course->lessons = $lessons;
            return $course->__toStdClass();
        }
    }
}