<?php
namespace ExtorioLMS\Components\BlockProcessors;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\User;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\Feedback;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * Displays your feedback for a selected course, letting you modify it, or create feedback
 *
 * Class LMSMyCourseFeedback
 */
class LMSMyCourseFeedback extends \Core\Classes\Commons\BlockProcessor {

    public $courseid;

    protected function _onView() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        $config = $this->_getConfig();
        if($config["ExtorioLMS"]["feedback"]["enabled"]) {
            if($loggedInUser) {
                $feedback = Feedback::findOne(
                    Query::n()
                        ->where(array(
                            "courseId" => $this->courseid,
                            "ownerId" => $loggedInUser->id
                        )),1
                );
                $this->displayFeedback($loggedInUser, $feedback ,Users::userEnrolledOnCourse($loggedInUser->id, $this->courseid));
            }
        }
    }

    /**
     * @param User $loggedInUser
     * @param Feedback $feedback
     * @param $isEnrolled
     *
     */
    private function displayFeedback($loggedInUser, $feedback, $isEnrolled) {
        $outer = uniqid();
        ?>
        <div id="<?=$outer?>">

        </div>
        <script>
            var outer = $('#<?=$outer?>');

            var userId = <?=$loggedInUser->id?>;
            var avatar = "<?=$loggedInUser->avatar?>";
            var longname = "<?=$loggedInUser->longname?>";
            var courseId = <?=$this->courseid?>;
            var feedback = <?php
                if($feedback) echo json_encode($feedback); else echo "null";
                ?>;
            var canCreate = true;
            var canModify = true;
            var isEnrolled = <?php
                if($isEnrolled) echo "true"; else echo "false";
                ?>;

            var displayFeedback = function() {
                outer.html('');
                var html = $('<div class="row">' +
                    '       <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">' +
                    '           <img class="img-thumbnail" style="width: 100%;" src="'+avatar+'" /><br /><br />' +
                    '       </div>' +
                    '       <div class="col-xs-9 col-sm-9 col-md-10 col-lg-10">' +
                    '           <select class="my_course_feedback_rating">' +
                    '               <option value="1">1</option>' +
                    '               <option value="2">2</option>' +
                    '               <option value="3">3</option>' +
                    '               <option value="4">4</option>' +
                    '               <option value="5">5</option>' +
                    '           </select>' +
                    '           <p style="display: none;" class="my_course_feedback_date"></p>' +
                    '           <div class="my_course_feedback_comment_outer"></div>' +
                    '       </div>' +
                    '       <div class="col-xs-12"><button style="display: none;" class="my_course_feedback_submit btn btn-block btn-primary" type="button">Submit</button><br /></div>' +
                    '</div>');
                if(feedback) {
                    console.log("test");
                    html.find('.my_course_feedback_date').show().html("last updated: " + $.timeago(feedback.dateUpdated));
                    //if we can modify
                    if(canModify) {
                        html.find('.my_course_feedback_rating').barrating({
                            theme: 'fontawesome-stars',
                            initialRating: feedback.rating,
                            onSelect: function(v,t,e) {
                                submitFeedback();
                            }
                        }).val(feedback.rating);
                        html.find('.my_course_feedback_comment_outer').html('<textarea class="form-control my_course_feedback_comment" placeholder="(optional) leave a comment...">'+feedback.comment+'</textarea><br />');
                        html.find('.my_course_feedback_submit').show().on("click", function() {
                            submitFeedback();
                        });
                    } else {
                        html.find('.my_course_feedback_rating').barrating({
                            theme: 'fontawesome-stars',
                            initialRating: feedback.rating,
                            readonly: true
                        });
                        html.find('.my_course_feedback_comment_outer').html('<p>'+feedback.comment+'</p>');
                    }
                } else {
                    if(canCreate && isEnrolled) {
                        html.find('.my_course_feedback_rating').barrating({
                            theme: 'fontawesome-stars',
                            initialRating: 3,
                            onSelect: function(v,t,e) {
                                submitFeedback();
                            }
                        }).val(3);
                        html.find('.my_course_feedback_comment_outer').html('<textarea class="form-control my_course_feedback_comment" placeholder="(optional) leave a comment..."></textarea><br />');
                        html.find('.my_course_feedback_submit').show().on("click", function() {
                            submitFeedback();
                        });
                    } else {
                        html = "";
                    }
                }
                outer.html(html);
            };
            var submitFeedback = function() {
                $('#<?=$outer?>').extorio_showLoader();
                var url = "";
                if(feedback) {
                    url = "/lms-feedback/" + feedback.id;
                } else {
                    url = "/lms-feedback";
                }
                $.extorio_api({
                    endpoint: url,
                    type: "POST",
                    data: {
                        data: {
                            ownerId: userId,
                            courseId: courseId,
                            rating: $('#<?=$outer?> .my_course_feedback_rating').val(),
                            comment: $('#<?=$outer?> .my_course_feedback_comment').val(),
                            approval: "pending"
                        }
                    },
                    oncomplete: function() {
                        $('#<?=$outer?>').extorio_hideLoader();
                    },
                    onsuccess: function(response) {
                        $('#<?=$outer?>').extorio_hideLoader();
                        feedback = response.data;
                        if(feedback.approval == "approved") {
                            $.extorio_messageSuccess("Feedback saved");
                        } else {
                            $.extorio_messageSuccess("Feedback saved and submitted for approval");
                        }
                        displayFeedback();
                    }
                });
            };
            displayFeedback();
        </script>
        <?php
    }

    protected function _onEdit() {
        $db = $this->_Extorio()->getDbInstanceDefault();
        $sql = 'SELECT id, name FROM extoriolms_classes_models_course ORDER BY name ASC';
        ?>
        <div class="form-group">
            <label for="courseid">Select a course</label>
            <select class="form-control" id="courseid" name="courseid">
                <?php
                $query = $db->query($sql);
                while($row = $query->fetchAssoc()) {
                    ?>
                <option <?php
                if($this->courseid == $row["id"]) echo 'selected="selected"';
                ?> value="<?=$row["id"]?>"><?=$row["name"]?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <?php
    }
}