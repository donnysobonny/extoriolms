<?php
namespace ExtorioLMS\Components\BlockProcessors;
/**
 * Displays a paginated list of the students enrolled on a course
 *
 * Class LMSCourseStudents
 */
class LMSCourseStudents extends \Core\Classes\Commons\BlockProcessor {

    public $courseid;
    public $usersperpage = 10;

    protected function _onView() {
        if(strlen($this->courseid) || $this->courseid > 0) {
            $outer = uniqid();
            $listGroup = uniqid();
            ?>
            <div id="<?=$outer?>">
                <div style="margin-bottom: 0;" class="list-group" >
                    <div style="padding: 5px;" class="list-group-item active">
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td>Page <span class="course_students_page"></span> of <span class="course_students_page_of"></span></td>
                                    <td style="text-align: right;">
                                        <button type="button" class="btn btn-default btn-xs course_students_left"><span class="fa fa-chevron-left"></span></button>&nbsp;
                                        <button type="button" class="btn btn-default btn-xs course_students_right"><span class="fa fa-chevron-right"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="<?=$listGroup?>">

                    </div>
                    <div style="padding: 5px;" class="list-group-item active">
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td>Page <span class="course_students_page"></span> of <span class="course_students_page_of"></span></td>
                                    <td style="text-align: right;">
                                        <button type="button" class="btn btn-default btn-xs course_students_left"><span class="fa fa-chevron-left"></span></button>&nbsp;
                                        <button type="button" class="btn btn-default btn-xs course_students_right"><span class="fa fa-chevron-right"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <script>
                var page = 0;
                var usersPerPage = <?=$this->usersperpage?>;
                var setup = function() {
                    $('#<?=$outer?> .course_students_left').on("click", function() {
                        page -= 1;
                        loadUsers();
                    });
                    $('#<?=$outer?> .course_students_right').on("click", function() {
                        page += 1;
                        loadUsers();
                    });
                };
                var loadUsers = function() {
                    $('#<?=$outer?>').extorio_showLoader();
                    $.ajax({
                        url: "/extorio/apis/lms-users/user_list",
                        type: "GET",
                        dataType: "json",
                        data: {
                            courseId: <?=$this->courseid?>,
                            orderBy: "username",
                            orderDirection: "asc",
                            limit: usersPerPage,
                            skip: page * usersPerPage
                        },
                        error: function(a,b,c) {
                            $('#<?=$outer?>').extorio_hideLoader();
                            $.extorio_messageError(c);
                        },
                        success: function(respose) {
                            $('#<?=$outer?>').extorio_hideLoader();
                            if(respose.error) {
                                $.extorio_messageError(respose.error_message);
                            } else {
                                var listgroup = $('#<?=$listGroup?>');
                                listgroup.html('');
                                if(respose.data.length > 0) {
                                    for(var i = 0; i < respose.data.length; i++) {
                                        var name = "";
                                        if(respose.data[i].firstname.length > 0 && respose.data[i].lastname.length > 0) {
                                            name = respose.data[i].firstname + " " + respose.data[i].lastname;
                                        } else {
                                            name = respose.data[i].username;
                                        }
                                        var item = $('<div style="padding: 5px;" class="list-group-item">' +
                                            '   <table cellspacing="0" cellpadding="0" border="0">' +
                                            '       <tbody>' +
                                            '           <tr>' +
                                            '               <td><img style="max-width: 30px;" class="img-rounded" src="'+respose.data[i].avatar+'" /></td>' +
                                            '               <td style="padding-left: 10px;">'+name+'</td>' +
                                            '           </tr>' +
                                            '       </tbody>' +
                                            '   </table>' +
                                            '</div>');
                                        listgroup.append(item);
                                    }
                                    var currentPage = page + 1;
                                    var totalPages = Math.round(respose.countUnlimited / usersPerPage);
                                    if(totalPages == 0) {
                                        totalPages = 1;
                                    }
                                    $('#<?=$outer?> .course_students_page').html(currentPage);
                                    $('#<?=$outer?> .course_students_page_of').html(totalPages);

                                    if(currentPage == 1) {
                                        $('#<?=$outer?> .course_students_left').attr("disabled", "disabled");
                                    } else {
                                        $('#<?=$outer?> .course_students_left').removeAttr('disabled');
                                    }

                                    if(currentPage < totalPages) {
                                        $('#<?=$outer?> .course_students_right').removeAttr('disabled');
                                    } else {
                                        $('#<?=$outer?> .course_students_right').attr("disabled", "disabled");
                                    }
                                } else {
                                    listgroup.append('<div style="padding: 5px;" class="list-group-item">' +
                                        '   There are no students on this course' +
                                        '</div>');
                                    $('#<?=$outer?> .course_students_right').attr("disabled", "disabled");
                                    $('#<?=$outer?> .course_students_left').attr("disabled", "disabled");
                                    $('#<?=$outer?> .course_students_page').html(1);
                                    $('#<?=$outer?> .course_students_page_of').html(1);
                                }
                            }
                        }
                    });
                };
                setup();
                loadUsers();

            </script>
            <?php
        }
    }

    protected function _onEdit() {
        $db = $this->_Extorio()->getDbInstanceDefault();
        $sql = 'SELECT id, name FROM extoriolms_classes_models_course ORDER BY name ASC';
        $query = $db->query($sql);
        ?>
        <div class="form-group">
            <label for="courseid">Select a course</label>
            <select class="form-control" id="courseid" name="courseid">
                <?php
                while($row = $query->fetchRow()) {
                    ?>
                <option <?php
                if($this->courseid == $row[0]) echo 'selected="selected"';
                ?> value="<?=$row[0]?>"><?=$row[1]?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="usersperpage">Users per page</label>
            <input value="<?=$this->usersperpage?>" type="number" class="form-control" id="usersperpage" name="usersperpage" placeholder="Users per page">
        </div>
        <?php
    }
}