<?php
namespace ExtorioLMS\Components\BlockProcessors;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * This displays a course within a thumbnail. This can be used inline, and is used by other block processors to display a course in a thumbnail.
 *
 * Class LMSCourseThumbnail
 */
class LMSCourseThumbnail extends \Core\Classes\Commons\BlockProcessor {

    public $courseid;
    public $displaydescription = false;

    protected function _onView() {
        if(strlen($this->courseid) && $this->courseid > 0) {
            echo Content::getCourseThumbnail($this->courseid)->__ToThumbnail($this->displaydescription);
        }
    }

    protected function _onEdit() {
        $db = $this->_Extorio()->getDbInstanceDefault();
        $sql = "SELECT id, name FROM extoriolms_classes_models_course WHERE approval = ('approved') ORDER BY name ASC";
        ?>
        <div class="form-group">
            <label for="exampleInputEmail1">Select a course</label>
            <select class="form-control" name="courseid">
                <option value="0">--select a course--</option>
                <?php
                $query = $db->query($sql);
                while($row = $query->fetchAssoc()) {
                    ?>
                    <option <?php
                    if($this->courseid == $row["id"]) echo 'selected="selected"';
                    ?> value="<?=$row["id"]?>"><?=$row["name"]?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->displaydescription) echo 'checked="checked"';
                ?> id="displaydescription" name="displaydescription" type="checkbox"> Display description
            </label>
        </div>
        <?php
    }
}