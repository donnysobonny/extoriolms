<?php
namespace ExtorioLMS\Components\BlockProcessors;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Models\CourseTag;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Displays a list of course tags
 *
 * Class LMSCourseTags
 */
class LMSCourseTags extends \Core\Classes\Commons\BlockProcessor {

    public $displaycounts = false;
    public $displayicons = true;

    protected function _onView() {
        $tags = CourseTag::findAll(
            Query::n()
                ->order(array("name" => "asc")),1
        );
        $url = Server::getRequestURL();
        $url = Strings::startsAndEndsWith($url,"/");
        ?>
        <ul class="nav nav-pills nav-stacked">
            <?php
            if(count($tags) > 0) {
                foreach($tags as $tag) {
                    $turl = "/courses/tags/".$tag->id."/".urlencode($tag->name)."/";
                    ?>
                    <li class="<?php
                    if($turl == $url) echo 'active';
                    ?>">
                        <a href="<?=$turl?>">
                            <?php
                            if($this->displayicons) {
                                ?><span class="fa fa-<?=$tag->icon?>"></span>&nbsp;&nbsp;<?php
                            }
                            ?><?=ucwords($tag->name)?>
                            <?php
                            if($this->displaycounts) {
                                ?>&nbsp;<span class="badge pull-right"><?=Content::getCourseCountByTagId($tag->id)?></span><?php
                            }
                            ?>
                        </a>
                    </li>
                    <?php
                }
            } else {
                ?><li>No tags found.</li><?php
            }
            ?>
        </ul>
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="row">
            <div class="col-xs-6">
                <div class="checkbox">
                    <label>
                        <input <?php
                        if($this->displayicons) echo 'checked="checked"';
                        ?> name="displayicons" type="checkbox"> Display icons
                    </label>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="checkbox">
                    <label>
                        <input <?php
                        if($this->displaycounts) echo 'checked="checked"';
                        ?> name="displaycounts" type="checkbox"> Display counts
                    </label>
                </div>
            </div>
        </div>
        <?php
    }
}