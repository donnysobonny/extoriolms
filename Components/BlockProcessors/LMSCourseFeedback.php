<?php
namespace ExtorioLMS\Components\BlockProcessors;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\Query;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Models\Feedback;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * Display the feedback for a selected course
 *
 * Class LMSCourseFeedback
 */
class LMSCourseFeedback extends \Core\Classes\Commons\BlockProcessor {

    public $courseid;
    public $limit = 5;
    public $ignorenocomment = false;

    protected function _onView() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if($loggedInUser) {
            $viewingUserId = $loggedInUser->id;
        } else {
            $viewingUserId = 0;
        }
        $outer = uniqid();
        $inner = uniqid();
        $loadmore = uniqid();
        ?>
        <div id="<?=$outer?>">
            <div id="<?=$inner?>">

            </div>
            <button id="<?=$loadmore?>" class="btn btn-block btn-default">Load more...</button>
            <br />
        </div>
        <script>
            var viewingUserId = <?=$viewingUserId?>;
            var outer = $('#<?=$outer?>');
            var inner = $('#<?=$inner?>');
            var loadmore = $('#<?=$loadmore?>');
            var courseid = <?=$this->courseid?>;
            var ignoreNoComment = <?php
            if($this->ignorenocomment) echo "true"; else echo "false";
            ?>;
            var limit = <?=$this->limit?>;
            var skip = 0;
            var countLoaded = 0;
            var loadFeedback = function() {
                outer.extorio_showLoader();
                $.extorio_api({
                    endpoint: "/lms-feedback/filter",
                    type: "GET",
                    data: {
                        courseId: courseid,
                        ignoreNoComment: ignoreNoComment,
                        limit: limit,
                        skip: skip
                    },
                    oncomplete: function() {
                        outer.extorio_hideLoader();
                    },
                    onsuccess: function(response) {
                        if(response.data.length <= 0) {
                            //hide the load more button
                            loadmore.hide();
                            //if not loaded any yet
                            if(skip == 0) {
                                inner.html('<p>This course has no feedback yet.</p>');
                            }
                        } else {

                            countLoaded += response.data.length;
                            //if there are no more to load
                            if(countLoaded >= response.countUnlimited) {
                                loadmore.hide();
                            }

                            for(var i = 0; i < response.data.length; i++) {
                                var html = $('<div class="row">' +
                                    '   <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">' +
                                    '       <div class="course_feedback_avatar">' +
                                    '           <img class="img-thumbnail" style="width: 100%;" src="'+response.data[i].ownerAvatar+'" />' +
                                    '       </div><br />' +
                                    '   </div>' +
                                    '   <div class="col-xs-9 col-sm-9 col-md-10 col-lg-10">' +
                                    '           <select class="course_feedback_rating">' +
                                    '               <option value="1">1</option>' +
                                    '               <option value="2">2</option>' +
                                    '               <option value="3">3</option>' +
                                    '               <option value="4">4</option>' +
                                    '               <option value="5">5</option>' +
                                    '           </select>' +
                                    '       <p><span class="course_feedback_longname">'+response.data[i].ownerShortName+'</span> - <small class="course_feedback_date"></small></p>' +
                                    '       <p>'+response.data[i].comment+'</p>' +
                                    '   </div>' +
                                    '</div>');
                                html.find('.course_feedback_date').html($.timeago(response.data[i].dateUpdated));
                                html.find('.course_feedback_rating').barrating({
                                    theme: 'fontawesome-stars',
                                    readonly: true
                                }).barrating('set', response.data[i].rating);
                                inner.append(html);
                            }
                        }
                        skip += limit;
                    }
                });
            };

            $(function() {
                loadmore.on("click", function() {
                    loadFeedback();
                });

                loadFeedback();
            });
        </script>
        <?php
    }

    protected function _onEdit() {
        $db = Extorio::get()->getDbInstanceDefault();
        $sql = 'SELECT id, name FROM extoriolms_classes_models_course ORDER BY name ASC';
        ?>
        <div class="form-group">
            <label for="limit">Number of feedbacks to display before loading more</label>
            <input type="number" class="form-control" id="limit" name="limit" value="<?=$this->limit?>">
        </div>
        <div class="form-group">
            <label for="courseid">Select a course</label>
            <select class="form-control" id="courseid" name="courseid">
                <?php
                $query = $db->query($sql);
                while($row = $query->fetchAssoc()) {
                    ?>
                <option <?php
                if($this->courseid == $row["id"]) echo 'selected="selected"';
                ?> value="<?=$row["id"]?>"><?=$row["name"]?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->ignorenocomment) echo 'checked="checked"';
                ?> name="ignorenocomment" id="ignorenocomment" type="checkbox"> Ignore feedback without any comments
            </label>
        </div>
        <?php
    }
}