<?php
namespace ExtorioLMS\Components\BlockProcessors;
use ExtorioLMS\Classes\Enums\ContentCategory;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Display a set of courses within a category
 *
 * Class LMSCategoryCourses
 */
class LMSCategoryCourses extends \Core\Classes\Commons\BlockProcessor {

    public $category = "";
    public $columns = 3;
    public $limit = 9;
    public $skip = 0;
    public $orderby = "name";
    public $orderdirection = "asc";
    public $displaydescriptions = false;
    public $emptymessage = "<p>No courses exist in this category.</p>";

    protected function _onView() {
        if(strlen($this->category)) {
            $content = Content::getCourseThumbnailsAsGrid(
                Content::getCourseThumbnailByCategory(
                    $this->category,
                    $this->limit,
                    $this->skip,
                    $this->orderby,
                    $this->orderdirection
                ),
                $this->displaydescriptions,
                $this->columns
            );
            if(strlen($content)) {
                echo $content;
                return;
            }
        }
        echo $this->emptymessage;
    }

    protected function _onEdit() {
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-xs-6">
                    <label for="columns">Number of courses in a row</label>
                    <select class="form-control" id="columns" name="columns">
                        <option <?php
                        if($this->columns == 1) echo 'selected="selected"';
                        ?> value="1">1</option>
                        <option <?php
                        if($this->columns == 2) echo 'selected="selected"';
                        ?> value="2">2</option>
                        <option <?php
                        if($this->columns == 3) echo 'selected="selected"';
                        ?> value="3">3</option>
                        <option <?php
                        if($this->columns == 4) echo 'selected="selected"';
                        ?> value="4">4</option>
                        <option <?php
                        if($this->columns == 6) echo 'selected="selected"';
                        ?> value="6">6</option>
                        <option <?php
                        if($this->columns == 12) echo 'selected="selected"';
                        ?> value="12">12</option>
                    </select>
                </div>
                <div class="col-xs-6">
                    <label for="number">Maximum courses to display</label>
                    <input class="form-control" type="number" name="limit" value="<?=$this->limit?>" />
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-xs-6">
                    <label for="orderby">Order courses by</label>
                    <select class="form-control" id="orderby" name="orderby">
                        <option <?php
                        if($this->orderby == "name") echo 'selected="selected"';
                        ?> value="name">Name</option>
                        <option <?php
                        if($this->orderby == "averageRating") echo 'selected="selected"';
                        ?> value="averageRating">Rating</option>
                        <option <?php
                        if($this->orderby == "totalNumEnrolled") echo 'selected="selected"';
                        ?> value="totalNumEnrolled">Number of students</option>
                    </select>
                </div>
                <div class="col-xs-6">
                    <label for="orderdirection">Order direction</label>
                    <select class="form-control" id="orderdirection" name="orderdirection">
                        <option <?php
                        if($this->orderdirection == "asc") echo 'selected="selected"';
                        ?> value="asc">Ascending</option>
                        <option <?php
                        if($this->orderdirection == "desc") echo 'selected="selected"';
                        ?> value="desc">Descending</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->displaydescriptions) echo 'checked="checked"';
                ?> name="displaydescriptions" id="displaydescriptions" type="checkbox"> Display descriptions
            </label>
        </div>
        <div class="form-group">
            <label for="emptymessage">Message to display when no courses found</label>
            <textarea name="emptymessage" id="emptymessage"><?=$this->emptymessage?></textarea>
        </div>
        <script>
            $(function() {
                $('#emptymessage').extorio_editable();
            });
        </script>
        <div class="form-group">
            <label for="category">Selected category</label>
            <select class="form-control" id="category" name="category">
                <?php
                foreach(ContentCategory::values() as $value) {
                    ?>
                <option <?php
                if($this->category == $value) echo 'selected="selected"';
                ?> value="<?=$value?>"><?=$value?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <?php
    }
}