<?php
namespace ExtorioLMS\Components\BlockProcessors;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * A user menu containing links to the main sections of the lms
 *
 * Class LMSUserMenu
 */
class LMSUserMenu extends \Core\Classes\Commons\BlockProcessor {

    public $displaycategories = true;
    public $displaytags = true;

    protected function _onView() {
        $uid = uniqid();
        $url = $this->_Extorio()->getCurrentUrl();
        $first = $url->getField(0);
        $second = $url->getField(1);
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        ?>
        <nav class="navbar navbar-default navbar-static-top" style="">
            <div class="container-fluid">
                <div class="navbar-header"><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#<?=$uid?>" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button></div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div id="<?=$uid?>" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown<?php
                        if($first == "courses") echo ' active';
                        ?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-bars"></span> Browse Courses</a>
                            <ul class="dropdown-menu">
                                <li><a href="/courses/"><span class="fa fa-th"></span> All Courses</a></li>
                                <?php
                                $db = $this->_Extorio()->getDbInstanceDefault();
                                if($this->displaycategories) {
                                    ?>
                                    <li class="dropdown-header">Categories</li>
                                    <?php
                                    $sql = 'SELECT id, name, icon FROM extoriolms_classes_models_coursecategory WHERE "parentId" = 0 ORDER BY name ASC';
                                    $query = $db->query($sql);
                                    while($row = $query->fetchAssoc()) {
                                        ?><li><a href="/courses/categories/<?=$row["id"]?>/<?=urlencode($row["name"])?>"><span class="fa fa-<?=$row["icon"]?>"></span> <?=ucwords($row["name"])?></a></li><?php
                                    }
                                }
                                if($this->displaytags) {
                                    ?><li class="dropdown-header">Tags</li><?php
                                    $sql = 'SELECT id, name, icon FROM extoriolms_classes_models_coursetag ORDER BY name ASC';
                                    $query = $db->query($sql);
                                    while($row = $query->fetchAssoc()) {
                                        ?><li><a href="/courses/tags/<?=$row["id"]?>/<?=urlencode($row["name"])?>"><span class="fa fa-<?=$row["icon"]?>"></span> <?=ucwords($row["name"])?></a></li><?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                    </ul>
                    <form method="get" action="/courses/search" class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="csearch" value="<?=$_GET["csearch"]?>" placeholder="Search courses...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button"><span class="fa fa-search"></span></button>
                                </span>
                            </div>
                        </div>
                    </form>
                    <?php
                    if($loggedInUser) {
                        ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <img style="max-height: 20px;" class="img-circle" src="<?=$loggedInUser->avatar?>" />
                                    <?=$loggedInUser->smartShortName()?>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li <?php
                                    if($first == "my-courses") echo 'class="active"';
                                    ?>><a href="/my-courses/"><span class="fa fa-th"></span> My Courses</a></li>
                                    <li><a href="/user/"><span class="fa fa-user"></span> Account</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/user/logout"><span class="fa fa-sign-out"></span> Log out</a></li>
                                </ul>
                            </li>
                        </ul>
                        <?php

                    } else {
                        ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="<?php
                            if($first == "user" && $second == "login") echo 'active';
                            ?>"><a href="/user/login"><span class="fa fa-sign-in"></span> Sign In</a></li>
                            <li class="<?php
                            if($first == "user" && $second == "register") echo 'active';
                            ?>"><a href="/user/register"><span class="fa fa-user-plus"></span> Sign Up</a></li>
                        </ul>
                        <?php
                    }
                    ?>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->displaycategories) echo 'checked="checked"';
                ?> name="displaycategories" type="checkbox"> Display categories in the "browse courses" dropdown
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->displaytags) echo 'checked="checked"';
                ?> name="displaytags" type="checkbox"> Display tags in the "browse courses" dropdown
            </label>
        </div>
        <?php
    }
}