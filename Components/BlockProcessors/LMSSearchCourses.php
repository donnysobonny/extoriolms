<?php
namespace ExtorioLMS\Components\BlockProcessors;
/**
 * Displays a search form to search for courses
 *
 * Class LMSSearchCourses
 */
class LMSSearchCourses extends \Core\Classes\Commons\BlockProcessor {

    public $label = "Course Finder";
    public $placeholder = "Search for a course...";
    public $size = "large";
    public $buttontext = "GO!";

    protected function _onView() {
        $uid = uniqid();
        ?>
        <form method="get" action="/courses/search">
            <div class="input-group <?php
            switch($this->size) {
                case "large" :
                    echo "input-group-lg";
                    break;
                case "small" :
                    echo "input-group-sm";
                    break;
            }
            ?>">
                <?php
                if(strlen($this->label)) {
                    ?><span class="input-group-addon" id="<?=$uid?>"><?=$this->label?></span><?php
                }
                ?>
                <input value="<?=$_GET["csearch"]?>" id="csearch" name="csearch" type="text" class="form-control" aria-describedby="<?=$uid?>" placeholder="<?=$this->placeholder?>">
                <span class="input-group-btn">
                <button class="btn btn-primary" type="submit"><?php
                    if(strlen($this->buttontext)) {
                        echo $this->buttontext;
                    } else {
                        ?><span class="fa fa-search"></span><?php
                    }
                    ?></button>
              </span>
            </div>
        </form>
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="form-group">
            <label for="size">Size</label>
            <select class="form-control" name="size" id="size">
                <option <?php
                if($this->size == "small") echo 'selected="selected"';
                ?> value="small">small</option>
                <option <?php
                if($this->size == "medium") echo 'selected="selected"';
                ?> value="medium">medium</option>
                <option <?php
                if($this->size == "large") echo 'selected="selected"';
                ?> value="large">large</option>
            </select>
        </div>
        <div class="form-group">
            <label for="label">Label</label>
            <input type="text" class="form-control" id="label" name="label" placeholder="Label" value="<?=$this->label?>">
        </div>
        <div class="form-group">
            <label for="placeholder">Placeholder</label>
            <input type="text" class="form-control" id="placeholder" name="placeholder" placeholder="Placeholder" value="<?=$this->placeholder?>">
        </div>
        <div class="form-group">
            <label for="buttontext">Button text</label>
            <input type="text" class="form-control" id="buttontext" name="buttontext" placeholder="Button text" value="<?=$this->buttontext?>">
        </div>
        <?php
    }
}