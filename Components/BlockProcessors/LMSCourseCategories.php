<?php
namespace ExtorioLMS\Components\BlockProcessors;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Models\CourseCategory;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Display course categories.
 *
 * Class LMSCourseCategories
 */
class LMSCourseCategories extends \Core\Classes\Commons\BlockProcessor {

    public $displaycounts = false;
    public $displayicons = true;

    protected function _onView() {
        $categories = CourseCategory::getAllCategories();
        $url = Server::getRequestURL();
        $url = Strings::startsAndEndsWith($url,"/");
        ?>
        <ul class="nav nav-pills nav-stacked">
            <?php
            if(count($categories) > 0) {
                foreach($categories as $category) {
                    $curl = "/courses/categories/".$category->id."/".urlencode($category->name)."/";
                    ?>
                    <li class="<?php
                    if($curl == $url) echo 'active';
                    ?>">
                        <a href="<?=$curl?>">
                            <?php
                            if($this->displayicons) {
                                ?><span class="fa fa-<?=$category->icon?>"></span>&nbsp;&nbsp;<?php
                            }
                            ?><?=ucwords($category->name)?>
                            <?php
                            if($this->displaycounts) {
                                ?>&nbsp;<span class="badge pull-right"><?=Content::getCourseCountByCategory($category->id)?></span><?php
                            }
                            ?>
                        </a>
                    </li>
                    <?php
                    if(count($category->subCategories)) {
                        foreach($category->subCategories as $subCategory) {
                            $curl = "/courses/categories/".$subCategory->id."/".urlencode($subCategory->name)."/";
                            ?>
                            <li class="<?php
                            if($curl == $url) echo 'active';
                            ?>">
                                <a href="<?=$curl?>">
                                    <?php
                                    if($this->displayicons) {
                                        ?><span class="fa fa-<?=$subCategory->icon?> gutter_margin_left"></span>&nbsp;&nbsp;<?php
                                    }
                                    ?>
                                    <?=ucwords($subCategory->name)?>
                                    <?php
                                    if($this->displaycounts) {
                                        ?>&nbsp;<span class="badge pull-right"><?=Content::getCourseCountByCategory($subCategory->id)?></span><?php
                                    }
                                    ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                }
            } else {
                ?><li>No categories found.</li><?php
            }
            ?>
        </ul>
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="row">
            <div class="col-xs-6">
                <div class="checkbox">
                    <label>
                        <input <?php
                        if($this->displayicons) echo 'checked="checked"';
                        ?> name="displayicons" type="checkbox"> Display icons
                    </label>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="checkbox">
                    <label>
                        <input <?php
                        if($this->displaycounts) echo 'checked="checked"';
                        ?> name="displaycounts" type="checkbox"> Display counts
                    </label>
                </div>
            </div>
        </div>
        <?php
    }
}