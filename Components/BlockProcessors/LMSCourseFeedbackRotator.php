<?php
namespace ExtorioLMS\Components\BlockProcessors;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\User;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\Feedback;

/**
 * Displays the most recent course feedbacks in rotation
 *
 * Class LMSCourseFeedbackRotator
 */
class LMSCourseFeedbackRotator extends \Core\Classes\Commons\BlockProcessor {

    public $limit = 5;
    public $minrating = 3;
    public $ignorenocomment = true;

    protected function _onView() {
        $where = array(
            "type" => ContentType::_course,
            "approval" => ContentApproval::_approved,
            "rating" => array(
                Query::_gte => $this->minrating
            )
        );
        if($this->ignorenocomment) {
            $where[] = array("comment" => array(
                Query::_ne => ""
            ));
        }

        $feedbacks = Feedback::findAll(
            Query::n()
                ->where($where)
                ->limit($this->limit)
                ->order(array("dateUpdated" => "desc"))
            ,1
        );
        ?>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                for($i = 0; $i < count($feedbacks); $i++) {
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?=$i?>" class="<?php
                    if($i == 0) {
                        echo "active";
                    }
                    ?>"></li>
                    <?php
                }
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php
                $n = 0;
                foreach ($feedbacks as $feedback) {
                    $user = User::findById($feedback->ownerId,1);
                    $course = Course::findById($feedback->courseId,1);
                    ?>
                    <div class="item<?php
                    if($n == 0) echo ' active';
                    ?>">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                            <div>
                                <img class="img-thumbnail" style="width: 100%" src="<?=$user->avatar?>" />
                            </div><br />
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-10">
                            <select data-rating="<?=$feedback->rating?>" class="course_feedback_rotator_rating">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <p>
                                <a href="/courses/<?=$course->id?>/<?=urlencode($course->name)?>"><?=$course->name?></a> -
                                <?=$user->longname?> -
                                <small data-date="<?=$feedback->dateUpdated?>" class="course_feedback_rotator_date"></small>
                            </p>
                            <p><?=$feedback->comment?></p>
                        </div>
                    </div>
                    <?php
                    $n++;
                }
                ?>
            </div>

            <!-- Controls -->
            <a style="background: none;" class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span style="margin-left: 0px; left: 3px;" class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a style="background: none;" class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span style="margin-right: 0px; right: 3px;" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <script>
            $(function() {
                $('.course_feedback_rotator_rating').each(function() {
                    var self = $(this);
                    self.barrating({
                        theme: 'fontawesome-stars',
                        readonly: true,
                        initialRating: self.attr('data-rating')
                    });
                });
                $('.course_feedback_rotator_date').each(function() {
                    var self = $(this);
                    self.html($.timeago(self.attr('data-date')));
                });
            })
        </script>
        <?php
    }

    protected function _onEdit() {
        ?>
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="limit">Maximum number of feedbacks to display</label>
                    <input type="number" class="form-control" id="limit" name="limit" value="<?=$this->limit?>">
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="minrating">Miniumum feedback rating</label>
                    <select class="form-control" name="minrating" id="minrating">
                        <option <?php
                        if($this->minrating == 1) echo 'selected="selected"';
                        ?> value="1">1</option>
                        <option <?php
                        if($this->minrating == 2) echo 'selected="selected"';
                        ?> value="2">2</option>
                        <option <?php
                        if($this->minrating == 3) echo 'selected="selected"';
                        ?> value="3">3</option>
                        <option <?php
                        if($this->minrating == 4) echo 'selected="selected"';
                        ?> value="4">4</option>
                        <option <?php
                        if($this->minrating == 5) echo 'selected="selected"';
                        ?> value="5">5</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="checkbox">
            <label>
                <input <?php
                if($this->ignorenocomment) echo 'checked="checked"';
                ?> name="ignorenocomment" id="ignorenocomment" type="checkbox"> Ignore feedback without any comments
            </label>
        </div>
        <?php
    }
}