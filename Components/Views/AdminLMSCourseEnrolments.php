<?php
namespace ExtorioLMS\Components\Views;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Manage course enrolments
 *
 * Class AdminLMSCourseEnrolments
 */
class AdminLMSCourseEnrolments extends \ExtorioLMS\Components\Controllers\AdminLMSCourseEnrolments {
    public function _onDefault($id = false) {
        if($id) {
            $this->_includeIncluder("datepicker");
            ?>
            <div class="alert alert-info">
                This enrolment has the status: <?=Content::getUserCourseStatusHtml($this->userCourse->status)?>
            </div>
            <p>
                TODO: Display useful info about this enrolment
            </p>
            <table style="width: auto;" class="table table-condensed table-striped" border="0">
                <tbody>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Date started</td>
                        <td><?=Strings::dateToTimeAgo($this->userCourse->dateStarted)?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Date updated</td>
                        <td><?=Strings::dateToTimeAgo($this->userCourse->dateUpdated)?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Date passed</td>
                        <td><?=Strings::dateToTimeAgo($this->userCourse->datePassed)?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Expiry</td>
                        <td>
                            <form method="post" action="">
                                <div class="input-group input-group-sm">
                                    <input value="<?=$this->userCourse->expiryDate?>" type="text" class="form-control" name="expiry" id="expiry" placeholder="No expiry set...">
                                    <span class="input-group-btn">
                                        <button name="updated" class="btn btn-primary" type="submit"><span class="fa fa-save"></span> update</button>
                                    </span>
                                </div>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Current points</td>
                        <td><?=$this->userCourse->currentPoints?>/<?=$this->course->maximumPoints?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Certificate</td>
                        <td><?php
                            if(strlen($this->userCourse->certificate)) {
                                ?>
                                <a target="_blank" href="<?=$this->userCourse->certificate?>"><span class="fa fa-eye"></span> view</a>
                                <?php
                            } else {
                                echo "N/A";
                            }
                            ?></td>
                    </tr>
                </tbody>
            </table>
            <?php
            if($this->userCourse->status != ContentStatus::_pending) {
                ?><button class="btn btn-xs btn-default enrolment_detail"><span class="fa fa-eye"></span> view progress</button><?php
            }
            ?>
            <button class="btn btn-xs btn-danger enrolment_delete"><span class="fa fa-trash"></span> delete</button>
            <script>
                $(function() {
                    $('#expiry').datepicker({
                        format: "yyyy-mm-dd 23:59:59",
                        autoclose: true
                    });

                    $('.enrolment_delete').on("click", function() {
                        $.extorio_modal({
                            title: "Delete enrolment",
                            content: "Are you sure that you want to delete this enrolment? This will also delete all work associated with this enrolment.",
                            closetext: "cancel",
                            conteinuetext: "delete",
                            oncontinuebutton: function() {
                                window.location.href = "<?=$this->_getUrlToDefault(array($id,"delete"))?>";
                            }
                        });
                    });

                    $('.enrolment_detail').on("click", function() {
                        $.extorio_dialog({
                            title: "View progress",
                            content: "Loading...",
                            size: "modal-lg",
                            onopen: function() {
                                var body = this.find('.modal-body');
                                body.extorio_showLoader();
                                $.extorio_api({
                                    endpoint: "/lms-user-courses/<?=$this->userCourse->id?>/detail_view",
                                    oncomplete: function() {
                                        body.extorio_hideLoader();
                                    },
                                    onsuccess: function(resp) {
                                        body.html(resp.data);
                                    }
                                })
                            }
                        })
                    });
                });
            </script>
            <?php
        } else {
            $this->f->displayFiltering();
            $this->f->displaySearching();
            ?>
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>User</th>
                    <th>Course</th>
                    <th>Status</th>
                    <th>Last action</th>
                    <th>Expiry</th>
                    <th>Progress</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if($this->courseEnrolments->numRows() > 0) {
                    while($row = $this->courseEnrolments->fetchAssoc()) {
                        ?>
                        <tr>
                            <td><a href="<?=Users::getEditUrl($row["userId"])?>"><?=$row["longname"]?></a></td>
                            <td><?=$row["name"]?></td>
                            <td><?=Content::getUserCourseStatusHtml($row["status"])?></td>
                            <td><?=Strings::dateToTimeAgo($row["dateUpdated"])?></td>
                            <td><?=Strings::dateToTimeAgo($row["expiryDate"])?></td>
                            <td>
                                <?php
                                $prog = round((intval($row["currentPoints"]) / intval($row["maximumPoints"])) * 100);
                                ?>
                                <div style="margin-bottom: 0;" class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="<?=$prog?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2px; width: <?=$prog?>%;">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a class="btn btn-xs btn-default" href="<?=$this->_getUrlToDefault(array($row["id"]))?>"><span class="fa fa-eye"></span> view</a>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="7">No enrolments found</td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
            $this->f->displayPagination();
        }
    }

    public function create() {
        $this->_includeIncluder("select");
        ?>
        <form method="post" action="">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="user">Select a user</label>
                        <select data-live-search="true" class="selectpicker form-control" name="user" id="user">
                            <?php
                            while($row = $this->userData->fetchAssoc()) {
                                ?><option value="<?=$row["id"]?>"><?=$row["longname"]?></option><?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="course">Select a course</label>
                        <select data-live-search="true" class="selectpicker form-control" name="course" id="course">
                            <?php
                            while($row = $this->courseData->fetchAssoc()) {
                                ?><option value="<?=$row["id"]?>"><?=$row["name"]?></option><?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" name="submit" class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
            <button type="submit" name="submit_exit" class="btn btn-info"><span class="fa fa-sign-out"></span> Save and exit</button>
        </form>
        <?php
    }
}