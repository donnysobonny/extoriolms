<?php
namespace ExtorioLMS\Components\Views;
use Core\Classes\Utilities\PGDB;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Manage course posts
 *
 * Class AdminLMSCoursePosts
 */
class AdminLMSCoursePosts extends \ExtorioLMS\Components\Controllers\AdminLMSCoursePosts {
    public function _onDefault($id = false) {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Content</th>
                    <th>User</th>
                    <th>Course</th>
                    <th>Approval</th>
                    <th>Sticky</th>
                    <th>Created</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($this->postData->numRows() > 0) {
                    while($row = $this->postData->fetchAssoc()) {
                        ?>
                <tr>
                    <td>
                        <?php
                        if(strlen($row["mainName"])) {
                            ?>RE: <?=$row["mainName"]?><?php
                        } else {
                            echo $row["name"];
                        }
                        ?>
                    </td>
                    <td>
                        <button data-target="#course_post_content_<?=$row["id"]?>" class="btn btn-xs btn-default view_course_post"><span class="fa fa-eye"></span> view content</button>
                        <div style="display: none;" id="course_post_content_<?=$row["id"]?>">
                            <?=$row["body"]?>
                        </div>
                    </td>
                    <td><?=$row["longname"]?></td>
                    <td><?=$row["courseName"]?></td>
                    <td><?=Content::getPostApprovalHtml($row["approval"])?></td>
                    <td><?php
                        if(PGDB::decodeBool($row["sticky"])) {
                            ?><span class="fa fa-check"></span><?php
                        } else {
                            ?><span class="fa fa-close"></span><?php
                        }
                        ?></td>
                    <td><?=Strings::dateToTimeAgo($row["dateCreated"])?></td>
                    <td>
                        <?php
                        if($row["approval"] != ContentApproval::_approved) {
                            ?>
                            <button data-id="<?=$row["id"]?>" class="btn btn-xs btn-success course_post_approve"><span class="fa fa-check"></span> approve</button>
                            <?php
                        }
                        if($row["approval"] != ContentApproval::_closed) {
                            ?>
                            <button data-id="<?=$row["id"]?>" class="btn btn-xs btn-warning course_post_close"><span class="fa fa-close"></span> close</button>
                            <?php
                        }
                        if($row["approval"] != ContentApproval::_cancelled) {
                            ?>
                            <button data-id="<?=$row["id"]?>" class="btn btn-xs btn-danger course_post_cancel"><span class="fa fa-close"></span> cancel</button>
                            <?php
                        }
                        if(PGDB::decodeBool($row["sticky"])) {
                            ?>
                            <button data-id="<?=$row["id"]?>" class="btn btn-xs btn-default course_post_unsticky"><span class="fa fa-sticky-note-o"></span> unsticky</button>
                            <?php
                        } else {
                            ?>
                            <button data-id="<?=$row["id"]?>" class="btn btn-xs btn-default course_post_sticky"><span class="fa fa-sticky-note-o"></span> sticky</button>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="8">No posts found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.course_post_approve').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Approve post",
                        content: "Approve this post?",
                        oncontinuebutton: function() {
                            window.location.href = "/extorio-admin/lms-course-posts/approve/" + id;
                        }
                    });
                });
                $('.course_post_close').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Close post",
                        content: "Close this post? Doing so will mean replies can no longer be made to this post.",
                        oncontinuebutton: function() {
                            window.location.href = "/extorio-admin/lms-course-posts/close/" + id;
                        }
                    });
                });
                $('.course_post_cancel').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Cancel post",
                        content: "Cancel this post? Doing so will close this post off from the public.",
                        oncontinuebutton: function() {
                            window.location.href = "/extorio-admin/lms-course-posts/cancel/" + id;
                        }
                    });
                });
                $('.course_post_unsticky').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Unsticky post",
                        content: "Unsticky this post?",
                        oncontinuebutton: function() {
                            window.location.href = "/extorio-admin/lms-course-posts/unsticky/" + id;
                        }
                    });
                });
                $('.course_post_sticky').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Sticky post",
                        content: "Sticky this post?",
                        oncontinuebutton: function() {
                            window.location.href = "/extorio-admin/lms-course-posts/sticky/" + id;
                        }
                    });
                });

                $('.view_course_post').on("click", function() {
                    var content = $($(this).attr("data-target")).html();
                    $.extorio_dialog({
                        title: "View course post",
                        content: content,
                        size: "modal-lg"
                    });
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }
}