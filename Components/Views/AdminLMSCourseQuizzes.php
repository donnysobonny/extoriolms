<?php
namespace ExtorioLMS\Components\Views;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\QuestionType;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Manage course quizzes
 *
 * Class AdminLMSCourseQuizzes
 */
class AdminLMSCourseQuizzes extends \ExtorioLMS\Components\Controllers\AdminLMSCourseQuizzes {
    public function _onDefault($id = false) {
        if(!$id) {
            $this->f->displayFiltering();
            $this->f->displaySearching();
            ?>
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>User</th>
                    <th>Course</th>
                    <th>Module</th>
                    <th>Attempt</th>
                    <th>Points</th>
                    <th>Status</th>
                    <th>Last Action</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if($this->quizData->numRows() > 0) {
                    while($row = $this->quizData->fetchAssoc()) {
                        ?>
                        <tr>
                            <td><?=$row["longname"]?></td>
                            <td><?=$row["coursename"]?></td>
                            <td><?=$row["modulename"]?></td>
                            <td><?=$row["currentAttempts"]?></td>
                            <td><?=$row["currentPoints"]?>/<?=$row["maximumPoints"]?></td>
                            <td><?=Content::getUserModuleStatusHtml($row["status"])?></td>
                            <td><?=Strings::dateToTimeAgo($row["dateUpdated"])?></td>
                            <td>
                                <a class="btn btn-xs btn-default" href="<?=$this->_getUrlToDefault(array($row["id"]))?>"><span class="fa fa-eye"></span> view</a>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="8">No quizzes found</td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
            $this->f->displayPagination();
        } else {
            $this->_includeIncluder("extorio_editable");
            switch($this->userModule->status) {
                case ContentStatus::_pending:
                    ?>
                    <div class="alert alert-info">
                        This users's quiz module is <?=Content::getUserModuleStatusHtml($this->userModule->status,"pending")?>.
                    </div>
                    <?php
                    break;
                case ContentStatus::_started:
                    ?>
                    <div class="alert alert-info">
                        The user has <?=Content::getUserModuleStatusHtml($this->userModule->status,"started")?> this quiz module.
                    </div>
                    <?php
                    break;
                case ContentStatus::_submitted:
                    ?>
                    <div class="alert alert-info">
                        The user has <?=Content::getUserModuleStatusHtml($this->userModule->status,"submitted")?> this quiz module for review.
                    </div>
                    <?php
                    break;
                case ContentStatus::_failed:
                    ?>
                    <div class="alert alert-info">
                        The user has <?=Content::getUserModuleStatusHtml($this->userModule->status,"failed")?> this quiz module.
                    </div>
                    <?php
                    break;
                case ContentStatus::_passed:
                    ?>
                    <div class="alert alert-info">
                        The user has <?=Content::getUserModuleStatusHtml($this->userModule->status,"passed")?> this quiz module.
                    </div>
                    <?php
                    break;
            }
            ?>
            <table style="width: auto;" class="table table-striped table-condensed">
                <tbody>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Course</td>
                        <td><?=Content::getCourseName($this->userModule->courseId)?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Module</td>
                        <td><?=Content::getModuleName($this->userModule->moduleId)?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Last update</td>
                        <td><?=Strings::dateToTimeAgo($this->userModule->dateUpdated)?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Date passed</td>
                        <td><?=Strings::dateToTimeAgo($this->userModule->datePassed)?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Current points</td>
                        <td><?=$this->userModule->currentPoints?>/<?=$this->module->maximumPoints?></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; text-align: right;">Current Attempts</td>
                        <td><?=$this->userModule->currentAttempts?></td>
                    </tr>
                </tbody>
            </table>
            <h3>Quiz Review</h3>
            <?=$this->module->quizDescription?>
            <?php
            for($i = 0; $i < count($this->module->questions); $i++) {
                $question = $this->module->questions[$i];
                $userQuestion = $this->userModule->userQuestions[$i];
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Question <?=($i+1)?> <small>(<?=$userQuestion->status?>)</small></h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        $questions = $question->questions;
                        $answers = $question->answers;
                        $userAnswers = $userQuestion->userAnswers;
                        switch($question->type) {
                            case QuestionType::_multipleChoice:
                                ?>
                                <p><strong>Question</strong></p>
                                <p><em><small><?=$questions["question"]?></small></em></p>
                                <p><strong>Selected answer</strong></p>
                                <p><em><small><?=$questions["options"][$userAnswers[0]]?></small></em></p>
                                <?php
                                break;
                            case QuestionType::_multipleResponse:
                                ?>
                                <p><strong>Question</strong></p>
                                <p><em><small><?=$questions["question"]?></small></em></p>
                                <p><strong>Selected answers</strong></p>
                                <?php
                                foreach($userAnswers as $answer) {
                                    ?><p><em><small><?=$questions["options"][$answer]?></small></em></p><?php
                                }
                                ?>
                                <?php
                                break;
                            case QuestionType::_trueOrFalse:
                                ?>
                                <p><strong>Question</strong></p>
                                <p><em><small><?=$questions[0]?></small></em></p>
                                <p><strong>Selected answer</strong></p>
                                <p><em><small><?php
                                            if($userAnswers[0]) echo "True"; else echo "False";
                                            ?></small></em></p>
                                <?php
                                break;
                            case QuestionType::_fillInTheBlanks:
                                ?>
                                <p><strong>Answer</strong></p>
                                <p><em><small><?php
                                            $n = 0;
                                            foreach($questions["words"] as $word) {
                                                if(is_array($word)) {
                                                    echo '<strong>'.$word[$userAnswers[$n]].'</strong> ';
                                                    $n++;
                                                } else {
                                                    echo $word.' ';
                                                }
                                            }
                                            ?></small></em></p>
                                <?php
                                break;
                            case QuestionType::_multilineText:
                                ?>
                                <p><strong>Question</strong></p>
                                <p><em><small><?=$questions[0]?></small></em></p>
                                <p><strong>Answer</strong></p>
                                <p><em><small><?=$userAnswers[0]?></small></em></p>
                                <?php
                                if($userQuestion->status == ContentStatus::_started) {
                                    ?>
                                    <div style="margin-bottom: 0;" class="panel panel-default">
                                        <div class="panel-heading">
                                            Response
                                        </div>
                                        <div class="panel-body">
                                            <form method="post" action="">
                                                <input type="hidden" name="user_question_id" value="<?=$userQuestion->id?>">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select class="form-control" name="user_question_status">
                                                        <option value="passed">passed</option>
                                                        <option value="failed">failed</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Feedback</label>
                                                    <textarea name="user_question_response" class="user_question_response"></textarea>
                                                </div>
                                                <button class="btn btn-sm btn-primary" type="submit" name="user_question_submit"><span class="fa fa-save"></span> Save</button>
                                                <button class="btn btn-sm btn-info" type="submit" name="user_question_submit_exit"><span class="fa fa-sign-out"></span> Save and exit</button>
                                            </form>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    if(strlen($userQuestion->response)) {
                                        ?>
                                        <div style="margin-bottom: 0;" class="panel panel-default">
                                            <div class="panel-heading">
                                                Response
                                            </div>
                                            <div class="panel-body">
                                                <?=$userQuestion->response?> <small>by <?=Users::getLongname($userQuestion->responseById)?> <?=Strings::dateToTimeAgo($userQuestion->responseDate)?></small>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                break;
                            case QuestionType::_fileSubmission:
                                echo "TODO: display the uploaded file?<br /><br />";
                                if($userQuestion->status == ContentStatus::_started) {
                                    ?>
                                    <div style="margin-bottom: 0;" class="panel panel-default">
                                        <div class="panel-heading">
                                            Response
                                        </div>
                                        <div class="panel-body">
                                            <form method="post" action="">
                                                <input type="hidden" name="user_question_id" value="<?=$userQuestion->id?>">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select class="form-control" name="user_question_status">
                                                        <option value="passed">passed</option>
                                                        <option value="failed">failed</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Feedback</label>
                                                    <textarea name="user_question_response" class="user_question_response"></textarea>
                                                </div>
                                                <button class="btn btn-sm btn-primary" type="submit" name="user_question_submit"><span class="fa fa-save"></span> Save</button>
                                                <button class="btn btn-sm btn-info" type="submit" name="user_question_submit_exit"><span class="fa fa-sign-out"></span> Save and exit</button>
                                            </form>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    if(strlen($userQuestion->response)) {
                                        ?>
                                        <div style="margin-bottom: 0;" class="panel panel-default">
                                            <div class="panel-heading">
                                                Response
                                            </div>
                                            <div class="panel-body">
                                                <?=$userQuestion->response?> <small>by <?=Users::getLongname($userQuestion->responseById)?> <?=Strings::dateToTimeAgo($userQuestion->responseDate)?></small>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                break;
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>
            <script>
                $(function() {
                    $('.user_question_response').extorio_editable_basic();
                });
            </script>
            <?php
        }
    }
}