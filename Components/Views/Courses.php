<?php
namespace ExtorioLMS\Components\Views;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Server;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Enums\LectureType;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\CourseFile;
use ExtorioLMS\Classes\Models\User_Course;
use ExtorioLMS\Classes\Models\User_Enrolment;
use ExtorioLMS\Classes\Utilities\Content;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class Courses
 */
class Courses extends \ExtorioLMS\Components\Controllers\Courses {
    public function _onDefault($id = false, $name = false) {
        $config = $this->_getConfig();
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        $viewingUserId = 0;
        if($loggedInUser) {
            $viewingUserId = $loggedInUser->id;
        }
        if($id) {
            /*
             * DISPLAYING SINGLE COURSE
             */
            $courseFile = CourseFile::findById($this->course->defaultFileId,1);
            ?>
            <div class="row">

                    <h3 style="margin: 0;"><?=$this->course->name?></h3>
                    <span class="text-muted">by <a href=""><?=$this->author->longname?></a></span>

                    <p></p>
                    <div class="row">
                        <div class="col-sm-6">
                            <img style="width: 100%;" src="<?php
                            if($courseFile) {
                                echo $courseFile->privateUrl(60*2);
                            } else {
                                echo $config["ExtorioLMS"]["courses"]["default_image"];
                            }
                            ?>">
                        </div>
                        <div style="padding-left: 35px; padding-right: 35px;" class="col-sm-6">
                            
                               <div class="row" style="padding-top:10px;">
                                <div class="col-xs-4">
                                    <h4 style="margin: 0;" class="text-muted"><span class="fa fa-thumbs-up"></span> <?=round(($this->course->averageRating/5)*100)?>%</h4>
                                    <a id="course_load_reviews" href="javascript:;">Reviews (<?=$this->reviewCount?>)</a>
                                </div>
                                <div class="col-xs-4">
                                    <h4 style="margin: 0;" class="text-muted"><span class="fa fa-user"></span> <?=$this->course->totalNumEnrolled?></h4>
                                    Students
                                </div>
                                <div class="col-xs-4">
                                    <h4 style="margin: 0;" class="text-muted"><span class="fa fa-wrench"></span> Difficulty</h4>
                                    <?=ucwords($this->course->difficulty)?>
                                </div>
                            </div>
                            
                            <div class="row" style="padding-top:10px;">
                                <strong>Category: </strong>
                                <a href="/courses/categories/<?=$this->course->category->id?>/<?=urlencode($this->course->category->name)?>"><span class="fa fa-<?=$this->course->category->icon?>"></span> <?=ucwords($this->course->category->name)?></a>
                                &nbsp;/&nbsp;&nbsp;<a href="/courses/categories/<?=$this->course->subCategory->id?>/<?=urlencode($this->course->subCategory->name)?>"><span class="fa fa-<?=$this->course->subCategory->icon?>"></span> <?=ucwords($this->course->subCategory->name)?></a>
                            </div>
                            
                            <div class="row" style="padding-top:10px;">
                                <strong>Tags: </strong>
                                <?php
                                foreach($this->course->tags as $tag) {
                                    ?><a href="/courses/tags/<?=$tag->id?>/<?=urlencode($tag->name)?>"><span class="fa fa-<?=$tag->icon?>"></span> <?=ucwords($tag->name)?></a>&nbsp;&nbsp;<?php
                                }
                                ?>
                            </div>
                            
                            <div class="row" style="padding-top:10px;">
                                <h5><strong>About this course</strong></h5>
                                <?=$this->course->description?>
                            </div>
                            
                            <div class="row">
                                <?php
                                if($loggedInUser) {
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?php
                                            $ue = User_Course::findOne(
                                                Query::n()
                                                    ->where(array(
                                                        "userId" => $viewingUserId,
                                                        "courseId" => $this->course->id
                                                    )),1
                                            );
                                            if($ue) {
                                                if(in_array($ue->status, array(
                                                    ContentStatus::_started,
                                                    ContentStatus::_passed
                                                ))) {
                                                    ?><a class="btn btn-success" href="/my-courses/<?=$ue->id?>"><span class="fa fa-chevron-right"></span> Go to course</a>&nbsp;&nbsp;&nbsp;<?php
                                                }
                                            }

                                            //if not enrolled, and course is self enrollable
                                            if(!$ue && $this->course->selfEnrollable) {
                                                ?><button type="button" class="btn btn-success courses_self_enrol"><span class="fa fa-chevron-right"></span> Enrol on course</button>&nbsp;&nbsp;&nbsp;<?php
                                            }

                                            ?><a class="btn btn-default" href="/my-courses/"><span class="fa fa-chevron-right"></span> Go to my courses</a>&nbsp;&nbsp;&nbsp;<?php
                                            ?>
                                        </div>
                                    </div>
                                    <br />
                                    <?php
                                }
                                ?>
                            </div>
                            
                        </div>
                    </div>
 
            </div>


            <div class="row paddingtop topmargin">
                <div class="col-lg-7">
                    <h4 style="margin-top: 0;">What's included</h4>
                    <ul class="list-group">
                        <?php
                        $si = 1;
                        $li = 1;
                        $qi = 1;
                        foreach($this->course->lessons as $lesson) {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading blue">
                                    <h3 class="panel-title">Section <?=$si?>: <?=$lesson->name?></h3>
                                </div>

                                <div class="list-group">
                                    <?php
                                    foreach($lesson->modules as $module) {
                                        $part = "";
                                        switch($module->type) {
                                            case ModuleType::_lecture :
                                                $part = "Lecture ".$li.": ".$module->name;
                                                $li++;
                                                break;
                                            case ModuleType::_quiz :
                                                $part = "Quiz ".$qi.": ".$module->name;
                                                $qi++;
                                                break;
                                        }
                                        ?>
                                        <div style="position: relative;" class="list-group-item<?php
                                        if($module->type == ModuleType::_quiz) echo ' list-group-item-warning';
                                        ?>">
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="vertical-align: middle;">
                                                            <h5 style="margin-bottom: 0;margin-top: 0;"><span class="fa fa-<?=Content::getModuleIcon($module->type,$module->lectureType)?>"></span> <?=$part?></h5>
                                                        </td>
                                                        <td style="vertical-align: middle; text-align: right;">
                                                            <?php
                                                            switch($module->type) {
                                                                case ModuleType::_lecture:
                                                                    if($module->previewable) {
                                                                        ?>
                                                                        <button data-name="<?=$module->name?>" data-id="<?=$module->id?>" class="btn btn-xs btn-primary course_page_preview_module"><span class="fa fa-play"></span> preview</button>
                                                                        <?php
                                                                    }
                                                                    break;
                                                                case ModuleType::_quiz:
                                                                    ?>
                                                                    <span><?php
                                                                        $count = count($module->questions);
                                                                        echo $count." question";
                                                                        if($count == 0 || $count > 1) {
                                                                            echo "s";
                                                                        }
                                                                        ?></span>
                                                                    <?php
                                                                    break;
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>

                            </div>
                            <?php
                            $si++;
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-lg-4 col-lg-offset-1">
                    <h4 style="margin-top: 0;">About <?=$this->author->shortname?></h4>
                    <div class="row">
                        <div class="col-xs-12">
                            <img style="float: left; margin-right: 15px; margin-bottom: 15px; max-width: 75px;" class="img-thumbnail" src="<?=$this->author->avatar?>">
                            <?php
                            if(strlen($this->author->bio)) {
                                ?><?=$this->author->bio?><?php
                            } else {
                                ?>This author does not have a bio yet...<?php
                            }
                            ?>
                        </div>
                    </div>
                    
                    <h4>Related Courses</h4>
                    <div class="row related-courses">
                        <div class="col-xs-12">
                            {b}lms-related-courses?course=<?=$this->course->id?>&columns=1&limit=4{/b}
                        </div>
                    </div>
                    
                    <?php
                    if($loggedInUser && $config["ExtorioLMS"]["feedback"]["enabled"] && Users::userEnrolledOnCourse($loggedInUser->id,$this->course->id)) {
                        ?>
                        <h4>Feedback</h4>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Leave feedback...</h3>
                            </div>
                            <div class="panel-body">
                                {b}lms-my-course-feedback?courseid=<?=$this->course->id?>{/b}
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <script>
                $(function() {
                    $('.courses_self_enrol').on("click", function() {
                        $.extorio_modal({
                            title: "Enrol onto course...",
                            content: "Are you sure you want to enrol yourself onto this course?",
                            closetext: "cancel",
                            continuetext: "yes",
                            oncontinuebutton: function() {
                                $.extorio_showFullPageLoader();
                                $.ajax({
                                    url: "/extorio/apis/lms-user-courses",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        userId: <?=$viewingUserId?>,
                                        courseId: <?=$this->course->id?>
                                    },
                                    error: function(a,b,c) {
                                        $.extorio_hideFullPageLoader();
                                        $.extorio_messageError(c);
                                    },
                                    success: function(response) {
                                        $.extorio_hideFullPageLoader();
                                        if(response.error) {
                                            $.extorio_messageError(response.error_message);
                                        } else {
                                            $.extorio_messageSuccess("You have successfully enrolled onto this course");
                                            $('.courses_self_enrol').remove();
                                        }
                                    }
                                })
                            }
                        });
                    });

                    $('.course_page_preview_module').on("click", function() {
                        var id = $(this).attr('data-id');
                        var name = $(this).attr('data-name');
                        $.extorio_dialog({
                            title: name,
                            size: "modal-lg",
                            content: "",
                            onopen: function() {
                                var modal = this;
                                var body = modal.find('.modal-body');
                                body.extorio_showLoader();
                                $.ajax({
                                    url: "/extorio/apis/lms-modules/" + id + "/preview",
                                    type: "GET",
                                    dataType: "json",
                                    error: function(a,b,c) {
                                        body.extorio_hideLoader();
                                        body.html(c);
                                    },
                                    success: function(response) {
                                        body.extorio_hideLoader();
                                        if(response.error) {
                                            body.html(response.error_message);
                                        } else {
                                            body.html(response.data);
                                        }
                                    }
                                })
                            }
                        });
                    });

                    $('#course_load_reviews').on("click", function() {
                        $.extorio_dialog({
                            title: "Reviews",
                            content: "",
                            size: "",
                            onopen: function() {
                                this.find(".modal-body").extorio_fetchBlockProcessorViewContent("lms-course-feedback", {
                                    courseid: <?=$this->course->id?>
                                });
                            }
                        });
                    });
                });
            </script>
            <?php
        } else {
            /*
             * DISPLAYING MANY COURSES
             */
            $this->f->displayFiltering();
            $orderBy = "";
            $orderDirection = "";
            switch($this->f->getFilter("order")) {
                case "name" :
                    $orderBy = "name";
                    $orderDirection = "asc";
                    break;
                case "rating" :
                    $orderBy = "averageRating";
                    $orderDirection = "desc";
                    break;
                case "students" :
                    $orderBy = "totalNumEnrolled";
                    $orderDirection = "desc";
                    break;
            }
            $content = Content::getCourseThumbnailsAsGrid(
                Content::getCourseThumbnails("",array(),$this->f->getLimit(),$this->f->getOffset(),$orderBy,$orderDirection),
                $this->displayDescriptions
            );
            if(strlen($content)) {
                echo $content;
            } else {
                echo "<p>Could not find any courses</p>";
            }
            $this->f->displayPagination();
        }
    }

    public function categories($categoryId = false, $categoryName = false) {
        if($categoryId) {
            ?>
            <div class="alert alert-info">Displaying courses within the category '<?=ucwords($this->category->name)?>'</div>
            <?php
            $this->f->displayFiltering();
            $orderBy = "";
            $orderDirection = "";
            switch($this->f->getFilter("order")) {
                case "name" :
                    $orderBy = "name";
                    $orderDirection = "asc";
                    break;
                case "rating" :
                    $orderBy = "averageRating";
                    $orderDirection = "desc";
                    break;
                case "students" :
                    $orderBy = "totalNumEnrolled";
                    $orderDirection = "desc";
                    break;
            }
            $content = Content::getCourseThumbnailsAsGrid(
                Content::getCourseThumbnailByCategory(
                    $this->category->id,
                    $this->f->getLimit(),
                    $this->f->getOffset(),
                    $orderBy,
                    $orderDirection
                ),
                $this->displayDescriptions
            );
            if(strlen($content)) {
                echo $content;
            } else {
                ?><p>This category does not contain any courses.</p><?php
            }
            $this->f->displayPagination();
        } else {
            ?>
            <p>Select a category from the list below</p>
            <ul>
                <?php
                foreach($this->categories as $category) {
                    ?><li><a href="/courses/categories/<?=$category->id?>/<?=urlencode($category->name)?>?_pg=0"><span class="fa fa-<?=$category->icon?>"></span> <?=ucwords($category->name)?></a><?php
                    if(count($category->subCategories) > 0) {
                        ?>
                        <ul>
                            <?php
                            foreach($category->subCategories as $subCategory) {
                            ?><li><a href="/courses/categories/<?=$subCategory->id?>/<?=urlencode($subCategory->name)?>?_pg=0"><span class="fa fa-<?=$subCategory->icon?>"></span> <?=ucwords($subCategory->name)?></a></li><?php
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    ?></li><?php
                }
                ?>
            </ul>
            <?php
        }
    }

    public function tags($tagId = false, $tagName = false) {
        if($tagId) {
            ?>
            <div class="alert alert-info">Displaying courses tagged with '<?=ucwords($this->tag->name)?>'</div>
            <?php
            $this->f->displayFiltering();
            $orderBy = "";
            $orderDirection = "";
            switch($this->f->getFilter("order")) {
                case "name" :
                    $orderBy = "name";
                    $orderDirection = "asc";
                    break;
                case "rating" :
                    $orderBy = "averageRating";
                    $orderDirection = "desc";
                    break;
                case "students" :
                    $orderBy = "totalNumEnrolled";
                    $orderDirection = "desc";
                    break;
            }
            $content = Content::getCourseThumbnailsAsGrid(
                Content::getCourseThumbnailsByTagId(
                    $this->tag->id,
                    $this->f->getLimit(),
                    $this->f->getOffset(),
                    $orderBy,
                    $orderDirection
                ),
                $this->displayDescriptions
            );
            if(strlen($content)) {
                echo $content;
            } else {
                ?><p>There are no courses with this tag.</p><?php
            }
            $this->f->displayPagination();
        } else {
            ?>
            <p>Select a tag from the list below</p>
            <ul>
                <?php
                foreach($this->tags as $tag) {
                    ?><li><a href="/courses/tags/<?=$tag->id?>/<?=urlencode($tag->name)?>?_pg=0"><span class="fa fa-<?=$tag->icon?>"></span> <?=ucwords($tag->name)?></a><br /><?=$tag->description?></li><?php
                }
                ?>
            </ul>
            <?php
        }
    }

    public function search() {
        ?>
        <div class="alert alert-info">
            Displaying courses that match the phrase '<?=$this->phraseRaw?>'
        </div>
        <?php
        $content = Content::getCourseThumbnailsAsGrid(
            Content::getCourseThumbnailsBySearchPhrase($this->phrase),
            $this->displayDescriptions
        );
        if(strlen($content)) {
            echo $content;
        } else {
            echo "<p>Could not find any courses with this phrase.</p>";
        }
    }
}