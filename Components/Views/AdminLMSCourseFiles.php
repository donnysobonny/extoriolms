<?php
namespace ExtorioLMS\Components\Views;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\CourseFilesFileType;
use ExtorioLMS\Classes\Enums\CourseFilesUploadType;

/**
 * Manage course files
 *
 * Class AdminLMSCourseFiles
 */
class AdminLMSCourseFiles extends \ExtorioLMS\Components\Controllers\AdminLMSCourseFiles {
    public function _onDefault() {
        $config = $this->_getConfig();
        $this->_includeIncluder("select");
        ?>
        <div class="row">
            <div class="col-xs-9">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-xs-3">
                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold; vertical-align: middle; text-align: right; padding-right: 10px; white-space: nowrap;">Limit</td>
                                        <td style="width: 100%;">
                                            <div class="form-search">
                                                <select data-width="100%" class="selectpicker" id="course_file_limit" name="course_file_limit">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="100">100</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-3">
                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold; vertical-align: middle; text-align: right; padding-right: 10px; white-space: nowrap;">Upload Type</td>
                                        <td style="width: 100%;">
                                            <div class="form-search">
                                                <select data-width="100%" class="selectpicker" id="course_file_upload_type" name="course_file_upload_type">
                                                    <option value="">Any Type</option>
                                                    <?php
                                                    foreach($this->uploadTypes as $k => $v) {
                                                        ?><option value="<?=$k?>"><?=$v?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-3">
                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold; vertical-align: middle; text-align: right; padding-right: 10px; white-space: nowrap;">File Type</td>
                                        <td style="width: 100%;">
                                            <div class="form-search">
                                                <select data-width="100%" class="selectpicker" id="course_file_file_type" name="course_file_file_type">
                                                    <option value="">Any Type</option>
                                                    <?php
                                                    foreach($this->fileTypes as $k => $v) {
                                                        ?><option value="<?=$k?>"><?=$v?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-3">
                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold; vertical-align: middle; text-align: right; padding-right: 10px; white-space: nowrap;">Content Type</td>
                                        <td style="width: 100%;">
                                            <div class="form-search">
                                                <select data-width="100%" class="selectpicker" id="course_file_content_type" name="course_file_content_type">
                                                    <option value="">Any Type</option>
                                                    <?php
                                                    foreach($this->contentTypes as $k => $v) {
                                                        ?><optgroup label="<?=Strings::titleSafe($k)?>">
                                                        <?php
                                                        foreach($v as $t) {
                                                            ?><option value="<?=$t?>"><?=$t?></option><?php
                                                        }
                                                        ?>
                                                        </optgroup><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <form method="GET" action="" id="course_file_search_form" class="form-inline">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="course_file_search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><span class="fa fa-search"></span></button>
                            </span>
                        </div>
                    </div>
                </form><br />

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Upload Type</th>
                        <th>File Type</th>
                        <th>Content Type</th>
                        <th><span class="fa fa-cog"></span></th>
                    </tr>
                    </thead>
                    <tbody id="course_file_data">

                    </tbody>
                </table>

                <div style="margin-bottom: 0;" class="well well-sm">
                    <div class="row">
                        <div class="col-xs-12">
                            <table style="width: 100%;" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <td style="vertical-align: middle;">
                                        Page <span id="course_files_cur_page"></span> of <span id="course_files_tot_page"></span> <small>(from <span id="course_files_tot_results"></span> total results)</small>
                                    </td>
                                    <td style="vertical-align: middle; text-align: right;">
                                        <div class="btn-group" role="group">
                                            <button id="course_file_prev" type="button" class="btn btn-default"><span class="fa fa-angle-left"></span> prev</button>
                                            <button id="course_file_next" type="button" class="btn btn-default">next <span class="fa fa-angle-right"></span></button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div style="margin-bottom: 0;" class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Upload new files</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="course_files_upload_upload_type">Upload type</label>
                            <div class="input-group input-group-sm">
                                <select class="form-control input-sm" id="course_files_upload_upload_type" aria-describedby="sizing-addon3">
                                    <option value="<?=CourseFilesUploadType::_courseSources?>">Course Sources</option>
                                    <option value="<?=CourseFilesUploadType::_moduleSources?>">Module Sources</option>
                                </select>
                                <span data-toggle="tooltip" data-placement="left" title="Select 'Course Sources' for files to be used as the courses default file and thumbnail. Select 'Module Sources' for files to be used in lecture modules." class="input-group-addon" id="sizing-addon3"><span class="fa fa-question"></span></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="course_files_upload_file_type">File type</label>
                            <div class="input-group input-group-sm">
                                <select class="form-control input-sm" id="course_files_upload_file_type" aria-describedby="sizing-addon4">
                                    <option value="<?=CourseFilesFileType::_videos?>">Videos</option>
                                    <option value="<?=CourseFilesFileType::_images?>">Images</option>
                                    <option value="<?=CourseFilesFileType::_audio?>">Audio</option>
                                    <option value="<?=CourseFilesFileType::_documents?>">Documents</option>
                                    <option value="<?=CourseFilesFileType::_subtitles?>">Subtitles</option>
                                </select>
                                <span data-toggle="tooltip" data-placement="left" title="Select the file type relevant to the file that you want to upload." class="input-group-addon" id="sizing-addon4"><span class="fa fa-question"></span></span>
                            </div>
                        </div>
                        <input style="display: none;" type="file" id="course_files_upload_file_input">
                        <button id="course_files_upload_new_file" class="btn btn-block btn-sm btn-primary">Select a file</button>
                        <hr />
                        <div id="course_files_upload_files">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {

                var bucket = "<?=$this->courseFileBucket?>";

                $('[data-toggle="tooltip"]').tooltip();

                $('#course_files_upload_new_file').on("click", function() {
                    var fileType = $('#course_files_upload_file_type').val();
                    var input = $('#course_files_upload_file_input');
                    switch (fileType) {
                        case "videos" :
                            input.attr("accept","video/*");
                            break;
                        case "images" :
                            input.attr("accept","image/*");
                            break;
                        case "audio" :
                            input.attr("accept","audio/*");
                            break;
                        case "documents" :
                            input.attr("accept","application/*");
                            break;
                    }
                    input.trigger("click");
                });

                $('#course_files_upload_file_input').on("change", function(e) {
                    var files = e.target.files;
                    if(files.length > 0) {
                        $.extorio_showFullPageLoader();
                        //create the lms course file first
                        $.extorio_api({
                            endpoint: "/lms-course-files/",
                            type: "POST",
                            data: {
                                data: {
                                    bucket: bucket,
                                    uploadType: $('#course_files_upload_upload_type').val(),
                                    fileType: $('#course_files_upload_file_type').val(),
                                    fileName: files[0].name,
                                    contentType: files[0].type
                                }
                            },
                            oncomplete: function() {
                                $.extorio_hideFullPageLoader();
                            },
                            onsuccess: function(resp) {
                                var courseFile = resp.data;
                                var signatureData = resp.signature;
                                var url = resp.url;

                                $('#course_files_upload_files').append('<div class="alert alert-info gutter_padding_half gutter_margin_bottom_half" id="course_file_upload_file_'+courseFile.id+'">' +
                                    '   <p>Uploading: '+courseFile.fileName+'</p>' +
                                    '   <div style="margin-bottom: 0; height: 5px;" class="progress">' +
                                    '       <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%; height: 5px;"></div>' +
                                    '   </div>' +
                                    '</div>');

                                var data = new FormData();
                                //add the signature data
                                for (var k in signatureData) {
                                    data.append(k, signatureData[k]);
                                }
                                //add the file data
                                data.append('file', files[0], files[0].name);
                                //and we're good to go!
                                $.ajax({
                                    xhr: function() {
                                        var xhr = new window.XMLHttpRequest();
                                        xhr.upload.addEventListener("progress", function(evt) {
                                            if (evt.lengthComputable) {
                                                var percentComplete = Math.round((evt.loaded / evt.total) * 100);
                                                $('#course_file_upload_file_'+courseFile.id+' .progress-bar').attr('aria-valuenow',percentComplete).css("width",percentComplete + "%");
                                            }
                                        }, false);
                                        return xhr;
                                    },
                                    url: url,
                                    type: 'POST',
                                    data: data,
                                    dataType: "text",
                                    cache: false,
                                    processData: false,
                                    contentType: false,
                                    error: function(a2,b2,c2) {
                                        $.extorio_messageError(c2);
                                    },
                                    success: function(response2) {
                                        console.log(response2);
                                        loadFiles();
                                        $('#course_file_upload_file_'+courseFile.id+' .progress-bar').addClass('progress-bar-success');
                                        $('#course_file_upload_file_'+courseFile.id).removeClass("alert-info").addClass("alert-success").fadeOut(2000,function() {
                                            $(this).remove();
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        $.extorio_messageError("Please select a valid file...");
                    }
                });

                var page = 0;
                var loadFiles = function() {
                    $.extorio_showFullPageLoader();

                    var data = {};
                    var uploadType = $('#course_file_upload_type').val();
                    if(uploadType.length > 0) {
                        data.uploadType = uploadType;
                    }
                    var fileType = $('#course_file_file_type').val();
                    if(fileType.length > 0) {
                        data.fileType = fileType;
                    }
                    var contentType = $('#course_file_content_type').val();
                    if(contentType.length > 0) {
                        data.contentType = contentType;
                    }
                    var search = $('#course_file_search').val();
                    if(search.length > 0) {
                        data.fileName = search;
                    }
                    data.bucket = "<?=$this->courseFileBucket?>";

                    data.limit = parseInt($('#course_file_limit').val());
                    data.skip = data.limit * page;
                    data.orderBy = "fileName";
                    data.orderDirection = "asc";

                    $.extorio_api({
                        endpoint: "/lms-course-files/filter",
                        type: "GET",
                        data: data,
                        oncomplete: function() {
                            $.extorio_hideFullPageLoader();
                        },
                        onsuccess: function(resp) {
                            var tbody = $('#course_file_data');
                            tbody.html('');
                            if(resp.data.length > 0) {
                                for(var i = 0; i < resp.data.length; i++) {
                                    var file = resp.data[i];
                                    var item = $('<tr>' +
                                        '   <td>'+file.fileName+'</td>' +
                                        '   <td>'+file.uploadType+'</td>' +
                                        '   <td>'+file.fileType+'</td>' +
                                        '   <td>'+file.contentType+'</td>' +
                                        '   <td>' +
                                        '       <a target="_blank" href="<?=$this->_getUrlToDefault()?>'+file.id+'" class="btn btn-xs btn-default"><span class="fa fa-external-link"></span> view</a>' +
                                        '   </td>' +
                                        '</tr>');
                                    tbody.append(item);
                                }
                            } else {
                                tbody.html('<tr><td colspan="5">No files found</td></tr>');
                            }
                            $('#course_files_cur_page').html(page + 1);
                            var totalPages = Math.ceil(resp.countUnlimited / data.limit);
                            if(totalPages <= 0) {
                                totalPages = 1;
                            }
                            $('#course_files_tot_page').html(totalPages);
                            $('#course_files_tot_results').html(resp.countUnlimited);
                            if(page > 0) {
                                $('#course_file_prev').removeClass("disabled").removeAttr("disabled");
                            } else {
                                $('#course_file_prev').addClass("disabled").attr("disabled","disabled");
                            }
                            if((page + 1) < totalPages) {
                                $('#course_file_next').removeClass("disabled").removeAttr("disabled");
                            } else {
                                $('#course_file_next').addClass("disabled").attr("disabled","disabled");
                            }
                        }
                    });
                };

                $('#course_file_prev').on("click", function() {
                    page--;
                    loadFiles();
                });
                $('#course_file_next').on("click", function() {
                    page++;
                    loadFiles();
                });
                $('#course_file_limit').on("change", function() {
                    page = 0;
                    loadFiles();
                });
                $('#course_file_upload_type').on("change", function() {
                    page = 0;
                    loadFiles();
                });
                $('#course_file_file_type').on("change", function() {
                    page = 0;
                    loadFiles();
                });
                $('#course_file_content_type').on("change", function() {
                    page = 0;
                    loadFiles();
                });
                $('#course_file_search_form').submit(function(e) {
                    e.preventDefault();
                    page = 0;
                    loadFiles();
                });

                loadFiles();
            });
        </script>
        <?php
    }
}