<?php
namespace ExtorioLMS\Components\Views;
/**
 * 
 *
 * Class AdminLMSDashboard
 */
class AdminLMSDashboard extends \ExtorioLMS\Components\Controllers\AdminLMSDashboard {
    public function _onDefault() {
        ?>
        <div class="alert alert-info">This page will eventually display a bunch of useful information for administration purposes.</div>
        <p>
            <?php
            if($this->pendingCourseCount > 0) {
                ?>
                There are currently <?=$this->pendingCourseCount?> courses pending approval. <a href="/extorio-admin/lms-courses/?with-status=pending">View pending courses</a>
                <?php
            } else {
                ?>
                There are no courses pending approval.
                <?php
            }
            ?>
        </p>
        <?php
    }
}