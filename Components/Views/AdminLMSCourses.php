<?php
namespace ExtorioLMS\Components\Views;

use Core\Classes\Commons\Extorio;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Manage courses
 *
 * Class AdminLMSCourses
 */
class AdminLMSCourses extends \ExtorioLMS\Components\Controllers\AdminLMSCourses
{
    public function _onDefault($id = false, $action = false)
    {
        if (!$id) {
            $this->f->displayFiltering();
            $this->f->displaySearching();
            ?>
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Approval</th>
                    <th>Rating</th>
                    <th>No. enrolled</th>
                    <th>No. lessons</th>
                    <th>No. lectures</th>
                    <th>No. quizzes</th>
                    <th>No. questions</th>
                    <th>Max points</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($this->courses) > 0) {
                    foreach ($this->courses as $course) {
                        ?>
                        <tr>
                            <td><?= $course->name ?></td>
                            <td><?= Content::getCourseApprovalHtml($course->approval) ?></td>
                            <td><?= $course->averageRating ?>/5</td>
                            <td><?= $course->totalNumEnrolled ?></td>
                            <td><?= $course->totalNumLessons ?></td>
                            <td><?= $course->totalNumLectures ?></td>
                            <td><?= $course->totalNumQuizzes ?></td>
                            <td><?= $course->totalNumQuestions ?></td>
                            <td><?= $course->maximumPoints ?></td>
                            <td>
                                <div class="btn-group" role="group">
                                    <a class="btn btn-xs btn-default"
                                       href="<?= $this->_getUrlToDefault(array($course->id)) ?>"><span
                                            class="fa fa-eye"></span> view</a>
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="fa fa-cog"></span>
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="<?= $this->_getUrlToDefault(array($course->id, "edit")) ?>"><span
                                                        class="fa fa-edit"></span> edit content</a>
                                            </li>
                                            <li>
                                                <a href="<?= $this->_getUrlToDefault(array($course->id, "clone")) ?>"><span
                                                        class="fa fa-clone"></span> clone</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="10">No courses found</td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php
            $this->f->displayPagination();
        } else {
            if (!$action) {
                switch ($this->course->approval) {
                    case ContentApproval::_pending:
                        ?>
                        <div class="alert alert-info">
                            This course is
                            currently <?= Content::getCourseApprovalHtml($this->course->approval, "pending") ?> approval
                        </div>
                        <?php
                        break;
                    case ContentApproval::_closed:
                        ?>
                        <div class="alert alert-info">
                            This course is
                            currently <?= Content::getCourseApprovalHtml($this->course->approval, "closed") ?>.
                        </div>
                        <?php
                        break;
                    case ContentApproval::_cancelled:
                        ?>
                        <div class="alert alert-info">
                            This course is
                            currently <?= Content::getCourseApprovalHtml($this->course->approval, "cancelled") ?> due to
                            the following reason: "<?= $this->course->cancelledReason ?>"
                        </div>
                        <?php
                        break;
                    case ContentApproval::_drafting:
                        ?>
                        <div class="alert alert-info">
                            This course is currently
                            in <?= Content::getCourseApprovalHtml($this->course->approval, "drafting") ?> mode
                        </div>
                        <?php
                        break;
                    case ContentApproval::_approved:
                        ?>
                        <div class="alert alert-success">
                            This course has
                            been <?= Content::getCourseApprovalHtml($this->course->approval, "approved") ?>.
                        </div>
                        <?php
                        break;
                }
                ?>
                <p>TODO: Display useful stats for the course!</p>
                <div>
                    <a href="<?= $this->_getUrlToDefault(array($this->course->id, "edit")) ?>"
                       class="btn btn-xs btn-primary"><span class="fa fa-edit"></span> edit course content</a>
                    <a  href="<?= $this->_getUrlToDefault(array($this->course->id, "preview")) ?>"
                       class="btn btn-xs btn-primary"><span class="fa fa-edit"></span> preview course content</a>
                    <?php
                    if ($this->course->approval != ContentApproval::_approved) {
                        ?>
                        <a href="javascript:;" class="btn btn-xs btn-success course_approve"><span
                                class="fa fa-check"></span> approve</a>
                        <?php
                    }
                    if ($this->course->approval != ContentApproval::_closed) {
                        ?>
                        <a href="javascript:;" class="btn btn-xs btn-warning course_close"><span
                                class="fa fa-close"></span> close</a>
                        <?php
                    }
                    if ($this->course->approval != ContentApproval::_cancelled) {
                        ?>
                        <a href="<?= $this->_getUrlToDefault(array($this->course->id, "cancel")) ?>"
                           class="btn btn-xs btn-danger"><span class="fa fa-close"></span> cancel</a>
                        <?php
                    }
                    ?>

                    <?php
                    if ($this->course->ownerId == $this->_getLoggedInUserId()) {
                        ?>
                        <a href="<?= $this->_getUrlToDefault(array($this->course->id, "clone")) ?>"
                           class="btn btn-xs btn-default"><span class="fa fa-clone"></span> clone</a>
                        <?php
                    }
                    ?>

                    <button class="btn btn-xs btn-danger course_delete"><span class="fa fa-trash"></span> delete
                    </button>
                </div>
                <script>
                    $(function () {
                        $('.course_approve').on("click", function () {
                            $.extorio_modal({
                                title: "Approve course",
                                content: "Are you sure that you want to approve this course? Doing so will put the course live, and will only allow minimal edits to be made.",
                                closetext: "cancel",
                                continuetext: "approve",
                                oncontinuebutton: function () {
                                    window.location.href = "<?=$this->_getUrlToDefault(array($this->course->id, "approve"))?>";
                                }
                            });
                        });
                        $('.course_close').on("click", function () {
                            $.extorio_modal({
                                title: "Close course",
                                content: "Are you sure that you want to close this course? Doing so will disallow new enrollments on the course.",
                                closetext: "cancel",
                                continuetext: "close",
                                oncontinuebutton: function () {
                                    window.location.href = "<?=$this->_getUrlToDefault(array($this->course->id, "close"))?>";
                                }
                            });
                        });
                        $('.course_delete').on("click", function () {
                            $.extorio_modal({
                                title: "Delete course",
                                content: "Are you sure that you want to close this course? This will also remove all data related to the course.",
                                closetext: "cancel",
                                continuetext: "delete",
                                oncontinuebutton: function () {
                                    window.location.href = "<?=$this->_getUrlToDefault(array($this->course->id, "delete"))?>";
                                }
                            });
                        });
                    });
                </script>
                <?php
            } else {
                switch ($action) {
                    case "edit" :

                        //$this->_appendToHead("<base href='/extorio-admin/lms-courses/" . $this->course->id . "/edit' />");
                        $this->_appendToHead("<base href='/extorio-admin/lms-courses/' />");

                        $this->_appendToHead("<link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/CourseCreator/styles/vendor.css'>");
                        $this->_appendToHead("<link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/CourseCreator/styles/main.css'>");
                        $this->_appendToBody("<script src='/Extensions/ExtorioLMS/Assets/CourseCreator/scripts/vendor.js'></script>");
                        $this->_appendToBody("<script src='/Extensions/ExtorioLMS/Assets/CourseCreator/scripts/scripts.js'></script>");
                        ?>
                        <div class="container-fluid" id="lms" ng-app="courseCreatorApp">
                            <div ui-view></div>
                        </div>
                        <script type="text/javascript"> var loggedInUserId =<?=$this->_getLoggedInUserId();?> </script>
                        <?php
                        break;
                    case "clone" :
                        ?>
                        <div class="alert alert-info">
                            Please enter the name of the new course below
                        </div>
                        <form method="post" action="">
                            <div class="form-group">
                                <label for="name">Course name</label>
                                <input type="text" name="name" id="name" class="form-control"
                                       value="<?= $this->course->name ?> (clone)"/>
                            </div>
                            <button type="submit" name="submitted" class="btn btn-default"><span
                                    class="fa fa-clone"></span> clone course
                            </button>
                        </form>
                        <?php
                        break;
                    case "cancel" :
                        ?>
                        <div class="alert alert-info">
                            Cancelling a course will result in enrolled users no longer being able to access the course,
                            and the course no longer being live.
                            If you are sure that you want to cancel the course, please enter a reason for cancellation
                            below.
                        </div>
                        <form method="post" action="">
                            <div class="form-group">
                                <label for="reason">Cancellation reason</label>
                                <textarea name="reason" id="reason" class="form-control"
                                          placeholder="The reason for cancellation is..."><?= $this->course->cancelledReason ?></textarea>
                            </div>
                            <button type="submit" name="submitted" class="btn btn-danger"><span
                                    class="fa fa-close"></span> cancel course
                            </button>
                        </form>
                        <?php
                        break;
                    case "preview" :
                        $cid=$this->course->id;
                        $this->_appendToHead("<base href='/extorio-admin/lms-courses/$cid/preview/' />");
                        $this->_appendToHead("<link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/CoursePlayer/styles/main.css'/><link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/CoursePlayer/styles/vendor.css'/>");
                        $this->_appendToBody("<script>window.isPreviewMode=true;</script><script src='/Extensions/ExtorioLMS/Assets/CoursePlayer/scripts/vendor.js'></script><script src='/Extensions/ExtorioLMS/Assets/CoursePlayer/scripts/scripts.js'></script><script src='https://content.jwplatform.com/libraries/o5KUXtrX.js'></script>");
                        ?>

                        <div class='container-fluid'  id="lms-course=player">
                            <div ui-view ></div>
                        </div>
                        <script type="text/javascript"> var loggedInUserId =<?=$this->_getLoggedInUserId();?> </script>
                        <script>
                            $(document).ready(bootstrap);
                            function bootstrap() {
                                var host = "/extorio/apis";

                                function init() {
                                    var userCourseId = getUserCourseId();
                                    if (userCourseId % 1 === 0) getUserCourse(userCourseId);
                                }

                                /**
                                 * Get user course id from url param
                                 * @returns {number}
                                 */
                                function getUserCourseId() {
                                    return <?= $this->course->id ?>;
                                }

                                /**
                                 * Get all the data the app needs to run
                                 * @param userCourseId
                                 */
                                function getUserCourse(userCourseId) {
                                    $.ajax({
                                        url: host + '/lms-courses/' + userCourseId + '',
                                        crossDomain: false,
                                        type: 'GET'
                                    }).then(function (response) {
                                        response = JSON.parse(response);
                                        window.course = response.data;
                                        bootstrapAngular();
                                    }, function (error) {
                                        throw "Whoops something broke, seemed like userCourse and the course could not be retrieved for the current userCourseId";
                                    });
                                }

                                /**
                                 * With all the required data in place start the app
                                 */
                                function bootstrapAngular() {
                                    var element = document.getElementById('lms-course=player')
                                    angular.bootstrap(element, ['coursePlayerApp']);
                                }

                                init();
                            }

                        </script>
                        <style>
                            .player-holder{
                                position:fixed !important;
                                left:0;z-index:3;
                                top:0;
                                width:100%;
                                height:100%;
                                background:white
                            }
                            #lms-player-content{top:10px}#lms-course-player-nav{top:10px}</style>
                        <?php
                        break;
                }
            }
        }
    }

    public function create()
    {
        $this->_appendToHead("<base href='/extorio-admin/lms-courses/create/' />");
        $this->_appendToHead("<link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/CourseCreator/styles/vendor.css'>");
        $this->_appendToHead("<link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/CourseCreator/styles/main.css'>");
        $this->_appendToBody("<script src='/Extensions/ExtorioLMS/Assets/CourseCreator/scripts/vendor.js'></script>");
        $this->_appendToBody("<script src='/Extensions/ExtorioLMS/Assets/CourseCreator/scripts/scripts.js'></script>");
        ?>
        <div class="container-fluid" id="lms" ng-app="courseCreatorApp">
            <div ui-view></div>
        </div>
        <script type="text/javascript"> var loggedInUserId =<?=$this->_getLoggedInUserId();?> </script>
        <?php
    }
}