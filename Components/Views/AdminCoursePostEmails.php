<?php
namespace ExtorioLMS\Components\Views;
/**
 * Manage the course post emails for the lms extension
 *
 * Class AdminCoursePostEmails
 */
class AdminCoursePostEmails extends \ExtorioLMS\Components\Controllers\AdminCoursePostEmails {
    public function _onDefault() {
        $this->_includeIncluder("extorio_editable");
        ?>
        <form method="post" action="">
            <div class="row">
                <div class="col-md-6">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">New Post <small>sent to all course subscribers when a new post is submitted</small></h3>
                        </div>
                        <div class="panel-body">
                            <p>
                                You can use the following replacers within the body of the email:<br />
                                <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                <span class="label label-primary">**COURSE_NAME**</span> - will be replaced with the course name<br />
                                <span class="label label-primary">**POST_NAME**</span> - will be replaced with the name of the post<br />
                                <span class="label label-primary">**POST_URL**</span> - will be replaced with a url to the post<br />
                                <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                            </p>
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if($this->config["ExtorioLMS"]["emails"]["course_posts"]["new_post"]["enabled"]) echo 'checked="checked"';
                                    ?> name="new_post_enabled" type="checkbox"> Enabled
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Email subject</label>
                                <input
                                    value="<?=$this->config["ExtorioLMS"]["emails"]["course_posts"]["new_post"]["subject"]?>"
                                    type="text"
                                    class="form-control"
                                    name="new_post_subject"
                                    placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="new_post_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["course_posts"]["new_post"]["body"]?></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">New Reply <small>sent to all post subscribers when a new reply is submitted</small></h3>
                        </div>
                        <div class="panel-body">
                            <p>
                                You can use the following replacers within the body of the email:<br />
                                <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                <span class="label label-primary">**REPLY_SHORTNAME**</span> - will be replaced with the replier's shortname<br />
                                <span class="label label-primary">**POST_NAME**</span> - will be replaced with the name of the post<br />
                                <span class="label label-primary">**POST_URL**</span> - will be replaced with a url to the post<br />
                                <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                            </p>
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if($this->config["ExtorioLMS"]["emails"]["course_posts"]["new_reply"]["enabled"]) echo 'checked="checked"';
                                    ?> name="new_reply_enabled" type="checkbox"> Enabled
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Email subject</label>
                                <input
                                    value="<?=$this->config["ExtorioLMS"]["emails"]["course_posts"]["new_reply"]["subject"]?>"
                                    type="text"
                                    class="form-control"
                                    name="new_reply_subject"
                                    placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="new_reply_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["course_posts"]["new_reply"]["body"]?></textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <br />
            <button type="submit" name="submitted" class="btn btn-primary">Update</button>
        </form>
        <script>
            $(function() {
                $('.email_body').extorio_editable();
            })
        </script>
        <?php
    }
}