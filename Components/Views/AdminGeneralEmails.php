<?php
namespace ExtorioLMS\Components\Views;
/**
 * Manage the general emails for the lms extension
 *
 * Class AdminGeneralEmails
 */
class AdminGeneralEmails extends \ExtorioLMS\Components\Controllers\AdminGeneralEmails {
    public function _onDefault() {
        $this->_includeIncluder("extorio_editable");
        ?>
        <form method="post" action="">

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">User Quiz Marked <small>sent to the enrolled user when a quiz has been manually marked</small></h3>
                        </div>
                        <div class="panel-body">
                            <p>
                                You can use the following replacers within the body of the email:<br />
                                <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                <span class="label label-primary">**CONTENT_NAME**</span> - will be replaced with the name of the course/session<br />
                                <span class="label label-primary">**CONTENT_TYPE**</span> - will be replaced with the type of content (session or course)<br />
                                <span class="label label-primary">**QUIZ_NAME**</span> - will be replaced with the quiz name<br />
                                <span class="label label-primary">**QUIZ_URL**</span> - will be replaced with a url to the quiz<br />
                                <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                            </p>
                            <div class="checkbox">
                                <label>
                                    <input <?php
                                    if($this->config["ExtorioLMS"]["emails"]["general"]["user_quiz_marked"]["enabled"]) echo 'checked="checked"';
                                    ?> name="general_user_quiz_marked_enabled" type="checkbox"> Enabled
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Email subject</label>
                                <input
                                    value="<?=$this->config["ExtorioLMS"]["emails"]["general"]["user_quiz_marked"]["subject"]?>"
                                    type="text"
                                    class="form-control"
                                    name="general_user_quiz_marked_subject"
                                    placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="general_user_quiz_marked_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["general"]["user_quiz_marked"]["body"]?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <button type="submit" name="submitted" class="btn btn-primary">Update</button>
        </form>
        <script>
            $(function() {
                $('.email_body').extorio_editable();
            })
        </script>
        <?php
    }
}