<?php
namespace ExtorioLMS\Components\Views;
/**
 * Manage the upload settings for the lms extension
 *
 * Class AdminLMSUploadSettings
 */
class AdminLMSUploadSettings extends \ExtorioLMS\Components\Controllers\AdminLMSUploadSettings {
    public function _onDefault() {
        ?>
        <form method="post" action="">

            <div class="form-group">
                <label for="course_file_bucket">Course file bucket</label>
                <p class="help-block">The access protected bucket used to store course files.</p>
                <input value="<?=$this->config["ExtorioLMS"]["courses"]["course_file_bucket"]?>" type="text" class="form-control" id="course_file_bucket" name="course_file_bucket">
            </div>

            <div class="form-group">
                <label for="editor_file_bucket">Editor file bucket</label>
                <p class="help-block">The public bucket used to store editor files.</p>
                <input value="<?=$this->config["ExtorioLMS"]["courses"]["editor_file_bucket"]?>" type="text" class="form-control" id="editor_file_bucket" name="editor_file_bucket">
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Aws S3 settings</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="aws_s3_region">S3 region</label>
                                <p class="help-block">The s3 region</p>
                                <input value="<?=$this->config["ExtorioLMS"]["aws"]["s3"]["region"]?>" type="text" class="form-control"
                                       id="aws_s3_region" name="aws_s3_region" placeholder="The s3 region">
                            </div>
                            <div class="form-group">
                                <label for="aws_s3_key">S3 key</label>
                                <p class="help-block">The s3 key</p>
                                <input value="<?=$this->config["ExtorioLMS"]["aws"]["s3"]["key"]?>" type="text" class="form-control"
                                       id="aws_s3_key" name="aws_s3_key" placeholder="The s3 key">
                            </div>
                            <div class="form-group">
                                <label for="aws_s3_secret">S3 secret</label>
                                <p class="help-block">The s3 secret</p>
                                <input value="<?=$this->config["ExtorioLMS"]["aws"]["s3"]["secret"]?>" type="text" class="form-control"
                                       id="aws_s3_secret" name="aws_s3_secret" placeholder="The s3 secret">
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Images</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="images_maximum_size">Maximum file size</label>
                                <p class="help-block">The maximum file size that images uploads can be (in bytes)</p>
                                <input value="<?=$this->config["ExtorioLMS"]["uploads"]["images"]["maximum_size"]?>" type="number" class="form-control"
                                       id="images_maximum_size" name="images_maximum_size" placeholder="Maximum file size">
                            </div>
                            <?php
                            $acceptedTypes = "";
                            foreach($this->config["ExtorioLMS"]["uploads"]["images"]["accepted_types"] as $type) {
                                $acceptedTypes .= $type.", ";
                            }
                            $acceptedTypes = substr($acceptedTypes,0,strlen($acceptedTypes)-2);
                            ?>
                            <div class="form-group">
                                <label for="images_accepted_types">Accepted file types</label>
                                <p class="help-block">The accepted file types for image updates (seperated by commas)</p>
                                <input value="<?=$acceptedTypes?>" type="text" class="form-control"
                                       id="images_accepted_types" name="images_accepted_types" placeholder="Accepted file types">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Video</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="videos_maximum_size">Maximum file size</label>
                                <p class="help-block">The maximum file size that video uploads can be (in bytes)</p>
                                <input value="<?=$this->config["ExtorioLMS"]["uploads"]["videos"]["maximum_size"]?>" type="number" class="form-control"
                                       id="videos_maximum_size" name="videos_maximum_size" placeholder="Maximum file size">
                            </div>
                            <?php
                            $acceptedTypes = "";
                            foreach($this->config["ExtorioLMS"]["uploads"]["videos"]["accepted_types"] as $type) {
                                $acceptedTypes .= $type.", ";
                            }
                            $acceptedTypes = substr($acceptedTypes,0,strlen($acceptedTypes)-2);
                            ?>
                            <div class="form-group">
                                <label for="videos_accepted_types">Accepted file types</label>
                                <p class="help-block">The accepted file types for video uploads (seperated by commas)</p>
                                <input value="<?=$acceptedTypes?>" type="text" class="form-control"
                                       id="videos_accepted_types" name="videos_accepted_types" placeholder="Accepted file types">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Audio</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="audio_maximum_size">Maximum file size</label>
                                <p class="help-block">The maximum file size that audio uploads can be (in bytes)</p>
                                <input value="<?=$this->config["ExtorioLMS"]["uploads"]["audio"]["maximum_size"]?>" type="number" class="form-control"
                                       id="audio_maximum_size" name="audio_maximum_size" placeholder="Maximum file size">
                            </div>
                            <?php
                            $acceptedTypes = "";
                            foreach($this->config["ExtorioLMS"]["uploads"]["audio"]["accepted_types"] as $type) {
                                $acceptedTypes .= $type.", ";
                            }
                            $acceptedTypes = substr($acceptedTypes,0,strlen($acceptedTypes)-2);
                            ?>
                            <div class="form-group">
                                <label for="audio_accepted_types">Accepted file types</label>
                                <p class="help-block">The accepted file types for audio uploads (seperated by commas)</p>
                                <input value="<?=$acceptedTypes?>" type="text" class="form-control"
                                       id="audio_accepted_types" name="audio_accepted_types" placeholder="Accepted file types">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Documents</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="documents_maximum_size">Maximum file size</label>
                                <p class="help-block">The maximum file size that document uploads can be (in bytes)</p>
                                <input value="<?=$this->config["ExtorioLMS"]["uploads"]["documents"]["maximum_size"]?>" type="number" class="form-control"
                                       id="documents_maximum_size" name="documents_maximum_size" placeholder="Maximum file size">
                            </div>
                            <?php
                            $acceptedTypes = "";
                            foreach($this->config["ExtorioLMS"]["uploads"]["documents"]["accepted_types"] as $type) {
                                $acceptedTypes .= $type.", ";
                            }
                            $acceptedTypes = substr($acceptedTypes,0,strlen($acceptedTypes)-2);
                            ?>
                            <div class="form-group">
                                <label for="documents_accepted_types">Accepted file types</label>
                                <p class="help-block">The accepted file types for document uploads (seperated by commas)</p>
                                <input value="<?=$acceptedTypes?>" type="text" class="form-control"
                                       id="documents_accepted_types" name="documents_accepted_types" placeholder="Accepted file types">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" name="submitted" class="btn btn-primary">Update</button>
        </form>
        <?php
    }
}