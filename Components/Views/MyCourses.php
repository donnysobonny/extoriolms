<?php
namespace ExtorioLMS\Components\Views;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Utilities\Content;
use ExtorioLMS\Classes\Utilities\Users;

/**
 *
 *
 * Class MyCourses
 */
class MyCourses extends \ExtorioLMS\Components\Controllers\MyCourses {
public function _onDefault()
{
    //control
    $this->control(func_get_args());
}

protected function courseList() {
$config = $this->_getConfig();
$loggedInUser = $this->_Extorio()->getLoggedInUser();
if (count($this->courseListData)) {
$columns = 4;
$nextRow = 0;
$i = 0;
foreach ($this->courseListData as $data) {
if ($i == $nextRow) {
if ($i > 0) {
?></div><?php
}
?>
<div class="row"><?php
    $nextRow += $columns;
    }
    $progress = round((intval($data["currentPoints"]) / intval($data["courseMaximumPoints"])) * 100);
    $thumbnail = strlen($data["courseThumbnail"]) ? $data["courseThumbnail"] : $config["ExtorioLMS"]["courses"]["default_thumbnail_image"];
    $expired = false;
    if (strlen($data["expiryDate"])) {
        if (strtotime($data["expiryDate"]) < time()) {
            $expired = true;
        }
    }
    $passed = false;
    ?>

    <div class="col-lg-<?= round(12 / $columns) ?> col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="card-header" style="background-image: url(<?= $thumbnail ?>);">
                <div class="card-header-mask">
                </div>
            </div>
            <div class="card-body">
                <div class="card-body-header">
                    <h1><a href="/my-courses/<?= $data["userEnrolmentId"] ?>"><?= $data["courseName"] ?></a></h1>
                    <p class="card-body-header-sentence">
                        <?= $this->category ?>
                    </p>
                </div>
                <div class="card-body-footer">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="<?= $progress ?>" aria-valuemin="0"
                             aria-valuemax="100" style="min-width: 2em; width: <?= $progress ?>%;">
                            <?= $progress ?>%
                        </div>
                    </div>
                </div>
                <div class="card-body-footer">
                    <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                        <tr>
                            <td style="padding-right 10px; text-align: right; font-weight: bold;">Status</td>
                            <td style="padding-left: 10px;"><?php
                                if ($expired) {
                                    ?><span class="text-danger">Expired</span> <span
                                        data-date="<?= $data["expiryDate"] ?>"
                                        class="my_courses_timeago"></span>
                                    <br/><?php
                                } else {
                                    switch ($data["status"]) {
                                        case ContentStatus::_pending:
                                            ?><span class="text-muted">Not started</span><br/><?php
                                            break;
                                        case ContentStatus::_started :
                                            ?><span class="text-warning">Started</span> <span
                                            data-date="<?= $data["dateStarted"] ?>" class="my_courses_timeago"></span>
                                            <br/><?php
                                            break;
                                        case ContentStatus::_passed :
                                            $passed = true;
                                            ?><span class="text-success">Passed</span> <span
                                            data-date="<?= $data["datePassed"] ?>" class="my_courses_timeago"></span>
                                            <br/><?php
                                            break;
                                        default:
                                            ?><span class="text-muted">Unknown</span><br/><?php
                                            break;
                                    }
                                }
                                ?></td>
                        </tr>
                        <?php
                        if (!$expired) {
                            ?>
                            <tr>
                                <td style="padding-right 10px; text-align: right; font-weight: bold;">Expiry</td>
                                <td style="padding-left: 10px;"><?php
                                    if (strlen($data["expiryDate"])) {
                                        echo date("jS F Y, H:i", strtotime($data["expiryDate"]));
                                    } else {
                                        if ($data["status"] != ContentStatus::_pending) {
                                            ?><span class="text-muted">never expires</span><?php
                                        } else {
                                            ?><span class="text-muted">not set</span><?php
                                        }
                                    }
                                    ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>

                <div class="card-body-cta" style="padding-top:0;">
                    <?php
                    if ($data["status"] == ContentStatus::_pending && !$expired) {
                        ?>
                        <button data-id="<?= $data["userEnrolmentId"] ?>"
                                class="btn btn-success my_courses_start"><span
                                class="fa fa-chevron-right"></span> Start Course
                        </button>
                        <?php
                    } else if ($data["status"] == ContentStatus::_started && !$expired) {
                        ?>
                        <a class="btn btn-success" href="/my-courses/<?= $data["userEnrolmentId"] ?>"><span
                                class="fa fa-chevron-right"></span> Continue Course</a>
                        <?php
                    } else {
                        ?>
                        <a class="btn btn-primary" href="/my-courses/<?= $data["userEnrolmentId"] ?>"><span
                                class="fa fa-chevron-right"></span> View course</a>
                        <?php
                    }
                    ?>
                </div>

                <div class="card-body-footer">
                    <h5>Most recent post</h5>

                    <?php
                    if ($data["has_recent"]) {
                        ?>
                        <strong><a
                                href="/my-courses/<?= $data["userEnrolmentId"] ?>/posts/<?= $data["postId"] ?>"><?= $data["postName"] ?></a></strong>
                        <small>by <?= $data["postUserShortName"] ?> <span data-date="<?= $data["postDateCreated"] ?>"
                                                                          class="my_courses_timeago"></span></small>
                        <br/>
                        <span class="text-muted">
                                <?php
                                $body = $data["postBodyPlain"];
                                if (strlen($body) >= 60) {
                                    $body = substr($body, 0, 57) . "...";
                                }
                                echo $body;
                                ?> <a href="/my-courses/<?= $data["userEnrolmentId"] ?>/posts/<?= $data["postId"] ?>">read
                                more</a>
                            </span>
                        <?php
                    } else {
                        ?>
                        No recent posts...
                        <?php
                    }
                    ?>
                </div>
                <a class="btn btn-primary btn-sm" href="/my-courses/<?= $data["userEnrolmentId"] ?>/posts"><span
                        class="fa fa-chevron-right"></span> View all posts</a>
            </div>
        </div>

    </div>
    <?php
    $i++;
    }
    ?></div>
<script>
    $('.my_courses_timeago').each(function () {
        $(this).html($.timeago($(this).attr("data-date")));
    });

    $('.my_courses_start').on("click", function () {
        var userCourseId = $(this).attr('data-id');
        $.extorio_modal({
            "title": "Start course",
            "content": "Are you sure that you want to start this course?",
            "size": "modal-sm",
            "canceltext": "cancel",
            "continuetext": "start course",
            "oncontinuebutton": function () {
                $.extorio_showFullPageLoader();
                $.ajax({
                    url: "/extorio/apis/lms-user-courses/" + userCourseId + "/start",
                    type: "POST",
                    dataType: "json",
                    error: function (a, b, c) {
                        $.extorio_hideFullPageLoader();
                        $.extorio_messageError(c);
                    },
                    success: function (response) {
                        if (response.error) {
                            $.extorio_hideFullPageLoader();
                            $.extorio_messageError(response.error_message);
                        } else {
                            window.location.href = "/my-courses/" + userCourseId;
                        }
                    }
                });
            }
        });
    });
</script>
<?php
} else {
    ?>
    <p>You are not enrolled on any courses. <a href="/courses/">View courses</a></p>
    <?php
}
}

protected function course($userCourseId, $userModuleId = false)
{
    $this->_appendToHead("<base href='/my-courses/' />");

    /* //PROD VERSION */
    //$this->_appendToHead("<link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/CoursePlayer/styles/main.css'/><link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/CoursePlayer/styles/vendor.css'/>");
    //$this->_appendToBody("<script src='/Extensions/ExtorioLMS/Assets/CoursePlayer/scripts/vendor.js'></script><script src='/Extensions/ExtorioLMS/Assets/CoursePlayer/scripts/scripts.js'></script><script src='https://content.jwplatform.com/libraries/o5KUXtrX.js'></script>");


    //DEV VERSION

    $this->_appendToHead('<link rel="stylesheet" type="text/css" href="/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/styles/main.css" />');
    foreach ($this->bowerComponentsCss as $srcValue) {
        $this->_appendToHead("<link rel=\"stylesheet\" type=\"text/css\" href=\"$this->bowerComponentsPath$srcValue\">");
    }
    foreach ($this->bowerComponentsJs as $srcValue) {
        $this->_appendToBody("<script type=\"text/javascript\" src=\"$this->bowerComponentsPath$srcValue\"></script>");
    }
    foreach ($this->appScripts as $srcValue) {
        $this->_appendToBody("<script type=\"text/javascript\" src=\"$this->appPath$srcValue\"></script>");
    }
    foreach ($this->appControllers as $srcValue) {
        $this->_appendToBody("<script type=\"text/javascript\" src=\"$this->appPath$srcValue\"></script>");
    }
    foreach ($this->appServices as $srcValue) {
        $this->_appendToBody("<script type=\"text/javascript\" src=\"$this->appPath$srcValue\"></script>");
    }
    foreach ($this->appServices as $srcValue) {
        $this->_appendToBody("<script type=\"text/javascript\" src=\"$this->appPath$srcValue\"></script>");
    }
    foreach ($this->appDirectives as $srcValue) {
        $this->_appendToBody("<script type=\"text/javascript\" src=\"$this->appPath$srcValue\"></script>");
    }
    foreach ($this->appConstants as $srcValue) {
        $this->_appendToBody("<script type=\"text/javascript\" src=\"$this->appPath$srcValue\"></script>");
    }

    //Misc
    $this->_appendToBody("<script type=\"text/javascript\" src=\"http://www.microsoftTranslator.com/ajax/v3/WidgetV3.ashx?siteData=ueOIGRSKkd965FeEGM5JtQ**\"></script>");
    $this->_appendToBody("<script type=\"text/javascript\" src=\"/Extensions/ExtorioLMS/Assets/CoursePlayerDev/bower_components/pdfjs-dist/build/pdf.combined.js\"></script>");

    ?>
    <div class='container-fluid' id="lms-course-player">
        <div ui-view></div>
        <toaster-container
            toaster-options="{'position-class': 'toast-top-center', 'time-out': 1000}"></toaster-container>
    </div>

    <script>
        $(document).ready(bootstrap);

        function bootstrap() {
            var host = "/extorio/apis";
            var element;

            function init() {
                element = $('#lms-course-player');

                $('#extorio_progress_bar_top').remove();
                $('body > nav').remove();
                $('#layout_wrapper').remove();
                $('body').css('padding-top', 0);

                getUserCourse(getUserCourseId());
            }

            /**
             * Get user course id from url param
             * @returns {number}
             */
            function getUserCourseId() {
                return <?= $userCourseId ?>;
            }

            /**
             * Get all the data the app needs to run
             * @param userCourseId
             */
            function getUserCourse(userCourseId) {
                $.ajax({
                    url: host + '/lms-user-courses/' + userCourseId + '/detail',
                    crossDomain: false,
                    type: 'GET'
                }).then(function (response) {
                    response = JSON.parse(response);
                    window.userCourse = response.data;
                    window.course = response.course;
                    bootstrapAngular();
                }, function (error) {
                    throw "Whoops something broke, seemed like userCourse and the course could not be retrieved for the current userCourseId";
                });
            }

            /**
             * With all the required data in place start the app
             */
            function bootstrapAngular() {
                $('body').prepend(element);
                angular.bootstrap(element, ['coursePlayerApp']);
            }

            init();
        }

    </script>
    <?php
}

protected function coursePostsCreate($userEnrolmentId)
{
    $this->_includeIncluder("extorio_editable");
    ?>
    <form method="post" action="">
        <div class="form-group">
            <label for="title">Title</label>
            <input value="<?= $this->title ?>" type="text" class="form-control" id="title" name="title"
                   placeholder="Give your post a title...">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Post</label>
            <textarea id="body" name="body" placeholder="Enter some content..."><?= $this->body ?></textarea>
        </div>
        <div class="checkbox">
            <label>
                <input id="subscribe" name="subscribe" checked="checked" type="checkbox"> Subscribe to this post
            </label>
        </div>
        <button type="submit" name="new_post_create" class="btn btn-primary btn-block">Submit post</button>
    </form>
    <script>
        $(function () {
            $('#body').extorio_editable_basic();
        })
    </script>
    <?php
}

protected function coursePostsSearch($userEnrolmentId)
{

}

protected function coursePosts($userEnrolmentId)
{
    ?>
    <div class="list-group">
        <?php
        while ($row = $this->stickyPostData->fetchAssoc()) {
            ?>
            <div class="list-group-item list-group-item-warning">
                <h4 class="list-group-item-heading"><a
                        href="/my-courses/<?= $userEnrolmentId ?>/posts/<?= $row["id"] ?>"><?= $row["name"] ?></a></h4>
                <p class="list-group-item-text">
                    <?= Strings::shorten($row["bodyPlain"], 140) ?> (<a
                        href="/my-courses/<?= $userEnrolmentId ?>/posts/<?= $row["id"] ?>">read more</a>)
                </p>
                <small style="margin: 0;">
                    <span class="label label-warning">sticky</span> <?php
                    switch ($row["approval"]) {
                        case ContentApproval::_pending :
                            ?><span class="label label-default"> pending</span>&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                            break;
                        case ContentApproval::_closed:
                            ?><span class="label label-default"><span
                                class="fa fa-lock"></span> closed</span>&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                            break;
                        case ContentApproval::_cancelled:
                            ?><span class="label label-default"><span
                                class="fa fa-close"></span> cancelled</span>&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                            break;
                        default:
                            ?>&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                            break;
                    }
                    if ($row["totalNumReplies"] == 1) {
                        ?>1 reply&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                    } else {
                        ?><?= $row["totalNumReplies"] ?> replies&nbsp;&nbsp;-&nbsp;&nbsp;<?PHP
                    }
                    if ($row["totalNumReplies"] <= 0) {
                        ?>started by <?= $row["ulongname"] ?> <?= Strings::dateToTimeAgo($row["dateCreated"]) ?><?php
                    } else {
                        ?>most recent by <?= $row["rlongname"] ?> <?= Strings::dateToTimeAgo($row["dateReplied"]) ?><?php
                    }
                    ?>
                </small>
            </div>
            <?php
        }
        ?>
    </div>
    <p>
        <a href="/my-courses/<?= $userEnrolmentId ?>/posts/create" class="btn btn-sm btn-primary"><span
                class="fa fa-plus"></span> Create new post</a>
    </p>
    <?php
    if ($this->postData->numRows() > 0) {
        ?>
        <div class="list-group">
            <?php
            while ($row = $this->postData->fetchAssoc()) {
                ?>
                <div class="list-group-item">
                    <h4 class="list-group-item-heading"><a
                            href="/my-courses/<?= $userEnrolmentId ?>/posts/<?= $row["id"] ?>"><?= $row["name"] ?></a>
                    </h4>
                    <p class="list-group-item-text">
                        <?= Strings::shorten($row["bodyPlain"], 140) ?> (<a
                            href="/my-courses/<?= $userEnrolmentId ?>/posts/<?= $row["id"] ?>">read more</a>)
                    </p>
                    <small style="margin: 0;">
                        <?php
                        switch ($row["approval"]) {
                            case ContentApproval::_pending :
                                ?><span class="label label-default"> pending</span>&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                                break;
                            case ContentApproval::_closed:
                                ?><span class="label label-default"><span
                                    class="fa fa-lock"></span> closed</span>&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                                break;
                            case ContentApproval::_cancelled:
                                ?><span class="label label-default"><span
                                    class="fa fa-close"></span> cancelled</span>&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                                break;
                        }
                        if ($row["totalNumReplies"] == 1) {
                            ?>1 reply&nbsp;&nbsp;-&nbsp;&nbsp;<?php
                        } else {
                            ?><?= $row["totalNumReplies"] ?> replies&nbsp;&nbsp;-&nbsp;&nbsp;<?PHP
                        }
                        if ($row["totalNumReplies"] <= 0) {
                            ?>started by <?= $row["ulongname"] ?> <?= Strings::dateToTimeAgo($row["dateCreated"]) ?><?php
                        } else {
                            ?>most recent by <?= $row["rlongname"] ?> <?= Strings::dateToTimeAgo($row["dateReplied"]) ?><?php
                        }
                        ?>
                    </small>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    } else {
        ?><p>No posts found. <a href="/my-courses/<?= $userEnrolmentId ?>/posts/create">Be the first!</a></p><?php
    }
    ?>
    <?php

    $this->f->displayPagination();
}

protected function coursePostsPost($userEnrolmentId, $postId, $replyId = false)
{
    $this->_includeIncluder("extorio_editable");
    ?>
    <h1 id="course_post_name"><?= $this->post->name ?></h1>
    <div class="list-group">
        <div class="list-group-item list-group-item-warning">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img style="width: 100px;" class="media-object img-thumbnail" src="<?= $this->author->avatar ?>"
                             alt="">
                    </a>
                </div>
                <div class="media-body">
                    <?php
                    switch ($this->post->approval) {
                        case ContentApproval::_pending:
                            ?>
                            <div style="padding: 5px 10px; margin-bottom: 0px;" class="alert alert-info">
                                This post is currently pending approval
                            </div>
                            <p></p>
                            <?php
                            break;
                        case ContentApproval::_closed:
                            ?>
                            <div style="padding: 5px 10px; margin-bottom: 0px;" class="alert alert-info">
                                This post is currently closed and can therefore not be replied to
                            </div>
                            <p></p>
                            <?php
                            break;
                        case ContentApproval::_cancelled:
                            ?>
                            <div style="padding: 5px 10px; margin-bottom: 0px;" class="alert alert-info">
                                This post is currently cancelled and is therefore closed off from the public
                            </div>
                            <p></p>
                            <?php
                            break;
                    }
                    ?>
                    <div id="course_post_body"><?= $this->post->body ?></div>
                    <hr style="margin: 0;"/>
                    <small class="text-muted">
                        by <?= $this->author->longname ?> <?= Strings::dateToTimeAgo($this->post->dateCreated) ?><?php
                        if ($this->post->userId == $this->_getLoggedInUserId()) {
                            ?>&nbsp;&nbsp;-&nbsp;&nbsp;<a data-id="<?= $this->post->id ?>" href="javascript:;"
                                                          class="course_post_edit"><span class="fa fa-edit"></span> edit</a>
                            <?php
                        }
                        ?></small>
                </div>
            </div>
        </div>
    </div>
    <div class="list-group">
        <?php
        if ($this->postData->numRows() > 0) {
            while ($row = $this->postData->fetchAssoc()) {
                ?>
                <div id="course_post_<?= $row["id"] ?>" class="list-group-item<?php
                if ($replyId == $row["id"]) echo ' list-group-item-success';
                ?>">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img style="width: 100px;" class="media-object img-thumbnail"
                                     src="<?= $row["avatar"] ?>" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <?php
                            switch ($row["approval"]) {
                                case ContentApproval::_pending:
                                    ?>
                                    <div style="padding: 5px 10px; margin-bottom: 0px;" class="alert alert-info">
                                        This post is currently pending approval
                                    </div>
                                    <p></p>
                                    <?php
                                    break;
                                case ContentApproval::_closed:
                                    ?>
                                    <div style="padding: 5px 10px; margin-bottom: 0px;" class="alert alert-info">
                                        This post is currently closed
                                    </div>
                                    <p></p>
                                    <?php
                                    break;
                                case ContentApproval::_cancelled:
                                    ?>
                                    <div style="padding: 5px 10px; margin-bottom: 0px;" class="alert alert-info">
                                        This post is currently cancelled and is therefore closed off from the public
                                    </div>
                                    <p></p>
                                    <?php
                                    break;
                            }
                            ?>
                            <div id="course_post_<?= $row["id"] ?>_body"><?= $row["body"] ?></div>
                            <hr style="margin: 0;"/>
                            <small class="text-muted">
                                by <?= $row["ulongname"] ?> <?= Strings::dateToTimeAgo($row["dateCreated"]) ?><?php
                                if ($row["userId"] == $this->_getLoggedInUserId()) {
                                    ?>&nbsp;&nbsp;-&nbsp;&nbsp;<a data-id="<?= $row["id"] ?>" href="javascript:;"
                                                                  class="course_reply_edit"><span
                                            class="fa fa-edit"></span> edit</a>
                                    <?php
                                }
                                ?></small>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <script>
        $(function () {
            $('.course_post_edit').on("click", function () {
                var id = $(this).attr('data-id');
                $.extorio_modal({
                    title: "Edit post",
                    size: "modal-lg",
                    content: '' +
                    '<div class="form-group">' +
                    '   <label for="course_post_edit_name">Title</label>' +
                    '   <input type="text" class="form-control" id="course_post_edit_name" placeholder="Give your post a title...">' +
                    '</div>' +
                    '<div class="form-group">' +
                    '   <label for="course_post_edit_body">Post</label>' +
                    '   <textarea id="course_post_edit_body"></textarea>' +
                    '</div>' +
                    '',
                    onopen: function () {
                        this.find('#course_post_edit_name').val($('#course_post_name').html());
                        this.find('#course_post_edit_body').val($('#course_post_body').html());
                        this.find('#course_post_edit_body').extorio_editable_basic();
                    },
                    continuetext: "Save",
                    oncontinuebutton: function () {
                        var name = this.find('#course_post_edit_name').val();
                        var body = this.find('#course_post_edit_body').val();
                        $.extorio_showFullPageLoader();
                        $.extorio_api({
                            endpoint: "/lms-course-posts/" + id,
                            type: "POST",
                            data: {
                                data: {
                                    name: name,
                                    body: body
                                }
                            },
                            oncomplete: function () {
                                $.extorio_hideFullPageLoader();
                            },
                            onsuccess: function (data) {
                                $.extorio_messageSuccess("Post updated");
                                $('#course_post_name').html(data.data.name);
                                $('#course_post_body').html(data.data.body);
                            }
                        });
                    }
                });
            });

            $('.course_reply_edit').on("click", function () {
                var id = $(this).attr('data-id');
                $.extorio_modal({
                    title: "Edit reply",
                    size: "modal-lg",
                    content: '' +
                    '<div class="form-group">' +
                    '   <label for="course_post_edit_body">Post</label>' +
                    '   <textarea id="course_post_edit_body"></textarea>' +
                    '</div>' +
                    '',
                    onopen: function () {
                        this.find('#course_post_edit_body').val($('#course_post_' + id + '_body').html());
                        this.find('#course_post_edit_body').extorio_editable_basic();
                    },
                    continuetext: "Save",
                    oncontinuebutton: function () {
                        var body = this.find('#course_post_edit_body').val();
                        $.extorio_showFullPageLoader();
                        $.extorio_api({
                            endpoint: "/lms-course-posts/" + id,
                            type: "POST",
                            data: {
                                data: {
                                    body: body
                                }
                            },
                            oncomplete: function () {
                                $.extorio_hideFullPageLoader();
                            },
                            onsuccess: function (data) {
                                $.extorio_messageSuccess("Reply updated");
                                $('#course_post_' + id + '_body').html(data.data.body);
                            }
                        });
                    }
                });
            });
        });
    </script>
    <?php
    $this->f->displayPagination();
    if ($this->post->approval == ContentApproval::_approved) {
        ?>
        <h5>Reply to this post</h5>
        <form method="post" action="">
            <textarea name="course_post_reply" id="course_post_reply"></textarea>
            <p></p>
            <button type="submit" name="course_post_submit" class="btn btn-sm btn-primary">Submit reply</button>
        </form>
        <script>
            $(function () {
                $('#course_post_reply').extorio_editable_basic();
            });
        </script>
        <?php
    }
    if ($replyId) {
        ?>
        <script>
            $(function () {
                $('html, body').animate({
                    scrollTop: $("#course_post_<?=$replyId?>").offset().top
                }, 250);
            });
        </script>
        <?php
    }
}
}