<?php
namespace ExtorioLMS\Components\Views;
/**
 * Manage the course settings for the lms extension
 *
 * Class AdminLMSCourseSettings
 */
class AdminLMSCourseSettings extends \ExtorioLMS\Components\Controllers\AdminLMSCourseSettings {
    public function _onDefault() {
        ?>
        <form method="post" action="" enctype="multipart/form-data">
            <div class="checkbox">
                <label>
                    <input <?php
                    if($this->config["ExtorioLMS"]["courses"]["auto_approve"]) echo 'checked="checked"';
                    ?> name="courses_auto_approve" id="courses_auto_approve" type="checkbox"> Auto approve courses
                </label>
                <p class="help-block">If this is disabled, courses must be manually approved</p>
            </div>
            <div class="form-group">
                <label for="courses_minimum_expiry">Minimum expiry minutes</label>
                <p class="help-block">Here we can set the minimum length of time that a course enrolment can last. Setting this to 0 will allow course enrolments to last forever.</p>
                <input value="<?=$this->config["ExtorioLMS"]["courses"]["minimum_expiry_minutes"]?>" type="number" class="form-control" id="courses_minimum_expiry" name="courses_minimum_expiry" placeholder="Minimum expiry minutes">
            </div>
            <div class="checkbox">
                <label>
                    <input <?php
                    if($this->config["ExtorioLMS"]["courses"]["allow_self_enrollable"]) echo 'checked="checked"';
                    ?> name="courses_allow_self_enrollable" id="courses_allow_self_enrollable" type="checkbox"> Allow self-enrollable courses
                </label>
                <p class="help-block">Whether or not courses are allowed to be self-enrollable</p>
            </div>
            <div class="form-group">
                <label for="course_expiry_when">When to set the expiry date</label>
                <p class="help-block">Here we can specify when the expiry date is set on a course enrolment.</p>
                <select id="course_expiry_when" name="course_expiry_when" class="form-control">
                    <option <?php
                    if($this->config["ExtorioLMS"]["courses"]["expiry_set_when"] == "on_create") echo 'selected="selected"';
                    ?> value="on_create">When the course enrolment is created</option>
                    <option <?php
                    if($this->config["ExtorioLMS"]["courses"]["expiry_set_when"] == "on_start") echo 'selected="selected"';
                    ?> value="on_start">When the course enrolment is started</option>
                </select>
            </div>
            <div class="checkbox">
                <label>
                    <input <?php
                    if($this->config["ExtorioLMS"]["courses"]["certificate_public"]) echo 'checked="checked"';;
                    ?> name="course_certificate_public" type="checkbox"> Certificates are public
                </label>
                <p class="help-block">This allows students to view their certificates. Otherwise, only admins are able to view certificates.</p>
            </div>
            <div class="clearfix">
                <img class="gutter_margin_right_half gutter_margin_bottom" src="<?=$this->config["ExtorioLMS"]["courses"]["default_image"]?>" style="float: left; max-width: 200px;">
                <div class="form-group">
                    <label for="course_default_image">The default course image</label>
                    <p class="help-block">This image will be used as the default course image for courses without any default image.</p>
                    <input type="file" id="course_default_image" name="course_default_image">
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Modules</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["ExtorioLMS"]["modules"]["allow_previewable"]) echo 'checked="checked"';
                            ?> name="modules_allow_previewable" id="modules_allow_previewable" type="checkbox"> Allow previewable modules
                        </label>
                        <p class="help-block">Whether or not lecture modules are allowed to be previewable</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Lectures</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["lectures"]["allow_video"]) echo 'checked="checked"';
                                            ?> name="modules_allow_video" id="modules_allow_video" type="checkbox"> Allow video lectures
                                        </label>
                                        <p class="help-block">Whether or not video lectures are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["lectures"]["allow_audio"]) echo 'checked="checked"';
                                            ?> name="modules_allow_audio" id="modules_allow_audio" type="checkbox"> Allow audio lectures
                                        </label>
                                        <p class="help-block">Whether or not audio lectures are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["lectures"]["allow_images"]) echo 'checked="checked"';
                                            ?> name="modules_allow_image" id="modules_allow_image" type="checkbox"> Allow image lectures
                                        </label>
                                        <p class="help-block">Whether or not image lectures are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["lectures"]["allow_text"]) echo 'checked="checked"';
                                            ?> name="modules_allow_text" id="modules_allow_text" type="checkbox"> Allow text lectures
                                        </label>
                                        <p class="help-block">Whether or not text lectures are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["lectures"]["allow_documents"]) echo 'checked="checked"';
                                            ?> name="modules_allow_document" id="modules_allow_document" type="checkbox"> Allow document lectures
                                        </label>
                                        <p class="help-block">Whether or not document lectures are allowed</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Quizzes</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="quizzes_minimum_maximum_attempts">Minimum maximum attempts</label>
                                        <p class="help-block">The minimum value that the maximum attempts can be on a quiz. Setting this to zero will allow quizzes to have infinite attempts</p>
                                        <input value="<?=$this->config["ExtorioLMS"]["modules"]["quizzes"]["minimum_maximum_attempts"]?>" type="number" class="form-control"
                                               id="quizzes_minimum_maximum_attempts" name="quizzes_minimum_maximum_attempts" placeholder="Minimum maximum attempts">
                                    </div>
                                    <div class="form-group">
                                        <label for="quizzes_minimum_pass_points">Minimum pass points percentage</label>
                                        <p class="help-block">This is the minimum percentage of a quizzes maximum points that the pass points can be.</p>
                                        <input value="<?=$this->config["ExtorioLMS"]["modules"]["quizzes"]["minimum_pass_points_percentage"]?>" type="number" class="form-control"
                                               id="quizzes_minimum_pass_points" name="quizzes_minimum_pass_points" placeholder="Minimum pass points percentage">
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["quizzes"]["allow_multiple_response"]) echo 'checked="checked"';
                                            ?> name="quizzes_allow_multiple_choice" id="quizzes_allow_multiple_choice" type="checkbox"> Allow multiple choice questions
                                        </label>
                                        <p class="help-block">Whether or not multiple choice questions are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["quizzes"]["allow_multiple_response"]) echo 'checked="checked"';
                                            ?> name="quizzes_allow_multiple_response" id="quizzes_allow_multiple_response" type="checkbox"> Allow multiple response questions
                                        </label>
                                        <p class="help-block">Whether or not multiple response questions are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["quizzes"]["allow_true_false"]) echo 'checked="checked"';
                                            ?> name="quizzes_allow_true_false" id="quizzes_allow_true_false" type="checkbox"> Allow true or false questions
                                        </label>
                                        <p class="help-block">Whether or not true or false questions are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["quizzes"]["allow_fill_gaps"]) echo 'checked="checked"';
                                            ?> name="quizzes_allow_fill_gaps" id="quizzes_allow_fill_gaps" type="checkbox"> Allow fill in the gaps questions
                                        </label>
                                        <p class="help-block">Whether or not fill in the gaps questions are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["quizzes"]["allow_multiline_text"]) echo 'checked="checked"';
                                            ?> name="quizzes_allow_multiline" id="quizzes_allow_multiline" type="checkbox"> Allow multiline text questions (requires manual marking!)
                                        </label>
                                        <p class="help-block">Whether or not multiline text questions are allowed</p>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["modules"]["quizzes"]["allow_file_submission"]) echo 'checked="checked"';
                                            ?> name="quizzes_allow_file" id="quizzes_allow_file" type="checkbox"> Allow file submission questions (requires manual marking!)
                                        </label>
                                        <p class="help-block">Whether or not file submissions questions are allowed</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Course Posts</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["ExtorioLMS"]["course_posts"]["enabled"]) echo 'checked="checked"';;
                            ?> name="posts_enabled" type="checkbox"> Enabled
                        </label>
                        <p class="help-block">Whether or not course posts and the course forum are enabled.</p>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["ExtorioLMS"]["course_posts"]["auto_approve"]) echo 'checked="checked"';
                            ?> name="posts_auto_approve" id="posts_auto_approve" type="checkbox"> Auto approve course posts
                        </label>
                        <p class="help-block">Disabling this will mean that all courses posts must be manually approved</p>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["ExtorioLMS"]["course_posts"]["allow_attachments"]) echo 'checked="checked"';
                            ?> name="posts_allow_attachments" id="posts_allow_attachments" type="checkbox"> Allow attachments
                        </label>
                        <p class="help-block">Whether or not attachments can be uploaded with a new post</p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Feedback</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["ExtorioLMS"]["feedback"]["enabled"]) echo 'checked="checked"';
                            ?> name="feedback_enabled" id="feedback_enabled" type="checkbox"> Enabled
                        </label>
                        <p class="help-block">Whether or not feedback is enabled</p>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["ExtorioLMS"]["feedback"]["auto_approve_uncommented"]) echo 'checked="checked"';
                            ?> name="feedback_approve_uncommented" id="feedback_approve_uncommented" type="checkbox"> Auto approve feedback with no comment
                        </label>
                        <p class="help-block">When feedback is submitted/updated without a comment, should it be auto approved?</p>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input <?php
                            if($this->config["ExtorioLMS"]["feedback"]["auto_approve_commented"]) echo 'checked="checked"';
                            ?> name="feedback_approve_commented" id="feedback_approve_commented" type="checkbox"> Auto approve feedback with comment
                        </label>
                        <p class="help-block">When feedback is submitted/updated with a comment, should it be auto approved?</p>
                    </div>
                </div>
            </div>

            <button type="submit" name="submitted" class="btn btn-primary">Update</button>
        </form>
        <?php
    }
}