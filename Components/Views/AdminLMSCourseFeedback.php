<?php
namespace ExtorioLMS\Components\Views;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Manage course feedback
 *
 * Class AdminLMSCourseFeedback
 */
class AdminLMSCourseFeedback extends \ExtorioLMS\Components\Controllers\AdminLMSCourseFeedback {
    public function _onDefault() {
        $this->f->displayFiltering();
        $this->f->displaySearching();
        ?>
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>User</th>
                    <th>Course</th>
                    <th>Rating</th>
                    <th>Comment</th>
                    <th>Approval</th>
                    <th>Updated</th>
                    <th><span class="fa fa-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($this->feedbackData->numRows() > 0) {
                    while($row = $this->feedbackData->fetchAssoc()) {
                        ?>
                <tr>
                    <td><?=$row["longname"]?></td>
                    <td><?=$row["name"]?></td>
                    <td><?=$row["rating"]?>/5</td>
                    <td><?php
                        if(strlen($row["comment"])) {
                            echo $row["comment"];
                        } else {
                            echo 'N/A';
                        }
                        ?></td>
                    <td><?=Content::getFeedbackApprovalHtml($row["approval"])?></td>
                    <td><?=Strings::dateToTimeAgo($row["dateUpdated"])?></td>
                    <td>
                        <?php
                        if($row["approval"] != ContentApproval::_approved) {
                            ?><button class="btn btn-xs btn-success approve_feedback" data-id="<?=$row["id"]?>"><span class="fa fa-check"></span> approve</button>
                            <?php
                        }
                        if($row["approval"] != ContentApproval::_cancelled) {
                            ?><button class="btn btn-xs btn-danger cancel_feedback" data-id="<?=$row["id"]?>"><span class="fa fa-close"></span> cancel</button>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                        <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="7">No feedback found</td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <script>
            $(function() {
                $('.approve_feedback').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Approve feedback",
                        size: "modal-sm",
                        content: "Approve this feedback?",
                        closetext: "close",
                        continuetext: "continue",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("approve")?>" + id
                        }
                    })
                });
                $('.cancel_feedback').on("click", function() {
                    var id = $(this).attr('data-id');
                    $.extorio_modal({
                        title: "Cancel feedback",
                        size: "modal-sm",
                        content: "Cancel this feedback?",
                        closetext: "close",
                        continuetext: "continue",
                        oncontinuebutton: function() {
                            window.location.href = "<?=$this->_getUrlToMethod("cancel")?>" + id
                        }
                    })
                });
            });
        </script>
        <?php
        $this->f->displayPagination();
    }
}