<?php
namespace ExtorioLMS\Components\Views;
/**
 * Manage the course emails for the lms extension
 *
 * Class AdminCourseEmails
 */
class AdminCourseEmails extends \ExtorioLMS\Components\Controllers\AdminCourseEmails {
    public function _onDefault() {
        $this->_includeIncluder("extorio_editable");
        ?>
        <form method="post" action="">

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Enrolled Users</h3>
                        </div>
                        <div class="panel-body">

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Course Enrolment <small>sent to a user after enrolling onto a course</small></h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        You can use the following replacers within the body of the email:<br />
                                        <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                        <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                        <span class="label label-primary">**COURSE_NAME**</span> - will be replaced with the course name<br />
                                        <span class="label label-primary">**COURSE_URL**</span> - will be replaced with a url to the course<br />
                                        <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                        <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                        <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                                    </p>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["emails"]["courses"]["course_enrollments"]["enabled"]) echo 'checked="checked"';
                                            ?> name="course_enrolment_enabled" type="checkbox"> Enabled
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Email subject</label>
                                        <input
                                            value="<?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_enrollments"]["subject"]?>"
                                            type="text"
                                            class="form-control"
                                            name="course_enrolment_subject"
                                            placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="course_enrolment_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_enrollments"]["body"]?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Course Passed <small>sent to a user when they pass a course. Can be used to contain the certificate url</small></h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        You can use the following replacers within the body of the email:<br />
                                        <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                        <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                        <span class="label label-primary">**COURSE_NAME**</span> - will be replaced with the course name<br />
                                        <span class="label label-primary">**COURSE_URL**</span> - will be replaced with a url to the course<br />
                                        <span class="label label-primary">**CERTIFICATE_URL**</span> - will be replaced with a url to the certificate<br />
                                        <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                        <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                        <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                                    </p>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["emails"]["courses"]["passed_course"]["enabled"]) echo 'checked="checked"';
                                            ?> name="course_passed_enabled" type="checkbox"> Enabled
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Email subject</label>
                                        <input
                                            value="<?=$this->config["ExtorioLMS"]["emails"]["courses"]["passed_course"]["subject"]?>"
                                            type="text"
                                            class="form-control"
                                            name="course_passed_subject"
                                            placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="course_passed_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["courses"]["passed_course"]["body"]?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Course Cancelled <small>sent to all enrolled users when a course is cancelled</small></h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        You can use the following replacers within the body of the email:<br />
                                        <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                        <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                        <span class="label label-primary">**COURSE_NAME**</span> - will be replaced with the course name<br />
                                        <span class="label label-primary">**REASON**</span> - will be replaced with the reason the course was cancelled<br />
                                        <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                        <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                        <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                                    </p>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["emails"]["courses"]["course_cancelled_enrolled"]["enabled"]) echo 'checked="checked"';
                                            ?> name="course_cancelled_enabled" type="checkbox"> Enabled
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Email subject</label>
                                        <input
                                            value="<?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_cancelled_enrolled"]["subject"]?>"
                                            type="text"
                                            class="form-control"
                                            name="course_cancelled_subject"
                                            placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="course_cancelled_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_cancelled_enrolled"]["body"]?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Course Expiry <small>sent to users that have a course enrollment that is due to expire soon</small></h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        You can use the following replacers within the body of the email:<br />
                                        <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                        <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                        <span class="label label-primary">**COURSE_NAME**</span> - will be replaced with the course name<br />
                                        <span class="label label-primary">**COURSE_URL**</span> - will be replaced with a url to the course<br />
                                        <span class="label label-primary">**EXPIRY_DATE**</span> - will be replaced with the expiry date<br />
                                        <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                        <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                        <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                                    </p>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["emails"]["courses"]["course_expiry"]["enabled"]) echo 'checked="checked"';
                                            ?> name="course_expiry_enabled" type="checkbox"> Enabled
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Days before expiry</label>
                                        <p class="help-block">This is the number of days before the expiry date that the email will be sent</p>
                                        <input
                                            value="<?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_expiry"]["days_before"]?>"
                                            type="number"
                                            class="form-control"
                                            name="course_expiry_days_before"
                                            placeholder="Days before">
                                    </div>
                                    <div class="form-group">
                                        <label>Email subject</label>
                                        <input
                                            value="<?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_expiry"]["subject"]?>"
                                            type="text"
                                            class="form-control"
                                            name="course_expiry_subject"
                                            placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="course_expiry_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_expiry"]["body"]?></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Course owners</h3>
                        </div>
                        <div class="panel-body">

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Course Approved <small>sent to the course owner when their course is approved</small></h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        You can use the following replacers within the body of the email:<br />
                                        <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                        <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                        <span class="label label-primary">**COURSE_NAME**</span> - will be replaced with the course name<br />
                                        <span class="label label-primary">**COURSE_URL**</span> - will be replaced with a url to the course<br />
                                        <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                        <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                        <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                                    </p>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["emails"]["courses"]["course_approved"]["enabled"]) echo 'checked="checked"';
                                            ?> name="course_approved_enabled" type="checkbox"> Enabled
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Email subject</label>
                                        <input
                                            value="<?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_approved"]["subject"]?>"
                                            type="text"
                                            class="form-control"
                                            name="course_approved_subject"
                                            placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="course_approved_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_approved"]["body"]?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Course Closed <small>sent to the course owner when their course is closed</small></h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        You can use the following replacers within the body of the email:<br />
                                        <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                        <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                        <span class="label label-primary">**COURSE_NAME**</span> - will be replaced with the course name<br />
                                        <span class="label label-primary">**COURSE_URL**</span> - will be replaced with a url to the course<br />
                                        <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                        <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                        <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                                    </p>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["emails"]["courses"]["course_closed"]["enabled"]) echo 'checked="checked"';
                                            ?> name="course_closed_enabled" type="checkbox"> Enabled
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Email subject</label>
                                        <input
                                            value="<?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_closed"]["subject"]?>"
                                            type="text"
                                            class="form-control"
                                            name="course_closed_subject"
                                            placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="course_closed_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_closed"]["body"]?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Course Cancelled <small>sent to the course owner when their course is cancelled</small></h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        You can use the following replacers within the body of the email:<br />
                                        <span class="label label-primary">**SHORTNAME**</span> - will be replaced with the user's shortname<br />
                                        <span class="label label-primary">**LONGNAME**</span> - will be replaced with the user's longname<br />
                                        <span class="label label-primary">**COURSE_NAME**</span> - will be replaced with the course name<br />
                                        <span class="label label-primary">**COURSE_URL**</span> - will be replaced with a url to the course<br />
                                        <span class="label label-primary">**REASON**</span> - will be replaced with a url to the reason for cancellation<br />
                                        <span class="label label-primary">**APPLICATION_ADDRESS**</span> - will be replaced with your website address<br />
                                        <span class="label label-primary">**APPLICATION_NAME**</span> - will be replaced with your website name<br />
                                        <span class="label label-primary">**APPLICATION_COMPANY**</span> - will be replaced with your website company name<br />
                                    </p>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php
                                            if($this->config["ExtorioLMS"]["emails"]["courses"]["course_cancelled"]["enabled"]) echo 'checked="checked"';
                                            ?> name="course_cancelled_owner_enabled" type="checkbox"> Enabled
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Email subject</label>
                                        <input
                                            value="<?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_cancelled"]["subject"]?>"
                                            type="text"
                                            class="form-control"
                                            name="course_cancelled_owner_subject"
                                            placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <label>Email body</label>
                        <textarea
                            class="email_body"
                            name="course_cancelled_owner_body"
                            placeholder="Body"><?=$this->config["ExtorioLMS"]["emails"]["courses"]["course_cancelled"]["body"]?></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <br />
            <button type="submit" name="submitted" class="btn btn-primary">Update</button>
        </form>
        <script>
            $(function() {
                $('.email_body').extorio_editable();
            })
        </script>
        <?php
    }
}