<?php
namespace ExtorioLMS\Components\Apis;
/**
 *
 *
 * Class LMSConfig
 */
class LMSConfig extends \Core\Classes\Commons\Api {
    private $sections = array(
        "courses",
        "course_posts",
        "modules",
        "feedback",
        "uploads"
    );

    public function _onDefault($section = false) {
        if(!$section) {
            $this->_badRequest("You must provide a section. For example: /lms-cofig/[SECTION]");
        }
        if(!in_array($section,$this->sections)) {
            $this->_notFound();
        }
        $config = $this->_Extorio()->getConfig();
        $this->_output->data = $config["ExtorioLMS"][$section];
    }
}