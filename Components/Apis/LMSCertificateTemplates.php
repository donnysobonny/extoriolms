<?php
namespace ExtorioLMS\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\PGDB;
use ExtorioLMS\Classes\Enums\CertificateTemplatePrivileges;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\CertificateTemplate;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class LMSCertificateTemplates
 */
class LMSCertificateTemplates extends \Core\Classes\Commons\Api {

    public function site_default() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_failApi("You must be logged in to do that");
        }

        //must be able to create,edit etc
        if(!\Core\Classes\Utilities\Users::userHasPrivileges_OR($loggedInUser->id,array(
            "courses_create",
            "courses_modify_all"
        ))) {
            $this->_accessDenied("You are not able to view the site default template from here");
        }

        $this->_output->data = CertificateTemplate::findOne(Query::n()->where(array("siteDefault" => true)));
    }

    public $name;
    public $orderBy = "name";
    public $orderDirection = "ASC";
    public $limit = 50;
    public $skip = 0;

    public function filter() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_failApi("You must be logged in to do that");
        }

        $sql = '
        SELECT
        CT."ownerId",
        CT.id,
        CT."siteDefault",
        CT.name,
        CT.description,
        CT.body

        FROM extoriolms_classes_models_certificatetemplate CT

        ';
        $sqlCount = '
        SELECT

        count(DISTINCT CT.id)

        FROM extoriolms_classes_models_certificatetemplate CT

        ';

        //build the where part
        $where = "";
        $params = array();
        $paramIndex = 1;

        $where .= 'CT."ownerId" = $'.$paramIndex.' AND ';
        $params[] = $loggedInUser->id;
        $paramIndex++;

        if(strlen($this->name)) {
            $where .= 'CT."name" = $'.$paramIndex." AND ";
            $params[] = strval($this->name);
            $paramIndex++;
        }
        $where = substr($where,0,strlen($where)-4);
        if(strlen($where)) {
            $sql .= "WHERE ".$where." ";
            $sqlCount .= "WHERE ".$where." ";
        }

        //order
        if(strlen($this->orderBy)) {
            $sql .= 'ORDER BY "'.strval($this->orderBy).'" ';
            if(strlen($this->orderDirection)) {
                if(strtolower($this->orderDirection) == "asc") {
                    $sql .= "ASC ";
                } elseif(strtolower($this->orderDirection) == "desc") {
                    $sql .= "DESC ";
                }
            }
        }
        //limit
        if(!is_null($this->limit) && $this->limit > 0) {
            $sql .= "LIMIT ".intval($this->limit)." ";
        }
        //skip
        if(!is_null($this->skip) && $this->skip > 0) {
            $sql .= "OFFSET ".intval($this->skip)." ";
        }

        //                $this->_output->sql = $sql;
        //                $this->_output->sqlCount = $sqlCount;

        $db = $this->_Extorio()->getDbInstanceDefault();
        $cts = array();
        $query = $db->query($sql,$params);
        while($row = $query->fetchRow()) {
            $ct = new \stdClass();
            $ct->ownerId = intval($row[0]);
            $ct->siteDefault = PGDB::decodeBool($row[2]);
            $ct->name = $row[3];
            $ct->description = $row[4];
            $ct->body = $row[5];
            $ct->id = intval($row[1]);
            $cts[] = $ct;
        }

        $this->_output->data = $cts;
        $this->_output->countLimited = intval($query->numRows());

        $row = $db->query($sqlCount,$params)->fetchRow();
        $this->_output->countUnlimited = intval($row[0]);
    }

    public $data;

    public function _onDefault($templateId = false, $action = false) {

        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_failApi("You must be logged in to do that");
        } else {
            switch($this->_httpMethod) {
                case "GET" :

                    //the only GET request here is to get by id
                    if(!$templateId) {
                        $this->_failApi("Invalid api request");
                    } else {
                        $ct = CertificateTemplate::findById($templateId,1);
                        if(!$ct) {
                            $this->_notFound();
                        }
                        if($ct->ownerId != $loggedInUser->id) {
                            $this->_accessDenied();
                        }
                        $this->_output->data = $ct;
                    }

                    break;
                case "POST" :

                    if(!$templateId) {
                        //creating a new template
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set, or can not be read");
                        }

                        if(!\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,CertificateTemplatePrivileges::_certificate_templates_create)) {
                            $this->_accessDenied("You are not able to create certificate templates");
                        }

                        $ct = CertificateTemplate::constructFromArray($this->data);
                        $ct->id = null;
                        $ct->ownerId = $loggedInUser->id;
                        $ct->pushThis();
                        //output
                        $this->_output->data = $ct;
                    } else {
                        if(!$action) {
                            //updating
                            if(!is_array($this->data)) {
                                $this->_failApi("The data property is not set, or can not be read");
                            }
                            $ct = CertificateTemplate::findById($templateId,1);
                            if(!$ct) {
                                $this->_notFound("Trying to update a certificate template that does not exist");
                            }
                            $access = false;
                            if($ct->ownerId == $loggedInUser->id) {
                                $access = true;
                            }
                            elseif(\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,CertificateTemplatePrivileges::_certificate_templates_modify)) {
                                $access = true;
                            }
                            if(!$access) {
                                $this->_accessDenied("You are not able to modify this certificate template");
                            }

                            $dataCt = CertificateTemplate::constructFromArray($this->data);
                            $ct->name = $dataCt->name;
                            $ct->description = $dataCt->description;
                            $ct->body = $dataCt->body;

                            $ct->pushThis();
                            //output
                            $this->_output->data = $ct;
                        } else {
                            switch($action) {
                                case "delete" :

                                    $ct = CertificateTemplate::findById($templateId,1);
                                    if(!$ct) {
                                        $this->_notFound("You are trying to delete a certificate template that doesn't exist");
                                    }

                                    $access = false;
                                    if($ct->ownerId == $loggedInUser->id) {
                                        $access = true;
                                    }
                                    elseif(\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,CertificateTemplatePrivileges::_certificate_templates_delete)) {
                                        $access = true;
                                    }
                                    if(!$access) {
                                        $this->_accessDenied("You are not able to delete this certificate template");
                                    }

                                    $ct->deleteThis();
                                    break;
                                case "site_default" :

                                    $ct = CertificateTemplate::findById($templateId,1);
                                    if(!$ct) {
                                        $this->_notFound("You are trying to set a template to site default that doesn't exist");
                                    }

                                    $access = false;
                                    if($ct->ownerId == $loggedInUser->id) {
                                        $access = true;
                                    }
                                    elseif(\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,CertificateTemplatePrivileges::_certificate_templates_modify)) {
                                        $access = true;
                                    }
                                    if(!$access) {
                                        $this->_accessDenied("You are not able to modify this certificate template");
                                    }

                                    $ct->siteDefault = true;
                                    $ct->pushThis();
                                    //output
                                    $this->_output->data = $ct;

                                    break;
                            }
                        }
                    }

                    break;
            }
        }
    }
}