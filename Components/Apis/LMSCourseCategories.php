<?php
namespace ExtorioLMS\Components\Apis;
use Core\Classes\Helpers\Query;
use ExtorioLMS\Classes\Enums\CourseCategoryPrivileges;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\CourseCategory;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class LMSCourseCategories
 */
class LMSCourseCategories extends \Core\Classes\Commons\Api {

    public $name;
    public $parentId = 0;
    public $orderBy = "name";
    public $orderDirection = "asc";
    public $limit = 250;
    public $skip = 0;

    public function filter() {
        $query = Query::n();
        $where = array();

        if(strlen($this->name)) {
            $where[] = array("name" => $this->name);
        }
        if(strlen($this->parentId)) {
            $where[] = array("parentId" => $this->parentId);
        }

        $queryCount = clone($query);

        $query->order(array($this->orderBy => $this->orderDirection));
        $query->limit($this->limit);
        $query->skip($this->skip);

        $this->_output->data = CourseCategory::findAll($query,1);

        $this->_output->countLimited = CourseCategory::findCount($query);
        $this->_output->countUnlimited = CourseCategory::findCount($queryCount);
    }

    public $data;

    public function tree() {
        $this->_output->data = CourseCategory::getAllCategories();
    }

    public function _onDefault($categoryId = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                if(!$categoryId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_output->data = CourseCategory::findById($categoryId,1);
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :

                $loggedInUser = $this->_Extorio()->getLoggedInUser();
                if(!$loggedInUser) {
                    $this->_accessDenied("You must be logged in to do that");
                }

                if(!$categoryId) {
                    /*
                     * CREATE NEW CATEGORY
                     */
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }

                    if(!\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,CourseCategoryPrivileges::_course_categories_create)) {
                        $this->_accessDenied("You are not able to create course categories");
                    }

                    $dcategory = CourseCategory::constructFromArray($this->data);
                    $dcategory->id = null;
                    $dcategory->pushThis();

                    $this->_output->data = $dcategory;
                } else {
                    if(!$action) {
                        /*
                         * UPDATING EXISTING
                         */
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }

                        $category = CourseCategory::findById($categoryId,1);
                        if(!$category) {
                            $this->_notFound("Trying to update a category that does not exist");
                        }

                        if(!\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,CourseCategoryPrivileges::_course_categories_modify)) {
                            $this->_accessDenied("You are not able to modify course categories");
                        }

                        $dcategory = CourseCategory::constructFromArray($this->data);
                        $category->name = $dcategory->name;
                        $category->parentId = $dcategory->parentId;
                        $category->icon = $dcategory->icon;

                        $category->pushThis();

                        $this->_output->data = $category;

                    } else {
                        switch($action) {
                            case "delete" :

                                $category = CourseCategory::findById($categoryId,1);
                                if(!$category) {
                                    $this->_notFound("Trying to delete a category that does not exist");
                                }

                                if(!\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,CourseCategoryPrivileges::_course_categories_delete)) {
                                    $this->_accessDenied("You are not able to delete course categories");
                                }

                                $category->deleteThis();

                                break;
                        }
                    }
                }

                break;
        }
    }
}