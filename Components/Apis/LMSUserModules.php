<?php
namespace ExtorioLMS\Components\Apis;
use ExtorioLMS\Classes\Models\User_Module;

/**
 * 
 *
 * Class LMSUserModules
 */
class LMSUserModules extends \Core\Classes\Commons\Api {
    public function _onDefault($userModuleId = false, $action = false) {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_accessDenied("You must be logged in to do that.");
        }

        switch($this->_httpMethod) {
            case "GET" :
                if(!$userModuleId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $um = User_Module::findById($userModuleId,2);
                        if($um) {
                            if($um->userId != $loggedInUser->id) {
                                $this->_accessDenied("You can only access your own user modules");
                            }
                            $this->_output->data = $um;
                        } else {
                            $this->_output->data = null;
                        }
                    } else {
                        $this->_badRequest();
                    }
                }
                break;
            case "POST" :
                if(!$userModuleId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        switch($action) {
                            case "start" :

                                $um = User_Module::findById($userModuleId,2);
                                if(!$um) {
                                    $this->_notFound("You are trying to start a module that does not exist");
                                }

                                //can only start own module
                                if($um->userId != $loggedInUser->id) {
                                    $this->_accessDenied("You can only start your own module");
                                }

                                //try and start the module
                                $um->start();

                                $this->_output->data = $um;

                                break;
                            case "current" :

                                $um = User_Module::findById($userModuleId,2);
                                if(!$um) {
                                    $this->_notFound("You are trying to current a module that does not exist");
                                }

                                //can only start own module
                                if($um->userId != $loggedInUser->id) {
                                    $this->_accessDenied("You can only set your own modules to current");
                                }

                                //try and set to current
                                $um->current();

                                $this->_output->data = $um;

                                break;
                            case "submit" :

                                $um = User_Module::findById($userModuleId,2);
                                if(!$um) {
                                    $this->_notFound("You are trying to submit a module that does not exist");
                                }

                                //can only submit own modules
                                if($um->userId != $loggedInUser->id) {
                                    $this->_accessDenied("You are only able to submit your own modules");
                                }

                                //submit
                                $um->submit();

                                $this->_output->data = $um;

                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}