<?php
namespace ExtorioLMS\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Models\Enum;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentDifficulty;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Enums\CourseFilesFileType;
use ExtorioLMS\Classes\Enums\CourseFilesUploadType;
use ExtorioLMS\Classes\Enums\LectureType;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Enums\QuestionType;
use ExtorioLMS\Classes\Enums\RoleCategory;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class LMSEnums
 */
class LMSEnums extends \Core\Classes\Commons\Api {

    public $value;

    private $types = array(
        "course-files-upload-type",
        "course-files-file-type",
        "module-type",
        "lecture-type",
        "question-type",
        "role-category",
        "content-type",
        "content-status",
        "content-approval",
        "content-difficulty"
    );

    public function _onDefault($type = false, $action = false) {
        if(!in_array($type, $this->types)) {
            $this->_failApi("The type '".$type."' is not a valid enum type");
        } else {
            switch($this->_httpMethod) {
                case "GET" :

                    switch($type) {
                        case "course-files-upload-type" :
                            $this->_output->data = CourseFilesUploadType::values();
                            break;
                        case "course-files-file-type" :
                            $this->_output->data = CourseFilesFileType::values();
                            break;
                        case "module-type" :
                            $this->_output->data = ModuleType::values();
                            break;
                        case "lecture-type" :
                            $this->_output->data = LectureType::values();
                            break;
                        case "question-type" :
                            $this->_output->data = QuestionType::values();
                            break;
                        case "role-category" :
                            $this->_output->data = RoleCategory::values();
                            break;
                        case "content-type" :
                            $this->_output->data = ContentType::values();
                            break;
                        case "content-status" :
                            $this->_output->data = ContentStatus::values();
                            break;
                        case "content-approval" :
                            $this->_output->data = ContentApproval::values();
                            break;
                        case "content-difficulty" :
                            $this->_output->data = ContentDifficulty::values();
                            break;
                    }

                    break;
                case "POST" :

                    if(!$action) {
                        $this->_failApi("Invalid api request");
                    } else {
                        if(!strlen($this->value)) {
                            $this->_failApi("The value property is not set");
                        } else {
                            if(!in_array($type, array(
                                "content-difficulty"
                            ))) {
                                $this->_failApi("The type '".$type."' cannot be modified using this api");
                            } else {
                                $loggedInUser = $this->_Extorio()->getLoggedInUser();
                                if(!$loggedInUser) {
                                    $this->_failApi("You must be logged in to do that");
                                } else {
                                    switch($action) {
                                        case "add" :
                                            switch($type) {
                                                case "content-difficulty" :

                                                    //can user add values
                                                    if(!\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,"course_difficulty_create")) {
                                                        $this->_failApi("You are not able to add values to this enum");
                                                    } else {
                                                        $enum = Enum::findOne(
                                                            Query::n()
                                                                ->where(array(
                                                                    "label" => "content difficulty",
                                                                    "extensionName" => "ExtorioLMS"
                                                                )),1
                                                        );
                                                        if(!$enum) {
                                                            $this->_failApi("Could not find the content difficulty enum...");
                                                        } else {
                                                            if(in_array($this->value, $enum->values)) {
                                                                $this->_failApi("The value '".$this->value."' already exists in this enum");
                                                            } else {
                                                                $enum->values[] = strval($this->value);
                                                                $enum->pushThis();

                                                                $this->_output->data = $enum->values;
                                                            }
                                                        }
                                                    }

                                                    break;
                                            }
                                            break;
                                        case "remove" :
                                            switch($type) {
                                                case "content-difficulty" :

                                                    //can user remove
                                                    if(!\Core\Classes\Utilities\Users::userHasPrivilege($loggedInUser->id,"content_difficulty_delete")) {
                                                        $this->_failApi("You are not able to remove values from this enum");
                                                    } else {
                                                        $enum = Enum::findOne(
                                                            Query::n()
                                                                ->where(array(
                                                                    "label" => "content difficulty",
                                                                    "extensionName" => "ExtorioLMS"
                                                                )),1
                                                        );
                                                        if(!$enum) {
                                                            $this->_failApi("Could not find the content difficulty enum...");
                                                        } else {
                                                            if(!in_array($this->value, $enum->values)) {
                                                                $this->_failApi("This enum does not contain the value '".$this->value."'");
                                                            } else {
                                                                $newValues = array();
                                                                foreach($enum->values as $value) {
                                                                    if($value != $this->value) {
                                                                        $newValues[] = $value;
                                                                    }
                                                                }
                                                                $enum->values = $newValues;
                                                                $enum->pushThis();

                                                                $this->_output->data = $enum->values;
                                                            }
                                                        }
                                                    }

                                                    break;
                                            }
                                            break;
                                        default:
                                            $this->_failApi("'".$action."' is not a valid action");
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    break;
            }
        }
    }
}