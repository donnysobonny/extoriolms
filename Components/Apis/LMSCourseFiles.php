<?php
namespace ExtorioLMS\Components\Apis;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\Query;
use ExtorioLMS\Classes\Enums\CoursePrivileges;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\CourseFile;
use ExtorioLMS\Classes\Utilities\Misc;

/**
 * 
 *
 * Class LMSCourseFiles
 */
class LMSCourseFiles extends \Core\Classes\Commons\Api {

    public $uploadType;
    public $fileType;
    public $fileName;
    public $bucket;
    public $contentType;
    public $orderBy = "fileName";
    public $orderDirection = "asc";
    public $limit = 50;
    public $skip = 0;

    public function filter() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_accessDenied("You must be logged in to do that");
        }

        $query = Query::n();
        $where = array();
        $where[] = array("userId" => $loggedInUser->id);

        if(strlen($this->uploadType)) {
            $where[] = array("uploadType" => $this->uploadType);
        }
        if(strlen($this->fileType)) {
            $where[] = array("fileType" => $this->fileType);
        }
        if(strlen($this->fileName)) {
            $where[] = array("fileName" => array(
                Query::_lk => "%".$this->fileName."%"
            ));
        }
        if(strlen($this->bucket)) {
            $where[] = array("bucket" => $this->bucket);
        }
        if(strlen($this->contentType)) {
            $where[] = array("contentType" => $this->contentType);
        }

        $query->where($where);
        $queryCount = clone($query);

        $query->order(array($this->orderBy => $this->orderDirection));
        $query->limit($this->limit);
        $query->skip($this->skip);

        $this->_output->data = CourseFile::findAll($query,1);

        $this->_output->countLimited = CourseFile::findCount($query);
        $this->_output->countUnlimited = CourseFile::findCount($queryCount);
    }

    public $data = array();

    public function bucket() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_accessDenied("You must be logged in to do that");
        }

        $url = Misc::createS3Client()->getObjectUrl($this->bucket, '-');
        $this->_output->data = substr($url,0,strlen($url)-1);
    }

    public function _onDefault($id = false, $action = false) {

        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_accessDenied("You must be logged in to do that");
        }

        switch($this->_httpMethod) {
            case "GET" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        switch($action) {
                            case "url" :
                                $cf = CourseFile::findById($id,1);
                                if($cf) {
                                    $this->_output->data = $cf->publicUrl();
                                } else {
                                    $this->_output->data = null;
                                }
                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
            case "POST" :

                if(!is_array($this->data)) {
                    $this->_failApi("The data property is not set or could not be read");
                }

                $dCourseFile = CourseFile::constructFromArray($this->data);
                //if there is already a course file with the upload type, file type, file name, bucket and user, re-use it
                $cf = CourseFile::findOne(Query::n()->where(array(
                    "userId" => $loggedInUser->id,
                    "uploadType" => $dCourseFile->uploadType,
                    "fileType" => $dCourseFile->fileType,
                    "fileName" => $dCourseFile->fileName,
                    "bucket" => $dCourseFile->bucket
                )));
                if($cf) {
                    $dCourseFile->id = $cf->id;
                } else {
                    $dCourseFile->id = null;
                }
                $dCourseFile->userId = $loggedInUser->id;

                $access = false;
                //if can create, or modify courses
                if(\Core\Classes\Utilities\Users::userHasPrivileges_OR($loggedInUser->id,array(
                    CoursePrivileges::_courses_create,
                    CoursePrivileges::_courses_modify_all
                ))) {
                    $access = true;
                }

                if(!$access) {
                    $this->_accessDenied("You are not able to generate course files");
                }

                //save the reference
                $dCourseFile->pushThis();

                $this->_output->data = $dCourseFile;

                $config = Extorio::get()->getConfig();

                // Options and Settings
                $algorithm = "AWS4-HMAC-SHA256";
                $service = "s3";
                $date = gmdate('Ymd\THis\Z');
                $shortDate = gmdate('Ymd');
                $requestType = "aws4_request";

                // Step 1: Generate the Scope
                $scope = array(
                    $config["ExtorioLMS"]["aws"]["s3"]["key"],
                    $shortDate,
                    $config["ExtorioLMS"]["aws"]["s3"]["region"],
                    $service,
                    $requestType
                );
                $credentials = implode('/', $scope);

                // Step 2: Making a Base64 Policy
                $policy = array(
                    'expiration' => '20020-01-01T00:00:00Z',
                    'conditions' => array(
                        array('bucket' => $dCourseFile->bucket),
                        array('acl' => 'private'),
                        array('key' => $dCourseFile->key),
                        array('Content-Type' => $dCourseFile->contentType),
                        array("content-length-range", 0, $config["ExtorioLMS"]["uploads"][$dCourseFile->fileType]["maximum_size"]),
                        array('x-amz-credential' => $credentials),
                        array('x-amz-algorithm' => $algorithm),
                        array('x-amz-date' => $date)
                    )
                );
                $base64Policy = base64_encode(json_encode($policy));

                // Step 3: Signing your Request (Making a Signature)
                $dateKey = hash_hmac('sha256', $shortDate, 'AWS4' . $config["ExtorioLMS"]["aws"]["s3"]["secret"], true);
                $dateRegionKey = hash_hmac('sha256', $config["ExtorioLMS"]["aws"]["s3"]["region"], $dateKey, true);
                $dateRegionServiceKey = hash_hmac('sha256', $service, $dateRegionKey, true);
                $signingKey = hash_hmac('sha256', $requestType, $dateRegionServiceKey, true);

                $signature = hash_hmac('sha256', $base64Policy, $signingKey);

                // Step 4: Build form inputs
                // This is the data that will get sent with the form to S3
                $this->_output->signature = array(
                    'key' => $dCourseFile->key,
                    'Content-Type' => $dCourseFile->contentType,
                    'acl' => "private",
                    'policy' => $base64Policy,
                    'X-amz-credential' => $credentials,
                    'X-amz-algorithm' => $algorithm,
                    'X-amz-date' => $date,
                    'X-amz-signature' => $signature
                );

                $url = Misc::createS3Client()->getObjectUrl($dCourseFile->bucket, '-');
                $this->_output->url = substr($url,0,strlen($url)-1);

                break;
        }
    }
}