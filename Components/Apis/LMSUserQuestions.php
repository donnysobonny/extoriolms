<?php
namespace ExtorioLMS\Components\Apis;
use ExtorioLMS\Classes\Models\User_Question;

/**
 * 
 *
 * Class LMSUserQuestions
 */
class LMSUserQuestions extends \Core\Classes\Commons\Api {

    public $data;

    public function _onDefault($userQuestionId = false, $action = false) {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_accessDenied("You must be logged in to do that");
        }

        switch($this->_httpMethod) {
            case "GET" :
                $this->_badRequest();
                break;
            case "POST" :
                if(!$userQuestionId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        switch($action) {
                            case "save" :

                                if(!is_array($this->data)) {
                                    $this->_failApi("The data property is not set or could not be read");
                                }

                                $uq = User_Question::findById($userQuestionId,1);
                                if(!$uq) {
                                    $this->_notFound("Trying to save a user question that does not exist");
                                }

                                if($uq->userId != $loggedInUser->id) {
                                    $this->_accessDenied("You can only save your own user questions");
                                }

                                $duq = User_Question::constructFromArray($this->data);
                                //extract the answers
                                $uq->userAnswers = $duq->userAnswers;

                                //try to save
                                $uq->save();

                                $this->_output->data = $uq;

                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
        }
    }
}