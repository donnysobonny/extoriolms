<?php
namespace ExtorioLMS\Components\Apis;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\CoursePrivileges;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Utilities\Content;

/**
 *
 *
 * Class LMSCourses
 */
class LMSCourses extends \Core\Classes\Commons\Api {

    public $data;
    public $name;
    public $reason;

    public function _onDefault($courseId = false, $action = false) {

        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        $viewingUserId = 0;
        if($loggedInUser) {
            $viewingUserId = $loggedInUser->id;
        }

        switch($this->_httpMethod) {
            case "GET" :

                if(!$courseId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $course = Course::findById($courseId,INF);
                        if($course) {
                            $this->_output->data = Content::stripCourse($viewingUserId, $course);
                        } else {
                            $this->_output->data = null;
                        }
                    }
                }

                break;
            case "POST" :

                if(!$courseId) {
                    /*
                     * CREATING A NEW COURSE
                     */
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set or could not be read");
                    }
                    if(!Users::userHasPrivilege($viewingUserId,CoursePrivileges::_courses_create)) {
                        $this->_accessDenied("You are not able to create courses");
                    }

                    $dCourse = Course::constructFromArray($this->data);

                    //approval must be drafting/pending at this point
                    if(!in_array($dCourse->approval, array(
                        ContentApproval::_drafting,
                        ContentApproval::_pending
                    ))) {
                        $this->_failApi("When creating a course, it's approval must either be drafting or pending");
                    }

                    $dCourse->id = null;
                    $dCourse->ownerId = $viewingUserId;

                    $dCourse->pushThis();

                    $this->_output->data = Content::stripCourse($viewingUserId, $dCourse);
                } else {
                    if(!$action) {
                        /*
                         * Updating existing course
                         */
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set or could not be read");
                        }

                        $course = Course::findById($courseId,INF);
                        if(!$course) {
                            $this->_failApi("Trying to modify a course that does not exist");
                        }

                        $access = false;
                        //if owner
                        if($course->ownerId == $viewingUserId) {
                            $access = true;
                        }
                        //if can modify
                        elseif(Users::userManagesUserAndHasPrivilege($viewingUserId,$course->ownerId,CoursePrivileges::_courses_modify_all)) {
                            $access = true;
                        }
                        if(!$access) {
                            $this->_accessDenied("You are not able to modify this course");
                        }

                        $dCourse = Course::constructFromArray($this->data);

                        //extract the allowed properties
                        $course->name = $dCourse->name;
                        $course->description = $dCourse->description;
                        $course->category = $dCourse->category;
                        $course->subCategory = $dCourse->subCategory;
                        $course->difficulty = $dCourse->difficulty;
                        $course->tags = $dCourse->tags;
                        $course->expiryMinutes = $dCourse->expiryMinutes;
                        $course->certificateTemplate = $dCourse->certificateTemplate;

                        if(
                            in_array($course->approval, array(
                                ContentApproval::_drafting,
                                ContentApproval::_pending
                            )) &&
                            in_array($dCourse->approval, array(
                                ContentApproval::_drafting,
                                ContentApproval::_pending
                            ))
                        ) {
                            $course->approval = $dCourse->approval;
                        }

                        $course->selfEnrollable = $dCourse->selfEnrollable;
                        $course->lessons = $dCourse->lessons;
                        $course->defaultFileId = $dCourse->defaultFileId;

                        $course->pushThis();

                        $this->_output->data = Content::stripCourse($viewingUserId, $course);
                    } else {
                        $this->_badRequest();
                    }
                }

                break;
        }
    }
}