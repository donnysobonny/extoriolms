<?php
namespace ExtorioLMS\Components\Apis;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\CourseCategoryPrivileges;
use ExtorioLMS\Classes\Enums\CourseTagPrivileges;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\CourseTag;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class LMSCourseTags
 */
class LMSCourseTags extends \Core\Classes\Commons\Api {

    public $phrase;
    public $limit = 10;

    public function search() {
        if(!strlen($this->phrase)) {
            $this->_failApi("You must include a search phrase in your search request");
        } else {
            //clean up the phrase
            $this->phrase = strtolower(Strings::removeRecurring(Strings::removeQuotes(Strings::removeSpecialCharacters($this->phrase))," "));
            //seperate works with &
            $words = explode(" ",$this->phrase);
            $clean = "";
            foreach($words as $word) {
                if(strlen($word)) {
                    $clean .= $word." & ";
                }
            }
            $clean = substr($clean,0,strlen($clean)-3);
            if(!strlen($clean)) {
                $this->_failApi("Invalid search phrase. Please try again.");
            } else {
                $sql = '
SELECT
t.id,
t.name,
t.description

FROM extoriolms_classes_models_coursetag t,
to_tsquery(\''.$clean.'\') query

WHERE
query @@ to_tsvector(t.name) OR
query @@ to_tsvector(t.description)

ORDER BY ts_rank_cd(to_tsvector(t.name), query) + ts_rank_cd(to_tsvector(t.description), query) ';

                //limit
                if(strlen($this->limit) && $this->limit > 0) {
                    $sql .= "LIMIT ".intval($this->limit)." ";
                } else {
                    $sql .= "LIMIT 10 ";
                }

                $db = $this->_Extorio()->getDbInstanceDefault();
                $tags = array();
                $query = $db->query($sql);
                while($row = $query->fetchRow()) {
                    $tag = new \stdClass();
                    $tag->id = intval($row[0]);
                    $tag->name = $row[1];
                    $tag->description = $row[2];
                    $tags[] = $tag;
                }
                $this->_output->data = $tags;
            }
        }



    }

    public $data;

    public function _onDefault($tagId = false, $action = false) {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if($loggedInUser) {
            $viewingUserId = $loggedInUser->id;
        } else {
            $viewingUserId = 0;
        }

        switch($this->_httpMethod) {
            case "GET" :

                //only GET request is to get by id
                if(!$tagId) {
                    $this->_badRequest();
                } else {
                    $this->_output->data = CourseTag::findById($tagId,1);
                }

                break;
            case "POST" :

                if(!$tagId) {
                    //creating a new tag
                    if(!is_array($this->data)) {
                        $this->_failApi("The data property is not set, or cannot be read");
                    } else {
                        if(!\Core\Classes\Utilities\Users::userHasPrivilege($viewingUserId,CourseTagPrivileges::_course_tags_create)) {
                            $this->_accessDenied("You are not able to create course tags");
                        }
                        $tag = CourseTag::constructFromArray($this->data);
                        $tag->id = null;
                        $tag->pushThis();
                        $this->_output->data = $tag;
                    }
                } else {
                    if(!$action) {
                        //updating
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set, or cannot be read");
                        } else {
                            if(!\Core\Classes\Utilities\Users::userHasPrivilege($viewingUserId,CourseCategoryPrivileges::_course_categories_modify)) {
                                $this->_failApi("You are not able to modify course tags");
                            } else {
                                $tag = CourseTag::findById($tagId,1);
                                if(!$tag) {
                                    $this->_failApi("You are trying to modify a course tag that does not exist");
                                } else {
                                    $dTag = CourseTag::constructFromArray($this->data);

                                    $tag->name = $dTag->name;
                                    $tag->description = $dTag->description;

                                    $tag->pushThis();
                                    //output
                                    $this->_output->data = $tag;
                                }
                            }
                        }
                    } else {
                        switch($action) {
                            case "delete" :
                                if(!\Core\Classes\Utilities\Users::userHasPrivilege($viewingUserId,CourseTagPrivileges::_course_tags_delete)) {
                                    $this->_accessDenied("You are not able to delete course tags");
                                }
                                $tag = CourseTag::findById($tagId,1);
                                if(!$tag) {
                                    $this->_notFound("You are trying to delete a course tag that does not exist");
                                } else {
                                    $tag->deleteThis();
                                }
                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }

                break;
        }
    }
}