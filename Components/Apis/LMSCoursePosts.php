<?php
namespace ExtorioLMS\Components\Apis;
use ExtorioLMS\Classes\Models\CoursePost;

/**
 * This api is used to modify course posts
 *
 * Class LMSCoursePosts
 */
class LMSCoursePosts extends \Core\Classes\Commons\Api {

    public $data;

    public function _onDefault($id = false, $action = false) {
        switch($this->_httpMethod) {
            case "GET" :
                $this->_badRequest();
                break;
            case "POST" :
                if(!$id) {
                    $this->_badRequest();
                } else {
                    if(!$action) {

                        $this->_output->test = $this->_Extorio()->getInputStream();

                        if(!is_array($this->data)) {
                            $this->_failApi("The data property could not be read or is not set");
                        }
                        $cp = CoursePost::findById($id);
                        if(!$cp) {
                            $this->_notFound();
                        }
                        if($cp->userId != $this->_getLoggedInUserId()) {
                            $this->_accessDenied("You can only modify your own posts");
                        }

                        $dcp = CoursePost::constructFromArray($this->data);
                        $cp->name = $dcp->name;
                        $cp->body = $dcp->body;
                        $cp->pushThis();

                        $this->_output->data = $cp;

                    } else {
                        $this->_badRequest();
                    }
                }
                break;
        }
    }
}