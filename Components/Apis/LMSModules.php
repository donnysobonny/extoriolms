<?php
namespace ExtorioLMS\Components\Apis;
use ExtorioLMS\Classes\Enums\CoursePrivileges;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\Module;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class LMSModules
 */
class LMSModules extends \Core\Classes\Commons\Api {

    public $nocache = false;

    public function _onDefault($moduleId = false, $action = false) {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        $viewingUserId = 0;
        if($loggedInUser) {
            $viewingUserId = $loggedInUser->id;
        }

        switch($this->_httpMethod) {
            case "GET" :
                if(!$moduleId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        switch($action) {
                            case "preview" :
                                $age = 60*60*2;
                                //cache
                                $this->_cacheOutput($age);

                                $module = Module::findById($moduleId,1);
                                if(!$module) {
                                    $this->_notFound("Could not find module");
                                }

                                if($module->type != ModuleType::_lecture) {
                                    $this->_failApi("You can only preview lecture modules");
                                }

                                $course = Course::findById($module->courseId,1);
                                if(!$course) {
                                    $this->_notFound("Could not find the associated course");
                                }

                                $access = false;
                                //if previewable
                                if($module->previewable) {
                                    $access = true;
                                }
                                //if owner
                                elseif($course->ownerId == $viewingUserId) {
                                    $access = true;
                                }
                                //if can modify
                                elseif(\Core\Classes\Utilities\Users::userManagesUserAndHasPrivilege($viewingUserId,$course->ownerId,CoursePrivileges::_courses_modify_all)) {
                                    $access = true;
                                }
                                //if enrolled
                                elseif(Users::userEnrolledOnCourse($viewingUserId, $course->id)) {
                                    $access = true;
                                }

                                if(!$access) {
                                    $this->_accessDenied("You do not have access to preview this module");
                                }

                                $display = $module->fetchPreview($age);
                                if(!strlen($display)) {
                                    $this->_failApi("There was a problem previewing this lecture module.");
                                } else {
                                    $this->_output->data = $display;
                                }
                                break;
                            case "url" :
                                if(!$this->nocache) {
                                    $age = 60*60*2;
                                    //cache
                                    $this->_cacheOutput($age);
                                }

                                $module = Module::findById($moduleId,1);
                                if(!$module) {
                                    $this->_notFound("Could not find module");
                                }

                                if($module->type != ModuleType::_lecture) {
                                    $this->_failApi("You can only preview lecture modules");
                                }

                                $course = Course::findById($module->courseId,1);
                                if(!$course) {
                                    $this->_notFound("Could not find the associated course");
                                }

                                $access = false;
                                //if previewable
                                if($module->previewable) {
                                    $access = true;
                                }
                                //if owner
                                elseif($course->ownerId == $viewingUserId) {
                                    $access = true;
                                }
                                //if can modify
                                elseif(\Core\Classes\Utilities\Users::userManagesUserAndHasPrivilege($viewingUserId,$course->ownerId,CoursePrivileges::_courses_modify_all)) {
                                    $access = true;
                                }
                                //if enrolled
                                elseif(Users::userEnrolledOnCourse($viewingUserId, $course->id)) {
                                    $access = true;
                                }

                                if(!$access) {
                                    $this->_accessDenied("You do not have access to preview this module");
                                }

                                $this->_output->data = $module->fetchUrl($age);
                                break;
                            default:
                                $this->_failApi("Invalid api request");
                                break;
                        }
                    }
                }
                break;
            case "POST" :
                $this->_badRequest();
                break;
        }
    }
}