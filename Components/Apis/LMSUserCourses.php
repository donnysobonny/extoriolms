<?php
namespace ExtorioLMS\Components\Apis;
use Core\Classes\Helpers\Query;
use Core\Classes\Utilities\Arrays;
use Core\Classes\Utilities\PGDB;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\User_Course;
use ExtorioLMS\Classes\Models\User_Module;
use ExtorioLMS\Classes\Utilities\Content;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class LMSUserCourses
 */
class LMSUserCourses extends \Core\Classes\Commons\Api {

    public $userId;
    public $courseId;
    public $expiryDate;

    public function _onDefault($userCourseId = false, $action = false) {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_accessDenied("You must be logged in to do that");
        }

        switch($this->_httpMethod) {
            case "GET" :
                if(!$userCourseId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $uc = User_Course::findById($userCourseId,1);
                        if($uc) {
                            if($uc->userId != $loggedInUser->id) {
                                $this->_accessDenied("You can only access your own user courses");
                            }
                            $this->_output->data = $uc;
                        } else {
                            $this->_output->data = null;
                        }
                        if($uc->userId == $loggedInUser->id) {
                            $this->_output->data = $uc;
                        } else {

                        }
                    } else {
                        switch($action) {
                            case "detail" :
                                $uc = User_Course::findById($userCourseId,1);
                                if(!$uc) {
                                    $this->_notFound();
                                }

                                $course = Course::findById($uc->courseId,INF);
                                if(!$course) {
                                    $this->_notFound("Could not find the associated course");
                                }

                                $access = false;
                                if($loggedInUser->id == $uc->userId) {
                                    $access = true;
                                }
                                elseif(\Core\Classes\Utilities\Users::userManagesUserAndHasPrivilege($loggedInUser->id,$uc->userId,"user_courses_modify_all")) {
                                    $access = true;
                                }
                                elseif(
                                    $course->ownerId == $loggedInUser->id &&
                                    \Core\Classes\Utilities\Users::userManagesUserAndHasPrivilege($loggedInUser->id,$uc->userId,"user_courses_modify_own")
                                ) {
                                    $access = true;
                                }
                                if(!$access) {
                                    $this->_accessDenied("You are not able to view this user course in detail");
                                }

                                $this->_output->course = Content::stripCourse($loggedInUser->id, $course);

                                $uc->userLessons = array();
                                foreach($course->lessons as $lesson) {
                                    $userLesson = new \stdClass();
                                    $userLesson->lessonId = $lesson->id;
                                    $userLesson->userModules = array();
                                    foreach($lesson->modules as $module) {
                                        $um = User_Module::findOne(
                                            Query::n()
                                                ->where(array(
                                                    "userId" => $uc->userId,
                                                    "courseId" => $course->id,
                                                    "moduleId" => $module->id
                                                )),2
                                        );
                                        $userLesson->userModules[] = $um;
                                    }
                                    $uc->userLessons[] = $userLesson;
                                }

                                $this->_output->data = $uc;

                                break;
                            case "detail_view" :
                                $this->_cacheOutput(30);

                                $uc = User_Course::findById($userCourseId,1);
                                if(!$uc) {
                                    $this->_notFound();
                                }

                                $course = Course::findById($uc->courseId,1);
                                if(!$course) {
                                    $this->_notFound("Could not find the associated course");
                                }

                                $access = false;
                                if($loggedInUser->id == $uc->userId) {
                                    $access = true;
                                }
                                elseif(\Core\Classes\Utilities\Users::userManagesUserAndHasPrivilege($loggedInUser->id,$uc->userId,"user_courses_modify_all")) {
                                    $access = true;
                                }
                                elseif(
                                    $course->ownerId == $loggedInUser->id &&
                                    \Core\Classes\Utilities\Users::userManagesUserAndHasPrivilege($loggedInUser->id,$uc->userId,"user_courses_modify_own")
                                ) {
                                    $access = true;
                                }
                                if(!$access) {
                                    $this->_accessDenied("You are not able to view this user course in detail");
                                }

                                $db = $this->_getDbInstanceDefault();
                                $data = array();
                                $courseProgress = 0;
                                for($i = 0; $i < count($course->lessons); $i++) {
                                    $lesson = $course->lessons[$i];
                                    $sql = '
                                    SELECT
                                    L.name,
                                    L.modules,
                                    (
                                      SELECT SUM("maximumPoints") FROM extoriolms_classes_models_module WHERE id = ANY(L.modules)
                                    ) AS "maximumPoints",
                                    (
                                      SELECT SUM("currentPoints") FROM extoriolms_classes_models_user_module WHERE "moduleId" = ANY(L.modules) AND "userCourseId" = $2
                                    ) AS "currentPoints"

                                    FROM extoriolms_classes_models_lesson L WHERE L.id = $1
                                    ';
                                    $l = $db->query($sql,array($lesson,$uc->id))->fetchAssoc();
                                    $modules = Arrays::smartCast(PGDB::decodeArray($l["modules"]));
                                    $l["modules"] = array();
                                    foreach($modules as $module) {
                                        $sql = '
                                        SELECT
                                        M.name,
                                        M."maximumPoints",
                                        M.type,
                                        M."lectureType",
                                        UM.status,
                                        UM."currentPoints",
                                        UM."currentAttempts"

                                        FROM extoriolms_classes_models_module M

                                        LEFT JOIN extoriolms_classes_models_user_module UM ON
                                        UM."moduleId" = M.id AND
                                        UM."userCourseId" = $1

                                        WHERE M.id = $2
                                        ';
                                        $m = $db->query($sql,array($uc->id,$module))->fetchAssoc();
                                        $l["modules"][] = $m;
                                    }
                                    $courseProgress += intval($l["currentPoints"]);
                                    $data[] = $l;
                                }

                                $html = "";
                                ob_start();
//                                echo "<pre>";
//                                print_r($data);
//                                echo "</pre>";
                                $currentPerc = round(($courseProgress / $course->maximumPoints) * 100);
                                ?>
                                <table style="margin: 0;" class="table table-condensed">
                                    <tbody>
                                        <tr class="active">
                                            <td style="vertical-align: middle;">
                                                <h3 style="margin: 0;"><?=$course->name?> <small>(<?=$uc->status?>)</small></h3>
                                            </td>
                                            <td style="vertical-align: middle; min-width: 30%;">
                                                <?php
                                                if($uc->status == ContentStatus::_passed) {
                                                    ?>
                                                    <div style="margin: 0;" class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="<?=$currentPerc?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?=$currentPerc?>%;">
                                                            PASSED
                                                        </div>
                                                    </div>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <div style="margin: 0;" class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="<?=$currentPerc?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?=$currentPerc?>%;">
                                                            <?=$currentPerc?>%
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                        foreach($data as $lesson) {
                                            $lessonPerc = round((intval($lesson["currentPoints"]) / intval($lesson["maximumPoints"])) * 100);
                                            ?>
                                            <tr class="active">
                                                <td style="padding-left: 20px; vertical-align: middle;">
                                                    <h4 style="margin: 0;"><?=$lesson["name"]?></h4>
                                                </td>
                                                <td style="vertical-align: middle; min-width: 30%;">
                                                    <div style="margin: 0;" class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="<?=$lessonPerc?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?=$lessonPerc?>%;">
                                                            <?=$lessonPerc?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                            foreach($lesson["modules"] as $module) {
                                                $modulePerc = round((intval($module["currentPoints"]) / intval($module["maximumPoints"])) * 100);
                                                ?>
                                                <tr class="<?php
                                                switch($module["status"]) {
                                                    case ContentStatus::_started:
                                                        echo "warning";
                                                        break;
                                                    case ContentStatus::_failed:
                                                        echo "danger";
                                                        break;
                                                    case ContentStatus::_submitted:
                                                        echo "info";
                                                        break;
                                                    case ContentStatus::_passed:
                                                        echo "success";
                                                        break;
                                                }
                                                ?>">
                                                    <td style="padding-left: 40px; vertical-align: middle;">
                                                        <h5 style="margin: 0;"><span class="fa fa-<?=Content::getModuleIcon($module["type"],$module["lectureType"])?>"></span> <?=$module["name"]?> <small>(<?=$module["status"]?>)</small></h5>
                                                    </td>
                                                    <td style="vertical-align: middle; min-width: 30%;">
                                                        <div style="margin: 0;" class="progress">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="<?=$modulePerc?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?=$modulePerc?>%;">
                                                                <?=$modulePerc?>%
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>

                                <?php
                                foreach($data as $lesson) {

                                }
                                $html = ob_get_clean();
                                $this->_output->data = $html;
                                $this->_output->progress = $currentPerc;

                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }
                break;
            case "POST" :

                if(!$userCourseId) {
                    $this->_badRequest();
                } else {
                    if(!$action) {
                        $this->_badRequest();
                    } else {
                        switch($action) {
                            case "start" :

                                $uc = User_Course::findById($userCourseId,1);
                                if(!$uc) {
                                    $this->_notFound();
                                }

                                //can only start own
                                if($uc->userId != $loggedInUser->id) {
                                    $this->_accessDenied("You can only start your own course enrolment");
                                }

                                //try and start
                                $uc->start();

                                $this->_output->data = $uc;

                                break;
                            default:
                                $this->_badRequest();
                                break;
                        }
                    }
                }

                break;
        }
    }
}