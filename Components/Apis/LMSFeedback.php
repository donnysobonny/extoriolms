<?php
namespace ExtorioLMS\Components\Apis;
use Core\Classes\Commons\Extorio;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\FeedbackPrivileges;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\Feedback;
use ExtorioLMS\Classes\Utilities\Users;

/**
 * 
 *
 * Class LMSFeedback
 */
class LMSFeedback extends \Core\Classes\Commons\Api {

    public function _onBegin() {
        $config = $this->_getConfig();
        if(!$config["ExtorioLMS"]["feedback"]["enabled"]) {
            $this->_failApi("Course feedback is disabled on this website");
        }
    }

    public $courseId;
    public $ratingFrom;
    public $ratingTo;
    public $ignoreNoComment = false;
    public $limit = 10;
    public $skip = 0;

    public function filter() {
        $db = Extorio::get()->getDbInstanceDefault();
        $sql = '
        SELECT

        F.id,
        F."ownerId",
        U.avatar as "ownerAvatar",
        U.shortname as "ownerShortName",
        F."courseId",
        C.name as "courseName",
        F."dateUpdated",
        F.rating,
        F.comment,
        F.approval

        FROM extoriolms_classes_models_feedback F

        LEFT JOIN core_classes_models_user U ON
        F."ownerId" = U.id
        LEFT JOIN extoriolms_classes_models_course C ON
        C.id = F."courseId"
        ';

        $sqlCount = '

        SELECT

        count(F.id) FROM extoriolms_classes_models_feedback F

        LEFT JOIN core_classes_models_user U ON
        F."ownerId" = U.id
        LEFT JOIN extoriolms_classes_models_course C ON
        C.id = F."courseId"
        ';

        //build the where
        $where = "";
        $params = array();
        $paramIndex = 1;

        $where .= 'F.approval = $'.$paramIndex.' AND ';
        $params[] = ContentApproval::_approved;
        $paramIndex++;

        if(strlen($this->courseId)) {
            $where .= 'F."courseId" = $'.$paramIndex." AND ";
            $params[] = $this->courseId;
            $paramIndex++;
        }
        if(strlen($this->ratingFrom)) {
            $where .= 'F.rating >= $'.$paramIndex." AND ";
            $params[] = intval($this->ratingFrom);
            $paramIndex++;
        }
        if(strlen($this->ratingTo)) {
            $where .= 'F.rating <= $'.$paramIndex." AND ";
            $params[] = intval($this->ratingTo);
            $paramIndex++;
        }
        if(!is_null($this->ignoreNoComment)) {
            if($this->ignoreNoComment) {
                $where .= 'F.comment != $'.$paramIndex." AND ";
                $params[] = "";
                $paramIndex++;
            }
        }
        if(strlen($where)) {
            $sql .= "WHERE ".substr($where,0,strlen($where)-4);
            $sqlCount .= "WHERE ".substr($where,0,strlen($where)-4);
        }

        $sql .= 'ORDER BY F."dateUpdated" DESC ';

        if(strlen($this->limit) && $this->limit > 0) {
            $sql .= "LIMIT ".intval($this->limit)." ";
        }
        if(strlen($this->skip) && $this->skip > 0) {
            $sql .= "OFFSET ".intval($this->skip)." ";
        }

        //                $this->_output->sql = $sql;
        //                $this->_output->sqlCount = $sqlCount;

        $query = $db->query($sql,$params);
        $feedbacks = array();
        while($row = $query->fetchRow()) {
            $feedback = new \stdClass();
            $feedback->id = intval($row[0]);
            $feedback->ownerId = intval($row[1]);
            $feedback->ownerAvatar = $row[2];
            $feedback->ownerShortName = $row[3];
            $feedback->courseId = intval($row[4]);
            $feedback->contentName = $row[5];
            $feedback->dateUpdated = $row[6];
            $feedback->rating = intval($row[7]);
            $feedback->comment = $row[8];
            $feedback->approval = $row[9];

            $feedbacks[] = $feedback;
        }

        $this->_output->data = $feedbacks;
        $this->_output->countLimited = $query->numRows();

        $row = $db->query($sqlCount,$params)->fetchRow();
        $this->_output->countUnlimited = intval($row[0]);
    }

    public $data;

    public function _onDefault($feedbackId = false, $action = false) {

        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if($loggedInUser) {
            $viewingUserId = $loggedInUser->id;
        } else {
            $viewingUserId = 0;
        }

        switch($this->_httpMethod) {
            case "GET" :
                $this->_badRequest();
                break;
            case "POST" :

                if($viewingUserId == 0) {
                    $this->_accessDenied("You must be logged in to do that");
                } else {
                    if(!$feedbackId) {
                        //trying to create feedback
                        if(!is_array($this->data)) {
                            $this->_failApi("The data property is not set, or cannot be read");
                        } else {
                            $fb = Feedback::constructFromArray($this->data);
                            $fb->id = null;
                            $fb->ownerId = $loggedInUser->id;
                            $fb->pushThis();
                            //output
                            $this->_output->data = $fb;
                        }
                    } else {
                        if(!$action) {
                            //updating existing
                            if(!is_array($this->data)) {
                                $this->_failApi("The data property is not set, or cannot be read");
                            }

                            $fb = Feedback::findById($feedbackId,1);
                            if(!$fb) {
                                $this->_notFound("You are trying to modify feedback that does not exist");
                            }
                            $course = Course::findById($fb->courseId,1);
                            if(!$course) {
                                $this->_notFound("Could not find associated course");
                            }

                            $access = false;
                            //if own
                            if($fb->ownerId == $viewingUserId) {
                                $access = true;
                            }
                            elseif(\Core\Classes\Utilities\Users::userManagesUserAndHasPrivilege($viewingUserId,$course->ownerId,FeedbackPrivileges::_feedback_modify_all)) {
                                $access = true;
                            } elseif($course->ownerId == $viewingUserId && \Core\Classes\Utilities\Users::userHasPrivilege($viewingUserId,FeedbackPrivileges::_feedback_modify_own)) {
                                $access = true;
                            }

                            if(!$access) {
                                $this->_accessDenied("You are not able to modify this feedback");
                            } else {
                                $dataFb = Feedback::constructFromArray($this->data);
                                //extract valid properties
                                $fb->rating = $dataFb->rating;
                                $fb->comment = $dataFb->comment;
                                $fb->pushThis();
                                //output
                                $this->_output->data = $fb;
                            }

                        } else {
                            $this->_badRequest();
                        }
                    }
                }

                break;
        }
    }

}