<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\PGDB_ResultSet;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\User;
use Core\Classes\Models\UserGroup;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\User_Course;

/**
 * Manage course enrolments
 *
 * Class AdminLMSCourseEnrolments
 */
class AdminLMSCourseEnrolments extends \Core\Classes\Commons\Controller {

    public $canModifyAll;
    public $canModifyOwn;
    public $canCreate;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_course_enrolments"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canModifyAll = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_courses_modify_all");
        $this->canModifyOwn = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_courses_modify_own");
        $this->canCreate = Users::userHasPrivileges_OR($this->_getLoggedInUserId(),array(
            "user_courses_create_all",
            "user_courses_create_own"
        ));
    }

    /**
     * @var SimpleFiltering
     */
    public $f;
    /**
     * @var PGDB_ResultSet
     */
    public $courseEnrolments;

    /**
     *
     * @var User
     */
    public $user;
    /**
     *
     * @var User_Course
     */
    public $userCourse;
    /**
     *
     * @var Course
     */
    public $course;

    public function _onDefault($id = false, $action = false) {
        if($id) {
            if(!$action) {
                $this->userCourse = User_Course::findById($id);
                if(!$this->userCourse) {
                    $this->_redirectToDefault();
                }
                $this->course = Course::findById($this->userCourse->courseId);

                if(isset($_POST["updated"])) {
                    $expiry = $_POST["expiry"];
                    $access = false;
                    if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$this->userCourse->userId,"user_courses_modify_all")) {
                        $access = true;
                    }
                    elseif(
                        $this->course->ownerId == $this->_getLoggedInUserId() &&
                        Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$this->userCourse->userId,"user_courses_modify_own")
                    ) {
                        $access = true;
                    }
                    if(!$access) {
                        $this->_messageWarning("You are not able to modify this enrolment");
                    } else {
                        $this->userCourse->expiryDate = $expiry;
                        $this->userCourse->pushThis();
                        $this->_messageSuccess("Enrolment updated");
                    }
                }

                $this->user = User::findById($this->userCourse->userId);

                $this->_Extorio()->setTargetBreadCrumbs(array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Course Enrolments","/extorio-admin/lms-course-enrolments/"),
                    BreadCrumb::n(true,$this->user->longname." (".$this->course->name.")")
                ));
            } else {
                switch($action) {
                    case "delete" :
                        $this->userCourse = User_Course::findById($id);
                        if(!$this->userCourse) {
                            $this->_redirectToDefault();
                        }
                        $this->course = Course::findById($this->userCourse->courseId);

                        $access = false;
                        if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$this->userCourse->userId,"user_courses_delete_all")) {
                            $access = true;
                        }
                        elseif(
                            $this->course->ownerId == $this->_getLoggedInUserId() &&
                            Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$this->userCourse->userId,"user_courses_delete_own")
                        ) {
                            $access = true;
                        }

                        if(!$access) {
                            $this->_messageWarning("You are not able to delete this enrolment");
                            $this->_redirectToDefault(array($id));
                        }

                        $this->userCourse->deleteThis();
                        $this->_messageSuccess("Enrolment deleted");
                        $this->_redirectToDefault();

                        break;
                }
            }

        } else {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Course Enrolments","/extorio-admin/lms-course-enrolments/"),
                BreadCrumb::n(false,"Create new enrolment","/extorio-admin/lms-course-enrolments/create/","plus",!$this->canCreate)
            ));

            $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
            $this->f->addFilter("limit", array(
                10 => 10,
                25 => 25,
                100 => 100
            ));
            $filters = array();
            if($this->canModifyAll) {
                $filters[""] = "Everyone's Courses";
            }
            $filters["own"] = "My Courses";
            $this->f->addFilter("show",$filters);

            $loggedInUser = $this->_getLoggedInUser();
            $filters = array();
            if($loggedInUser->userGroup->manageAll) {
                $filters[""] = "Any user group";
            }
            $ugs = $this->_getLoggedInUserManageGroups();
            foreach($ugs as $ug) {
                $filters[$ug->id] = $ug->name;
            }
            $this->f->addFilter("user-group",$filters);

            $this->f->addFilter("state",array(
                "" => "Any state",
                "pending" => "Pending",
                "started" => "Started",
                "passed" => "Passed",
                "expired" => "Expired"
            ));
            $this->f->addEmpty();
            $this->f->addFilter("order",array(
                "user" => "User",
                "course" => "Course"
            ));
            $this->f->setSearchTypes(array(
                "user" => "User",
                "course" => "Course"
            ));

            $this->f->extractFiltering();
            $this->f->setLimit($this->f->getFilter("limit"));

            $sql = '
            SELECT

            UC.id,
            UC.status,
            UC."dateUpdated",
            UC."expiryDate",
            UC."currentPoints",

            C.name,
            C."maximumPoints",
            U.id as "userId",
            U.longname

            FROM extoriolms_classes_models_user_course UC
            LEFT JOIN extoriolms_classes_models_course C ON C.id = UC."courseId"
            LEFT JOIN core_classes_models_user U ON U.id = UC."userId"
            ';
                $sqlCount = '
            SELECT count(UC.id)

            FROM extoriolms_classes_models_user_course UC
            LEFT JOIN extoriolms_classes_models_course C ON C.id = UC."courseId"
            LEFT JOIN core_classes_models_user U ON U.id = UC."userId"
            ';

            $where = "";
            $params = array();
            $paramIndex = 1;

            if(strlen($this->f->getFilter("show"))) {
                switch($this->f->getFilter("show")) {
                    case "own" :
                        $where .= 'C."ownerId" = $'.$paramIndex." AND ";
                        $params[] = $this->_getLoggedInUserId();
                        $paramIndex++;
                        break;
                }
            }
            if(strlen($this->f->getFilter("state"))) {
                switch($this->f->getFilter("state")) {
                    case "pending" :
                        $where .= 'UC.status = $'.$paramIndex." AND ";
                        $params[] = ContentStatus::_pending;
                        $paramIndex++;
                        break;
                    case "started" :
                        $where .= 'UC.status = $'.$paramIndex." AND ";
                        $params[] = ContentStatus::_started;
                        $paramIndex++;
                        break;
                    case "passed" :
                        $where .= 'UC.status = $'.$paramIndex." AND ";
                        $params[] = ContentStatus::_passed;
                        $paramIndex++;
                        break;
                    case "expired" :
                        $where .= 'UC."expiryDate" <= NOW() AND ';
                        break;
                }
            }
            if(strlen($this->f->getFilter("user-group"))) {
                $where .= 'U."userGroup" = $'.$paramIndex." AND ";
                $params[] = $this->f->getFilter("user-group");
                $paramIndex++;
            }
            if(strlen($this->f->getSearchQuery())) {
                switch($this->f->getSearchType()) {
                    case "user" :
                        $where .= '(LOWER(U.username) LIKE LOWER($'.$paramIndex.') OR LOWER(U.firstname) LIKE LOWER($'.$paramIndex.') OR LOWER(U.email) LIKE LOWER($'.$paramIndex.')) AND ';
                        $params[] = "%".$this->f->getSearchQuery()."%";
                        $paramIndex++;
                        break;
                    case "course" :
                        $where .= 'LOWER(C.name) LIKE LOWER($'.$paramIndex.') AND ';
                        $params[] = "%".$this->f->getSearchQuery()."%";
                        $paramIndex++;
                        break;
                }
            }

            if(strlen($where)) {
                $sql .= "WHERE ".substr($where,0,strlen($where)-4);
                $sqlCount .= "WHERE ".substr($where,0,strlen($where)-4);
            }

            switch($this->f->getFilter("order")) {
                case "user" :
                    $sql .= 'ORDER BY U.username ';
                    break;
                case "course" :
                    $sql .= 'ORDER BY C.name ';
                    break;
            }

            $sql .= 'LIMIT '.intval($this->f->getLimit())." ";
            $sql .= 'OFFSET '.($this->f->getLimit() * $this->f->getPage())." ";

            $db = $this->_getDbInstanceDefault();
            $this->courseEnrolments = $db->query($sql,$params);
            $row = $db->query($sqlCount,$params)->fetchRow();
            if($row) {
                $this->f->setCount(intval($row[0]));
            }
        }
    }

    /**
     * @var PGDB_ResultSet
     */
    public $userData;
    /**
     * @var PGDB_ResultSet
     */
    public $courseData;

    public function create() {
        if(!$this->canCreate) {
            $this->_messageWarning("You are not able to create enrolments");
            $this->_redirectToDefault();
        }

        if(isset($_POST["submit"]) || isset($_POST["submit_exit"])) {
            $uc = User_Course::n();
            $uc->courseId = intval($_POST["course"]);
            $uc->userId = intval($_POST["user"]);

            $error = false;
            try {
                $uc->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Enrolment created");
                if(isset($_POST["submit_exit"])) {
                    $this->_redirectToDefault();
                } else {
                    $this->_redirectToDefault(array($uc->id));
                }
            }
        }

        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(false,"Course Enrolments","/extorio-admin/lms-course-enrolments/"),
            BreadCrumb::n(true,"Create new enrolment","/extorio-admin/lms-course-enrolments/create/","plus",!$this->canCreate)
        ));

        $canCreateAll = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_courses_create_all");
        $sql = '
        SELECT id, name FROM extoriolms_classes_models_course
        WHERE approval = $1
        ';
        $params = array(ContentApproval::_approved);
        if(!$canCreateAll) {
            $sql .= 'AND "ownerId" = $2 ';
            $params[] = $this->_getLoggedInUserId();
        }
        $sql .= 'ORDER BY name ASC';
        $db = $this->_getDbInstanceDefault();
        $this->courseData = $db->query($sql,$params);
        $user = $this->_getLoggedInUser();
        $sql = 'SELECT id, longname FROM core_classes_models_user
        WHERE "canLogin" = true
        ';
        if(!$user->userGroup->manageAll) {
            $in = "";
            foreach($user->userGroup->manageGroups as $gid) {
                $in .= $gid.",";
            }
            $sql .= 'AND "userGroup" IN (';
            $sql .= substr($in,0,strlen($in)-1);
            $sql .= ') ';
        }
        $sql .= 'ORDER BY username ASC';
        $this->userData = $db->query($sql);
    }
}