<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the general settings for the lms extension
 *
 * Class AdminLMSGeneralSettings
 */
class AdminLMSGeneralSettings extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_general_settings"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public $config = array();

    public function _onDefault() {
        $this->config = $this->_Extorio()->getConfig();

        if(isset($_POST["updated"])) {

            $this->_Extension()->_mergeConfigOverride(array(
                "ExtorioLMS" => array(
                    "users" => array(
                        "default_role_group" => intval($_POST["users_default_role_group"])
                    ),
                )
            ));

            $this->_Extorio()->messageSuccess("Settings updated");
            $this->config = $this->_Extorio()->getConfig();
        }
    }
}