<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;

/**
 * Manage the upload settings for the lms extension
 *
 * Class AdminLMSUploadSettings
 */
class AdminLMSUploadSettings extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_upload_settings"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public $config = array();

    public function _onDefault() {
        $this->config = $this->_Extorio()->getConfig();

        if(isset($_POST["submitted"])) {

            $imageTypes = $_POST["images_accepted_types"];
            $imageTypes = Strings::removeWhitespace($imageTypes);
            $imageTypes = explode(",",$imageTypes);

            $videoTypes = $_POST["videos_accepted_types"];
            $videoTypes = Strings::removeWhitespace($videoTypes);
            $videoTypes = explode(",",$videoTypes);

            $audioTypes = $_POST["audio_accepted_types"];
            $audioTypes = Strings::removeWhitespace($audioTypes);
            $audioTypes = explode(",",$audioTypes);

            $documentTypes = $_POST["documents_accepted_types"];
            $documentTypes = Strings::removeWhitespace($documentTypes);
            $documentTypes = explode(",",$documentTypes);

            $this->_Extension()->_mergeConfigOverride(array(
                "ExtorioLMS" => array(
                    "aws" => array(
                        "s3" => array(
                            "region" => $_POST["aws_s3_region"],
                            "key" => $_POST["aws_s3_key"],
                            "secret" => $_POST["aws_s3_secret"],
                        )
                    ),
                    "uploads" => array(
                        "images" => array(
                            "accepted_types" => $imageTypes,
                            "maximum_size" => intval($_POST["images_maximum_size"])
                        ),
                        "videos" => array(
                            "accepted_types" => $videoTypes,
                            "maximum_size" => intval($_POST["videos_maximum_size"])
                        ),
                        "audio" => array(
                            "accepted_types" => $audioTypes,
                            "maximum_size" => intval($_POST["audio_maximum_size"])
                        ),
                        "documents" => array(
                            "accepted_types" => $documentTypes,
                            "maximum_size" => intval($_POST["documents_maximum_size"])
                        )
                    ),
                    "courses" => array(
                        "course_file_bucket" => $_POST["course_file_bucket"],
                        "editor_file_bucket" => $_POST["editor_file_bucket"]
                    )
                )
            ));

            $this->_Extorio()->messageSuccess("Settings updated");
            $this->config = $this->_Extorio()->getConfig();
        }
    }
}