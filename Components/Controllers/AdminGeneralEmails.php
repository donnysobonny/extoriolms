<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the general emails for the lms extension
 *
 * Class AdminGeneralEmails
 */
class AdminGeneralEmails extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_general_emails"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public $config = array();

    public function _onDefault() {
        $this->config = $this->_Extorio()->getConfig();

        if(isset($_POST["submitted"])) {
            $this->_Extension()->_mergeConfigOverride(array(
                "ExtorioLMS" => array(
                    "emails" => array(
                        "general" => array(
                            "user_quiz_marked" => array(
                                "enabled" => isset($_POST["general_user_quiz_marked_enabled"]),
                                "subject" => $_POST["general_user_quiz_marked_subject"],
                                "body" => $_POST["general_user_quiz_marked_body"]
                            )
                        )
                    )
                )
            ));

            $this->_Extorio()->messageSuccess("Updated");

            $this->config = $this->_Extorio()->getConfig();
        }
    }
}