<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Commons\FileSystem\File;
use Core\Classes\Commons\FileSystem\Image;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the course settings for the lms extension
 *
 * Class AdminLMSCourseSettings
 */
class AdminLMSCourseSettings extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_course_settings"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public $config = array();

    public function _onDefault() {
        $this->config = $this->_Extorio()->getConfig();

        if(isset($_POST["submitted"])) {

            $defaultFile = $this->config["ExtorioLMS"]["courses"]["default_image"];
            $defaultFileThumbnail = $this->config["ExtorioLMS"]["courses"]["default_thumbnail_image"];
            if(strlen($_FILES["course_default_image"]["name"])) {
                $file = $_FILES["course_default_image"];
                $check = File::constructFromPath($file["name"]);
                if(!in_array($check->extension(), array(
                    "jpg",
                    "jpeg",
                    "png",
                    "gif"
                ))) {
                    $this->_Extorio()->messageError("The default course file is not a valid image file type");
                    $this->_redirectToDefault();
                }
                if(move_uploaded_file($file["tmp_name"],"Extensions/ExtorioLMS/Assets/images/".$file["name"])) {
                    /** @var File $copy */
                    $copy = File::constructFromPath("Extensions/ExtorioLMS/Assets/images/".$file["name"])->copy("Extensions/ExtorioLMS/Assets/images/".$check->fileName()."_thumbnail.".$check->extension());
                    $image = $copy->__toImage();
                    //set the largest side to 448
                    $image->resizeLargestSide(448);
                    //crop to 448 by 252
                    $image->cropAtCenter(448,252);
                    //save
                    $image->save();

                    $defaultFile = "/Extensions/ExtorioLMS/Assets/images/".$file["name"];
                    $defaultFileThumbnail = "/Extensions/ExtorioLMS/Assets/images/".$check->fileName()."_thumbnail.".$check->extension();
                } else {
                    $this->_Extorio()->messageError("There was a problem uploading the default image");
                    $this->_redirectToDefault();
                }
            }

            $this->_Extension()->_mergeConfigOverride(array(
                "ExtorioLMS" => array(
                    "courses" => array(
                        "auto_approve" => isset($_POST["courses_auto_approve"]),
                        "minimum_expiry_minutes" => intval($_POST["courses_minimum_expiry"]),
                        "allow_self_enrollable" => isset($_POST["courses_allow_self_enrollable"]),
                        "expiry_set_when" => $_POST["course_expiry_when"],
                        "certificate_public" => isset($_POST["course_certificate_public"]),
                        "default_image" => $defaultFile,
                        "default_thumbnail_image" => $defaultFileThumbnail
                    ),
                    "course_posts" => array(
                        "enabled" => isset($_POST["posts_enabled"]),
                        "auto_approve" => isset($_POST["posts_auto_approve"]),
                        "allow_attachments" => isset($_POST["posts_allow_attachments"]),
                    ),
                    "modules" => array(
                        "allow_previewable" => isset($_POST["modules_allow_previewable"]),
                        "lectures" => array(
                            "allow_video" => isset($_POST["modules_allow_video"]),
                            "allow_audio" => isset($_POST["modules_allow_audio"]),
                            "allow_images" => isset($_POST["modules_allow_image"]),
                            "allow_text" => isset($_POST["modules_allow_text"]),
                            "allow_documents" => isset($_POST["modules_allow_document"])
                        ),
                        "quizzes" => array(
                            "minimum_maximum_attempts" => intval($_POST["quizzes_minimum_maximum_attempts"]),
                            "minimum_pass_points_percentage" => intval($_POST["quizzes_minimum_pass_points"]),
                            "allow_multiple_choice" => isset($_POST["quizzes_allow_multiple_choice"]),
                            "allow_multiple_response" => isset($_POST["quizzes_allow_multiple_response"]),
                            "allow_true_false" => isset($_POST["quizzes_allow_true_false"]),
                            "allow_fill_gaps" => isset($_POST["quizzes_allow_fill_gaps"]),
                            "allow_multiline_text" => isset($_POST["quizzes_allow_multiline"]),
                            "allow_file_submission" => isset($_POST["quizzes_allow_file"])
                        )
                    ),
                    "feedback" => array(
                        "enabled" => isset($_POST["feedback_enabled"]),
                        "auto_approve_uncommented" => isset($_POST["feedback_approve_uncommented"]),
                        "auto_approve_commented" => isset($_POST["feedback_approve_commented"])
                    )
                )
            ));

            $this->_Extorio()->messageSuccess("Settings updated");
            $this->config = $this->_Extorio()->getConfig();
        }
    }
}