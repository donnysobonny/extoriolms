<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\CourseFilesFileType;
use ExtorioLMS\Classes\Enums\CourseFilesUploadType;
use ExtorioLMS\Classes\Models\CourseFile;

/**
 * Manage course files
 *
 * Class AdminLMSCourseFiles
 */
class AdminLMSCourseFiles extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_course_files"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public $uploadTypes = array();
    public $fileTypes = array();
    public $contentTypes = array();
    public $courseFileBucket = "";

    public function _onDefault($id = false) {
        if(!$id) {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"My Course Files")
            ));
            $config = $this->_getConfig();
            $this->courseFileBucket = $config["ExtorioLMS"]["courses"]["course_file_bucket"];
            foreach(CourseFilesUploadType::values() as $v) {
                $this->uploadTypes[$v] = Strings::titleSafe($v);
            }
            foreach(CourseFilesFileType::values() as $v) {
                $this->fileTypes[$v] = Strings::titleSafe($v);
            }
            foreach($config["ExtorioLMS"]["uploads"]["images"]["accepted_types"] as $t) {
                $this->contentTypes["images"][] = $t;
            }
            foreach($config["ExtorioLMS"]["uploads"]["videos"]["accepted_types"] as $t) {
                $this->contentTypes["videos"][] = $t;
            }
            foreach($config["ExtorioLMS"]["uploads"]["audio"]["accepted_types"] as $t) {
                $this->contentTypes["audio"][] = $t;
            }
            foreach($config["ExtorioLMS"]["uploads"]["documents"]["accepted_types"] as $t) {
                $this->contentTypes["documents"][] = $t;
            }
            foreach($config["ExtorioLMS"]["uploads"]["subtitles"]["accepted_types"] as $t) {
                $this->contentTypes["subtitles"][] = $t;
            }
        } else {
            $cf = CourseFile::findById($id,1);
            if(!$cf) {
                $this->_redirectTo404PageNotFoundPage();
            }
            if($cf->userId != $this->_getLoggedInUserId()) {
                $this->_redirectTo401AccessDeniedPage();
            }
            $url = $cf->privateUrl();
            header("Location: ".$url);
            exit;
        }
    }
}