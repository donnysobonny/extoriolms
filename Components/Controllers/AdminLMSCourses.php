<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\CoursePrivileges;
use ExtorioLMS\Classes\Models\Course;

/**
 * Manage courses
 *
 * Class AdminLMSCourses
 */
class AdminLMSCourses extends \Core\Classes\Commons\Controller {

    public $canCreate;
    public $canModifyAll;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_courses"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->canCreate = Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_create);
        $this->canModifyAll = Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_modify_all);
    }

    /**
     * @var SimpleFiltering
     */
    public $f;

    /**
     * @var Course[]
     */
    public $courses = array();
    /**
     * @var Course
     */
    public $course;
    public function _onDefault($id = false, $action = false) {
        if(!$id) {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Courses","/extorio-admin/lms-courses/"),
                BreadCrumb::n(false,"Create new course","/extorio-admin/lms-courses/create","plus",!$this->canCreate)
            ));

            $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
            $this->f->addFilter("limit", array(
                10 => 10,
                25 => 25,
                100 => 100
            ));
            $filters = array();
            if($this->canModifyAll) {
                $filters[""] = "Everyone's Courses";
            }
            $filters["own"] = "My Courses";
            $this->f->addFilter("show",$filters);
            $this->f->addFilter("with-status",array(
                "" => "Any Status",
                "pending" => "Pending Approval",
                "approved" => "Approved",
                "closed" => "Closed",
                "cancelled" => "Cancelled"
            ));
            $this->f->addFilter("order",array(
                "new" => "Newest First",
                "old" => "Oldest First",
                "name" => "Name",
                "most_stu" => "Most Students",
                "least_stu" => "Least Students",
                "most_pop" => "Highest Rating",
                "least_pop" => "Lowest Rating"
            ));
            $this->f->setSearchTypes(array(
                "name" => "Name",
                "owner" => "Owner"
            ));

            $this->f->extractFiltering();
            $this->f->setLimit($this->f->getFilter("limit"));

            $db = $this->_getDbInstanceDefault();
            $sql = '
            SELECT C.id FROM extoriolms_classes_models_course C

            LEFT JOIN core_classes_models_user U ON C."ownerId" = U.id
            ';
            $sqlCount = '
            SELECT COUNT(C.id) FROM extoriolms_classes_models_course C

            LEFT JOIN core_classes_models_user U ON C."ownerId" = U.id
            ';
            $where = "";
            $params = array();
            $paramIndex = 1;
            if(strlen($this->f->getFilter("show"))) {
                switch($this->f->getFilter("show")) {
                    case "own" :
                        $where .= 'C."ownerId" = $'.$paramIndex." AND ";
                        $params[] = $this->_getLoggedInUserId();
                        $paramIndex++;
                        break;
                }
            }
            if(strlen($this->f->getFilter("with-status"))) {
                switch($this->f->getFilter("with-status")) {
                    case "pending" :
                        $where .= 'C.approval = $'.$paramIndex." AND ";
                        $params[] = ContentApproval::_pending;
                        $paramIndex++;
                        break;
                    case "approved" :
                        $where .= 'C.approval = $'.$paramIndex." AND ";
                        $params[] = ContentApproval::_approved;
                        $paramIndex++;
                        break;
                    case "closed" :
                        $where .= 'C.approval = $'.$paramIndex." AND ";
                        $params[] = ContentApproval::_closed;
                        $paramIndex++;
                        break;
                    case "cancelled" :
                        $where .= 'C.approval = $'.$paramIndex." AND ";
                        $params[] = ContentApproval::_cancelled;
                        $paramIndex++;
                        break;
                }
            }
            if(strlen($this->f->getSearchQuery())) {
                switch($this->f->getSearchType()) {
                    case "name" :
                        $where .= 'LOWER(C.name) LIKE LOWER($'.$paramIndex.') AND ';
                        $params[] = "%".$this->f->getSearchQuery()."%";
                        $paramIndex++;
                        break;
                    case "owner" :
                        $where .= '(LOWER(U.username) LIKE LOWER($'.$paramIndex.') OR LOWER(U.firstname) LIKE LOWER($'.$paramIndex.') OR LOWER(U.email) LIKE LOWER($'.$paramIndex.')) AND ';
                        $params[] = "%".$this->f->getSearchQuery()."%";
                        $paramIndex++;
                        break;
                }
            }

            if(strlen($where)) {
                $sql .= "WHERE ".substr($where,0,strlen($where)-4);
                $sqlCount .= "WHERE ".substr($where,0,strlen($where)-4);
            }

            switch($this->f->getFilter("order")) {
                case "new" :
                    $sql .= 'ORDER BY C."dateCreated" DESC ';
                    break;
                case "old" :
                    $sql .= 'ORDER BY C."dateCreated" ASC ';
                    break;
                case "name" :
                    $sql .= 'ORDER BY C."name" ASC ';
                    break;
                case "most_stu" :
                    $sql .= 'ORDER BY C."totalNumEnrolled" DESC ';
                    break;
                case "least_stu" :
                    $sql .= 'ORDER BY C."totalNumEnrolled" ASC ';
                    break;
                case "most_pop" :
                    $sql .= 'ORDER BY C."averageRating" DESC ';
                    break;
                case "least_pop" :
                    $sql .= 'ORDER BY C."averageRating" ASC ';
                    break;
            }

            $sql .= "LIMIT ".intval($this->f->getLimit())." ";
            $sql .= "OFFSET ".($this->f->getLimit() * $this->f->getPage())." ";

            $this->courses = Course::findAllByResultSet($db->query($sql,$params),2);
            $row = $db->query($sqlCount,$params)->fetchRow();
            if($row) {
                $this->f->setCount(intval($row[0]));
            }

        } else {
            $this->course = Course::findById($id,INF);
            if(!$this->course) {
                $this->_redirectTo404PageNotFoundPage();
            }
            $access = false;
            if($this->course->ownerId == $this->_getLoggedInUserId()) {
                $access = true;
            }
            elseif($this->canModifyAll) {
                $access = true;
            }
            if(!$access) {
                $this->_messageWarning("You do not have access to modify this course");
                $this->_redirectToDefault();
            }

            if(!$action) {
                $this->_Extorio()->setTargetBreadCrumbs(array(
                    BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                    BreadCrumb::n(false,"Courses","/extorio-admin/lms-courses/"),
                    BreadCrumb::n(true,$this->course->name,"/extorio-admin/lms-courses/".$this->course->id."/")
                ));

            } else {
                switch($action) {
                    case "edit" :
                        $this->_Extorio()->setTargetBreadCrumbs(array(
                            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                            BreadCrumb::n(false,"Courses","/extorio-admin/lms-courses/"),
                            BreadCrumb::n(false,$this->course->name,"/extorio-admin/lms-courses/".$this->course->id."/"),
                            BreadCrumb::n(true,"Edit","","edit")
                        ));

                        break;
                    case "clone" :
                        $this->_Extorio()->setTargetBreadCrumbs(array(
                            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                            BreadCrumb::n(false,"Courses","/extorio-admin/lms-courses/"),
                            BreadCrumb::n(false,$this->course->name,"/extorio-admin/lms-courses/".$this->course->id."/"),
                            BreadCrumb::n(true,"Clone","","clone")
                        ));

                        if($this->course->ownerId != $this->_getLoggedInUserId()) {
                            $this->_messageWarning("Currently, you are only able to clone your own courses");
                            $this->_redirectToDefault();
                        }
                        if(!$this->canCreate) {
                            $this->_messageWarning("You must be able to create new courses in order to clone a course");
                            $this->_redirectToDefault(array($this->course->id,"edit"));
                        }
                        if(isset($_POST["submitted"])) {
                            $name = $_POST["name"];
                            $error = false;
                            $clone = null;
                            try {
                                $clone = $this->course->cloneThis($name);
                            } catch(\Exception $ex) {
                                $error = $ex->getMessage();
                            }
                            if($error) {
                                $this->_messageError($error);
                            } else {
                                $this->_messageSuccess("Course successfully cloned");
                            }
                            $this->_redirectToDefault(array($clone->id));
                        }
                        break;
                    case "approve" :
                        $access = false;
                        if($this->course->ownerId == $this->_getLoggedInUserId() && Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_approve_own)) {
                            $access = true;
                        }
                        elseif(Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_approve_all)) {
                            $access = true;
                        }
                        if(!$access) {
                            $this->_messageWarning("You are not able to approve this course");
                            $this->_redirectToDefault(array($this->course->id,"edit"));
                        }
                        $error = false;
                        try {
                            $this->course->approve();
                        } catch(\Exception $ex) {
                            $error = $ex->getMessage();
                        }
                        if($error) {
                            $this->_messageError($error);
                        } else {
                            $this->_messageSuccess("Course successfully approved");
                        }
                        $this->_redirectToDefault(array($this->course->id));
                        break;
                    case "close" :
                        $access = false;
                        if($this->course->ownerId == $this->_getLoggedInUserId() && Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_close_own)) {
                            $access = true;
                        }
                        elseif(Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_close_all)) {
                            $access = true;
                        }
                        if(!$access) {
                            $this->_messageWarning("You are not able to close this course");
                            $this->_redirectToDefault(array($this->course->id,"edit"));
                        }
                        $error = false;
                        try {
                            $this->course->close();
                        } catch(\Exception $ex) {
                            $error = $ex->getMessage();
                        }
                        if($error) {
                            $this->_messageError($error);
                        } else {
                            $this->_messageSuccess("Course successfully closed");
                        }
                        $this->_redirectToDefault(array($this->course->id));
                        break;
                    case "cancel" :
                        $this->_Extorio()->setTargetBreadCrumbs(array(
                            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                            BreadCrumb::n(false,"Courses","/extorio-admin/lms-courses/"),
                            BreadCrumb::n(false,$this->course->name,"/extorio-admin/lms-courses/".$this->course->id."/"),
                            BreadCrumb::n(true,"Cancel","","remove")
                        ));

                        $access = false;
                        if($this->course->ownerId == $this->_getLoggedInUserId() && Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_cancel_own)) {
                            $access = true;
                        }
                        elseif(Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_cancel_all)) {
                            $access = true;
                        }
                        if(!$access) {
                            $this->_messageWarning("You are not able to cancel this course");
                            $this->_redirectToDefault(array($this->course->id,"edit"));
                        }
                        if(isset($_POST["submitted"])) {
                            $reason = $_POST["reason"];
                            $error = false;
                            try {
                                $this->course->cancel($reason);
                            } catch(\Exception $ex) {
                                $error = $ex->getMessage();
                            }
                            if($error) {
                                $this->_messageError($error);
                            } else {
                                $this->_messageSuccess("Course successfully cancelled");
                            }
                            $this->_redirectToDefault(array($this->course->id));
                        }
                        break;
                    case "delete" :
                        $access = false;
                        if($this->course->ownerId == $this->_getLoggedInUserId() && Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_remove_own)) {
                            $access = true;
                        }
                        elseif(Users::userHasPrivilege($this->_getLoggedInUserId(),CoursePrivileges::_courses_remove_all)) {
                            $access = true;
                        }
                        if(!$access) {
                            $this->_messageWarning("You are not able to delete this course");
                            $this->_redirectToDefault(array($this->course->id,"edit"));
                        }
                        $error = false;
                        try {
                            $this->course->deleteThis();
                        } catch(\Exception $ex) {
                            $error = $ex->getMessage();
                        }
                        if($error) {
                            $this->_messageError($error);
                        } else {
                            $this->_messageSuccess("Course successfully deleted");
                        }
                        $this->_redirectToDefault();
                        break;
                    case "preview":
                        $this->_Extorio()->setTargetBreadCrumbs(array(
                            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                            BreadCrumb::n(false,"Courses","/extorio-admin/lms-courses/"),
                            BreadCrumb::n(false,$this->course->name,"/extorio-admin/lms-courses/".$this->course->id."/"),
                            BreadCrumb::n(true,"Preview","","preview")
                        ));
                        break;
                    default:
                        $this->_redirectToDefault();
                        break;
                }
            }
        }
    }

    public function create() {
        if(!$this->canCreate) {
            $this->_messageWarning("You are not able to create new courses");
            $this->_redirectToDefault();
        }
        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(false,"Courses","/extorio-admin/lms-courses/"),
            BreadCrumb::n(true,"Create new course","/extorio-admin/lms-courses/create","plus",!$this->canCreate)
        ));
    }
}