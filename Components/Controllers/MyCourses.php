<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Commons\Extorio;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\PGDB_ResultSet;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Enums\RoleIds;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\CoursePost;
use ExtorioLMS\Classes\Models\User_Course;
use ExtorioLMS\Classes\Models\User_Enrolment;
use ExtorioLMS\Classes\Utilities\Content;
use ExtorioLMS\Classes\Utilities\Users;

/**
 *
 *
 * Class MyCourses
 */
class MyCourses extends \Core\Classes\Commons\Controller {

    public $bowerComponentsPath = "/Extensions/ExtorioLMS/Assets/CoursePlayerDev/bower_components/";
    public $bowerComponentsJs = [
        "angular/angular.js",
        "angular-animate/angular-animate.js",
        "angular-resource/angular-resource.js",
        "angular-sanitize/angular-sanitize.js",
        "ui-router/release/angular-ui-router.js",
        "angular-bootstrap/ui-bootstrap-tpls.js",
        "pdfjs-dist/build/pdf.js",
        "pdfjs-dist/build/pdf.worker.js",
        "angular-pdf/dist/angular-pdf.js",
        "summernote/dist/summernote.js",
        "angular-summernote/dist/angular-summernote.js",
        "AngularJS-Toaster/toaster.js"

    ];
    public $bowerComponentsCss = [
        "summernote/dist/summernote.css",
        "AngularJS-Toaster/toaster.css"
    ];

    public $appPath = "/Extensions/ExtorioLMS/Assets/CoursePlayerDev/app/scripts/";
    public $appScripts = [
        "app.modules.js",
        "app.run.js",
        "app.states.js",
        "app.config.js",
    ];
    public $appControllers = [
        "controllers/core/navigation.controller.js",
        "controllers/core/sub-navigation.controller.js",
        "controllers/core/header.controller.js",
        "controllers/lecture/video.controller.js",
        "controllers/lecture/audio.controller.js",
        "controllers/lecture/image.controller.js",
        "controllers/lecture/text.controller.js",
        "controllers/lecture/overview.controller.js",
        "controllers/lecture/document.controller.js",
        "controllers/quiz/multiple-choice.controller.js",
        "controllers/quiz/multiple-response.controller.js",
        "controllers/quiz/true-or-false.controller.js",
        "controllers/quiz/fill-in-the-blanks.controller.js",
        "controllers/quiz/multiline-text.controller.js",
        "controllers/quiz/file-submission.controller.js",
        "controllers/quiz/quiz-landing.controller.js"
    ];
    public $appServices = [
        "services/extorio-api.service.js",
        "services/session.service.js",
        "services/extorio-error-handler.service.js",
        "services/jwplayer.service.js",
        "services/sub-navigation.service.js",
        "services/helpers.service.js",
        "services/user-course.service.js",
        "services/share.service.js",
        "services/share.service.js",
        "services/state-params.service.js",
        "services/closed-caption.service.js",
        "services/module.service.js"
    ];
    public $appDirectives = [
        "directives/jwplayer.directive.js",
        "directives/fill-in-the-blanks.directive.js",
        "directives/fill-in-the-blanks-dropdown.directive.js",
        "directives/question-feedback.directive.js",
        "directives/question-preview-multiple-choice.directive.js",
        "directives/question-preview-multiple-response.directive.js",
        "directives/question-preview-true-or-false.directive.js",
        "directives/question-preview-fill-in-the-blanks.directive.js",
        "directives/question-preview-file-submission.directive.js",
        "directives/question-preview-multiline-text.directive.js",
    ];
    public $appConstants = [
        "constants/summernote-config.constant.js",
        "constants/tooltips.constant.js"
    ];


    public function _onBegin() {
        if(!$this->_Extorio()->getLoggedInUser()) {
            $this->_redirectTo401AccessDeniedPage();
        }

    }

    public function _onDefault() {
        //control
        $this->control(func_get_args());
    }

    final protected function control($args) {
        if(count($args) > 0 && strlen($args[0])) {
            if(count($args) >= 2 && is_numeric($args[0]) && is_numeric($args[1])) {
                $this->course($args[0], $args[1]);
            } elseif(count($args) >= 2 && $args[1] == "posts" && is_numeric($args[0])) {
                if(count($args) == 2) {
                    $this->coursePosts($args[0]);
                } elseif(count($args) == 3 && $args[2] == "create") {
                    $this->coursePostsCreate($args[0]);
                } elseif(count($args) == 3 && $args[2] == "search") {
                    $this->coursePostsSearch($args[0]);
                } elseif(count($args) == 3 && is_numeric($args[2])) {
                    $this->coursePostsPost($args[0], $args[2]);
                } elseif(count($args) == 4 && is_numeric($args[2]) && is_numeric($args[3])) {
                    $this->coursePostsPost($args[0], $args[2], $args[3]);
                } else {
                    $this->coursePosts($args[0]);
                }
            } elseif(count($args) >= 1 && is_numeric($args[0])) {
                $this->course($args[0]);
            } else {
                $this->courseList();
            }
        } else {
            $this->courseList();
        }
    }

    public $courseListData = array();

    protected function courseList() {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        $db = $this->_Extorio()->getDbInstanceDefault();
        $sql = '
        SELECT

        C.id as "courseId",
        C.name as "courseName",
        C."defaultThumbnail" as "courseThumbnail",
        C."maximumPoints" as "courseMaximumPoints",
        UE.id as "userEnrolmentId",
        UE."expiryDate" as "expiryDate",
        UE.status as "status",
        UE."dateStarted" as "dateStarted",
        UE."dateUpdated" as "dateUpdated",
        UE."datePassed" as "datePassed",
        UE."certificate" as "certificate",
        UE."currentPoints" as "currentPoints"

        FROM extoriolms_classes_models_course C

        LEFT JOIN extoriolms_classes_models_user_course UE ON
        UE."courseId" = C.id

        WHERE
        (
            C.approval = $1 OR
            C.approval = $2
        ) AND
        UE."userId" = $3

        ORDER BY C.name
        ';
        $query = $db->query($sql, array(
            ContentApproval::_approved,
            ContentApproval::_closed,
            $loggedInUser->id
        ));
        while($row = $query->fetchAssoc()) {
            $this->courseListData[$row["courseId"]] = $row;

            $sql = '
            SELECT

            P.id as "postId",
            P.name as "postName",
            P."dateCreated" as "postDateCreated",
            P."userId" as "postUserId",
            CASE
                WHEN LENGTH(U.firstname) > 0 THEN
                    CONCAT(UPPER(LEFT(U.firstname,1)), SUBSTR(U.firstname,2,LENGTH(U.firstname)))
                ELSE
                    CONCAT(UPPER(LEFT(U.username,1)), SUBSTR(U.username,2,LENGTH(U.username)))
            END AS "postUserShortName",
            U.avatar as "postUserAvatar",
            P."bodyPlain" as "postBodyPlain"

            FROM extoriolms_classes_models_coursepost P

            LEFT JOIN core_classes_models_user U ON
            P."userId" = U.id

            WHERE
            (
              P.approval = $1 OR
              P.approval = $2
            ) AND
            P."courseId" = $3 AND
            P."parentId" = 0

            ORDER BY P."dateCreated" DESC

            LIMIT 1
            ';
            $irow = $db->query($sql, array(
                ContentApproval::_approved,
                ContentApproval::_closed,
                intval($row["courseId"])
            ))->fetchAssoc();
            if($irow) {
                $this->courseListData[$row["courseId"]]["has_recent"] = true;

                foreach($irow as $k => $v) {
                    $this->courseListData[$row["courseId"]][$k] = $irow[$k];
                }

            } else {
                $this->courseListData[$row["courseId"]]["has_recent"] = false;
            }
        }
    }

    protected function course($userCourseId, $userModuleId = false) {

        $this->authenticateUserEnrolment($userCourseId);

        if($userModuleId) {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false, "My Courses", "/my-courses/"),
                BreadCrumb::n(false, Content::getCourseNameByUserCourse($userCourseId), "/my-courses/".$userCourseId),
                BreadCrumb::n(true, Content::getModuleNameByUserModule($userModuleId))
            ));
        } else {
            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false, "My Courses", "/my-courses/"),
                BreadCrumb::n(true, Content::getCourseNameByUserCourse($userCourseId))
            ));
        }
    }

    /**
     * @var Course
     */
    public $course;
    /**
     * @var User_Course
     */
    public $userEnrolment;

    public $title;
    public $body;

    protected function coursePostsCreate($userEnrolmentId) {
        $this->authenticateUserEnrolment($userEnrolmentId);

        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false, "My Courses", "/my-courses/"),
            BreadCrumb::n(false, Content::getCourseNameByUserCourse($userEnrolmentId), "/my-courses/".$userEnrolmentId),
            BreadCrumb::n(false, "Posts", "/my-courses/".$userEnrolmentId."/posts"),
            BreadCrumb::n(true, "Create")
        ));

        if(isset($_POST["new_post_create"])) {
            $this->title = $_POST["title"];
            $this->body = $_POST["body"];

            $loggedInUser = $this->_Extorio()->getLoggedInUser();
            $error = false;
            $post = CoursePost::n();
            try {
                if(!$loggedInUser) {
                    throw new \Exception("You must be logged in to do that");
                }

                $ue = User_Course::findById($userEnrolmentId,1);
                if(!$ue) {
                    throw new \Exception("Could not find your user enrolment");
                }

                $post->userId = $loggedInUser->id;
                $post->courseId = $ue->courseId;
                $post->body = $this->body;
                $post->name = $this->title;
                $post->approval = ContentApproval::_pending;
                $post->pushThis();

            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if(!$error) {
                $this->_Extorio()->messageSuccess("Your post was successfully created");
                $this->_redirectToDefault(array($userEnrolmentId,"posts",$post->id));

                if(isset($_POST["subscribe"])) {
                    Content::subscribeToCoursePost($loggedInUser->id, $post->id);
                }
            } else {
                $this->_Extorio()->messageError($error);
            }
        }
    }

    protected function coursePostsSearch($userEnrolmentId) {
        $this->authenticateUserEnrolment($userEnrolmentId);

    }

    /**
     * @var PGDB_ResultSet
     */
    public $stickyPostData;
    /**
     * @var PGDB_ResultSet
     */
    public $postData;
    /**
     * @var SimpleFiltering
     */
    public $f;

    protected function coursePosts($userEnrolmentId) {

        $this->authenticateUserEnrolment($userEnrolmentId);

        $this->userEnrolment = User_Course::findById($userEnrolmentId,1);
        if(!$this->userEnrolment) $this->_redirectToDefault();

        $this->course = Course::findById($this->userEnrolment->courseId, 1);
        if(!$this->course) $this->_redirectToDefault();

        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false, "My Courses", "/my-courses/"),
            BreadCrumb::n(false, Content::getCourseNameByUserCourse($userEnrolmentId), "/my-courses/".$userEnrolmentId),
            BreadCrumb::n(true, "Posts")
        ));

        $db = $this->_getDbInstanceDefault();

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe(Server::getRequestURL()));
        $this->f->setLimit(25);
        $this->f->extractFiltering();

        $sql = '
        SELECT
        P.id,
        P.name,
        P."bodyPlain",
        U.longname as ulongname,
        R.longname as rlongname,
        P."dateCreated",
        P."dateReplied",
        P."totalNumReplies",
        P."approval"

        FROM extoriolms_classes_models_coursepost P
        LEFT JOIN core_classes_models_user U ON
        U.id = P."userId"
        LEFT JOIN core_classes_models_user R ON
        R.id = P."replierId"

        WHERE
        P."courseId" = $1 AND
        P."parentId" = 0 AND
        P.sticky = TRUE AND
        (
          P.approval = $2 OR
          P.approval = $3 OR
          P."userId" = $4
        )

        ORDER BY P."dateUpdated" DESC
        ';
        $this->stickyPostData = $db->query($sql,array($this->course->id,ContentApproval::_approved,ContentApproval::_closed,$this->_getLoggedInUserId()));

        $sql = '
        SELECT
        P.id,
        P.name,
        P."bodyPlain",
        U.longname as ulongname,
        R.longname as rlongname,
        P."dateCreated",
        P."dateReplied",
        P."totalNumReplies",
        P."approval"

        FROM extoriolms_classes_models_coursepost P
        LEFT JOIN core_classes_models_user U ON
        U.id = P."userId"
        LEFT JOIN core_classes_models_user R ON
        R.id = P."replierId"

        WHERE
        P."courseId" = $1 AND
        P."parentId" = 0 AND
        P.sticky = FALSE AND
        (
          P.approval = $2 OR
          P.approval = $3 OR
          P."userId" = $4
        )

        ORDER BY P."dateUpdated" DESC
        ';
        $sql .= 'LIMIT '.$this->f->getLimit().' OFFSET '.$this->f->getOffset();
        $sqlCount = '
        SELECT COUNT(P.id)

        FROM extoriolms_classes_models_coursepost P

        WHERE
        P."courseId" = $1 AND
        P."parentId" = 0 AND
        P.sticky = FALSE AND
        (
          P.approval = $2 OR
          P.approval = $3 OR
          P."userId" = $4
        )';

        $this->postData = $db->query($sql,array($this->course->id,ContentApproval::_approved,ContentApproval::_closed,$this->_getLoggedInUserId()));
        $row = $db->query($sqlCount,array($this->course->id,ContentApproval::_approved,ContentApproval::_closed,$this->_getLoggedInUserId()))->fetchRow();
        if($row) {
            $this->f->setCount($row[0]);
        }
    }

    /**
     * @var CoursePost
     */
    public $post;
    /**
     * @var User
     */
    public $author;

    protected function coursePostsPost($userEnrolmentId, $postId, $replyId = false) {

        $this->authenticateUserEnrolment($userEnrolmentId);

        $this->post = CoursePost::findById($postId);
        if(!$this->post) {
            $this->_redirectToDefault(array($userEnrolmentId,"posts"));
        }
        if($this->post->parentId > 0) {
            $this->_redirectToDefault(array($userEnrolmentId,"posts"));
        }

        if(isset($_POST["course_post_submit"])) {
            $p = CoursePost::n();
            $p->body = $_POST["course_post_reply"];
            $p->courseId = $this->post->courseId;
            $p->parentId = $this->post->id;
            $p->userId = $this->_getLoggedInUserId();
            $p->approval = ContentApproval::_pending;
            $p->sticky = false;

            $error = false;
            try{
                if($this->post->approval != ContentApproval::_approved) {
                    throw new \Exception("This post cannot be replied to.");
                }
                $p->pushThis();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }

            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Reply submitted");
                $this->_redirectToDefault(array($userEnrolmentId,"posts",$postId,$p->id));
            }
        }

        $this->author = User::findById($this->post->userId);

        $limit = 10;

        $url = "/my-courses/".$userEnrolmentId."/posts/".$postId."/";
        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($url),$url);
        $this->f->setLimit($limit);
        $this->f->extractFiltering();

        $db = $this->_getDbInstanceDefault();

        if($replyId) {
            $sql = '
            SELECT rnum
            FROM (
                SELECT
                id,
                ROW_NUMBER() OVER (ORDER BY "dateCreated" ASC) AS rnum

                FROM extoriolms_classes_models_coursepost

                WHERE "parentId" = $1 AND
                sticky = FALSE AND
                (
                    approval = $2 OR
                    approval = $3 OR
                    "userId" = $4
                )
            ) sub
            WHERE sub.id = $5
            ';
            $row = $db->query($sql,array($this->post->id,ContentApproval::_approved,ContentApproval::_closed,$this->_getLoggedInUserId(),$replyId))->fetchRow();
            if($row) {
                $this->f->setPage(ceil((intval($row[0])/$limit))-1);
            }
        }



        $sql = '
        SELECT
        P.id,
        P.body,
        P."userId",
        U.longname as ulongname,
        U.avatar,
        P."dateCreated",
        P."approval"

        FROM extoriolms_classes_models_coursepost P
        LEFT JOIN core_classes_models_user U ON
        U.id = P."userId"

        WHERE
        P."parentId" = $1 AND
        P.sticky = FALSE AND
        (
          P.approval = $2 OR
          P.approval = $3 OR
          P."userId" = $4
        )

        ORDER BY P."dateCreated" ASC
        ';
        $sql .= 'LIMIT '.$this->f->getLimit().' OFFSET '.$this->f->getOffset();
        $sqlCount = '
        SELECT COUNT(P.id)

        FROM extoriolms_classes_models_coursepost P

        WHERE
        P."parentId" = $1 AND
        P.sticky = FALSE AND
        (
          P.approval = $2 OR
          P.approval = $3 OR
          P."userId" = $4
        )';

        $this->postData = $db->query($sql,array($this->post->id,ContentApproval::_approved,ContentApproval::_closed,$this->_getLoggedInUserId()));
        $row = $db->query($sqlCount,array($this->post->id,ContentApproval::_approved,ContentApproval::_closed,$this->_getLoggedInUserId()))->fetchRow();
        if($row) {
            $this->f->setCount($row[0]);
        }

        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false, "My Courses", "/my-courses/"),
            BreadCrumb::n(false, Content::getCourseNameByUserCourse($userEnrolmentId), "/my-courses/".$userEnrolmentId),
            BreadCrumb::n(false, "Posts", "/my-courses/".$userEnrolmentId."/posts"),
            BreadCrumb::n(true, Content::getPostName($postId))
        ));
    }

    private function authenticateUserEnrolment($userEnrolmentId) {
        $loggedInUser = $this->_Extorio()->getLoggedInUser();
        if(!$loggedInUser) {
            $this->_redirectToDefault();
        } else {
            if(!Users::userEnrolmentBelongsToUser($userEnrolmentId, $loggedInUser->id)) {
                $this->_redirectToDefault();
            }
        }
    }
}