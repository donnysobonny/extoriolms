<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the course emails for the lms extension
 *
 * Class AdminCourseEmails
 */
class AdminCourseEmails extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_course_emails"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public $config = array();

    public function _onDefault() {
        $this->config = $this->_Extorio()->getConfig();

        if(isset($_POST["submitted"])) {
            $this->_Extension()->_mergeConfigOverride(array(
                "ExtorioLMS" => array(
                    "emails" => array(
                        "courses" => array(
                            "course_enrollments" => array(
                                "enabled" => isset($_POST["course_enrolment_enabled"]),
                                "subject" => $_POST["course_enrolment_subject"],
                                "body" => $_POST["course_enrolment_body"]
                            ),
                            "passed_course" => array(
                                "enabled" => isset($_POST["course_passed_enabled"]),
                                "subject" => $_POST["course_passed_subject"],
                                "body" => $_POST["course_passed_body"]
                            ),
                            "course_cancelled_enrolled" => array(
                                "enabled" => isset($_POST["course_cancelled_enabled"]),
                                "subject" => $_POST["course_cancelled_subject"],
                                "body" => $_POST["course_cancelled_body"]
                            ),
                            "course_expiry" => array(
                                "enabled" => isset($_POST["course_expiry_enabled"]),
                                "days_before" => intval($_POST["course_expiry_days_before"]),
                                "subject" => $_POST["course_expiry_subject"],
                                "body" => $_POST["course_expiry_body"]
                            ),
                            "course_approved" => array(
                                "enabled" => isset($_POST["course_approved_enabled"]),
                                "subject" => $_POST["course_approved_subject"],
                                "body" => $_POST["course_approved_body"]
                            ),
                            "course_closed" => array(
                                "enabled" => isset($_POST["course_closed_enabled"]),
                                "subject" => $_POST["course_closed_subject"],
                                "body" => $_POST["course_closed_body"]
                            ),
                            "course_cancelled" => array(
                                "enabled" => isset($_POST["course_cancelled_owner_enabled"]),
                                "subject" => $_POST["course_cancelled_owner_subject"],
                                "body" => $_POST["course_cancelled_owner_body"]
                            ),
                        )
                    )
                )
            ));

            $this->_Extorio()->messageSuccess("Updated");

            $this->config = $this->_Extorio()->getConfig();
        }
    }
}