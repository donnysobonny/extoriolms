<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Users;

/**
 * Manage the course post emails for the lms extension
 *
 * Class AdminCoursePostEmails
 */
class AdminCoursePostEmails extends \Core\Classes\Commons\Controller {
    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_course_post_emails"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
    }

    public $config = array();

    public function _onDefault() {
        $this->config = $this->_Extorio()->getConfig();

        if(isset($_POST["submitted"])) {
            $this->_Extension()->_mergeConfigOverride(array(
                "ExtorioLMS" => array(
                    "emails" => array(
                        "course_posts" => array(
                            "new_post" => array(
                                "enabled" => isset($_POST["new_post_enabled"]),
                                "subject" => $_POST["new_post_subject"],
                                "body" => $_POST["new_post_body"],
                            ),
                            "new_reply" => array(
                                "enabled" => isset($_POST["new_reply_enabled"]),
                                "subject" => $_POST["new_reply_subject"],
                                "body" => $_POST["new_reply_body"]
                            ),
                        ),
                    )
                )
            ));

            $this->_Extorio()->messageSuccess("Updated");

            $this->config = $this->_Extorio()->getConfig();
        }
    }
}