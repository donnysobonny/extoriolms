<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\PGDB_ResultSet;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\FeedbackPrivileges;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\Feedback;

/**
 * Manage course feedback
 *
 * Class AdminLMSCourseFeedback
 */
class AdminLMSCourseFeedback extends \Core\Classes\Commons\Controller {

    public $modifyAll;
    public $modifyOwn;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_course_feedback"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->modifyAll = Users::userHasPrivilege($this->_getLoggedInUserId(),"feedback_modify_all");
        $this->modifyOwn = Users::userHasPrivilege($this->_getLoggedInUserId(),"feedback_modify_own");
    }

    /**
     * @var SimpleFiltering
     */
    public $f;

    /**
     * @var PGDB_ResultSet
     */
    public $feedbackData;

    public function _onDefault() {

        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"Course Feedback","/extorio-admin/lms-course-feedback/")
        ));

        $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
        $this->f->addFilter("limit", array(
            10 => 10,
            25 => 25,
            100 => 100
        ));
        $filters = array();
        if($this->modifyAll) {
            $filters[""] = "Everyone's courses";
        }
        $filters["own"] = "Own courses";
        $this->f->addFilter("show",$filters);
        $this->f->addFilter("with-status",array(
            "" => "Any status",
            "pending" => "Pending approval",
            "approved" => "Approved",
            "cancelled" => "Cancelled"
        ));
        $this->f->addFilter("order", array(
            "date" => "Date",
            "rating" => "Rating"
        ));

        $this->f->setSearchTypes(array(
            "user" => "User",
            "course" => "Course"
        ));

        $this->f->extractFiltering();
        $this->f->setLimit($this->f->getFilter("limit"));

        $sql = '
        SELECT

        F.id,
        U.longname,
        C.name,
        F."dateUpdated",
        F.rating,
        F.comment,
        F.approval

        FROM extoriolms_classes_models_feedback F
        LEFT JOIN core_classes_models_user U ON
        U.id = F."ownerId"
        LEFT JOIN extoriolms_classes_models_course C ON
        C.id = F."courseId"
        ';
        $sqlCount = '
        SELECT count(F.id)

        FROM extoriolms_classes_models_feedback F
        LEFT JOIN core_classes_models_user U ON
        U.id = F."ownerId"
        LEFT JOIN extoriolms_classes_models_course C ON
        C.id = F."courseId"
        ';
        $where = "";
        $params = array();
        $index = 1;
        if(strlen($this->f->getFilter("show"))) {
            switch($this->f->getFilter("show")) {
                case "own" :
                    $where .= 'C."ownerId" = $'.$index." AND ";
                    $params[] = $this->_getLoggedInUserId();
                    $index++;
                    break;
            }
        }
        if(strlen($this->f->getFilter("with-status"))) {
            switch($this->f->getFilter("with-status")) {
                case "pending" :
                    $where .= 'F.approval = $'.$index." AND ";
                    $params[] = ContentApproval::_pending;
                    $index++;
                    break;
                case "approved" :
                    $where .= 'F.approval = $'.$index." AND ";
                    $params[] = ContentApproval::_approved;
                    $index++;
                    break;
                case "cancelled" :
                    $where .= 'F.approval = $'.$index." AND ";
                    $params[] = ContentApproval::_cancelled;
                    $index++;
                    break;
            }
        }
        if(strlen($this->f->getSearchQuery())) {
            switch($this->f->getSearchType()) {
                case "user" :
                    $where .= '(LOWER(U.firstname) LIKE LOWER($'.$index.') OR LOWER(U.username) LIKE LOWER($'.$index.') OR LOWER(U.email) LIKE LOWER($'.$index.')) AND ';
                    $params[] = "%".$this->f->getSearchQuery()."%";
                    $index++;
                    break;
                case "course" :
                    $where .= 'LOWER(C.name) LIKE LOWER($'.$index.') AND ';
                    $params[] = "%".$this->f->getSearchQuery()."%";
                    $index++;
                    break;
            }
        }

        if(strlen($where)) {
            $sql .= 'WHERE '.substr($where,0,strlen($where)-4);
            $sqlCount .= 'WHERE '.substr($where,0,strlen($where)-4);
        }

        switch($this->f->getFilter("order")) {
            case "date" :
                $sql .= 'ORDER BY F."dateUpdated" DESC ';
                break;
            case "rating" :
                $sql .= 'ORDER BY F."rating" DESC ';
                break;
        }

        $sql .= 'LIMIT '.$this->f->getLimit().' OFFSET '.$this->f->getOffset();

        $db = $this->_getDbInstanceDefault();
        $this->feedbackData = $db->query($sql,$params);
        $row = $db->query($sqlCount,$params)->fetchRow();
        $this->f->setCount($row[0]);
    }

    public function approve($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        $fb = Feedback::findById($id,1);
        if(!$fb) {
            $this->_redirectToDefault();
        }
        $c = Course::findById($fb->courseId,1);
        if(!$c) {
            $this->_redirectToDefault();
        }
        $access = false;
        if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$c->ownerId,FeedbackPrivileges::_feedback_approve_all)) {
            $access = true;
        }
        elseif(
            $c->ownerId == $this->_getLoggedInUserId() &&
            Users::userHasPrivilege($this->_getLoggedInUserId(),FeedbackPrivileges::_feedback_approve_own)
        ) {
            $access = true;
        }
        if(!$access) {
            $this->_messageWarning("You are not able to approve this feedback");
        } else {
            $fb->approve();
            $this->_messageSuccess("Feedback approved");
        }
        $this->_redirectToDefault();
    }

    public function cancel($id = false) {
        if(!$id) {
            $this->_redirectToDefault();
        }
        $fb = Feedback::findById($id,1);
        if(!$fb) {
            $this->_redirectToDefault();
        }
        $c = Course::findById($fb->courseId,1);
        if(!$c) {
            $this->_redirectToDefault();
        }
        $access = false;
        if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$c->ownerId,FeedbackPrivileges::_feedback_cancel_all)) {
            $access = true;
        }
        elseif(
            $c->ownerId == $this->_getLoggedInUserId() &&
            Users::userHasPrivilege($this->_getLoggedInUserId(),FeedbackPrivileges::_feedback_cancel_own)
        ) {
            $access = true;
        }
        if(!$access) {
            $this->_messageWarning("You are not able to cancel this feedback");
        } else {
            $fb->cancel();
            $this->_messageSuccess("Feedback cancelled");
        }
        $this->_redirectToDefault();
    }
}