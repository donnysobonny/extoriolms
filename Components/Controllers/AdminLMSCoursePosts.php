<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Helpers\PGDB_ResultSet;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\CoursePostPrivileges;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\CoursePost;

/**
 * Manage course posts
 *
 * Class AdminLMSCoursePosts
 */
class AdminLMSCoursePosts extends \Core\Classes\Commons\Controller {

    public $modifyAll;
    public $modifyOwn;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_course_posts"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->modifyAll = Users::userHasPrivilege($this->_getLoggedInUserId(),"course_posts_modify_all");
        $this->modifyOwn = Users::userHasPrivilege($this->_getLoggedInUserId(),"course_posts_modify_own");
    }

    /**
     * @var SimpleFiltering
     */
    public $f;

    /**
     * @var PGDB_ResultSet
     */
    public $postData;

    public function _onDefault($id = false) {
        if(!$id) {
            $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
            $this->f->addFilter("limit", array(
                10 => 10,
                25 => 25,
                100 => 100
            ));
            $filters = array();
            if($this->modifyAll) {
                $filters[] = "Everyone's courses";
            }
            $filters["own"] = "Own courses";
            $this->f->addFilter("show",$filters);
            $this->f->addFilter("with-status",array(
                "" => "Any status",
                "pending" => "Pending approval",
                "approved" => "Approved",
                "closed" => "Closed",
                "cancelled" => "Cancelled"
            ));
            $this->f->addFilter("type",array(
                "" => "Replies & main posts",
                "reply" => "Replies only",
                "main" => "Main posts only"
            ));
            $this->f->addFilter("sticky",array(
                "" => "Sticky & unsticky",
                "sticky" => "Sticky only",
                "unsticky" => "Unsticky only"
            ));
            $this->f->setSearchTypes(array(
                "user" => "User",
                "title" => "Post title",
                "course" => "Course name"
            ));

            $this->f->extractFiltering();
            $this->f->setLimit($this->f->getFilter("limit"));

            $sql = '
            SELECT

            P.id,
            P."parentId",
            P.name,
            P.body,
            PP.name as "mainName",
            PP.body as "mainBody",
            U.longname,
            C.name as "courseName",
            P.approval,
            P.sticky,
            P."dateCreated"

            FROM extoriolms_classes_models_coursepost P
            LEFT JOIN extoriolms_classes_models_coursepost PP ON
            PP.id = P."parentId"
            LEFT JOIN extoriolms_classes_models_course C ON
            C.id = P."courseId"
            LEFT JOIN core_classes_models_user U ON
            U.id = P."userId"
            ';
            $sqlCount = '
            SELECT COUNT(P.id)

            FROM extoriolms_classes_models_coursepost P
            LEFT JOIN extoriolms_classes_models_coursepost PP ON
            PP.id = P."parentId"
            LEFT JOIN extoriolms_classes_models_course C ON
            C.id = P."courseId"
            LEFT JOIN core_classes_models_user U ON
            U.id = P."userId"
            ';
            $where = "";
            $params = array();
            $index = 1;

            if(strlen($this->f->getFilter("show"))) {
                switch($this->f->getFilter("show")) {
                    case "own" :
                        $where .= 'C."ownerId" = $'.$index.' AND ';
                        $params[] = $this->_getLoggedInUserId();
                        $index++;
                        break;
                }
            }
            if(strlen($this->f->getFilter("with-status"))) {
                switch($this->f->getFilter("with-status")) {
                    case "pending" :
                        $where .= 'P.approval = $'.$index.' AND ';
                        $params[] = ContentApproval::_pending;
                        $index++;
                        break;
                    case "approved" :
                        $where .= 'P.approval = $'.$index.' AND ';
                        $params[] = ContentApproval::_approved;
                        $index++;
                        break;
                    case "closed" :
                        $where .= 'P.approval = $'.$index.' AND ';
                        $params[] = ContentApproval::_closed;
                        $index++;
                        break;
                    case "cancelled" :
                        $where .= 'P.approval = $'.$index.' AND ';
                        $params[] = ContentApproval::_cancelled;
                        $index++;
                        break;
                }
            }
            if(strlen($this->f->getFilter("type"))) {
                switch($this->f->getFilter("type")) {
                    case "reply" :
                        $where .= 'P."parentId" > 0 AND ';
                        break;
                    case "main" :
                        $where .= 'P."parentId" = 0 AND ';
                        break;
                }
            }
            if(strlen($this->f->getFilter("sticky"))) {
                switch($this->f->getFilter("sticky")) {
                    case "sticky" :
                        $where .= 'P.sticky = true AND ';
                        break;
                    case "unsticky" :
                        $where .= 'P.sticky = false AND ';
                        break;
                }
            }
            if(strlen($this->f->getSearchQuery())) {
                switch($this->f->getSearchType()) {
                    case "user" :
                        $where .= '(LOWER(U.username) LIKE LOWER($'.$index.') OR LOWER(U.firstname) LIKE LOWER($'.$index.') OR LOWER(U.email) LIKE LOWER($'.$index.')) AND ';
                        $params[] = $this->f->getSearchQuery();
                        $index++;
                        break;
                    case "title" :
                        $where .= '(LOWER(P.name) LIKE LOWER($'.$index.') OR LOWER(PP.name) LIKE LOWER($'.$index.')) AND ';
                        $params[] = $this->f->getSearchQuery();
                        $index++;
                        break;
                    case "course" :
                        $where .= 'LOWER(C.name) LIKE LOWER($'.$index.') AND ';
                        $params[] = $this->f->getSearchQuery();
                        $index++;
                        break;
                }
            }

            if(strlen($where)) {
                $sql .= 'WHERE '.substr($where,0,strlen($where)-4);
                $sqlCount .= 'WHERE '.substr($where,0,strlen($where)-4);
            }

            $sql .= 'ORDER BY "dateCreated" DESC LIMIT '.$this->f->getLimit()." OFFSET ".$this->f->getOffset();
            $db = $this->_getDbInstanceDefault();
            $this->postData = $db->query($sql,$params);
            $row = $db->query($sqlCount,$params)->fetchRow();
            $this->f->setCount($row[0]);
        }
    }

    public function approve($id = false) {
        $cp = CoursePost::findById($id);
        if(!$cp) {
            $this->_redirectToDefault();
        }
        $course = Course::findById($cp->courseId);
        if(!$course) {
            $this->_redirectToDefault();
        }
        $access = false;
        if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$course->ownerId,"course_posts_approve_all")) {
            $access = true;
        }
        elseif(
            $course->ownerId == $this->_getLoggedInUserId() &&
            Users::userHasPrivilege($this->_getLoggedInUserId(),"course_posts_approve_own")
        ) {
            $access = true;
        }
        if(!$access) {
            $this->_messageWarning("You are not able to approve this post");
        } else {
            $error = false;
            try{
                $cp->approve();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Post approved");
            }
        }
        $this->_redirectToDefault();
    }

    public function close($id = false) {
        $cp = CoursePost::findById($id);
        if(!$cp) {
            $this->_redirectToDefault();
        }
        $course = Course::findById($cp->courseId);
        if(!$course) {
            $this->_redirectToDefault();
        }
        $access = false;
        if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$course->ownerId,"course_posts_close_all")) {
            $access = true;
        }
        elseif(
            $course->ownerId == $this->_getLoggedInUserId() &&
            Users::userHasPrivilege($this->_getLoggedInUserId(),"course_posts_close_own")
        ) {
            $access = true;
        }
        if(!$access) {
            $this->_messageWarning("You are not able to close this post");
        } else {
            $error = false;
            try{
                $cp->close();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Post closed");
            }
        }
        $this->_redirectToDefault();
    }

    public function cancel($id = false) {
        $cp = CoursePost::findById($id);
        if(!$cp) {
            $this->_redirectToDefault();
        }
        $course = Course::findById($cp->courseId);
        if(!$course) {
            $this->_redirectToDefault();
        }
        $access = false;
        if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$course->ownerId,"course_posts_cancel_all")) {
            $access = true;
        }
        elseif(
            $course->ownerId == $this->_getLoggedInUserId() &&
            Users::userHasPrivilege($this->_getLoggedInUserId(),"course_posts_cancel_own")
        ) {
            $access = true;
        }
        if(!$access) {
            $this->_messageWarning("You are not able to cancel this post");
        } else {
            $error = false;
            try{
                $cp->cancel();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Post cancelled");
            }
        }
        $this->_redirectToDefault();
    }

    public function sticky($id = false) {
        $cp = CoursePost::findById($id);
        if(!$cp) {
            $this->_redirectToDefault();
        }
        $course = Course::findById($cp->courseId);
        if(!$course) {
            $this->_redirectToDefault();
        }
        $access = false;
        if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$course->ownerId,"course_posts_sticky_all")) {
            $access = true;
        }
        elseif(
            $course->ownerId == $this->_getLoggedInUserId() &&
            Users::userHasPrivilege($this->_getLoggedInUserId(),"course_posts_sticky_own")
        ) {
            $access = true;
        }
        if(!$access) {
            $this->_messageWarning("You are not able to sticky this post");
        } else {
            $error = false;
            try{
                $cp->sticky();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Post stickied");
            }
        }
        $this->_redirectToDefault();
    }

    public function unsticky($id = false) {
        $cp = CoursePost::findById($id);
        if(!$cp) {
            $this->_redirectToDefault();
        }
        $course = Course::findById($cp->courseId);
        if(!$course) {
            $this->_redirectToDefault();
        }
        $access = false;
        if(Users::userManagesUserAndHasPrivilege($this->_getLoggedInUserId(),$course->ownerId,"course_posts_sticky_all")) {
            $access = true;
        }
        elseif(
            $course->ownerId == $this->_getLoggedInUserId() &&
            Users::userHasPrivilege($this->_getLoggedInUserId(),"course_posts_sticky_own")
        ) {
            $access = true;
        }
        if(!$access) {
            $this->_messageWarning("You are not able to unsticky this post");
        } else {
            $error = false;
            try{
                $cp->unsticky();
            } catch(\Exception $ex) {
                $error = $ex->getMessage();
            }
            if($error) {
                $this->_messageError($error);
            } else {
                $this->_messageSuccess("Post unstickied");
            }
        }
        $this->_redirectToDefault();
    }
}