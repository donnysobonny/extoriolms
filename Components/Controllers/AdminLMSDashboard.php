<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Models\Course;

/**
 * 
 *
 * Class AdminLMSDashboard
 */
class AdminLMSDashboard extends \Core\Classes\Commons\Controller {

    public $pendingCourseCount = 0;

    public function _onDefault() {
        $this->_Extorio()->setTargetBreadCrumbs(array(
            BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
            BreadCrumb::n(true,"Dashboard")
        ));

        $this->pendingCourseCount = Course::findCount(Query::n()->where(array(
            "approval" => ContentApproval::_pending
        )));
    }
}