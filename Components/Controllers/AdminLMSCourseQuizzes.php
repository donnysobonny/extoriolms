<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\PGDB_ResultSet;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\UserGroup;
use Core\Classes\Utilities\Server;
use Core\Classes\Utilities\Strings;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\LectureType;
use ExtorioLMS\Classes\Enums\ModuleType;
use ExtorioLMS\Classes\Models\Module;
use ExtorioLMS\Classes\Models\User_Module;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * Manage course quizzes
 *
 * Class AdminLMSCourseQuizzes
 */
class AdminLMSCourseQuizzes extends \Core\Classes\Commons\Controller {

    public $modifyAll;
    public $modifyOwn;

    public function _onBegin() {
        if(!Users::userHasPrivileges_OR($this->_getLoggedInUser()->id,array(
            "extorio_pages_all",
            "extorio_pages_lms_course_quizzes"
        ))) {
            $this->_redirectTo401AccessDeniedPage(array(
                "r" => Server::getRequestURI(),
                array(),
                401
            ));
        }
        $this->modifyAll = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_quizzes_modify_all");
        $this->modifyOwn = Users::userHasPrivilege($this->_getLoggedInUserId(),"user_quizzes_modify_own");
    }

    /**
     * @var SimpleFiltering
     */
    public $f;

    /**
     *
     * @var PGDB_ResultSet
     */
    public $quizData;

    /**
     * @var User_Module
     */
    public $userModule;
    /**
     * @var Module
     */
    public $module;

    public function _onDefault($id = false) {
        if(!$id) {
            /*
             * LIST QUIZZES
             */
            $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getDbInstanceDefault()));
            $this->f->addFilter("limit", array(
                10 => 10,
                25 => 25,
                100 => 100
            ));
            $filters = array();
            if($this->modifyAll) {
                $filters[""] = "Everyone's courses";
            }
            $filters["own"] = "Own courses";
            $this->f->addFilter("show",$filters);
            $loggedInUser = $this->_getLoggedInUser();
            $filters = array();
            if($loggedInUser->userGroup->manageAll) {
                $filters[""] = "Any user group";
                foreach(UserGroup::findAll(Query::n()->order("name")) as $ug) {
                    $filters[$ug->id] = $ug->name;
                }
            } else {
                foreach($loggedInUser->userGroup->manageGroups as $id) {
                    $filters[$id] = Users::getUserGroupName($id);
                }
            }
            $this->f->addFilter("user-group",$filters);
            $this->f->addFilter("status",array(
                "" => "Any status",
                "submitted" => "Submitted for marking",
                "pending" => "Pending",
                "started" => "Started",
                "passed" => "Passed",
                "failed" => "Failed"
            ));
            $this->f->setSearchTypes(array(
                "user" => "User",
                "course" => "Course",
                "module" => "Module"
            ));

            $this->f->extractFiltering();
            $this->f->setLimit($this->f->getFilter("limit"));

            $sql = '
            SELECT

            UQ.id,
            U.longname,
            C.name as coursename,
            M.name as modulename,
            UQ."currentAttempts",
            UQ."currentPoints",
            M."maximumPoints",
            UQ.status,
            UQ."dateUpdated"

            FROM extoriolms_classes_models_user_module UQ

            LEFT JOIN core_classes_models_user U ON
            U.id = UQ."userId"

            LEFT JOIN extoriolms_classes_models_course C ON
            C.id = UQ."courseId"

            LEFT JOIN extoriolms_classes_models_module M ON
            M.id = UQ."moduleId"

            ';
            $sqlCount = '
            SELECT COUNT(UQ.id)

            FROM extoriolms_classes_models_user_module UQ

            LEFT JOIN core_classes_models_user U ON
            U.id = UQ."userId"

            LEFT JOIN extoriolms_classes_models_course C ON
            C.id = UQ."courseId"

            LEFT JOIN extoriolms_classes_models_module M ON
            M.id = UQ."moduleId"

            ';

            $where = '';
            $params = array();
            $index = 1;

            $where .= 'M.type = $'.$index." AND ";
            $params[] = ModuleType::_quiz;
            $index++;

            if(strlen($this->f->getFilter("show"))) {
                switch($this->f->getFilter("show")) {
                    case "own" :
                        $where .= 'C."ownerId" = $'.$index." AND ";
                        $params[] = $this->_getLoggedInUserId();
                        $index++;
                        break;
                }
            }
            if(strlen($this->f->getFilter("user-group"))) {
                $where .= 'U."userGroup" = $'.$index." AND ";
                $params[] = $this->f->getFilter("user-group");
                $index++;
            }
            if(strlen($this->f->getFilter("status"))) {
                switch($this->f->getFilter("status")) {
                    case "submitted":
                        $where .= 'UQ.status = $'.$index." AND ";
                        $params[] = ContentStatus::_submitted;
                        $index++;
                        break;
                    case "pending" :
                        $where .= 'UQ.status = $'.$index." AND ";
                        $params[] = ContentStatus::_pending;
                        $index++;
                        break;
                    case "started" :
                        $where .= 'UQ.status = $'.$index." AND ";
                        $params[] = ContentStatus::_started;
                        $index++;
                        break;
                    case "passed" :
                        $where .= 'UQ.status = $'.$index." AND ";
                        $params[] = ContentStatus::_passed;
                        $index++;
                        break;
                    case "failed" :
                        $where .= 'UQ.status = $'.$index." AND ";
                        $params[] = ContentStatus::_failed;
                        $index++;
                        break;
                }
            }
            if(strlen($this->f->getSearchQuery())) {
                switch($this->f->getSearchType()) {
                    case "user" :
                        $where .= '(LOWER(U.username) LIKE LOWER($'.$index.') OR LOWER(U.email) LIKE LOWER($'.$index.') OR LOWER(U.firstname) LIKE LOWER($'.$index.')) AND ';
                        $params[] = "%".$this->f->getSearchQuery()."%";
                        $index++;
                        break;
                    case "course" :
                        $where .= 'LOWER(C.name) LIKE LOWER($'.$index.') AND ';
                        $params[] = "%".$this->f->getSearchQuery()."%";
                        $index++;
                        break;
                    case "module" :
                        $where .= 'LOWER(M.name) LIKE LOWER($'.$index.') AND ';
                        $params[] = "%".$this->f->getSearchQuery()."%";
                        $index++;
                        break;
                }
            }

            if(strlen($where)) {
                $sql .= "WHERE ".substr($where,0,strlen($where)-4);
                $sqlCount .= "WHERE ".substr($where,0,strlen($where)-4);
            }

            $sql .= 'ORDER BY UQ."dateUpdated" DESC ';
            $sql .= 'LIMIT '.$this->f->getLimit()." ";
            $sql .= 'OFFSET '.$this->f->getOffset()." ";

            $db = $this->_getDbInstanceDefault();
            $this->quizData = $db->query($sql,$params);

            $row = $db->query($sqlCount,$params)->fetchRow();
            $this->f->setCount($row[0]);

            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(true,"Course Quizzes")
            ));
        } else {
            $this->userModule = $um = User_Module::findById($id,INF);
            if(!$um) {
                $this->_redirectToDefault();
            }
            $this->module = Module::findById($um->moduleId,INF);
            if(isset($_POST["user_question_submit"]) || isset($_POST["user_question_submit_exit"])) {
                $id = intval($_POST["user_question_id"]);
                $status = $_POST["user_question_status"];
                $response = $_POST["user_question_response"];
                $questions = $this->userModule->userQuestions;
                foreach($questions as $question) {
                    if($question->id == $id) {
                        $question->status = $status;
                        $question->response = $response;
                        $question->responseDate = date("Y-m-d H:i:s");
                        $question->responseById = $this->_getLoggedInUserId();
                    }
                }
                $this->userModule->userQuestions = $questions;
                $error = false;
                try {
                    $this->userModule->mark();
                } catch(\Exception $ex) {
                    $error = $ex->getMessage();
                }
                if($error) {
                    $this->_messageError($error);
                } else {
                    $this->_messageSuccess("Response saved");
                    if(isset($_POST["user_question_submit_exit"])) {
                        $this->_redirectToDefault();
                    } else {
                        $this->_redirectToDefault(array($this->userModule->id));
                    }
                }
            }

            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false,"Extorio Admin","/extorio-admin/"),
                BreadCrumb::n(false,"Course Quizzes","/extorio-admin/lms-course-quizzes/"),
                BreadCrumb::n(true, Users::getLongname($um->userId)." (".Content::getCourseName($um->courseId)." / ".Content::getModuleName($um->moduleId).")")
            ));
        }
    }
}