<?php
namespace ExtorioLMS\Components\Controllers;
use Core\Classes\Helpers\BreadCrumb;
use Core\Classes\Helpers\Query;
use Core\Classes\Helpers\SimpleFiltering;
use Core\Classes\Models\User;
use Core\Classes\Utilities\Strings;
use ExtorioLMS\Classes\Enums\ContentCategory;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Helpers\CourseThumbnail;
use ExtorioLMS\Classes\Models\CourseCategory;
use ExtorioLMS\Classes\Models\CourseTag;
use ExtorioLMS\Classes\Utilities\Content;

/**
 * 
 *
 * Class Courses
 */
class Courses extends \Core\Classes\Commons\Controller {

    /**
     * @var \ExtorioLMS\Classes\Models\Course
     */
    public $course;
    /**
     * @var User
     */
    public $author;

    public $reviewCount = 0;

    /**
     * @var SimpleFiltering
     */
    public $f;

    public function _onDefault($id = false, $name = false) {
        if($id) {
            /*
             * DISPLAYING SINGLE COURSE
             */
            $this->course = \ExtorioLMS\Classes\Models\Course::findById($id,3);
            if(!$this->course) {
                $this->_redirectTo404PageNotFoundPage();
            }
            if(!$name) {
                $this->_redirectToDefault(array($id,urlencode($this->course->name)));
            }
            $this->author = User::findById($this->course->ownerId,2);

            $this->reviewCount = Content::getCourseFeedbackCount($this->course->id);

            $this->_Extorio()->setTargetBreadCrumbs(
                array(
                    BreadCrumb::n(false,"Courses", "/courses/"),
                    BreadCrumb::n(true,$this->course->name),
                )
            );
        } else {
            /*
             * DISPLAYING MANY COURSES
             */
            $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToDefault()));
            $this->f->addFilter("limit", array(
                12 => 12,
                24 => 24,
                120 => 120
            ));
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addFilter("order", array(
                "name" => "Name",
                "rating" => "Rating",
                "students" => "Number of students"
            ));
            $this->f->extractFiltering();
            $this->f->setLimit($this->f->getFilter("limit"));
            $this->f->setCount(Content::getCourseCount(""));
        }
    }

    /**
     *
     * @var CourseThumbnail[]
     */
    public $courseThumbnails = array();

    //TODO: need to make this configurable
    public $displayDescriptions = true;

    /**
     * @var CourseCategory
     */
    public $category;
    /**
     * @var CourseCategory[]
     */
    public $categories = array();

    public function categories($categoryId = false, $categoryName = false) {
        if($categoryId) {
            $this->category = CourseCategory::findById($categoryId,1);
            if(!$this->category) {
                $this->_redirectTo404PageNotFoundPage();
            }
            if(!$categoryName) {
                $this->_redirectToMethod("categories", array($this->category->id, urlencode($this->category->name)));
            }

            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false, "Courses", "/courses/"),
                BreadCrumb::n(false, "Categories", "/courses/categories/"),
                BreadCrumb::n(true, ucwords($this->category->name))
            ));

            $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToMethod("categories")));
            $this->f->addFilter("limit", array(
                12 => 12,
                24 => 24,
                120 => 120
            ));
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addFilter("order", array(
                "name" => "Name",
                "rating" => "Rating",
                "students" => "Number of students"
            ));
            $this->f->extractFiltering();
            $this->f->setLimit($this->f->getFilter("limit"));
            $this->f->setCount(Content::getCourseCountByCategory($categoryId));
        } else {
            $this->categories = CourseCategory::getAllCategories();
        }
    }

    /**
     *
     * @var CourseTag[]
     */
    public $tags = array();
    /**
     *
     * @var CourseTag
     */
    public $tag;

    public function tags($tagId = false, $tagName = false) {
        if($tagId) {
            $this->tag = CourseTag::findById($tagId,1);
            if(!$this->tag) {
                $this->_redirectTo404PageNotFoundPage();
            }
            if(!$tagName) {
                $this->_redirectToMethod("tags", array($this->tag->id, $this->tag->name));
            }

            $this->_Extorio()->setTargetBreadCrumbs(array(
                BreadCrumb::n(false, "Courses", "/courses/"),
                BreadCrumb::n(false, "Tags", "/courses/tags/"),
                BreadCrumb::n(true, ucwords($this->tag->name))
            ));

            $this->f = SimpleFiltering::n(Strings::propertyNameSafe($this->_getUrlToMethod("tags")));
            $this->f->addFilter("limit", array(
                12 => 12,
                24 => 24,
                120 => 120
            ));
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addEmpty();
            $this->f->addFilter("order", array(
                "name" => "Name",
                "rating" => "Rating",
                "students" => "Number of students"
            ));
            $this->f->extractFiltering();
            $this->f->setLimit($this->f->getFilter("limit"));
            $this->f->setCount(Content::getCourseCountByTagId($tagId));
        } else {
            $this->tags = CourseTag::findAll(
                Query::n()
                    ->order(array("name" => "asc"))
            );
        }
    }

    public $phrase = "";
    public $phraseRaw = "";

    public function search() {
        if(isset($_GET["csearch"]) && strlen($_GET["csearch"])) {
            $this->phrase = $this->phraseRaw = $_GET["csearch"];
            //clean up the phrase
            $this->phrase = strtolower(Strings::removeRecurring(Strings::removeQuotes(Strings::removeSpecialCharacters($this->phrase))," "));
            //seperate words with &
            $words = explode(" ",$this->phrase);
            $clean = "";
            foreach($words as $word) {
                if(strlen($word)) {
                    $clean .= $word." & ";
                }
            }
            $this->phrase = substr($clean,0,strlen($clean)-3);
            if(!strlen($this->phrase)) {
                $this->_redirectToDefault();
            }
        } else {
            $this->_redirectToDefault();
        }
    }
}