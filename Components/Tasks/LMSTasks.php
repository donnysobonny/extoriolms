<?php
namespace ExtorioLMS\Components\Tasks;
use ExtorioLMS\Classes\Enums\ContentApproval;
use ExtorioLMS\Classes\Enums\ContentStatus;
use ExtorioLMS\Classes\Enums\ContentType;
use ExtorioLMS\Classes\Models\Course;
use ExtorioLMS\Classes\Models\CoursePost;
use ExtorioLMS\Classes\Models\Session;
use ExtorioLMS\Classes\Models\User_Enrolment;
use ExtorioLMS\Classes\Utilities\Emails;

/**
 * 
 *
 * Class LMSTasks
 */
class LMSTasks extends \Core\Classes\Commons\Task {

    private $sleepTime = 0.1;

    public function clean_up_courses() {
        //removes course that have been drafting for 7 days or more
        $db = $this->_Extorio()->getDbInstanceDefault();
        $sql = 'SELECT id FROM extoriolms_classes_models_course WHERE approval = $1 AND "dateCreated" <= $2 LIMIT 100';
        $query = $db->query($sql,array(ContentApproval::_drafting,date("Y-m-d 23:59:59", time() - (60*60*24*7))));
        while($row = $query->fetchRow()) {
            $course = Course::findById($row[0],1);
            if($course) {
                $course->deleteThis();
            }
            sleep($this->sleepTime);
        }
    }

    public function send_course_expiry_emails() {
        $config = $this->_Extorio()->getConfig();

        $sql = 'SELECT
        id
        FROM extoriolms_classes_models_user_enrolment

        WHERE
        "type" = $4 AND
        "status" != $3 AND
        "expiryDate" IS NOT NULL AND
        "expiryDate" >= $1 AND
        "expiryDate" <= $2
        ';
        $db = $this->_Extorio()->getDbInstanceDefault();
        $time = time() + (60*60*24*$config["ExtorioLMS"]["emails"]["courses"]["course_expiry"]["days_before"]);
        $query = $db->query($sql,array(
            date("Y-m-d 00:00:00", $time),
            date("Y-m-d 23:59:59", $time),
            ContentStatus::_passed,
            ContentType::_course
        ));

        $this->_logToTask("found ".$query->numRows()." enrolments. Sending emails now");

        while($row = $query->fetchRow()) {
            $ue = User_Enrolment::findById($row[0],1,true);
            if($ue) {
                Emails::sendCourseExpiry_user($ue);
                //wait briefly
                sleep($this->sleepTime);
            }
        }

        $this->_logToTask("All emails sent");
    }

    public function send_course_cancellation_emails($courseId=false) {
        if(!$courseId) {
            $this->_failTask("You must provide the course id");
            return;
        }

        $course = Course::findById($courseId,1);
        if(!$course) {
            $this->_failTask("Could not find course");
            return;
        }

        $this->_logToTask("Sending email to owner first");

        Emails::sendCourseCancelled_owner($course);

        sleep($this->sleepTime);

        $this->_logToTask("Sending emails to enrolled users now");

        Emails::sendCourseCancelled_user($course,$this->sleepTime);

        $this->_logToTask("done!");
    }

    public function send_new_course_post_emails($postId = false) {
        if(!$postId) {
            $this->_failTask("You must provide the post id");
            return;
        }

        $post = CoursePost::findById($postId,1);
        if(!$post) {
            $this->_failTask("Could not find the course post");
            return;
        }

        $this->_logToTask("Sending emails to course subscribers");

        Emails::sendNewPost($post, $this->sleepTime);

        $this->_logToTask("done!");
    }

    public function send_reply_course_post_emails($postId = false) {
        if(!$postId) {
            $this->_failTask("You must provide a post id");
            return;
        }

        $post = CoursePost::findById($postId,1);
        if(!$post) {
            $this->_failTask("Could not find the post");
            return;
        }

        $this->_logToTask("Sending emails to post subscribers");

        Emails::sendNewReply($post, $this->sleepTime);

        $this->_logToTask("done!");
    }
}