<?php
namespace ExtorioLMS\Components\Extension;

use Core\Classes\Commons\Extorio;
use Core\Classes\Enums\FontAwesomeIcons;
use Core\Classes\Enums\InternalEvents;
use Core\Classes\Enums\PHPTypes;
use Core\Classes\Enums\PropertyBasicType;
use Core\Classes\Enums\PropertyType;
use Core\Classes\Helpers\AdminMenuLeft_Item;
use Core\Classes\Helpers\AdminMenuLeft_Menu;
use Core\Classes\Helpers\LayoutSystem;
use Core\Classes\Models\BlockProcessorCategory;
use Core\Classes\Models\Property;
use Core\Classes\Models\Template;
use Core\Classes\Models\Theme;
use Core\Classes\Models\UserPrivilegeCategory;
use Core\Classes\Utilities\Users;
use ExtorioLMS\Classes\Models\CourseCategory;

class Extension extends \Core\Classes\Commons\Extension {
    protected function _onStart() {
        $config = $this->_Extorio()->getConfig();

        //CUSTOM LESS FILES
        $this->_Extorio()->bindActionToEvent(InternalEvents::_theme_compile_less_files, function() {
            return array(
                //list the file paths to less files that need to be compiled to css here
            );
        });
        //CUSTOM CSS FILES
        $this->_Extorio()->bindActionToEvent(InternalEvents::_theme_minify_css_files, function() {
            return array(
                //list the file paths to css files that need to be minified here
            );
        });
        //CUSTOM JS FILES
        $this->_Extorio()->bindActionToEvent(InternalEvents::_theme_minify_js_files, function() {
            return array(
                //list the file paths to js files that need to be minified here
            );
        });

        if($this->_Extorio()->onExtorioAdminPage()) {
            //setup the left menus
            $loggedInUser = $this->_Extorio()->getLoggedInUser();
            if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                "extorio_menus_all",
                "extorio_menus_lmsmenu"
            ))) {
                $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_menus",function() {
                    return AdminMenuLeft_Menu::n("lmsmenu","LMS","",FontAwesomeIcons::_book);
                },401,false);
            }

            $this->_Extorio()->bindActionToEvent("extorio_admin_menu_left_items",function($menuName) {
                $loggedInUser = Extorio::get()->getLoggedInUser();
                switch($menuName) {
                    case "lmsmenu" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_dashboard"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Dashboard","manage courses","/extorio-admin/lms-dashboard/","/extorio-admin/lms-dashboard/",FontAwesomeIcons::_dashboard);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_courses"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Courses","manage courses","/extorio-admin/lms-courses/","/extorio-admin/lms-courses/",FontAwesomeIcons::_book);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_course_files"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Course Files","manage course files","/extorio-admin/lms-course-files/","/extorio-admin/lms-course-files/",FontAwesomeIcons::_filesO);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_course_enrolments"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Course Enrolments","manage course enrolments","/extorio-admin/lms-course-enrolments/","/extorio-admin/lms-course-enrolments/",FontAwesomeIcons::_book);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_course_quizzes"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Course Quizzes","manage course quizzes","/extorio-admin/lms-course-quizzes/","/extorio-admin/lms-course-quizzes/",FontAwesomeIcons::_questionCircle);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_course_posts"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Course Posts","manage course posts","/extorio-admin/lms-course-posts/","/extorio-admin/lms-course-posts/",FontAwesomeIcons::_rss);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_course_feedback"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("Course Feedback","manage course feedback","/extorio-admin/lms-course-feedback/","/extorio-admin/lms-course-feedback/",FontAwesomeIcons::_star);
                        }

                        return $items;
                        break;
                    case "emailmanagement" :
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_general_emails"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("LMS General Emails","manage general emails","/extorio-admin/lms-general-emails/","/extorio-admin/lms-general-emails/",FontAwesomeIcons::_envelope);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_course_emails"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("LMS Course Emails","manage course emails","/extorio-admin/lms-course-emails/","/extorio-admin/lms-course-emails/",FontAwesomeIcons::_envelope);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_course_post_emails"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("LMS Course Post Emails","manage course post emails","/extorio-admin/lms-course-post-emails/","/extorio-admin/lms-course-post-emails/",FontAwesomeIcons::_envelope);
                        }
                        return $items;
                        break;
                    case "settings":
                        $items = array();
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_general_settings"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("LMS General Settings","manage general settings","/extorio-admin/lms-general-settings/","/extorio-admin/lms-general-settings/",FontAwesomeIcons::_cog);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_course_settings"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("LMS Course Settings","manage course settings","/extorio-admin/lms-course-settings/","/extorio-admin/lms-course-settings/",FontAwesomeIcons::_cog);
                        }
                        if(Users::userHasPrivileges_OR($loggedInUser->id,array(
                            "extorio_pages_all",
                            "extorio_pages_lms_upload_settings"
                        ))) {
                            $items[] = AdminMenuLeft_Item::n("LMS Upload Settings","manage upload settings","/extorio-admin/lms-upload-settings/","/extorio-admin/lms-upload-settings/",FontAwesomeIcons::_cog);
                        }
                        return $items;
                        break;
                }
            },101);
        }

        //load bar rating
        $this->_Extorio()->appendToHead("<script type='application/javascript' src='/Extensions/ExtorioLMS/Assets/jquery-bar-rating/jquery.barrating.min.js'></script>");
        $this->_Extorio()->appendToHead("<link rel='stylesheet' href='/Extensions/ExtorioLMS/Assets/jquery-bar-rating/themes/fontawesome-stars.css' />");
        //load jwplayer
        $this->_Extorio()->appendToHead("<script type='application/javascript' src='/Extensions/ExtorioLMS/Assets/jwplayer/jwplayer.js'></script>");
        $this->_Extorio()->appendToHead("<script type='application/javascript'>jwplayer.key='".$config["ExtorioLMS"]["jwplayer"]["key"]."';</script>");
        //and the ExtorioLMS js file
        $this->_Extorio()->appendToHead("<script type='application/javascript' src='/Extensions/ExtorioLMS/Assets/js/ExtorioLMS.js'></script>");
        //timeago
        $this->_Extorio()->includeIncluder("timeago");

        //$this->_Extorio()->cacheConfig();
    }

    protected function _onInstall() {
        //before we can modify the User model, we must create the models and enums that it uses

        /*
         * ENUMS
         */
        //ContentType
        $contentTypeE = $this->_installEnum("content type","content types",array(
            "course"
        ));
        //ContentDifficulty
        $contentDifficultyE = $this->_installEnum("content difficulty","content difficulty",array(
            "novice",
            "intermediate",
            "advanced"
        ));
        //ContentStatus
        $contentStatusE = $this->_installEnum("content status","content status",array(
            "pending",
            "started",
            "submitted",
            "failed",
            "passed"
        ));
        //ContentApproval
        $contentApprovalE = $this->_installEnum("content approval","content approval",array(
            "drafting",
            "pending",
            "approved",
            "closed",
            "cancelled"
        ));
        //ModuleType
        $moduleTypeE = $this->_installEnum("module type","module type",array(
            "lecture",
            "quiz"
        ));
        $lectureTypeE = $this->_installEnum("lecture type","lecture type",array(
            "video",
            "audio",
            "image",
            "text",
            "document"
        ));
        $questionTypeE = $this->_installEnum("question type","question type",array(
            "multiple-choice",
            "multiple-response",
            "true-or-false",
            "fill-in-the-blanks",
            "multiline-text",
            "file-submission"
        ));
        //CourseFilesUploadType
        $courseFilesUploadTypeE = $this->_installEnum("course files upload type","course files upload type", array(
            "course-sources",
            "module-sources",
            "text-sources",
            "post-attachments",
            "file-submission"
        ));
        //CourseFilesFileType
        $courseFilesFileTypeE = $this->_installEnum("course files file type","course files file type", array(
            "images",
            "videos",
            "audio",
            "documents",
            "subtitles"
        ));
        $this->_installEnum(
            "User Course Privileges",
            "",
            array(
                "user_courses_read_all",
                "user_courses_read_own",
                "user_courses_create_all",
                "user_courses_create_own",
                "user_courses_modify_all",
                "user_courses_modify_own",
                "user_courses_delete_all",
                "user_courses_delete_own"
            )
        );
        $this->_installEnum(
            "User Quiz Privileges",
            "",
            array(
                "user_quizzes_read_all",
                "user_quizzes_read_own",
                "user_quizzes_modify_all",
                "user_quizzes_modify_own"
            )
        );
        $this->_installEnum(
            "Course Privileges",
            "",
            array(
                "courses_create",
                "courses_modify_all",
                "courses_remove_all",
                "courses_remove_own",
                "courses_approve_all",
                "courses_approve_own",
                "courses_close_all",
                "courses_close_own",
                "courses_cancel_all",
                "courses_cancel_own"
            )
        );
        $this->_installEnum(
            "Course Post Privileges",
            "",
            array(
                "course_posts_modify_all",
                "course_posts_approve_all",
                "course_posts_approve_own",
                "course_posts_close_all",
                "course_posts_close_own",
                "course_posts_cancel_all",
                "course_posts_cancel_own",
                "course_posts_sticky_all",
                "course_posts_sticky_own"
            )
        );
        $this->_installEnum(
            "Course Category Privileges",
            "",
            array(
                "course_categories_create",
                "course_categories_modify",
                "course_categories_delete"
            )
        );
        $this->_installEnum(
            "Course Tag Privileges",
            "",
            array(
                "course_tags_create",
                "course_tags_modify",
                "course_tags_delete"
            )
        );
        $this->_installEnum(
            "Feedback Privileges",
            "",
            array(
                "feedback_modify_all",
                "feedback_modify_own",
                "feedback_approve_all",
                "feedback_approve_own",
                "feedback_cancel_all",
                "feedback_cancel_own"
            )
        );
        $this->_installEnum(
            "Certificate template privileges",
            "",
            array(
                "certificate_templates_create",
                "certificate_templates_modify",
                "certificate_templates_delete"
            )
        );
        $this->_installEnum(
            "Content Difficulty Privileges",
            "",
            array(
                "content_difficulty_create",
                "content_difficulty_delete"
            )
        );

        /*
         * Models
         */
        //CourseSubscriber
        $this->_installModel(
            "Course Subscriber",
            array(
                Property::q("course id","The id of the course",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("user id","The id of the user",true,PropertyType::_basic,PropertyBasicType::_number)
            ),
            "A course subscriber",
            false
        );
        //CoursePostSubscriber
        $this->_installModel(
            "Course Post Subscriber",
            array(
                Property::q("course post id","The id of the course post",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("user id","The id of the user",true,PropertyType::_basic,PropertyBasicType::_number)
            ),
            "A course post subscriber"
        );
        //Course Post
        $this->_installModel(
            "Course Post",
            array(
                Property::q("course id","The id of the course",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("parent id","The parent post of this post",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("date created","The creation date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("date updated","The update date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("date replied","The reply date",true,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("user id","The id of the user",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("replier id","The id of the replier",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("name","The name of the post",true,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("body","The body of the post",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("body plain","The body of the post as plain text",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("attachment file id","The id of an attachment course file",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("approval","The approval status of this post",false,PropertyType::_enum,"","","",$contentApprovalE->namespace),
                Property::q("aticky","Whether this post is sticky",true,PropertyType::_basic,PropertyBasicType::_checkbox),
                Property::q("total num replies","The number of replies",false,PropertyType::_basic,PropertyBasicType::_number),
            ),
            "A course post"
        );
        //Feedback
        $this->_installModel(
            "Feedback",
            array(
                Property::q("owner id","The owner of the feedback",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("course id","The id of the course",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("date updated","The update date",true,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("rating","The rating",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("comment","The comment",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("approval","The approval of the feedback",false,PropertyType::_enum,"","","",$contentApprovalE->namespace),
            ),
            "A feedback"
        );
        //Course File
        $this->_installModel(
            "Course File",
            array(
                Property::q("user id","The owner of the file",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("upload type","The type of upload",true,PropertyType::_enum,"","","",$courseFilesUploadTypeE->namespace),
                Property::q("file type","The type of file",true,PropertyType::_enum,"","","",$courseFilesFileTypeE->namespace),
                Property::q("file name","The name of the file",true,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("bucket","The name of the bucket",true,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("key","The key",false,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("content type","The content type",true,PropertyType::_basic,PropertyBasicType::_textfield)
            ),
            "A course file"
        );
        //Course Category
        $courseCategoryM = $this->_installModel(
            "Course Category",
            array(
                Property::q("name","The name",true,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("parent id","The parent category",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("icon","The icon",false,PropertyType::_enum,"","","",'\Core\Classes\Enums\FontAwesomeIcons')
            ),
            "A course category"
        );
        //Course Tag
        $courseTagM = $this->_installModel(
            "Course Tag",
            array(
                Property::q("name","The name",true,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("description","The description",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("icon","The icon",false,PropertyType::_enum,"","","",'\Core\Classes\Enums\FontAwesomeIcons')
            ),
            "A course tag"
        );
        //Certificate Template
        $certificateTemplateM = $this->_installModel(
            "Certificate Template",
            array(
                Property::q("owner id","The owner id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("site default","Whether this template is the default one",true,PropertyType::_basic,PropertyBasicType::_checkbox),
                Property::q("name","The name",false,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("description","The description",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("body","The body",false,PropertyType::_basic,PropertyBasicType::_textarea)
            ),
            "A certificate template"
        );
        //Question
        $questionM = $this->_installModel(
            "Question",
            array(
                Property::q("type","The type",false,PropertyType::_enum,"","","",$questionTypeE->namespace),
                Property::q("questions","The question data",false,PropertyType::_meta),
                Property::q("fail feedback","The response if question is wrong",false,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("success feedback","The response if question is correct",false,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("answers","The answers",false,PropertyType::_meta),
                Property::q("max chars","The max chars",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("max file size","The max file size",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("accepted file types","The accepted file types",false,PropertyType::_meta)
            ),
            "A question"
        );
        //Module
        $moduleM = $this->_installModel(
            "Module",
            array(
                Property::q("name","The name",true,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("course id","The course id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("previewable","Whether this module is previewable",false,PropertyType::_basic,PropertyBasicType::_checkbox),
                Property::q("type","The type",false,PropertyType::_enum,"","","",$moduleTypeE->namespace),
                Property::q("quiz description","The quiz description",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("lecture type","The lecture type",true,PropertyType::_enum,"","","",$lectureTypeE->namespace),
                Property::q("lecture file id","The lecture file id",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("lecture body","The lecture body",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("subtitle file id","The lecture file id",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("maximum points","The maximum points",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("pass points","The pass points",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("maximum attempts","The maximum attempts",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("questions","The questions",false,PropertyType::_complex,"",PHPTypes::_array,$questionM->namespace)
            ),
            "A module"
        );
        //Lesson
        $lessonM = $this->_installModel(
            "Lesson",
            array(
                Property::q("name","The name",true,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("modules","The modules",false,PropertyType::_complex,"",PHPTypes::_array,$moduleM->namespace)
            ),
            "A lesson"
        );
        //Course
        $this->_installModel(
            "Course",
            array(
                Property::q("owner id","The owner id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("date created","The creation date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("date updated","The update date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("name","The name",true,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("description","The description",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("category","The category",true,PropertyType::_complex,"",PHPTypes::_object,$courseCategoryM->namespace),
                Property::q("sub category","The sub category",true,PropertyType::_complex,"",PHPTypes::_object,$courseCategoryM->namespace),
                Property::q("difficulty","The difficulty",true,PropertyType::_enum,"","","",$contentDifficultyE->namespace),
                Property::q("tags","The tags",true,PropertyType::_complex,"",PHPTypes::_array,$courseTagM->namespace),
                Property::q("expiry minutes","The expiry minutes",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("certificate template","The certificate template",false,PropertyType::_complex,"",PHPTypes::_object,$certificateTemplateM->namespace),
                Property::q("approval","The approval",true,PropertyType::_enum,"","","",$contentApprovalE->namespace),
                Property::q("cancelled reason","The cancelled reason",false,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("self enrollable","Whether this course is self enrollable",true,PropertyType::_basic,PropertyBasicType::_checkbox),
                Property::q("lessons","The lessons",false,PropertyType::_complex,"",PHPTypes::_array,$lessonM->namespace),
                Property::q("default file id","The default file id",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("default thumbnail","The default thumbnail",false,PropertyType::_basic,PropertyBasicType::_textfield),
                Property::q("average rating","The average rating",true,PropertyType::_basic,PropertyBasicType::_decimal),
                Property::q("total num enrolled","The total number enrolled",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("maximum points","The maximum points",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("total num lessons","The total num lessons",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("total num lectures","The total num lectures",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("total num quizzes","The total num quizzes",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("total num questions","The total num questions",false,PropertyType::_basic,PropertyBasicType::_number)
            ),
            "A course"
        );
        //User_Course
        $this->_installModel(
            "User_Course",
            array(
                Property::q("user id","The user id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("course id","The course id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("status","The status",true,PropertyType::_enum,"","","",$contentStatusE->namespace),
                Property::q("date started","The started date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("date updated","The updated date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("date passed","The passed date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("expiry date","The expiry date",true,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("current points","The current points",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("certificate","The certificate",false,PropertyType::_basic,PropertyBasicType::_textfield)
            ),
            "A user course"
        );
        //User_Question
        $userQuestionM = $this->_installModel(
            "User_Question",
            array(
                Property::q("user id","The user id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("question id","The question id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("status","The status",true,PropertyType::_enum,"","","",$contentStatusE->namespace),
                Property::q("date updated","The updated date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("date passed","The passed date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("response","The response",false,PropertyType::_basic,PropertyBasicType::_textarea),
                Property::q("response date","The response date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("response by id","The id of the responder",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("user answers","The user answers",false,PropertyType::_meta)
            ),
            "A user question"
        );
        //User_Module
        $this->_installModel(
            "User_Module",
            array(
                Property::q("user id","The user id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("course id","The course id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("user course id","The user course id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("module id","The module id",true,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("status","The status",true,PropertyType::_enum,"","","",$contentStatusE->namespace),
                Property::q("current","The current module",true,PropertyType::_basic,PropertyBasicType::_checkbox),
                Property::q("date updated","The updated date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("date passed","The passed date",false,PropertyType::_basic,PropertyBasicType::_datetime),
                Property::q("current points","The current points",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("current attempts","The current points",false,PropertyType::_basic,PropertyBasicType::_number),
                Property::q("user questions","The user questions",false,PropertyType::_complex,"",PHPTypes::_array,$userQuestionM->namespace)
            ),
            "A user module"
        );


        /*
         * Adding apis
         */

        //lms-config
        $this->_installApi("LMS Config","This api is used to get the config settings for the LMS",true);
        //lms-user-courses
        $this->_installApi("LMS User Courses","This api is used to manage user courses",true);
        //lms-user-modules
        $this->_installApi("LMS User Modules","This api is used to manage user modules",true);
        //lms-user-questions
        $this->_installApi("LMS User Questions","This api is used to manage user questions",true);
        //lms-courses
        $this->_installApi("LMS Courses","This api is used to manage courses",true);
        //lms-modules
        $this->_installApi("LMS Modules","This api is used to manage modules",true);
        //lms-course-tags
        $this->_installApi("LMS Course Tags","This api is used to manage course tags",true);
        //lms-course-categories
        $this->_installApi("LMS Course Categories","This api is used to manage course categories",true);
        //lms-course-posts
        $this->_installApi("LMS Course Posts","This api is used to modify course posts",true);
        //lms-feedback
        $this->_installApi("LMS Feedback","This api is used to manage feedback",true);
        //lms-certificate-templates
        $this->_installApi("LMS Certificate Templates","This api is used to manage certificate templates",true);
        //lms-enums
        $this->_installApi("LMS Enums","This api is used to manage enums",true);
        //lms-course-files
        $this->_installApi("LMS Course Files","This api is used to manage course files",true);

        /*
         * Adding the block processors
         */
        $this->_installBlockProcessor("LMS Course Categories","Display the course categories",BlockProcessorCategory::findByName("Menus"),true,true);
        $this->_installBlockProcessor("LMS Course Tags","Display the course tags",BlockProcessorCategory::findByName("Menus"),true,true);
        $this->_installBlockProcessor("LMS Selected Courses","Display a set of selected courses",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS Related Courses","Display a set of related courses",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS Category Courses","Display a set of courses within a category",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS Tagged Courses","Display a set of courses with a tag",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS Search Courses","Display a search form to search courses",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS Course Thumbnail","Display a course thumbnail for a course",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS My Course Feedback","Display my course feedback for a certain course",BlockProcessorCategory::findByName("Interactive Content"),true,true);
        $this->_installBlockProcessor("LMS Course Feedback","Display the feedback for a selected course",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS Course Feedback Rotator","Displays the most recent course feedback in rotation",BlockProcessorCategory::findByName("Interactive Content"),true,true);
        $this->_installBlockProcessor("LMS Course Enrolled Users","Displays a list of users enrolled on a course",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS Course Students","Displays a paginated list of the students enrolled on a course",BlockProcessorCategory::findByName("General Content"),true,true);
        $this->_installBlockProcessor("LMS User Menu","A user menu containing links to the main sections of the lms",BlockProcessorCategory::findByName("Menus"),true,true);

        /*
         * Finished installing components etc, generate file cache
         */
        $this->generateFileCache("Classes/Enums","Classes/Enums");
        $this->generateFileCache("Classes/Helpers","Classes/Helpers");
        $this->generateFileCache("Classes/Models","Classes/Models");
        $this->generateFileCache("Classes/Utilities","Classes/Utilities");


        /*
         * Adding default data
         */

        /*
         * Privilege categories
         */
        $userCoursesPC = $this->_installPrivilegeCategory(
            "User Courses",
            "Privileges related to user courses",
            "book"
        );
        $userQuizzesPC = $this->_installPrivilegeCategory(
            "User Quizzes",
            "Privileges related to user quizzes",
            FontAwesomeIcons::_questionCircle
        );
        $coursesPC = $this->_installPrivilegeCategory(
            "Courses",
            "Privileges related to courses",
            "book"
        );
        $coursePostsPC = $this->_installPrivilegeCategory(
            "Course Posts",
            "Privileges related to course posts",
            "book"
        );
        $courseCategoriesPC = $this->_installPrivilegeCategory(
            "Course Categories",
            "Privileges related to course categories",
            "book"
        );
        $courseTagsPC = $this->_installPrivilegeCategory(
            "Course Tags",
            "Privileges related to course tags",
            "book"
        );
        $feedbackPC = $this->_installPrivilegeCategory(
            "Feedback",
            "Privileges related to feedback",
            FontAwesomeIcons::_square
        );
        $certificateTemplatesPC = $this->_installPrivilegeCategory(
            "Certificate Templates",
            "Privileges related to certificate templates",
            FontAwesomeIcons::_square
        );
        $contentDifficultyPC = $this->_installPrivilegeCategory(
            "Content Difficulty",
            "Privileges related to content difficulty",
            FontAwesomeIcons::_wrench
        );

        /*
         * Privileges
         */
        $this->_installPrivilege(
            "user_courses_create_all",
            "create enrolments for users you manage, on all courses",
            $userCoursesPC
        );
        $this->_installPrivilege(
            "user_courses_create_own",
            "create enrolments for users you manage, on own courses",
            $userCoursesPC
        );
        $this->_installPrivilege(
            "user_courses_modify_all",
            "modify enrolments for users that you manage, on all courses",
            $userCoursesPC
        );
        $this->_installPrivilege(
            "user_courses_modify_own",
            "modify enrolments for users that you manage, on own courses",
            $userCoursesPC
        );
        $this->_installPrivilege(
            "user_courses_delete_all",
            "delete enrolments for users that you manage, on all courses",
            $userCoursesPC
        );
        $this->_installPrivilege(
            "user_courses_delete_own",
            "delete enrolments for users that you manage, on own courses",
            $userCoursesPC
        );

        $this->_installPrivilege(
            "user_quizzes_modify_all",
            "modify the quiz modules for users that you manage, for all courses",
            $userQuizzesPC
        );
        $this->_installPrivilege(
            "user_quizzes_modify_own",
            "modify the quiz modules for users that you manage, for own courses",
            $userQuizzesPC
        );

        $this->_installPrivilege(
            "courses_create",
            "allows the ability to create courses",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_modify_all",
            "allows the ability to modify the courses belonging to users that you manage",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_remove_all",
            "allows the ability to remove courses that belong to users that you manage",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_remove_own",
            "allows the ability to remove own courses",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_approve_all",
            "allows the ability to approve courses that belong to users that you manage",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_approve_own",
            "allows the ability to approve own courses",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_close_all",
            "allows the ability to close courses that belong to users that you manage",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_close_own",
            "allows the ability to close own courses",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_cancel_all",
            "allows the ability to cancel courses that belong to users that you manage",
            $coursesPC
        );
        $this->_installPrivilege(
            "courses_cancel_own",
            "allows the ability to cancel own courses",
            $coursesPC
        );

        $this->_installPrivilege(
            "course_posts_modify_all",
            "allows the ability to modify the posts of courses that belong to users that you manage",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_modify_own",
            "allows the ability to modify the posts of your own courses",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_approve_all",
            "allows the ability to approve the posts of courses that belong to users that you manage",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_approve_own",
            "allows the ability to approve the posts on own courses",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_close_all",
            "allows the ability to close the posts of courses that belong to users that you manage",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_close_own",
            "allows the ability to close the posts of own courses",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_cancel_all",
            "allows the ability to cancel the posts of courses that belong to users that you manage",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_cancel_own",
            "allows the ability to cancel the posts of own courses",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_sticky_all",
            "allows the ability to sticky the posts of courses that belong to users that you manage",
            $coursePostsPC
        );
        $this->_installPrivilege(
            "course_posts_sticky_own",
            "allows the ability to sticky the posts of own courses",
            $coursePostsPC
        );

        $this->_installPrivilege(
            "course_categories_create",
            "",
            $courseCategoriesPC
        );
        $this->_installPrivilege(
            "course_categories_modify",
            "",
            $courseCategoriesPC
        );
        $this->_installPrivilege(
            "course_categories_delete",
            "",
            $courseCategoriesPC
        );

        $this->_installPrivilege(
            "course_tags_create",
            "",
            $courseTagsPC
        );
        $this->_installPrivilege(
            "course_tags_modify",
            "",
            $courseTagsPC
        );
        $this->_installPrivilege(
            "course_tags_delete",
            "",
            $courseTagsPC
        );

        $this->_installPrivilege(
            "feedback_modify_all",
            "allows the ability to modify the feedback of courses that belong to users that you manage",
            $feedbackPC
        );
        $this->_installPrivilege(
            "feedback_modify_own",
            "allows the ability to modify the feedback of own courses",
            $feedbackPC
        );
        $this->_installPrivilege(
            "feedback_approve_all",
            "allows the ability to approve the feedback of courses that belong to uses that you manage",
            $feedbackPC
        );
        $this->_installPrivilege(
            "feedback_approve_own",
            "allows the ability to approve the feedback of own courses",
            $feedbackPC
        );
        $this->_installPrivilege(
            "feedback_cancel_all",
            "allows the ability to cancel the feedback of courses that belong to users that you manage",
            $feedbackPC
        );
        $this->_installPrivilege(
            "feedback_cancel_own",
            "allows the ability to cancel the feedback of own courses",
            $feedbackPC
        );

        $this->_installPrivilege(
            "certificate_templates_create",
            "",
            $certificateTemplatesPC
        );
        $this->_installPrivilege(
            "certificate_templates_modify",
            "",
            $certificateTemplatesPC
        );
        $this->_installPrivilege(
            "certificate_templates_delete",
            "",
            $certificateTemplatesPC
        );

        $this->_installPrivilege(
            "content_difficulty_create",
            "",
            $contentDifficultyPC
        );
        $this->_installPrivilege(
            "content_difficulty_delete",
            "",
            $contentDifficultyPC
        );

        $extorioMenusPC = UserPrivilegeCategory::findByName("Extorio Menus");
        $extorioPagesPC = UserPrivilegeCategory::findByName("Extorio Pages");

        $this->_installPrivilege(
            "extorio_menus_lmsmenu",
            "Displays the LMS menu",
            $extorioMenusPC
        );

        $this->_installPrivilege(
            "extorio_pages_lms_general_emails",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_course_emails",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_course_post_emails",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_general_settings",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_course_settings",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_upload_settings",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_dashboard",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_courses",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_course_files",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_course_quizzes",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_course_enrolments",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_course_posts",
            "",
            $extorioPagesPC
        );
        $this->_installPrivilege(
            "extorio_pages_lms_course_feedback",
            "",
            $extorioPagesPC
        );

        /*
         * Default categories
         */
        $c = CourseCategory::n();
        $c->id = 1;
        $c->name = "Main Category";
        $c->icon = FontAwesomeIcons::_square;
        $c->parentId = 0;
        $c->pushThis();

        $c = CourseCategory::n();
        $c->id = 2;
        $c->name = "Sub Category";
        $c->icon = FontAwesomeIcons::_square;
        $c->parentId = 1;
        $c->pushThis();

        /*
         * Setting up the admin pages
         */
        $extorioTheme = Theme::findByName("Extorio Admin");
        $extorioTemplate = Template::findByName("Extorio Admin");

        //extorio-admin/lms-course-emails
        $this->_installPage(
            "Admin Course Emails",
            "Manage the course emails for the lms extension",
            "/extorio-admin/lms-course-emails/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-course-post-emails
        $this->_installPage(
            "Admin Course Post Emails",
            "Manage the course post emails for the lms extension",
            "/extorio-admin/lms-course-post-emails/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-general-emails
        $this->_installPage(
            "Admin General Emails",
            "Manage the general emails for the lms extension",
            "/extorio-admin/lms-general-emails/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-general-settings
        $this->_installPage(
            "Admin LMS General Settings",
            "Manage the general settings for the lms extension",
            "/extorio-admin/lms-general-settings/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-course-settings
        $this->_installPage(
            "Admin LMS Course Settings",
            "Manage the course settings for the lms extension",
            "/extorio-admin/lms-course-settings/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-upload-settings
        $this->_installPage(
            "Admin LMS Upload Settings",
            "Manage the upload settings for the lms extension",
            "/extorio-admin/lms-upload-settings/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-courses
        $this->_installPage(
            "Admin LMS Dashboard",
            "LMS Dashboard",
            "/extorio-admin/lms-dashboard/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-courses
        $this->_installPage(
            "Admin LMS Courses",
            "Manage courses",
            "/extorio-admin/lms-courses/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-course-files
        $this->_installPage(
            "Admin LMS Course Files",
            "Manage course files",
            "/extorio-admin/lms-course-files/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-course-enrolments
        $this->_installPage(
            "Admin LMS Course Enrolments",
            "Manage course enrolments",
            "/extorio-admin/lms-course-enrolments/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-course-quizzes
        $this->_installPage(
            "Admin LMS Course Quizzes",
            "Manage course quizzes",
            "/extorio-admin/lms-course-quizzes/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-course-posts
        $this->_installPage(
            "Admin LMS Course Posts",
            "Manage course posts",
            "/extorio-admin/lms-course-posts/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );
        //extorio-admin/lms-course-feedback
        $this->_installPage(
            "Admin LMS Course Feedback",
            "Manage course feedback",
            "/extorio-admin/lms-course-feedback/",
            false,
            true,
            "",
            "",
            $extorioTemplate,
            $extorioTheme,
            LayoutSystem::getDefault(),
            true,
            0,
            array(),
            array(1),
            "",
            "",
            "",
            "",
            true
        );

        /*
         * Set up the site pages
         */

        $lmsCoursesTemplate = $this->_installTemplate(
            "LMS Courses Template",
            "The template used by the courses page",
            false,
            LayoutSystem::getDefault()
        );
        $lmsMyCoursesTemplate = $this->_installTemplate(
            "LMST My Courses Template",
            "The template used by the my-courses page",
            false,
            LayoutSystem::getDefault()
        );

        //courses
        $this->_installPage(
            "Courses",
            "Our courses",
            "/courses/",
            false,
            true,
            "",
            "",
            $lmsCoursesTemplate
        );
        //my-courses
        $this->_installPage(
            "My Courses",
            "View your courses",
            "/my-courses/",
            false,
            true,
            "",
            "",
            $lmsMyCoursesTemplate
        );

        /**
         * Set up tasks
         */
        $this->_installTask(
            "LMS Tasks",
            "Tasks related to the lms extension",
            true
        );
    }

    protected function _onUninstall() {
        $this->_uninstallAll();
    }
}