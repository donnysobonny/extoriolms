<?php
return array(
    //we need to autoload some of the models, because they are used within the start of the core extension, before this extension is started
    "autoload" => array(
        'ExtorioLMS\Classes\Models\RoleGroup' => "Extensions/ExtorioLMS/Classes/Models/RoleGroup.php",
        'ExtorioLMS\Classes\Models\B_RoleGroup' => "Extensions/ExtorioLMS/Classes/Models/internal/B_RoleGroup.php",
        'ExtorioLMS\Classes\Models\Role' => "Extensions/ExtorioLMS/Classes/Models/Role.php",
        'ExtorioLMS\Classes\Models\B_Role' => "Extensions/ExtorioLMS/Classes/Models/internal/B_Role.php",
        'ExtorioLMS\Classes\Models\User_PublicDetail' => "Extensions/ExtorioLMS/Classes/Models/User_PublicDetail.php",
        'ExtorioLMS\Classes\Models\B_User_PublicDetail' => "Extensions/ExtorioLMS/Classes/Models/internal/B_User_PublicDetail.php",
        'ExtorioLMS\Classes\Models\User_InternalDetail' => "Extensions/ExtorioLMS/Classes/Models/User_InternalDetail.php",
        'ExtorioLMS\Classes\Models\B_User_InternalDetail' => "Extensions/ExtorioLMS/Classes/Models/internal/B_User_InternalDetail.php",
        'ExtorioLMS\Classes\Models\User_Note' => "Extensions/ExtorioLMS/Classes/Models/User_Note.php",
        'ExtorioLMS\Classes\Models\B_User_Note' => "Extensions/ExtorioLMS/Classes/Models/internal/B_User_Note.php",
        //html2pdf
        'HTML2PDF' => "Extensions/ExtorioLMS/Libs/html2pdf/html2pdf.class.php",
        //AWS
        'Aws\Sdk' => "Extensions/ExtorioLMS/Libs/aws/aws-autoloader.php",
        'Aws\S3\S3Client' => "Extensions/ExtorioLMS/Libs/aws/aws-autoloader.php"
    ),
    "ExtorioLMS" => array(
        "aws" => array(
            "s3" => array(
                "region" => "",
                "key" => "",
                "secret" => "",
            )
        ),
        "jwplayer" => array(
            "key" => ""
        ),
        "courses" => array(
            "auto_approve" => false,
            "minimum_expiry_minutes" => 0,
            "allow_self_enrollable" => true,
            "expiry_set_when" => "on_start",
            "certificate_public" => true,
            "course_file_bucket" => "course_files",
            "editor_file_bucket" => "editor_files",
            "default_image" => "/Extensions/ExtorioLMS/Assets/images/default.png",
            "default_thumbnail_image" => "/Extensions/ExtorioLMS/Assets/images/default_thumbnail.png"
        ),
        "course_posts" => array(
            "enabled" => true,
            "auto_approve" => false,
            "allow_attachments" => false,
        ),
        "modules" => array(
            "allow_previewable" => true,
            "lectures" => array(
                "allow_video" => true,
                "allow_audio" => true,
                "allow_images" => true,
                "allow_text" => true,
                "allow_documents" => true
            ),
            "quizzes" => array(
                "minimum_maximum_attempts" => 0,
                "minimum_pass_points_percentage" => 75,
                "allow_multiple_choice" => true,
                "allow_multiple_response" => true,
                "allow_true_false" => true,
                "allow_fill_gaps" => true,
                "allow_multiline_text" => true,
                "allow_file_submission" => true
            )
        ),
        "feedback" => array(
            "enabled" => true,
            "auto_approve_uncommented" => true,
            "auto_approve_commented" => false
        ),
        "course_page" => array(
            "show_feedback" => true,
            "show_related" => true
        ),
        "uploads" => array(
            "images" => array(
                "accepted_types" => array(
                    'image/jpeg',
                    "image/png",
                    "image/gif",
                ),
                "maximum_size" => 10000000
            ),
            "videos" => array(
                "accepted_types" => array(
                    "video/mp4",
                ),
                "maximum_size" => 500000000
            ),
            "audio" => array(
                "accepted_types" => array(
                    "audio/mp3",
                ),
                "maximum_size" => 50000000
            ),
            "documents" => array(
                "accepted_types" => array(
                    "application/pdf"
                ),
                "maximum_size" => 10000000
            ),
            "subtitles" => array(
                "accepted_types" => array(
                    "text/vtt"
                ),
                "maximum_size" => 10000000
            )
        ),
        "emails" => array(
            "courses" => array(
                "course_enrollments" => array(
                    "enabled" => true,
                    "subject" => "Course Enrollment Successful!",
                    "body" => '<p><strong>Hello **SHORTNAME**,</strong></p><p>This email is to let you know that you have successfully enrolled onto the **COURSE_NAME** course. You can access this course by following the link below:<br><a href="**COURSE_URL**">**COURSE_URL**</a></p><p>If you have any questions, please do not hesitate to get in touch. Otherwise, good luck!</p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
                "passed_course" => array(
                    "enabled" => true,
                    "subject" => "You Have Successfully Passed Your Course!",
                    "body" => '<p><strong>Congratulations **SHORTNAME**!</strong></p><p>You have successfully passed the **COURSE_NAME** course. To view the results of your course, follow the link below:<br><a href="**COURSE_URL**">**COURSE_URL**</a></p><p>You can view your certificate of completion at any time by following the link below:<br><a href="**CERTIFICATE_URL**">**CERTIFICATE_URL**</a></p><p>If you have any questions, please do not hesitate to get in touch. </p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
                "course_cancelled_enrolled" => array(
                    "enabled" => true,
                    "subject" => "Your Course Has Been Cancelled",
                    "body" => '<p><b>Hello **SHORTNAME**,</b></p><p>This email is to notify you that the course **COURSE_NAME** has been cancelled. The reason for cancellation is stated below:<b><br></b></p><p><i>**REASON**</i></p><p>This means that you will no longer be able to access the course, or any of the work that you are yet to complete or have already completed. If you have any questions about this, please don\'t hesitate to get in touch.<i><br></i></p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
                "course_expiry" => array(
                    "enabled" => true,
                    "days_before" => 3,
                    "subject" => "Your Course Is About To Expire!",
                    "body" => '<p><b>Hello **SHORTNAME**,</b></p><p>This email is to notify you that your enrolment for the course **COURSE_NAME** is due to expire on the **EXPIRY_DATE**. You can access your course by following the link below:<br><a href="**COURSE_URL**">**COURSE_URL**</a><b><br></b></p><p>If you have any questions about this, please don\'t hesitate to get in touch.</p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
                "course_approved" => array(
                    "enabled" => true,
                    "subject" => "Your Course Has Been Approved",
                    "body" => '<p><b>Hello **SHORTNAME**,</b></p><p>This email is to inform you that your course **COURSE_NAME** has been approved. You can access your course by following the link below:<br><a href="**COURSE_URL**">**COURSE_URL**</a><b><br></b></p><p>If you have any questions, please do not hesitate to get in touch.<b><br></b></p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
                "course_closed" => array(
                    "enabled" => true,
                    "subject" => "Your Course Has Been Closed",
                    "body" => '<p><b>Hello **SHORTNAME**,</b></p><p>This email is to notify you that your course **COURSE_NAME** has been closed. You can access your course by following the link below:<br><a href="**COURSE_URL**">**COURSE_URL**</a><b><br></b></p><p>Closed courses can still be accessed by all enrolled users. However, new enrolments and course posts can not be created for closed courses.<b><br></b></p><p>If you have any questions, please do not hesitate to get in touch. </p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
                "course_cancelled" => array(
                    "enabled" => true,
                    "subject" => "Your Course Has Been Cancelled",
                    "body" => '<p><b>Hello **SHORTNAME**,</b></p><p>This email is to notify you that your course **COURSE_NAME** has been cancelled for the following reason:<br><br><i>**REASON**</i><b><br></b></p><p>This means that users are no longer able to access the course.<i><br></i></p><p>If you have any questions, please do not hesitate to get in touch.</p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
            ),
            "course_posts" => array(
                "new_post" => array(
                    "enabled" => true,
                    "subject" => "A new post has been submitted",
                    "body" => '<p><b>Hello **SHORTNAME**,</b></p><p>This email is to notify you that a new post has been submitted for the **COURSE_NAME** course, under the name of **POST_NAME**. To view this post, follow the link below:<br><a href="**POST_URL**">**POST_URL**</a><b><br></b></p><p>If you have any questions, please do not hesitate to get in touch.<b><br></b></p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
                "new_reply" => array(
                    "enabled" => true,
                    "subject" => "A new reply has been submitted",
                    "body" => '<p><b>Hello **SHORTNAME**,</b></p><p>This email is to notify you that **REPLY_SHORTNAME** has reployed to your post **POST_NAME**. To view this reply, please follow the link below:<br><a href="**POST_URL**">**POST_URL**</a><b><br></b></p><p>If you have any questions, please do not hesitate to get in touch.</p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                ),
            ),
            "general" => array(
                "user_quiz_marked" => array(
                    "enabled" => true,
                    "subject" => "Your quiz has been marked",
                    "body" => '<p><b>Hello **SHORTNAME**,</b></p><p>This email is to notify you that your quiz **QUIZ_NAME** for the **CONTENT_TYPE** **CONTENT_NAME** has been marked. You can access the quiz to view the results by following the link below:<br><a href="**QUIZ_URL**">**QUIZ_URL**</a><b><br></b></p><p>If you have any further questions, please do not hesitate to get in touch.<b><br></b></p><p>Thank you,<br>**APPLICATION_COMPANY**</p>'
                )
            )
        )
    )
);